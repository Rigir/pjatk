![PJATK](../../.assets/banner.png)
=========

* Zadanie 1
  * Napisz funkcję etykietującą, która oznacza jako spam wszystkie wiadomości zawierające słowo "HOT" pisane kapitalikami.
* Zadanie 2
  * Zapisz funkcję etykietującą która oznaczy jako poprawne te wiadomości, które są krótsze niż 10 słów i nie zawierają żadnego słowa napisanego kapitalikami.
* Zadanie 3
  * Napisz funkcję etykietującą, która oznaczy jako spam wiadomości zawierające więcej niż 3 przymiotniki. Wykorzystaj bibliotekę SpaCy do pre-processingu.
* Zadanie 4
  * Czy ostateczny model poprawił wynik w stosunku do głosowania większościowego i modelu `LabelModel`? Znacznie / nieznacznie?
* Zadanie 5
  * Dodaj napisane przez siebie funkcje etykietujące do listy lfs, po raz kolejny wytrenuj model zbiorczy nadający etykiety szkoleniowe, pobierz te etykiety i  wytrenuj regresję logistyczną dla wektora zliczeń słów. Ile tym razem próbek udało się oznaczyć? Czy polepszyło to klasyfikację?
* Zadanie 6
  * Przyjrzyj się wynikom. Czy są sensowne? Jak myślisz, dlaczego 'love' i 'hate' mają tak wysoki wynik podobieństwa?