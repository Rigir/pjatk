![PJATK](../../.assets/banner.png)
=========

* Zadanie 1
  * Po lematyzacji, ile unikatowych tokenów znajduje się w tekście?
* Zadanie 2
  * Jakia jest różnorodność leksykalna zbioru (tj. policz stosunek unikatowych tokenów, do wszystkich tokenów znajdujących się w korpusie)?
* Zadanie 3
  * Wypisz N najczęściej występujących tokenów. Wynik przedstaw na wykresie słupkowym.
* Zadanie 4
  * Wykorzystaj odległość edycyjną do stworzenia funkcji rekomendacyjnej, która będzie poprawiała słowa zawierające literówki.
* Zadanie 5
  * Napisz skrypt, który zwróci listę list tokenów dla korpusu Reuters. Lista *tokenized_and_preprocessed_reuters* powinna:
    1. zawierać tokeny zapisane wyłącznie małymi literami
    2. mieć usunięte tzw. 'stop words' (czyli słowa takie jak “the”, “a”, “an”, “in" - lista takich słów dostępna jest w pakiecie NLTK)...
    3.  oraz znaki interpunkcyjne (możesz skorzystać z wyrażeń regularnych)
    4. zawierać zlematyzowane słowa
    5. zawierać słowa po korekcie literówek
    6. mieć usunięte najczęstsze słowa z korpusu (wykorzystaj część funkcji z ćwiczenia 3, sam_a wybierz punkt odcięcia)
* Zadanie 6
  * Poeksperymentuj ze swoją wyszukiwarką! Czy preprocessing polepszył jej działanie? Sprawdź i porównaj wyniki dla kilku różnych zapytań. Wyniki opisz poniżej:
