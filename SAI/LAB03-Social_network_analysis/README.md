![PJATK](../../.assets/banner.png)
=========

* Zadanie 1
  1.   Stwórz graf dwudzielny i go wyrysuj.
  2.   Następnie stwórz jego projekcję ważoną.
  3.   Wyświetl projekcję z wykorzystaniem kodu z przykładu 4.
* Zadanie 2
  * Wyświetl histogram wartości lokalnego współczynnika gronowania dla wierzchołków grafu
* Zadanie 3
  * Znajdź funkcję i oblicz globalny współczynnik gronowania
* Zadanie 4
  * Przeklej obrazek z błędnie wyliczoną wartością przechodniości z przykładu podanego w linku i porównaj z przechodniością wyliczaną przez NetworkX
* Zadanie 5
  * Poszukaj, jak NetworkX domyślnie liczy centralność bliskości (dla krawędzi wejściowych, czy wyjściowych?). Wklej odpowiedni link do dokumentacji i fragment
* Zadanie 6
  * Jakie są inne algorytmy wizualizacji grafów (słowo-klucz: layouts)? Wypisz tyle, ile uda Ci się znaleźć w bibliotece Network X. Jeden z nich wybierz i wyrysuj powyższy graf.
* Zadanie 7
  * Napisz dwie funkcje:
    1. Pierwsza niech zwraca najwyższą wartość centralności pośredniczości dla grafu,
    2. Druga niech zwraca najniższą wartość centralności pośredniczości dla grafu
* Zadanie 8
  * Dla grafu g zwróć centralności pośredniczości wszystkich wierzchołków uwzględniające i nie uwzględniające rozpatrywanego wierzchołka.
* Zadanie 9
  * Policz centralność pośredniczości dla krawędzi grafu G. Podaj wynik znormalizowany, a następnie zapoznaj się z komentarzem odnośnie wyniku nieznormalizowanego.
* Zadanie 10
  * Napisz funkcję, która zwraca promień, średnicę, centrum i obrzeże grafu `G`.
* Zadanie 11
  * Wyznacz lokalny współczynnik gronowania dla dwóch wierzchołków: po jednym z centrum i obrzeża grafu `G`.
* Zadanie 12
  * Napisz funkcję, która zwraca globalny współczynnik gronowania oraz przechodniość grafu `G`.
* Zadanie 13
  * Wyznacz średnicę grafu i znajdź wszystkie pary wierzchołków, które łączy najkrótsza ścieżka o długości średnicy grafu.
* Zadanie 14
  * Napisz funkcję badającą niezawodność sieci. Weź pod uwagę zarówno krawędzie, jak i wierzchołki.
* Zadanie 15
  * Utwórz kopię grafu `G` i nazwij ją `H`. Usuń z grafu `H` minimalną liczbę krawędzi potrzebną do rozspójnienia grafu `G`. Z ilu spójnych składowych będzie się składać po tej modyfikacji?
* Zadanie 16
  * Wyznacz miarę stopnia (degree centrality), miarę bliskości (closeness centrality) i znormalizowaną miarę pośrednictwa (normalized betweeness centrality) wierzchołka `Fantine`.
* Zadanie 17
  * Załóżmy, że wracasz z frontu z ważną wiadomością dotyczącą wojny i z braku czasu możesz podzielić się tą wiadomością tylko z jedną osobą. Oczekuje sie, że osoba, która otrzyma wiadomość, przekaże ją wszystkim znanym sobie osobom. Ważne jest, aby nieprzekształcona wiadomość trafiła do jak największej liczby osób. Wiadomość łatwo przeinaczyć, dlatego nieprzekształcona może przejść tylko jeden krok, co oznacza, że jeśli wiadomość przejedzie więcej niż jeden krok w tej sieci, straci sens. Wykorzystaj wiedzę o powyższych miarach, aby wybrać najlepszego kandydata do przekazania wiadomości.
* Zadanie 18 
  * Tym razem znajdź osobę do przekazania wiadomości bez założenia o limicie odległości ograniczonym do 1. Nadal zakłada się, żeby odległość pomiędzy pierwszą,a ostatnią osobą, która otrzyma wiadomość, była jak najmniejsza.
* Zadanie 19
  * Załóżmy, że nadal nie obowiązuje ograniczenie odległości przekazywania wiadomości, ale teraz przeciwnik wojenny opracował strategię schwytania osoby z sieci, aby zakłócić rozprzestrzenianie się wiadomości. Zidentyfikuj pojedynczą najbardziej ryzykowną osobę do porwania w ramach strategii przeciwnika.
