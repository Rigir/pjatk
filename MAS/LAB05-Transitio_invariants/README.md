![PJATK](../../.assets/banner.png)
=========

![instrucions](resources/instruction.png)

Niezmieniki przejść:
===

1. Wypisz niezmienniki przejść.

| x   | T0  | T1  | T2  | T3  | T4  |
| --- | --- | --- | --- | --- | --- |
| P0  | -1  | 0   | -1  | 0   | 1   |
| P1  | 1   | -1  | 0   | 0   | 0   |
| P2  | 1   | 0   | 0   | 1   | -1  |
| P3  | 0   | 0   | 1   | -1  | 0   |
| P4  | 0   | 1   | 0   | 1   | -1  |

$$
M=
\begin{bmatrix}
-1 &  0 & -1 & 0 & 1\\ 
1 &  -1 & 0 & 0 & 0\\ 
1 &  0 & 0 & 1 & -1\\ 
0 &  0 & 1 & -1 & 0\\ 
0 &  1 & 0 & 1 & -1
\end{bmatrix}
\begin{bmatrix} 
T0\\ T1\\ T2\\ T3\\ T4\\ 
\end{bmatrix}
=
\begin{bmatrix} 
0
\end{bmatrix}
$$

$$
f(x)=
\begin{cases}
-T0-T2+T4=0\\
T0-T1=0\\
T0+T3-T4=0\\
T2-T3=0\\
T1+T3-T4=0
\end{cases}
=>
\begin{cases}
T0=T1\\
T2=T3\\
T0+T3=T4\\
\end{cases}
$$



* Jak wyciągnąć wnioski:
  * Jeśli każda tranzycja bierze udział to wtedy jest odwracalna.
  * Wymiar przestrzeni niezmienników liczy się poprzez zobaczenie ile tranzycji jest równa sobie.
  * Jakie tranzycje tworzą pętle.

* Wnioski:
  * Każda tranzycja bierze udział więc układ jest odwracalny.
  * Wymiar przestrzeni niezmienników wynosi: 2
  * Tranzycje tworzą pętle:
    * T0, T1, T4 
    * T2, T3, T4 
    * T0, T1, T2, T3, 2xT4


Wnioski
===
| T0  | T1  | T2  | T3  | T4  | Wnioski (tworzą pętlę) |
| --- | --- | --- | --- | --- | ---------------------- |
| 0   | 0   | 0   | 0   | 0   | null                   |
| 1   | 1   | 0   | 0   | 1   | T0, T1, T4             |
| 0   | 0   | 1   | 1   | 1   | T2, T3, T4             |
| 1   | 1   | 1   | 1   | 2   | T0, T1, T2, T3, 2xT4   |

* Jaki jest wymiar przestrzeni niezmieników przejść ? 2
* Ile jest niezmieników bazowych przejść ? 2
* Jakie, co oznaczają:
  * T0, T1, T4 tworzą pętle
  * T2, T3, T4 tworzą pętle
  * T0, T1, T2, T3, 2xT4 tworzą pętle
* Czy każde przejście należy do niezminnika?
     *  Sieć może być żywa 
     *  Sieć może być odwracalna