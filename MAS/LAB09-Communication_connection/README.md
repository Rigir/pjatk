![PJATK](../../.assets/banner.png)
=========
* Ograniczoność: 1
* Bezpieczeństwo: true
* Zachowawczość: false
* Odwracalność: false
* Żywotność: false


9_01:

![instrucions](resources/instruction.png)
![result](resources/result.png)

Przykładowe niezmieniki przejść:
===
* Jaki jest wymiar przestrzeni niezmieników przejść: 1
* Ile jest niezmieników bazowych przejść: 1
* Jakie, co oznaczaj ?
  * MHO0, MHO1, MS0, MS4, MS3, MS1, ML1, ML0 tworzą pętle
* Czy każde przejście należy do niezminnika: Nie
  * Sieć może nie być żywa, 
  * Sieć może nie być odwracalna

Przykładowe niezmienniki miejsc:
===
* Jaki jest wymiar przestrzeni niezmieników miejsc: 0
* Ile jest niezmieników bazowych miejsc: 0
* Jakie, co oznaczają ? 
  * Brak
* Czy każde miejsce należy do niezminnika: Nie
  * Sieć może nie być ograniczona.
