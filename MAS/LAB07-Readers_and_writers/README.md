![PJATK](../../.assets/banner.png)
=========

* Zaprojektuj sieć Petriego, która będzie rozwiązywać problem czytelników i pisarzy: 
  * W problemie tym, czytelnicy i pisarze mają dostęp dowspólnego zasobu - na przykład bazy danych - 
    * ale tylko jeden pisarz może w danym momencie zapisywać dane, a wielu czytelników może je jednocześnie odczytywać. 
    * Jednoczesny dostęp do zasobu może uzyskać dowolna liczba czytelników. Pisarz może otrzymać tylko dostęp wyłączny. 
    * Równocześnie z pisarzem dostępu do zasobu nie może otrzymać ani inny pisarz, ani czytelnik, gdyż mogłoby to spowodować błędy.
* Sieć powinna zapewnić, że zasób będzie zawsze w spójnym stanie. 
* Wyznacz niezmienniki miejsc i przejść. Napisz wnioski. 

---

![instrucions](resources/instruction.png)

Wypisz niezmienniki przejść.
===

| x   | T0  | T1  | T2  | T3  |
| --- | --- | --- | --- | --- |
| P0  | -1  | 1   | -1  | 1   |
| R0  | 1   | -1  | 0   | 0   |
| W0  | 0   | 0   | 1   | -1  |
| B0  | -1  | 1   | -3  | 3   |

$$
M=
\begin{bmatrix}
-1 &  1 & -1 & 1\\ 
1 &  -1 & 0 & 0 \\ 
0 &  0 & 1 & -1 \\ 
-1 &  1 & -3 & 3
\end{bmatrix}
\begin{bmatrix} 
T0\\ T1\\ T2\\ T3\\ 
\end{bmatrix}
=
\begin{bmatrix} 
0
\end{bmatrix}
$$

$$
f(x)=
\begin{cases}
-T0+T1-T2+T3=0\\
T0-T1=0\\
T2-T3=0\\
-T0+T1-3T2+3T3=0\\
\end{cases}
=>
\begin{cases}
T0=T1\\
T2=T3\\
\end{cases}
$$


Wnioski:
===
| T0  | T1  | T2  | T3  | Wnioski (tworzą pętlę) |
| --- | --- | --- | --- | ---------------------- |
| 1   | 1   | 0   | 0   | T0, T1                 |
| 0   | 0   | 1   | 1   | T2, T3                 |

* Jaki jest wymiar przestrzeni niezmieników przejść: 2
* Ile jest niezmieników bazowych przejść: 2
* Jakie, co oznaczają ? 
  * T0, T1 tworzą pętle
  * T2, T3 tworzą pętle
* Czy każde przejście należy do niezminnika: Tak
  * Sieć może być żywa, 
  * Sieć może być odwracalna


Wyznacz niezmienniki miejsc.
===

| x   | P0  | R0  | W0  | B0  |
| --- | --- | --- | --- | --- |
| T0  | -1  | 1   | 0   | -1  |
| T1  | 1   | -1  | 0   | 1   |
| T2  | -1  | 0   | 1   | -3  |
| T3  | 1   | 0   | -1  | 3   |

$$
M=
\begin{bmatrix}
-1 & 1 & 0 & -1 \\ 
1 & -1 & 0 & 1 \\
-1 & 0 & 1 & -3 \\ 
1 & 0 & -1 & 3 \\
\end{bmatrix}
\begin{bmatrix} 
P0\\ R0\\ W0\\ B0\\ 
\end{bmatrix}
=
\begin{bmatrix} 
0
\end{bmatrix}
$$

$$
f(x)=
\begin{cases}
-P0+R0-B0=0\\
P0-R0+B0=0\\
-P0+W0-3B0=0\\
P0-W0+3B0=0\\
\end{cases}
=>
\begin{cases}
R0=P0+B0\\
W0=P0+3B0
\end{cases}
$$

* Jak wyliczyć wektory wagowe:
  * Aby to wyliczyć na początku wyciągasz to co nie powiela się na lewo.
  * Poczym te po lewej stronie dajemy im albo 1,0. 
  * W tabeli podkładamy pod dwa wzory. 

Wnioski:
===
| P0  | B0  | R0  | W0  | Wnioski (wektory wagowe) |
| --- | --- | --- | --- | ------------------------ |
| 0   | 1   | 1   | 3   | B0, R0,3xW0              |
| 1   | 0   | 1   | 1   | P0, R0, W0               |
| 1   | 1   | 2   | 4   | Suma pozostałych         |

* Jaki jest wymiar przestrzeni niezmieników miejsc: 2
* Ile jest niezmieników bazowych miejsc: 2
* Jakie, co oznaczają ? 
  * 1 * B0 + 1 * R0 + 3 * W0 = const
  * 1 * P0 + 1 * R0 + 1 * W0 = const
* Czy każde miejsce należy do niezminnika: Tak
  * Sieć może być ograniczona.