Platform Independent Petri Net Editor
https://github.com/sarahtattersall/PIPE

---

* Ograniczoność: Maksymalna ilość żetonów w całej sieci
* Bezpieczeństwo: W każdym momencie co najwyżej jeden żeton w wszystkich miejscach.
* Zachowawczość: W całej sieci ma tyle samo żetonów.
* Odwracalność: 
  * Czy może wrócić do stanu początkowego z każdego punktu w grafie ?
  * ??Jeśli każda tranzycja bierze udział to wtedy jest odwracalna??
* Żywotność: Wszystkie części systemu są dostępne z każdego miejsca.

---

* Jaki jest wymiar przestrzeni niezmieników ? (miejsc, przejść) 
* Ile jest niezmieników bazowych ? (miejsc, przejść) 
  * Zawsze takie samo jak przestrzeń niezmieników.
* Jakie, co oznaczają ? 
  *  Miejsc: zbiory miejsc sieci, w których łączna (ewentualnie ważona) liczba żetonów jest stała.
  *  Przejść: tworzą pętlę.
* Czy każde (miejsce / przejście) należy do niezminnika?
  *  Miejsc:
     *  Ograniczona
  *  Przejść:
     *  Żywa, Odwracalna

Przykładowe niezmienniki miejsc:
===
| P0  | R0  | W0  | B0  | Wnioski (wektory wagowe) |
| --- | --- | --- | --- | ------------------------ |
| 0   | 1   | 3   | 1   | R0, 3xW0, B0             |
| 1   | 1   | 3   | 0   | P0, R0, 3xW0             |

* Jaki jest wymiar przestrzeni niezmieników miejsc: 2
* Ile jest niezmieników bazowych miejsc: 2
* Jakie, co oznaczają ? 
  * 1 * R0 + 3 * W0 + 1 * B0 = const
  * 1 * P0 + 1 * R0 + 3 * W0 = const
* Czy każde miejsce należy do niezminnika: Tak
  * Sieć może być ograniczona.

Przykładowe niezmieniki przejść:
===
| T0  | T1  | T2  | T3  | Wnioski (tworzą pętlę) |
| --- | --- | --- | --- | ---------------------- |
| 1   | 1   | 0   | 0   | T0, T1                 |
| 0   | 0   | 1   | 1   | T2, T3                 |

* Jaki jest wymiar przestrzeni niezmieników przejść: 2
* Ile jest niezmieników bazowych przejść: 2
* Jakie, co oznaczają ? 
  * T0, T1 tworzą pętle
  * T2, T3 tworzą pętle
* Czy każde przejście należy do niezminnika: Tak
  * Sieć może być żywa, 
  * Sieć może być odwracalna