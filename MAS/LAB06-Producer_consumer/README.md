![PJATK](../../.assets/banner.png)
=========

![instrucions](resources/instruction_01.png)

Niezmienniki miejsc:
===

1. Wypisz niezmienniki miejsc dla 6_1.

| x   | P0  | P1  | P2  | P3  | P4  |
| --- | --- | --- | --- | --- | --- |
| T0  | 1   | -1  | 0   | 0   | 0   |
| T1  | -1  | 1   | 1   | 0   | 0   |
| T2  | 0   | 0   | -1  | 1   | -1  |
| T3  | 0   | 0   | 0   | -1  | 1   |


$$
M=
\begin{bmatrix}
1 & -1 & 0 & 0 & 0\\ 
-1 & 1 & 1 & 0 & 0\\ 
0 & 0 & -1 & 1 & -1\\ 
0 & 0 & 0 & -1 & 1\\ 
\end{bmatrix}
\begin{bmatrix} 
P0\\ P1\\ P2\\ P3\\ P4\\ 
\end{bmatrix}
=
\begin{bmatrix} 
0
\end{bmatrix}
$$

$$
f(x)=
\begin{cases}
P0-P1=0\\
-P0+P1+P2=0\\
-P2+P3-P4=0\\
-P3+P4=0\\
\end{cases}
=>
\begin{cases}
P0=P1\\
P2=0\\
P3=P4\\
\end{cases}
$$

| P0  | P1  | P2  | P3  | P4  | Wnioski (wektory wagowe) |
| --- | --- | --- | --- | --- | ------------------------ |
| 0   | 0   | 0   | 0   | 0   | null                     |
| 1   | 1   | 0   | 0   | 0   | P0,P1                    |
| 0   | 0   | 0   | 1   | 1   | P3,P4                    |
| 1   | 1   | 0   | 1   | 1   | P0,P1,P3,P4              |

---

2. Wypisz niezmienniki miejsc dla 6_2.
   1. 6_2 ograniczyć wytwarzanie producenta. 

![instrucions](resources/instruction_02.png)

| x   | P0  | P1  | P2  | P3  | P4  | P5  |
| --- | --- | --- | --- | --- | --- | --- |
| T0  | 1   | -1  | 0   | 0   | 0   | -1  |
| T1  | -1  | 1   | 1   | 0   | 0   | 0   |
| T2  | 0   | 0   | -1  | 1   | -1  | 1   |
| T3  | 0   | 0   | 0   | -1  | 1   | 0   |

$$
M=
\begin{bmatrix}
1 & -1 & 0 & 0 & 0 & -1\\ 
-1 & 1 & 1 & 0 & 0 & 0\\ 
0 & 0 & -1 & 1 & -1 & 1\\ 
0 & 0 & 0 & -1 & 1 & 0\\ 
\end{bmatrix}
\begin{bmatrix} 
P0\\ P1\\ P2\\ P3\\ P4\\ P5\\ 
\end{bmatrix}
=
\begin{bmatrix} 
0
\end{bmatrix}
$$

$$
f(x)=
\begin{cases}
P0-P1-P5=0\\
-P0+P1+P2=0\\
-P2+P3-P4+P5=0\\
-P3+P4=0\\
\end{cases}
=>
\begin{cases}
P0=P1+P5\\
P2=P5 \\
P3=P4\\
\end{cases}
$$

| P0  | P1  | P2  | P3  | P4  | P5  | Wnioski (wektory wagowe) |
| --- | --- | --- | --- | --- | --- | ------------------------ |
| 1   | 1   | 0   | 0   | 0   | 0   | P0,P1                    |
| 0   | 0   | 0   | 1   | 1   | 0   | P3,P4                    |
| 1   | 0   | 1   | 0   | 0   | 1   | P0,P2,P5                 |

Wnioski:
===

6_1:
| P0  | P1  | P2  | P3  | P4  | Wnioski (wektory wagowe) |
| --- | --- | --- | --- | --- | ------------------------ |
| 1   | 1   | 0   | 0   | 0   | P0,P1                    |
| 0   | 0   | 0   | 1   | 1   | P3,P4                    |
| 1   | 1   | 0   | 1   | 1   | P0,P1,P3,P4              |

* Jaki jest wymiar przestrzeni niezmieników miejsc: 2
* Ile jest niezmieników bazowych miejsc: 2
* Jakie, co oznaczają ? 
  * 1 * P0 + 1 * P1 = const
  * 1 * P3 + 1 * P4 = const
  * 1 * P0 + 1 * P1 + 1 * P3 + 1 * P4 = const
* Czy każde miejsce należy do niezminnika: Nie
  * Sieć może nie być ograniczona.

---

6_2:
| P0  | P1  | P2  | P3  | P4  | P5  | Wnioski (wektory wagowe) |
| --- | --- | --- | --- | --- | --- | ------------------------ |
| 1   | 1   | 0   | 0   | 0   | 0   | P0,P1                    |
| 0   | 0   | 0   | 1   | 1   | 0   | P3,P4                    |
| 1   | 0   | 1   | 0   | 0   | 1   | P0,P2,P5                 |

* Jaki jest wymiar przestrzeni niezmieników miejsc: 3
* Ile jest niezmieników bazowych miejsc: 3
* Jakie, co oznaczają ? 
  * 1 * P0 + 1 * P1 = const
  * 1 * P3 + 1 * P4 = const
  * 1 * P0 + 1 * P2 + 1 * P5 = const
* Czy każde miejsce należy do niezminnika: Tak
  * Sieć może być ograniczona.

---

3. Porównać 1 i 2
   1. Zminiła się przestrzeń niezminików.
   2. Sieć 6_2 jest pokryta niezminikami, natomist 6_1 nie jest.
   3. Sieć 6_2 może być ograniczona, natomiast 6_1 może być nieogramiczona.
   4. Sieć 6_2 łączna ważona liczba żetonów jest stała, natomist w 6_1 nie jest.