![PJATK](../../.assets/banner.png)
=========
8_01:

![instrucions](resources/instruction_01.png)

![result](resources/result_01.png)

Niezmieniki przejść:
===
* Jaki jest wymiar przestrzeni niezmieników przejść: 2
* Ile jest niezmieników bazowych przejść: 2
* Jakie, co oznaczają ? 
  * T1, T2, T3, T5, T6 tworzą pętle
  * T0, T1, T4, T6 tworzą pętle
* Czy każde przejście należy do niezminnika: Tak
  * Sieć może być żywa, 
  * Sieć może być odwracalna

Niezmienniki miejsc:
===
* Jaki jest wymiar przestrzeni niezmieników miejsc: 3
* Ile jest niezmieników bazowych miejsc: 3
* Jakie, co oznaczają ? 
  * 1 * B0 + 1 * BC0 + 1 * CH0 = const
  * 1 * AC0 + 1 * WR0 = const
  * 1 * B0 + 1 * BAW0 = const
* Czy każde miejsce należy do niezminnika: Nie
  * Sieć może nie być ograniczona.

8_02:

![instrucions](resources/instruction_02.png)

![result](resources/result_02.png)

Przykładowe niezmienniki miejsc:
===
* Jaki jest wymiar przestrzeni niezmieników miejsc: 2
* Ile jest niezmieników bazowych miejsc: 2
* Jakie, co oznaczają ? 
  * 1 * B0 + 1 * CH0 + 1 * S0 = const
  * 1 * BUF0 + 2 * S0 + 1 * WR0  = const
* Czy każde miejsce należy do niezminnika: Nie
  * Sieć może nie być ograniczona.