import org.apache.pekko
import pekko.actor.{ActorSystem, Actor, ActorLogging, ActorRef, Props}

case class PlayWith(a: ActorRef)
case object Reject

class Player(name: String) extends Actor with ActorLogging {
  def receive: Receive = { case PlayWith(a1) =>
    context.become(play(a1))
    a1 ! PlayWith(self)
  }

  def play(a: ActorRef): Receive = {
    case PlayWith(_) =>
      a ! Reject
    case Reject =>
      log.info(s"${self.path.name}")
      a ! Reject
  }
}

@main
def mainProg: Unit = {
  val system = ActorSystem("PingPongSystem")
  val playerNames = List("Player1", "Player2", "Player3", "Player4")
  val players: List[ActorRef] =
    playerNames.map(name => system.actorOf(Props(new Player(name)), name))

  for {
    i <- players.indices
    nextIndex = (i + 1) % players.size
  } yield players(i) ! PlayWith(players(nextIndex))
}
