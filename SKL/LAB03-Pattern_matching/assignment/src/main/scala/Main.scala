import scala.annotation.tailrec

@main
def mainProg: Unit = {
  println(s"Zad01: ${divide(List.range(1, 11))}")
  println(s"Zad02: ${sum(List(Some(1), None, Some(2), None))}, ${sum(List(None, None, None))}")
  println(s"Zad03: ${compress(List('a', 'a', 'b', 'c', 'c', 'c', 'a', 'a', 'b', 'd'))}")
  println(s"Zad04: ${isSub(List('b', 'o', 'c', 'i', 'a', 'n'), List('a', 'b', 'c'))}")
}

// Zad01
def divide[T](list: List[T], evenId: List[T] = Nil, oddId: List[T] = Nil): (List[T], List[T]) = {
  list match {
    case Nil => (evenId.reverse, oddId.reverse)
    case x :: Nil => (x :: evenId).reverse -> oddId.reverse
    case x :: y :: tail => divide(tail, y :: evenId, x :: oddId)
  }
}

// Zad02
def sum(list: List[Option[Int]]): Option[Int] = {
  @tailrec
  def sumHelper(list: List[Option[Int]], acc: Int): Option[Int] = {
    list match {
      case Nil => { 
        acc match {
          case 0 => None 
          case _ => Some(acc)
        }
      }
      case None :: tail => sumHelper(tail, acc)
      case Some(x) :: tail => sumHelper(tail, acc + x)
    }
  }
  sumHelper(list, 0)
}
  
// Zad03
def compress(l: List[Char]): List[(Char, Int)] = {
  def compressHelper(input: List[Char], current: Char, count: Int, result: List[(Char, Int)]): List[(Char, Int)] = input match {
    case Nil => (current, count) :: result
    case x :: xs if x == current => compressHelper(xs, current, count + 1, result)
    case x :: xs => compressHelper(xs, x, 1, (current, count) :: result)
  }
  compressHelper(l.tail, l.head, 1, Nil).reverse
}

// Zad04
def isSub(l: List[Char], lSub: List[Char]): Boolean = {
  @tailrec
  def isSubHelper(list: List[Char], subList: List[Char], found: Boolean): Boolean = {
    (list, subList) match {
      case (Nil, Nil) => true
      case (_, Nil) => true
      case (Nil, _) => false
      case (c :: list, sc :: subList) if c == sc =>
        isSubHelper(list, subList, found = true)
      case (_, _) if found =>
        isSubHelper(l, subList, found = false)
      case (c :: list, _) =>
        isSubHelper(list, subList, found)
    }
  }

  isSubHelper(l, lSub, found = false)
}