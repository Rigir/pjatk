import scala.math.sqrt

@main
def mainProg: Unit = {
  val i1 = C(2.0, 3.0)
  val i2 = C(1.0, -2.0)
  val i3 = C(2.0, 3.0)

  println("Zad02_03")
  println(i1 + i2)
  println(i1 - i2)
  println(i1 * i2)

  println("Zad04")
  println(i1 / i2)

  println("Zad05")
  println(C(1.0, 1.0))
  println(C(1.0))

  println("Zad06")
  println(i1 + 5.3)
  println(i2 * 2.5)

  println("Zad07")
  println(s"i1: ${i1} | i2: ${i2} | i3 ${i3} \n")
  println(s"i1 == i2: ${i1 == i2}")
  println(s"i1 != i2: ${i1 != i2}")
  println(s"i1 < i2: ${i1 < i2}")
  println(s"i1 <= i2: ${i1 <= i2} \n")

  println(s"i1 == i3: ${i1 == i3}")
  println(s"i1 != i3: ${i1 != i3}")
  println(s"i1 > i3: ${i1 > i3}")
  println(s"i1 >= i3: ${i1 >= i3}")
}

class C(val re: Double, val im: Double) extends Ordered[C] {
  def this(re: Double) = this(re, 0.0)

  override def toString: String =
    if (im > 0) s"$re + ${im}i"
    else if (im < 0) s"$re - ${-im}i"
    else s"$re"

  def +(that: C): C = new C(re + that.re, im + that.im)
  def -(that: C): C = new C(re - that.re, im - that.im)
  def *(that: C): C =
    new C(re * that.re - im * that.im, re * that.im + im * that.re)
  def /(that: C): C = {
    if (that.re == 0 && that.im == 0)
      throw new IllegalArgumentException("Dzielenie przez zero")
    val denominator = that.re * that.re + that.im * that.im
    new C(
      (re * that.re + im * that.im) / denominator,
      (im * that.re - re * that.im) / denominator
    )
  }

  def +(that: Double): C = new C(re + that, im)
  def -(that: Double): C = new C(re - that, im)
  def *(that: Double): C = new C(re * that, im * that)
  def /(that: Double): C = new C(re / that, im / that)

  def ==(that: C): Boolean = re == that.re && im == that.im
  def !=(that: C): Boolean = !(this == that)

  override def <(that: C): Boolean = compare(that) < 0
  override def <=(that: C): Boolean = compare(that) <= 0
  override def >(that: C): Boolean = compare(that) > 0
  override def >=(that: C): Boolean = compare(that) >= 0

  def distanceFromOrigin: Double = sqrt(re * re + im * im)
  def compare(that: C): Int =
    distanceFromOrigin.compare(that.distanceFromOrigin)
}
