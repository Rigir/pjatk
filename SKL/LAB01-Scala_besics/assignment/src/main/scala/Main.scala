@main
def mainProg: Unit = {
  val text = "Test01\nTest02\nTest03"
  println(framedText(text))
}

def framedText(text: String): String = {
  val lines = text.split('\n')
  val maxLength = lines.maxBy(s => s.length).length
  val frame = "*" * (maxLength + 4)
  val framedLines = lines.map(line => "* " + line.padTo(maxLength, ' ') + " *")
  return (frame +: framedLines :+ frame).mkString("\n")
}
