import scala.annotation.tailrec

@main
def mainProg: Unit = {
  println(s"Zad01:")
  println(s"1. ${isOrdered(List(1, 3, 3, 6, 8), (_ <= _))}")
  println(s"2. ${isOrdered(List(1, 3, 3, 6, 8), (_ >= _))}")
  println(s"Zad02: ${merge(List(1, 3, 5, 8), List(2, 4, 6, 8, 10, 12))((m, n) => m < n)}")
  println(s"Zad03:")
  println(s"1. ${getFirst(List(1, 2, 3, 4, 5))(_ > 2)(_ + 2)}")
  println(s"2. ${getFirst(List(1, 2, 3, 4, 5))(_ > 5)(_ + 2)}")
  println(s"Zad04: ${modify(List(2, -1, 3, -8, 5), List(3, -3, 3, 0, -4, 5))(_ < _)(_ + _)}")
}

// Zad01
def isOrdered(list: List[Int], leq: (Int, Int) => Boolean): Boolean = {
  @tailrec
  def checkOrdered(lst: List[Int], prev: Int): Boolean = lst match {
    case Nil => true
    case head :: tail if leq(prev, head) => checkOrdered(tail, head)
    case _ => false
  }
  list match {
    case Nil => true
    case head :: tail => checkOrdered(tail, head)
  }
}

// Zad02
def merge(a: List[Int], b: List[Int])(leq: (Int, Int) => Boolean): List[Int] = {
  @tailrec
  def mergeHelper(a: List[Int], b: List[Int], acc: List[Int]): List[Int] = (a, b, acc) match {
    case (Nil, hB :: tB, hAcc :: tAcc) if (leq(hAcc, hB)) => mergeHelper(Nil, tB, hB :: acc)
    case (hA :: tA, Nil, hAcc :: tAcc) if (leq(hAcc, hA)) => mergeHelper(tA, Nil, hA :: acc)
    case (hA :: tA, hB :: tB, _) if (leq(hA, hB)) => mergeHelper(tA, b, hA :: acc)
    case (_, hB :: tB, _) => mergeHelper(a, tB, hB :: acc)
    case _ => acc.reverse
  }

  mergeHelper(a, b, Nil)
}

// Zad03
def getFirst(l: List[Int])(pred: Int => Boolean)(op: Int => Int): Option[Int] = {
  @tailrec
  def findFirst(list: List[Int]): Option[Int] = list match {
    case Nil => None
    case head :: tail if pred(head) => Some(op(head))
    case _ :: tail => findFirst(tail)
  }

  findFirst(l)
}

// Zad04
def modify(l1: List[Int], l2: List[Int])(
    pred: (Int, Int) => Boolean
)(op: (Int, Int) => Int): List[Int] = {
  @tailrec
  def modifyHelper(list1: List[Int], list2: List[Int], acc: List[Int]): List[Int] =
    (list1, list2) match {
      case (l1H :: l1T, l2H :: l2T) if (pred(l1H, l2H)) =>
        modifyHelper(l1T, l2T, op(l1H, l2H) :: acc)
      case (_ :: l1T, _ :: l2T) => modifyHelper(l1T, l2T, acc)
      case _ => acc.reverse
    }

  modifyHelper(l1, l2, Nil)
}
