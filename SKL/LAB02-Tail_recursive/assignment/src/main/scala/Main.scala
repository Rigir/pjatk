import scala.annotation.tailrec

@main
def mainProg: Unit = {
  println(s"Zad01: ${reverse("Hello, World!")}")
  println(s"Zad02: 1-${isPrime(1)}, 2-${isPrime(2)}, 4-${isPrime(4)}")
  println(s"Zad03: b(111)-d(${binToDec(111)})")
  println(s"Zad04: ${cFib(9)}")
}

// Zad01
def reverse(str: String): String = {
  @tailrec
  def reverseHelper(str: String,  acc: String): String = {
    if(str==""){acc}
    else reverseHelper(str.tail, str.head+:acc)
  }
  reverseHelper(str, "")
}

// Zad02
def isPrime(n: Int): Boolean = {
  @tailrec
  def isPrimeHelper(divisor: Int): Boolean = {
    if (divisor <= 1) true
    else if (n % divisor == 0) false
    else isPrimeHelper(divisor - 1)
  }

  if (n <= 1) false
  else isPrimeHelper(Math.sqrt(n).toInt)
}

// Zad03
def binToDec(bin: Int): Int = {
  @tailrec
  def binToDecHelper(bin: Int, result: Int, power: Int): Int = {
    if (bin == 0) result
    else binToDecHelper(bin / 10, result + bin % 10 * math.pow(2, power).toInt, power + 1)
  }

  binToDecHelper(bin, 0, 0)
}

//Zad04
def cFib(n: Int): Int = {
  @tailrec
  def cFibHelper(n: Int, a: Int, b: Int): Int = {
    if (n == 0) a
    else if (n == 1) b
    else cFibHelper(n - 1, b, a + b)
  }

  cFibHelper(n, 2, 1)
}