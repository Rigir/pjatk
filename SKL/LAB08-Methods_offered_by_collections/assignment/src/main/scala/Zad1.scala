@main
def zad1: Unit = {
  val lines = io.Source
    .fromResource("ogniem-i-mieczem.txt")(io.Codec.UTF8)
    .getLines
    .toList

  val result = histogram(lines, 50)
  println(result)
}

def histogram(lines: List[String], max: Int): String = {
  val text = lines.mkString("").toLowerCase

  val charFrequency = text
    .filter(_.isLetter)
    .groupBy(identity)
    .view
    .mapValues(_.length)
    .toMap
    .toList

  val sortedCharFrequency = charFrequency.sortBy { case (char, _) => char }
  val maxFrequency = sortedCharFrequency.map { case (_, freq) => freq }.max
  val normalizedCharFrequency = sortedCharFrequency.map { case (char, freq) =>
    val normalizedFreq = (freq.toDouble / maxFrequency * max).toInt
    (char, normalizedFreq)
  }

  normalizedCharFrequency
    .map { case (char, freq) =>
      s"$char:".padTo(4, ' ') + "*" * freq
    }
    .mkString("\n")
}
