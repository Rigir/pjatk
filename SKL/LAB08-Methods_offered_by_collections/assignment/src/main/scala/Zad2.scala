@main
def zad2: Unit = {
  case class Wojewodztwo(nazwa: String, min: Int)
  // max ID gminy z województwa w: w.min + 19999
  case class Wynik(
      ID: Int,
      KOALICJA_OBYWATELSKA: Int,
      LEWICA_RAZEM: Int,
      POLEXIT: Int,
      JEDNOSC_NARODU: Int,
      PIS: Int,
      EUROPA_CHRISTI: Int,
      WIOSNA: Int,
      KONFEDERACJA: Int,
      KUKIZ15: Int,
      POLSKA_FAIR_PLAY: Int
  )

  val wojewodztwa = List(
    Wojewodztwo("dolnoslaskie", 20000),
    Wojewodztwo("kujawsko-pomorskie", 40000),
    Wojewodztwo("lubelskie", 60000),
    Wojewodztwo("lubuskie", 80000),
    Wojewodztwo("lodzkie", 100000),
    Wojewodztwo("malopolskie", 120000),
    Wojewodztwo("mazowieckie", 140000),
    Wojewodztwo("opolskie", 160000),
    Wojewodztwo("podkarpackie", 180000),
    Wojewodztwo("podlaskie", 200000),
    Wojewodztwo("pomorskie", 220000),
    Wojewodztwo("slaskie", 240000),
    Wojewodztwo("swietokrzyskie", 260000),
    Wojewodztwo("warminsko-mazurskie", 280000),
    Wojewodztwo("wielkopolskie", 300000),
    Wojewodztwo("zachodniopomorskie", 320000)
  )

  val wyniki = io.Source
    .fromResource("wyniki.csv")(io.Codec.UTF8)
    .getLines
    .toList
    .map(l => {
      l.split(",").toList.map(_.toInt) match {
        case List(a, b, c, d, e, f, g, h, i, j, k) =>
          Wynik(a, b, c, d, e, f, g, h, i, j, k)
        case _ => throw new IllegalArgumentException
      }
    })

  val minimalnaRoznica = wojewodztwa
    .map { w =>
      val wynikiWojewodztwa =
        wyniki.filter(_.ID >= w.min).filter(_.ID <= w.min + 19999)
      val rozniceProcentowe = wynikiWojewodztwa.map { wynik =>
        val procentKO =
          wynik.KOALICJA_OBYWATELSKA.toDouble / (wynik.KOALICJA_OBYWATELSKA + wynik.PIS) * 100
        val procentPIS =
          wynik.PIS.toDouble / (wynik.KOALICJA_OBYWATELSKA + wynik.PIS) * 100
        Math.abs(procentKO - procentPIS)
      }
      (w, rozniceProcentowe.min)
    }

  val minDiff = minimalnaRoznica.map(_._2).min
  val regionsWithMinDiff = minimalnaRoznica.filter(_._2 == minDiff)

  regionsWithMinDiff.foreach { case (region, diff) =>
    println(s"Wojewodztwo: ${region.nazwa}")
    println(s"Minimalna roznica procentowa: $diff% \n")
  }
}
