@main
def mainProg: Unit = {
  println(s"Zad1: ${subSeq(Seq(1, 2, 3, 4, 5), 1, 3)}")
  println(s"Zad2: ${getMinMax(Seq(("a", 2.5), ("b", 1.0), ("c", 3.0)))}")
  println(s"Zad3: ${remElems(Seq(1, 2, 3, 4, 5), 2)}")
  println(s"Zad4: ${diff(Seq(1, 2, 3), Seq(2, 2, 1, 3))}")
  println(s"Zad5: ${sumOption(Seq(Some(5.4), Some(-2.0), Some(1.0), None, Some(2.6)))}")
  println(s"Zad6: ${deStutter(Seq(1, 1, 2, 4, 4, 4, 1, 3))}")
  println(s"Zad7:")
  println(s"(_<_): ${isOrdered(Seq(1, 2, 2, 4))(_ < _)}")
  println(s"(_<=_): ${isOrdered(Seq(1, 2, 2, 4))(_ <= _)}")
  println(s"Zad8: ${freq(Seq('a', 'b', 'a', 'c', 'c', 'a'))}")
  println(s"Zad9: ${threeNumbers(10)}")
}

def subSeq[A](seq: Seq[A], begIdx: Int, endIdx: Int): Seq[A] = {
  seq.drop(begIdx).take(endIdx - begIdx)
}

def getMinMax[A](seq: Seq[(A, Double)]): Option[(A, A)] = {
  if (seq.isEmpty) None
  else {
    val minElement = seq.minBy(_._2)._1
    val maxElement = seq.maxBy(_._2)._1
    Some((minElement, maxElement))
  }
}

def remElems[A](seq: Seq[A], k: Int): Seq[A] = {
  seq.zipWithIndex.filterNot { case (_, idx) => idx == k }.map(_._1)
}

def diff[A](seq1: Seq[A], seq2: Seq[A]): Seq[A] = {
  seq1.zip(seq2).filterNot { case (a, b) => a == b }.map(_._1)
}

def sumOption(seq: Seq[Option[Double]]): Double = {
  seq.flatten.sum
}

def deStutter[A](seq: Seq[A]): Seq[A] = seq.foldLeft(Seq.empty[A]) {
  case (acc, elem) if acc.isEmpty || acc.last != elem => acc :+ elem
  case (acc, _) => acc
}

def isOrdered[A](seq: Seq[A])(leq: (A, A) => Boolean): Boolean = {
  seq.sliding(2).forall { case Seq(a, b) => leq(a, b) }
}

def freq[A](seq: Seq[A]): Set[(A, Int)] = {
  seq.groupBy(identity).view.mapValues(_.size).toSet
}

def threeNumbers(n: Int): Seq[(Int, Int, Int)] = {
  val nums = (1 to n).toList
  for {
    a <- nums
    b <- nums
    c <- nums
    if a < b && (a * a) + (b * b) == (c * c)
  } yield (a, b, c)
}
