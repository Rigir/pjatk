import org.apache.pekko
import pekko.actor._
import scala.concurrent.duration._
import scala.util.Random

val StartGame = "StartGame"
case class CastleAttack()
case class CastleHit()
case class CommandAttack()
case class DefenderAttack(activeDefenders: Int)
case class InitializeCastle(numberOfDefenders: Int)
case class InitializeHigherPower(numberOfCastles: Int, numberOfDefenders: Int)

class Defender extends Actor {
  def receive: Receive = {
    case DefenderAttack(activeDefenders: Int) => {
      val hitProbability = activeDefenders.toDouble / (2 * 100)
      if (Random.nextDouble() < hitProbability) {
        sender() ! CastleHit
      }
    }
  }
}

class Castle(id: Int) extends Actor {
  var _defenders: List[ActorRef] = List.empty

  def receive: Receive = {
    case InitializeCastle(numberOfDefenders) => {
      _defenders = (1 to numberOfDefenders).map(_ => context.actorOf(Props[Defender]())).toList
      println(s"Castle $id created $numberOfDefenders defenders.")
    }

    case CastleAttack() => {
      _defenders.foreach(defender => defender ! DefenderAttack(_defenders.length))
    }

    case CastleHit => {
      _defenders = _defenders.filter(_ != sender())
      println(s"Castle $id get hit, ${_defenders.length} defenders left.")
      if (_defenders.isEmpty) {
        println(s"Castle $id lost the battle.")
        context.system.terminate()
      }
    }
  }
}

class HigherPower extends Actor {
  var _castles: List[ActorRef] = List.empty

  def receive: Receive = {
    case InitializeHigherPower(numberOfCastles, numberOfDefenders) => {
      _castles = (1 to numberOfCastles)
        .map(id => context.actorOf(Props(new Castle(id)), s"Castle$id"))
        .toList

      _castles.foreach(castle => castle ! InitializeCastle(numberOfDefenders))

      println(s"Created $numberOfCastles castles.")
    }

    case StartGame => {
      _castles.foreach(castle => castle ! CastleAttack())
    }
  }
}

@main
def mainProg: Unit = {
  val system = ActorSystem("system")
  val higherPower = system.actorOf(Props[HigherPower](), "HigherPower")

  higherPower ! InitializeHigherPower(2, 100)
  import system.dispatcher
  system.scheduler.scheduleWithFixedDelay(
    1.second,
    1.second,
    higherPower,
    StartGame
  )
}
