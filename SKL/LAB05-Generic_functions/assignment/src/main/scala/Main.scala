import scala.annotation.tailrec

type MSet[A] = A => Int

def printMSet[A](setName: String, mset: MSet[A], elements: List[A]): Unit = {
  val results = elements.map(element => s"${mset(element)}").mkString(", ")
  println(s"$setName: [$results]")
}

@main
def mainProg: Unit = {
  println(s"Zad01: ${insertInto[Int](List(1, 2, 4, 6), 3)(_ < _)}")
  println(s"Zad02: ${applyForAll(List(1, 3, 5))((n) => n + 3)}")
  println(s"Zad03:")
  println(s"1.Sum: ${compute(List(1, 2, 3, 4), 0)(_ + _)}")
  println(s"2.Product: ${compute(List(1, 2, 3, 4), 1)(_ * _)}")
  println(s"3.Sentence: ${compute(List("kota", " ", "ma", " ", "ala"), "")(_ + _)}")
  println(s"Zad04:")
  val mset1: MSet[Int] = (n: Int) =>
    n match {
      case 1 => 4
      case 3 => 1
      case _ => 0
    }

  val mset2: MSet[Int] = (n: Int) =>
    n match {
      case 1 => 2
      case 2 => 2
      case _ => 0
    }

  val printElements = List(1, 2, 3)
  printMSet("mset1", mset1, printElements)
  printMSet("mset2", mset2, printElements)
  printMSet("sumResult", sum(mset1, mset2), printElements)
  printMSet("diffResult", diff(mset1, mset2), printElements)
  printMSet("multResult", mult(mset1, mset2), printElements)
}

// Zad01
def insertInto[A](l: List[A], el: A)(leq: (A, A) => Boolean): List[A] = {
  @tailrec
  def insertHelper(acc: List[A], rest: List[A]): List[A] = rest match {
    case Nil => acc.reverse ::: List(el)
    case head :: tail if leq(el, head) => (acc.reverse ::: List(el)) ::: rest
    case head :: tail => insertHelper(head :: acc, tail)
  }

  insertHelper(Nil, l)
}

// Zad02
def applyForAll[A, B](l: List[A])(f: A => B): List[B] = {
  @tailrec
  def applyHelper(acc: List[B], rest: List[A]): List[B] = rest match {
    case Nil => acc.reverse
    case head :: tail => applyHelper(f(head) :: acc, tail)
  }

  applyHelper(Nil, l)
}

// Zad03
def compute[A, B](l: List[A], init: B)(op: (A, B) => B): B = {
  @tailrec
  def computeHelper(list: List[A], acc: B): B = list match {
    case Nil => acc
    case head :: tail => computeHelper(tail, op(head, acc))
  }

  computeHelper(l, init)
}

// Zad04
def compose[A, B, C](f: A => B)(g: B => C): A => C =
  (a: A) => g(f(a))

def prod[A, B, C, D](f: A => C, g: B => D): (A, B) => (C, D) =
  (a: A, b: B) => (f(a), g(b))

def lift[A, B, T](op: (T, T) => T)(f: A => T, g: B => T): (A, B) => T =
  (a: A, b: B) => op(f(a), g(b))

def sum[A](s1: MSet[A], s2: MSet[A]): MSet[A] =
  (a: A) => s1(a) + s2(a)

def diff[A](s1: MSet[A], s2: MSet[A]): MSet[A] =
  (a: A) => (s1(a) - s2(a))

def mult[A](s1: MSet[A], s2: MSet[A]): MSet[A] =
  (a: A) => s1(a) * s2(a)
