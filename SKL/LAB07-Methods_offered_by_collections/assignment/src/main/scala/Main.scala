val strefy: Seq[String] = java.util.TimeZone.getAvailableIDs.toSeq

case class Ocena(imie: String, nazwisko: String, wdziek: Int, spryt: Int)

@main
def mainProg: Unit = {
  println(s"Zad1: ${countChars("abc ABC")}")
  println(s"Zad2: ${strefy
      .filter(_.startsWith("Europe/"))
      .map(_.stripPrefix("Europe/"))
      .sortBy(s => (s.length, s))}")
  println(s"Zad3: ${score(Seq(1, 3, 2, 2, 4, 5))(Seq(2, 1, 2, 4, 7, 2))}")

  println(s"Zad4: ${calculateResults(
      List(
        Ocena("Jan", "Kowalski", 18, 15),
        Ocena("Anna", "Nowak", 20, 18),
        Ocena("Adam", "Mickiewicz", 16, 20),
        Ocena("Maria", "Curie", 19, 16),
        Ocena("Henryk", "Sienkiewicz", 17, 17),
        Ocena("Stefan", "Zeromski", 15, 19)
      )
    )}")
}

def countChars(str: String): Int = str.toSet.size

def score(code: Seq[Int])(move: Seq[Int]): (Int, Int) = {
  val blackDots = code.zip(move).count { case (c, m) => c == m }

  val codeFreq = code.groupBy(identity).view.mapValues(_.size).toMap
  val moveFreq = move.groupBy(identity).view.mapValues(_.size).toMap

  val whiteDots = codeFreq.foldLeft(0) { case (acc, (num, freq)) =>
    acc + math.min(freq, moveFreq.getOrElse(num, 0))
  } - blackDots

  (blackDots, whiteDots)
}

def calculateResults(ocenyZawodnikow: List[Ocena]): List[(Int, String, String, Double)] = {
  ocenyZawodnikow
    .groupBy(ocena => (ocena.imie, ocena.nazwisko))
    .view
    .mapValues { oceny =>
      val sredniaWdzieku = oceny.map(_.wdziek).sum.toDouble / oceny.size
      val sredniaSprytu = oceny.map(_.spryt).sum.toDouble / oceny.size
      (sredniaWdzieku + sredniaSprytu, sredniaWdzieku, sredniaSprytu)
    }
    .toMap
    .toList
    .sortBy { case ((_, _), (suma, wdziek, _)) =>
      (suma, wdziek)
    }
    .zipWithIndex
    .map { case (((imie, nazwisko), (suma, _, _)), miejsce) =>
      (miejsce + 1, imie, nazwisko, suma)
    }
}
