import org.apache.pekko
import pekko.actor._

case class Execute(text: List[String])
case class Result(words: Map[String, Int])
case class Task(text: List[String])
case class Initialize(numberOfWorkers: Int)

class Worker extends Actor {
  def receive: Receive = { case Execute(text) =>
    val words =
      text.flatMap(_.toLowerCase.split("\\W+")).groupBy(identity).view.mapValues(_.size).toMap
    sender() ! Result(words)
  }
}

class Supervisor extends Actor {
  var workers: List[ActorRef] = List.empty
  var remainingTexts: List[String] = List.empty
  var totalWordCount: Int = 0 // Variable to accumulate word counts

  def receive: Receive = {
    case Initialize(numberOfWorkers) =>
      workers = (1 to numberOfWorkers).map(_ => context.actorOf(Props[Worker]())).toList
      println(s"Created $numberOfWorkers workers.")

    case Task(text) =>
      remainingTexts = text
      distributeTasks()

    case Result(words) =>
      remainingTexts.headOption match {
        case Some(text) =>
          println(s"Result for text: $text")
          println(s"Number of different words: ${words.size} \n")
          totalWordCount += words.size
          remainingTexts = remainingTexts.tail
          distributeTasks()
        case None =>
          println(s"Processing completed. Total word count: $totalWordCount")
      }
  }

  def distributeTasks(): Unit = {
    if (remainingTexts.nonEmpty) {
      val textToSend = remainingTexts.head
      val workerToSend = workers.head

      workerToSend ! Execute(textToSend.split("\\s+").toList)
      remainingTexts = remainingTexts.tail
    } else {
      context.system.terminate()
    }
  }
}

@main
def mainProg: Unit = {
  val system = ActorSystem("WordCounter")
  val supervisor = system.actorOf(Props[Supervisor](), "supervisor")

  supervisor ! Initialize(5)

  val textToProcess =
    scala.io.Source.fromResource("ogniem_i_mieczem.txt")(io.Codec.UTF8).getLines.toList
  supervisor ! Task(textToProcess)
}
