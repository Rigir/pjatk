"use strict";

document.addEventListener("DOMContentLoaded", function () {
  rangeSlider("slajder");
});

function rangeSlider(id) {
  var range = document.getElementById(id),
    dragger = range.firstElementChild,
    draggerWidth = 10, // width of your dragger
    down = false,
    rangeWidth,
    rangeLeft;

  dragger.style.width = draggerWidth + "px";
  dragger.style.left = -draggerWidth + "px";
  dragger.style.marginLeft = draggerWidth / 2 + "px";

  range.addEventListener("mousedown", function (e) {
    rangeWidth = this.offsetWidth;
    rangeLeft = this.offsetLeft;
    down = true;
    updateDragger(e);
    return false;
  });

  document.addEventListener("mousemove", function (e) {
    updateDragger(e);
  });

  document.addEventListener("mouseup", function () {
    down = false;
  });

  function updateDragger(e) {
    if (down && e.pageX >= rangeLeft && e.pageX <= rangeLeft + rangeWidth) {
      dragger.style.left = e.pageX - rangeLeft - draggerWidth + "px";
    }
  }
}
