"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const list = document.getElementsByClassName("egzamin")[0];
  addClickEvent(list);
});

function addClickEvent(parent) {
  if (parent != null) {
    const allChildern = Array.from(parent.children);
    allChildern.forEach((child) => {
      child.addEventListener("click", function (e) {
        if (!e.ctrlKey) {
          clearList(allChildern);
        }
        child.classList.add("selected");
      });
    });
  }
}

function clearList(children) {
  children.forEach((child) => {
    child.classList.remove("selected");
  });
}
