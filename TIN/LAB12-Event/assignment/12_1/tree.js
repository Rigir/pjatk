"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const tree = document.getElementById("drzewo");
  addPullDownEvent(tree);
});

function addPullDownEvent(parent) {
  if (parent != null && parent.firstElementChild != null) {
    Array.from(parent.children).forEach((child) => {
      addPullDownEvent(child);
    });
    if (parent.nodeName == "LI" && parent.firstElementChild.nodeName == "OL") {
      let span = document.createElement("span");
      span.appendChild(parent.firstChild);
      parent.prepend(span);
      parent.firstElementChild.onclick = () => {
        Array.from(parent.lastElementChild.children).forEach((child) => {
          if (child.style.display == "list-item" || child.style.display == "") {
            child.style.display = "none";
          } else {
            child.style.display = "list-item";
          }
        });
      };
    }
  }
}
