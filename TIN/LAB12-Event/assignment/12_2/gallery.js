"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const thumbs = document.querySelectorAll(".thumbs a");
  const showcase = document.querySelector(".large_img");
  displayInShowcase(thumbs, showcase);
});

function displayInShowcase(parent, showcase) {
  if (parent != null) {
    parent.forEach((a) => {
      a.onclick = (event) => {
        event.preventDefault();
        showcase.setAttribute("title", a.getAttribute("title"));
        showcase.setAttribute("src", event.target.getAttribute("src"));
      };
    });
  }
}
