"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const circle = document.getElementsByClassName("koło")[0];
  circle.style.transition = "width,height";
  circle.style.transitionDuration = "3s";
  centerElements(circle);

  const body = document.body;
  body.style.height = "100vh";
  centerElements(body);

  circle.onclick = () => {
    circle.style.width = "300px";
    circle.style.height = "300px";
  };

  circle.ontransitionend = (event) => {
    if (event.propertyName == "background-color") {
      circle.style.fontSize = "24px";
      circle.style.fontWeight = "bold";
      circle.style;
      circle.innerHTML = "Lasto beth lemmen";
    } else {
      circle.style.transition = "background 3s";
      circle.style.background = "yellow";
    }
  };
});

function centerElements(parent) {
  parent.style.display = "grid";
  parent.style.placeItems = "center";
}
