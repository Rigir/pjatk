"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const inputPesel = document.getElementById("peselField");
  inputPesel.oninput = () => {
    const pesel = inputPesel.value;
    if (isValidPeselChecksum(pesel)) {
      let decodePesel = peselDecode(pesel);
      if (decodePesel.birthday != false) {
        const inputBirthday = document.getElementById("birthdayField");
        inputBirthday.value = decodePesel.birthday
          .toISOString()
          .substring(0, 10);
      }
      const inputGender = document.getElementById("genderField");
      decodePesel.gender != null
        ? (inputGender.value = decodePesel.gender)
        : (inputGender.value = "other");
    }
  };
});

function show(form) {
  if (
    !form.validatePesel.checked ||
    isValidPesel(form.pesel.value, form.birthday.value, form.gender.value)
  ) {
    alert(`
      Imię:\t\t ${form.firstName.value}
      Nazwisko:\t ${form.lastName.value}
      PESEL:\t ${form.pesel.value}
      Płeć:\t\t ${form.gender.value}
      Data urodzenia:\t ${form.birthday.value}
    `);
  }
}

function peselDecode(pesel) {
  let year = parseInt(pesel.substring(0, 2), 10);
  let month = parseInt(pesel.substring(2, 4), 10) - 1;
  let day = parseInt(pesel.substring(4, 6), 10);

  if (month > 80) {
    year += 1800;
    month -= 80;
  } else if (month >= 60) {
    year += 2200;
    month -= 60;
  } else if (month >= 40) {
    year += 2100;
    month -= 40;
  } else if (month >= 20) {
    year += 2000;
    month -= 20;
  } else {
    year += 1900;
  }

  let birthday = false;
  if (month >= 0 && month < 12 && day > 0 && day < 32) {
    birthday = new Date();
    birthday.setFullYear(year, month, day);
  }

  let gender =
    parseInt(pesel.substring(9, 10), 10) % 2 === 1 ? "male" : "female";

  return { gender, birthday };
}

function isValidPesel(pesel, birthday, gender) {
  if (isValidPeselChecksum(pesel)) {
    let decodePesel = peselDecode(pesel);
    if (decodePesel.birthday.toISOString().substring(0, 10) !== birthday) {
      alert(`Error: Data urodzenia nie zgadza się z peselem`);
      return false;
    }
    if (decodePesel.gender !== gender && gender !== "other") {
      alert(`Error: Płeć nie zgadza się z peselem`);
      return false;
    }
    return true;
  }
  alert(`Error: Pesel niepoprawny, suma kontrolna się nie zgadza`);
  return false;
}

function isValidPeselChecksum(pesel) {
  let weight = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
  let sum = 0;
  for (let i = 0; i < weight.length; i++) {
    sum += parseInt(pesel.substring(i, i + 1)) * weight[i];
  }
  sum %= 10;
  return (10 - sum) % 10 === parseInt(pesel.substring(10, 11));
}
