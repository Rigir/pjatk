"use strict";

function sum(...args) {
  let sum = 0;
  args.forEach((element) => {
    sum += element;
  });
  return sum;
}

// console.log(sum(0, 1));
