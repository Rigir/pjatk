"use strict";

function chomik() {
  let storage = "";
  return function (str) {
    if (typeof str === "string" && str.trim() !== "")
      return (storage += ` ${str.trim()}`);
    return storage.trimStart();
  };
}

// var cricetus = chomik();
// cricetus("gruszka");
// cricetus(" gruszka");
// cricetus("jabłko");
// console.log(cricetus());
