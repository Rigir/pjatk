"use strict";

function addClassName(obj, className) {
  if (typeof className === "string" && className.trim() !== "") {
    if (!obj.className.split(" ").includes(className)) {
      obj.className += ` ${className}`;
      obj.className.trimStart();
    }
  }
}

// var obj = {
//   className: "first bordered",
// };

// addClassName(obj, "visible");
// console.log(obj.className);
// addClassName(obj, "visible");
// console.log(obj.className);
