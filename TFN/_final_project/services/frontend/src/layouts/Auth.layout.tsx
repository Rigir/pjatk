import { useOutlet } from "react-router-dom";
import { useAuthState } from "../utils/providers/AuthProvider";
import { RequireCheck } from "../utils/RequireCheck"

export const AuthLayout = () => {
    const outlet = useOutlet();
    const authState = useAuthState();

    return (
        <RequireCheck
            state={!authState.isAuthenticated}
            redirectTo={"/"}>
            {outlet}
        </RequireCheck>
    )
}