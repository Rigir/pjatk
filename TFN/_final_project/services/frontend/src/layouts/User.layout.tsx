import { useOutlet } from "react-router-dom";
import { RequireCheck } from "../utils/RequireCheck"
import { useAuthState } from "../utils/providers/AuthProvider";

export const UserLayout = () => {
    const outlet = useOutlet();
    const authState = useAuthState();

    return (
        <RequireCheck
            state={authState.isAuthenticated}
            redirectTo={"/auth/login"}>
            {outlet}
        </RequireCheck>
    );
};