import { useOutlet } from "react-router-dom";
import { RequireCheck } from "../utils/RequireCheck"
import { useAuthState } from "../utils/providers/AuthProvider";
import UsersProvider from "../utils/providers/UsersProvider";

export const AdminLayout = () => {
    const outlet = useOutlet();
    const authState = useAuthState();

    return (
        <RequireCheck
            state={authState.isAdmin}
            redirectTo={"/"}>
            <UsersProvider>
                {outlet}
            </UsersProvider >
        </RequireCheck>
    );
};