import axios from "axios";

import Routes from './routes'
import { API_URL } from "./utils/secrets";
import { authBroadcast } from "./utils/services/broadcast.service"

axios.defaults.baseURL = API_URL;
axios.defaults.withCredentials = true;

export default function App() {
  authBroadcast();
  return (
    <div className="App">
      <Routes />
    </div>
  )
}
