import { lazy, Suspense } from 'react'

import {
    Routes,
    Route,
} from "react-router-dom";

import { AuthLayout } from '../layouts/Auth.layout';
import { UserLayout } from '../layouts/User.layout';
import { AdminLayout } from '../layouts/Admin.layout';

const NoMatch = lazy(() => import('../pages/NoMatch'))
const Login = lazy(() => import('../pages/Login'))
const SignUp = lazy(() => import('../pages/SignUp'))

const Profile = lazy(() => import('../pages/Profile'))
const Dashboard = lazy(() => import('../pages/Dashboard'))

export default function index() {
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <Routes>
                <Route path="/auth" element={<AuthLayout />}>
                    <Route path="login" element={<Login />} />
                    <Route path="signup" element={<SignUp />} />
                </Route>
                <Route path="/" element={<UserLayout />}>
                    <Route index element={<Profile />} />
                </Route>
                <Route path="/root" element={<AdminLayout />}>
                    <Route path="dashboard" element={<Dashboard />} />
                </Route>
                <Route path="*" element={<NoMatch />} />
            </Routes >
        </Suspense>
    );
}
