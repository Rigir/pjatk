import { Formik, Form, Field } from 'formik';

import { useLayoutEffect, useState } from 'react';

import { IUserDetails } from '../../utils/interfaces/user.interface'
import { getDetails, setDetails } from '../../utils/services/user.service';
import { DetailsSchema } from '../../utils/schemes/details.schema'
import { userGenderType, userCreditCardType } from '../../utils/interfaces/user.interface';


export function UserDetailsForm() {
  const [initialFormValues, setInitialFormValues] = useState({
    email: '',
    firstname: '',
    lastname: '',
    gender: userGenderType.OTHER,
    credit_card_type: userCreditCardType.STARTER,
    credit_card_number: '',
  });

  const loadUserDetails = async () => {
    try {
      const { data } = await getDetails();
      setInitialFormValues({
        ...initialFormValues,
        email: data.data.email,
        firstname: data.data.firstname,
        lastname: data.data.lastname,
        gender: data.data.gender,
        credit_card_type: data.data.credit_card_type,
        credit_card_number: data.data.credit_card_number,
      })
    } catch (error: any) {
      console.log({ type: "error", message: error.message });
    }
  }

  useLayoutEffect(() => {
    loadUserDetails();
  }, [])

  const handleForm = async (values: IUserDetails) => {
    console.log(values)
    try {
      await setDetails(values);
    } catch (error: any) {
      console.log({ type: "error", message: error.message });
    }
  };

  return (
    <div>
      <Formik
        enableReinitialize
        initialValues={initialFormValues}
        validationSchema={DetailsSchema}
        onSubmit={values => handleForm(values)}
      >
        {({ errors, touched }) => (
          <Form>
            <div>
              <label htmlFor="email">Email</label>
              <Field name="email" type="email" />
              {errors.email && touched.email ? (
                <div>{errors.email}</div>
              ) : null}
            </div>
            <div>
              <label htmlFor="firstname">First Name</label>
              <Field name="firstname" />
              {errors.firstname && touched.firstname ? (
                <div>{errors.firstname}</div>
              ) : null}
            </div>
            <div>
              <label htmlFor="lastname">Last Name</label>
              <Field name="lastname" />
              {errors.lastname && touched.lastname ? (
                <div>{errors.lastname}</div>
              ) : null}
            </div>
            <div>
              <label htmlFor="gender">Gender</label>
              <Field as="select" name="gender">
                {Object.values(userGenderType).map((i: string) => (<option key={i} value={i}>{i}</option>))}
              </Field>
              {errors.gender && touched.gender ? (
                <div>{errors.gender}</div>
              ) : null}
            </div>
            <div>
              <label htmlFor="credit_card_type">Credit card type</label>
              <Field as="select" name="credit_card_type">
                {Object.values(userCreditCardType).map((i: string) => (<option key={i} value={i}>{i}</option>))}
              </Field>
              {errors.credit_card_type && touched.credit_card_type ? (
                <div>{errors.credit_card_type}</div>
              ) : null}
            </div>
            <div>
              <label htmlFor="credit_card_number">Credit Card Number</label>
              <Field name="credit_card_number" />
              {errors.credit_card_number && touched.credit_card_number ? (
                <div>{errors.credit_card_number}</div>
              ) : null}
            </div>
            <div>
              <button className='update' type="submit">Update</button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  )
}