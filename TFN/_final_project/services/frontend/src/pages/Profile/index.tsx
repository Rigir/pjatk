import { Link } from 'react-router-dom';
import { logout } from '../../utils/services/auth.service';
import { deleteMe } from '../../utils/services/user.service';
import { UserDetailsForm } from './UserDetailsForm';

export default function index() {
  const channel = new BroadcastChannel("auth");

  const handleLogout = async () => {
    try {
      const { data } = await logout();
      console.log({ type: "success", message: data.message });
      channel.postMessage("logout_success");
    } catch (error: any) {
      console.log({ type: "error", message: error.message });
    } finally {
      window.location.reload();
    }
  };

  const handleDeleteMe = async () => {
    try {
      const { data } = await deleteMe();
      console.log({ type: "success", message: data.message });
      channel.postMessage("logout_success");
    } catch (error: any) {
      console.log({ type: "error", message: error.message });
    } finally {
      window.location.reload();
    }
  };

  return (
    <div>
      <h2>
        Profile
      </h2>
      <div className='myForm'>
        <UserDetailsForm />
      </div>
      <hr />
      <div>
        <button className='warrning' onClick={handleDeleteMe}>
          <span>Delete Account</span>
        </button>
        <button onClick={handleLogout}>
          <span>Logout</span>
        </button>
      </div>
    </div>
  )
}