import { Formik, Form, Field } from 'formik';
import { useNavigate, Link } from "react-router-dom";

import { login } from '../../utils/services/auth.service';
import { LogInSchema } from '../../utils/schemes/auth.schema'
import { ILogInForm } from '../../utils/interfaces/auth.interface';

import { useAuthState, useAuthDispatch } from '../../utils/providers/AuthProvider';

export default function index() {
  const navigate = useNavigate();
  const authstate = useAuthState
  const authDispatch = useAuthDispatch();
  const initialFormValues: ILogInForm = {
    email: 'a@a.com',
    password: 'aA@45678',
  }

  const handleForm = async (values: ILogInForm) => {
    try {
      const { data } = await login(values);
      authDispatch({ ...authstate, isAuthenticated: true, isAdmin: data.data.isAdmin, currentUser: data.data.email });
      navigate("/", { replace: true });
    } catch (error: any) {
      console.log({ type: "error", message: error.message });
    }
  };

  return (
    <div>
      <h1>Login</h1>
      <div className='myForm'>
        <Formik
          initialValues={initialFormValues}
          validationSchema={LogInSchema}
          onSubmit={values => handleForm(values)}
        >
          {({ errors, touched }) => (
            <Form>
              <div>
                <label htmlFor="email">Email</label>
                <Field name="email" type="email" />
                {errors.email && touched.email ? (
                  <div>{errors.email}</div>
                ) : null}
              </div>
              <div>
                <label htmlFor="password">Password</label>
                <Field name="password" type="password" />
                {errors.password && touched.password ? (
                  <div>{errors.password}</div>
                ) : null}
              </div>
              <div>
                <button type="submit">Submit</button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <div>
        <Link to="/auth/signup">
          Not have an account?
        </Link>
      </div>
    </div>
  )
}