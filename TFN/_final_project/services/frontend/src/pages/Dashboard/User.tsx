import { useState } from 'react';
import { useUsersDispatch } from '../../utils/providers/UsersProvider';
import { userCreditCardType, userGenderType } from '../../utils/interfaces/user.interface';

export function User({ data }: any) {
    const [details, setDetails] = useState(data);
    const { handleDeleteUser, handleUpdateUser } = useUsersDispatch();

    const handleChange = (e: any) => {
        setDetails({ ...details, [e.target.name]: e.target.value });
    }

    return (
        <tr key={details.id}>
            <td>{details.id}</td>
            <td>
                <input type="email" name="email" value={details.email} onChange={handleChange} />
            </td>
            <td>
                <input type="text" name="firstname" value={details.firstname} onChange={handleChange} />
            </td>
            <td>
                <input type="text" name="lastname" value={details.lastname} onChange={handleChange} />
            </td>
            <td>
                <select name="gender" value={details.gender} onChange={handleChange}>
                    {Object.values(userGenderType).map((i: string) => (<option key={i} value={i}>{i}</option>))}
                </select>
            </td>
            <td>
                <select name="credit_card_type" value={details.credit_card_type} onChange={handleChange}>
                    {Object.values(userCreditCardType).map((i: string) => (<option key={i} value={i}>{i}</option>))}
                </select>
            </td>
            <td>
                <input type="text" name="credit_card_number" value={details.credit_card_number} onChange={handleChange} />
            </td>
            <td>
                <button className="update" onClick={() => handleUpdateUser(details)}>
                    <span>Update</span>
                </button>
            </td>
            <td>
                <button className='warrning' onClick={() => handleDeleteUser(data.id)}>
                    <span>Delete</span>
                </button>
            </td>
        </tr>
    );
}
