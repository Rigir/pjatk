import { CSVLink } from "react-csv";

import { AddUser } from "./AddUser"
import { useUsersState } from "../../utils/providers/UsersProvider";
import { SearchBar } from './SearchBar'

export default function index() {
  const userState = useUsersState();

  return (
    <div>
      <h1>Dashboard</h1>
      <CSVLink
        data={userState}
        filename={"users_db.csv"}
        target="_blank"
      >
        Download to CSV
      </CSVLink>
      <AddUser />
      <SearchBar />
    </div >
  )
}