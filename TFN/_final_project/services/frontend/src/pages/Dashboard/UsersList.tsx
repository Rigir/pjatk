import { IUserShowcase } from '../../utils/interfaces/root.interface';
import { User } from './User';

export function UsersList({ users }: any) {
    return (<div>
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Email</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Gender</th>
                    <th>Credit_card_type</th>
                    <th>Credit_card_number</th>
                </tr>
            </thead>
            <tbody>
                {users.map((user: IUserShowcase) => (
                    <User key={user.id} data={user} />
                ))}
            </tbody>
        </table>
    </div>

    )
}