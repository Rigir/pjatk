import { useMemo, useRef, useState } from "react";
import { useUsersState } from '../../utils/providers/UsersProvider';
import { IUserShowcase } from "../../utils/interfaces/root.interface";
import { UsersList } from "./UsersList";

/**  
 THIS IS NOT HOW YOU SUPPOSED TO DO THAT.
 I JUST NEED TO USE USEREF AND USEMEMO AND IM OUT OF IDEAS 
*/
export function SearchBar() {
    const usersState = useUsersState();
    const [searchValue, setSearchValue] = useState("");
    const searchInput = useRef<any>("");
    const displayedUseres = useMemo(() =>
        usersState.filter((user: IUserShowcase) => {
            return user.email.toLocaleLowerCase().includes(searchValue);
        }), [searchValue])

    const onSearchClick = () => {
        setSearchValue(searchInput.current?.value.toLowerCase())
    }

    return (
        <div className="searchBarContainer">
            <div className="searchBar">
                <input type="text" onChange={onSearchClick} ref={searchInput} placeholder="Search by email..." />
            </div>
            <UsersList users={displayedUseres} />
        </div>
    )
}