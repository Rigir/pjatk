import { userCreditCardType, userGenderType } from '../../utils/interfaces/user.interface';
import { Field, Form, Formik } from 'formik';
import { ISingUpForm } from '../../utils/interfaces/auth.interface';
import { signup } from '../../utils/services/auth.service';


import { SignUpSchema } from '../../utils/schemes/auth.schema';
import { useUsersDispatch } from '../../utils/providers/UsersProvider';


export function AddUser() {
    const { handleAddUser } = useUsersDispatch()
    const initialFormValues: ISingUpForm = {
        email: 'f@f.com',
        password: 'aA@45678',
        password_confirm: 'aA@45678',
        firstname: 'a',
        lastname: 'a',
        gender: userGenderType.OTHER,
        credit_card_type: userCreditCardType.STARTER,
        credit_card_number: '1234567891123456',
    }

    const handleForm = async (values: ISingUpForm) => {
        try {
            await handleAddUser(values);
        } catch (error: any) {
            console.log({ type: "error", message: error.message });
        }
    };

    return (
        <div>
            <div className='addUserForm'>
                <Formik
                    initialValues={initialFormValues}
                    validationSchema={SignUpSchema}
                    onSubmit={values => handleForm(values)}
                >
                    {({ errors, touched }) => (
                        <Form>

                            <div>
                                <label htmlFor="email">Email</label>
                                <Field name="email" type="email" />
                                {errors.email && touched.email ? (
                                    <div>{errors.email}</div>
                                ) : null}
                            </div>
                            <div>
                                <label htmlFor="password">Password</label>
                                <Field name="password" type="password" />
                                {errors.password && touched.password ? (
                                    <div>{errors.password}</div>
                                ) : null}
                            </div>
                            <div>
                                <label htmlFor="password_confirm">Confirm Password</label>
                                <Field name="password_confirm" type="password" />
                                {errors.password_confirm && touched.password_confirm ? (
                                    <div>{errors.password_confirm}</div>
                                ) : null}
                            </div>

                            <div>
                                <label htmlFor="firstname">First Name</label>
                                <Field name="firstname" />
                                {errors.firstname && touched.firstname ? (
                                    <div>{errors.firstname}</div>
                                ) : null}
                            </div>
                            <div>
                                <label htmlFor="lastname">Last Name</label>
                                <Field name="lastname" />
                                {errors.lastname && touched.lastname ? (
                                    <div>{errors.lastname}</div>
                                ) : null}
                            </div>
                            <div>
                                <label htmlFor="gender">Gender</label>
                                <Field as="select" name="gender">
                                    {Object.values(userGenderType).map((i: string) => (<option key={i} value={i}>{i}</option>))}
                                </Field>
                                {errors.gender && touched.gender ? (
                                    <div>{errors.gender}</div>
                                ) : null}
                            </div>
                            <div>
                                <label htmlFor="credit_card_type">Credit card type</label>
                                <Field as="select" name="credit_card_type">
                                    {Object.values(userCreditCardType).map((i: string) => (<option key={i} value={i}>{i}</option>))}
                                </Field>
                                {errors.credit_card_type && touched.credit_card_type ? (
                                    <div>{errors.credit_card_type}</div>
                                ) : null}
                            </div>
                            <div>
                                <label htmlFor="credit_card_number">Credit Card Number</label>
                                <Field name="credit_card_number" />
                                {errors.credit_card_number && touched.credit_card_number ? (
                                    <div>{errors.credit_card_number}</div>
                                ) : null}
                            </div>
                            <div>
                                <button className='confirm' type="submit">Add user</button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>);
}
