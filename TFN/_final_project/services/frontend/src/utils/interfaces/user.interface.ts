
export enum userGenderType {
    MALE = 'male',
    FEMALE = 'female',
    OTHER = 'other'
}

export enum userCreditCardType {
    REWARDS = 'rewards',
    TRAVEL = 'travel',
    CASH = 'cash',
    STARTER = 'starter',
    BUSINESS = 'business',
    CO_BRANDED = 'co-branded'
}

interface IUserShared {
    email: string;
}

export interface IUserCredentials extends IUserShared {
    password: string;
}

export interface IUserDetails extends IUserShared {
    firstname: string;
    lastname: string;
    gender: userGenderType;
    credit_card_type: userCreditCardType;
    credit_card_number: string;
}

export interface IUserData extends IUserCredentials, IUserDetails { }
