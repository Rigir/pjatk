import { IUserData, IUserCredentials } from "./user.interface";

export interface ILogInForm extends IUserCredentials { }

export interface ISingUpForm extends IUserData {
    password_confirm: string;
}


