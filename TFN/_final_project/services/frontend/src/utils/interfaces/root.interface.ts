import { IUserData } from "./user.interface";


export interface IUserShowcase extends IUserData {
    id: number;
    isAdmin: boolean;
}

export interface IUserList {
    data: IUserShowcase[]
}

