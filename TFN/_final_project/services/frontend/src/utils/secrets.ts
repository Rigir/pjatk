import * as _ from "lodash";

export const ENVIRONMENT = _.defaultTo(import.meta.env.MODE, "development");
export const IS_PRODUCTION = ENVIRONMENT === "production";

export const API_URL = _.defaultTo(import.meta.env.API_DOMAIN, "http://localhost:9000");
