import { Navigate } from "react-router-dom";

export const RequireCheck = ({ children, state, redirectTo }: any) => {
    return state && children !== null ? children : <Navigate to={redirectTo} />;
}