import { createContext, useContext, useEffect, useState } from "react";
import { deleteUser, getUsers, updateUser } from "../services/root.service";
import { IUserDetails } from "../interfaces/user.interface";
import { ISingUpForm } from "../interfaces/auth.interface";
import { signup } from "../services/auth.service";

interface IUserHandlers {
    handleDeleteUser: ((id: number) => Promise<void>);
    handleUpdateUser: ((user: IUserDetails) => Promise<void>);
    handleAddUser: ((form: ISingUpForm) => Promise<void>);
}

const initialState: any = [];

const UsersStateContext = createContext(initialState);
const UsersDispatchContext = createContext({} as IUserHandlers);

export default function UsersProvider({ children }: any) {
    const [loading, setLoading] = useState(true);
    const [users, setUsers] = useState(initialState);

    const loadUserDetails = async () => {
        setLoading(true);
        try {
            const { data } = await getUsers();
            setUsers(data.data);
        } catch (err: any) {
            console.log({ type: "error", message: err.response.data.message });
        } finally {
            setLoading(false);
        }
    };

    const handleAddUser = async (form: ISingUpForm) => {
        try {
            await signup(form);
            loadUserDetails();
        } catch (error: any) {
            console.log({ type: "error", message: error.message });
        }
    };


    const handleUpdateUser = async (user: IUserDetails) => {
        try {
            await updateUser(user);
            loadUserDetails();
        } catch (error: any) {
            console.log({ type: "error", message: error.message });
        }
    };


    const handleDeleteUser = async (id: number) => {
        try {
            await deleteUser(id);
            loadUserDetails();
        } catch (error: any) {
            console.log({ type: "error", message: error.message });
        }
    };


    useEffect(() => {
        loadUserDetails();
    }, []);

    return (
        <UsersStateContext.Provider value={users}>
            <UsersDispatchContext.Provider value={{ handleDeleteUser, handleUpdateUser, handleAddUser }}>
                {!loading ? children : ''}
            </UsersDispatchContext.Provider>
        </UsersStateContext.Provider >
    )
}

export function useUsersState() {
    const context = useContext(UsersStateContext);
    if (!context) throw new Error("useUsersState must be used in UsersProvider");

    return context;
}

export function useUsersDispatch() {
    const context = useContext(UsersDispatchContext);
    if (!context) throw new Error("useUsersDispatch must be used in UsersProvider");

    return context;
}
