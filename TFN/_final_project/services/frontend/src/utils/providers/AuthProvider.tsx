import { type Dispatch, createContext, useContext, useEffect, useReducer, useState, Suspense } from "react";
import { fetchCurrentUser } from "../services/auth.service";

const initialState = {
    currentUser: '',
    isLoading: true,
    isAuthenticated: false,
    isAdmin: false
};

const AuthStateContext = createContext(initialState);
const AuthDispatchContext = createContext((() => undefined) as Dispatch<any>);

interface IAuthState {
    currentUser: string,
    isLoading: boolean,
    isAuthenticated: boolean,
    isAdmin: boolean,
}

function reducer(currentState: IAuthState, newState: IAuthState) {
    return { ...currentState, ...newState };
}

export default function AuthProvider({ children }: any) {
    const [loading, setLoading] = useState(true);
    const [state, dispatch] = useReducer(reducer, initialState);

    const loadCurrentUser = async () => {
        setLoading(true);
        try {
            const { data } = await fetchCurrentUser();
            console.log(data);
            dispatch({ ...state, isAuthenticated: true, isAdmin: data.data.isAdmin, currentUser: data.data.email });
        } catch (err: any) {
            console.log({ type: "error", message: err.message });
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        loadCurrentUser();
    }, []);

    return (
        <AuthStateContext.Provider value={state}>
            <AuthDispatchContext.Provider value={dispatch}>
                {!loading ? children : ''}
            </AuthDispatchContext.Provider>
        </AuthStateContext.Provider >
    )
}

export function useAuthState() {
    const context = useContext(AuthStateContext);
    if (!context) throw new Error("useAuthState must be used in AuthProvider");

    return context;
}

export function useAuthDispatch() {
    const context = useContext(AuthDispatchContext);
    if (!context) throw new Error("useAuthDispatch must be used in AuthProvider");

    return context;
}
