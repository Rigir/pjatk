import { string, object, mixed, SchemaOf, ref } from 'yup';
import { ILogInForm, ISingUpForm } from '../interfaces/auth.interface';
import { userCreditCardType, userGenderType } from '../interfaces/user.interface';

export const LogInSchema: SchemaOf<ILogInForm> = object({
    email: string()
        .email('Invalid email')
        .required('Required'),
    password: string()
        .min(8, 'Password must be 8 characters long!')
        .matches(/[0-9]/, 'Password requires a number')
        .matches(/[a-z]/, 'Password requires a lowercase letter')
        .matches(/[A-Z]/, 'Password requires an uppercase letter')
        .matches(/[^\w]/, 'Password requires a symbol')
        .required('Required'),
}).defined();

export const SignUpSchema: SchemaOf<ISingUpForm> = object({
    email: string()
        .email('Invalid email')
        .required('Required'),
    password: string()
        .min(8, 'Password must be 8 characters long!')
        .matches(/[0-9]/, 'Password requires a number')
        .matches(/[a-z]/, 'Password requires a lowercase letter')
        .matches(/[A-Z]/, 'Password requires an uppercase letter')
        .matches(/[^\w]/, 'Password requires a symbol')
        .required('Required'),
    password_confirm: string()
        .oneOf([ref('password'), null], 'Passwords must match')
        .required('Required'),
    firstname: string()
        .min(0, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastname: string()
        .min(0, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    gender: mixed()
        .oneOf(Object.values(userGenderType)),
    credit_card_type: mixed()
        .oneOf(Object.values(userCreditCardType))
        .required('Required'),
    credit_card_number: string()
        .min(16, 'Credit card number must be 16 characters long!')
        .max(19, 'Too Long!')
        .required('Required'),
}).defined();