import { string, object, mixed, SchemaOf, ref } from 'yup';
import { IUserDetails } from '../interfaces/user.interface';
import { userCreditCardType, userGenderType } from '../interfaces/user.interface';

export const DetailsSchema: SchemaOf<IUserDetails> = object({
    email: string()
        .email('Invalid email')
        .required('Required'),
    firstname: string()
        .min(0, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastname: string()
        .min(0, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    gender: mixed()
        .oneOf(Object.values(userGenderType)),
    credit_card_type: mixed()
        .oneOf(Object.values(userCreditCardType))
        .required('Required'),
    credit_card_number: string()
        .min(16, 'Credit card number must be 16 characters long!')
        .max(19, 'Too Long!')
        .required('Required'),
}).defined();