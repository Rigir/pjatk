import axios from "axios";
import { IUserDetails } from "../interfaces/user.interface";

export async function deleteMe() {
    return await axios.delete("/user/me");
}

export async function getDetails() {
    return await axios.get("/user/details");
}

export async function setDetails(form: IUserDetails) {
    return await axios.patch("/user/details", form);
}
