import axios from "axios";
import { ILogInForm, ISingUpForm } from "../interfaces/auth.interface";

export async function fetchCurrentUser() {
    return await axios.get("/auth/me");
}

export async function signup(form: ISingUpForm) {
    return await axios.post("/auth/signup", form);
}

export async function login(form: ILogInForm) {
    return await axios.post("/auth/login", form);
}

export async function logout() {
    return await axios.patch("/auth/logout");
}