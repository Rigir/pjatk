import axios from "axios";
import { IUserData, IUserDetails } from "../interfaces/user.interface";

export async function getUsers() {
    return await axios.get("/root/users");
}

export async function deleteUser(id: number) {
    return await axios.post("/root/users", { id: id });
}

export async function updateUser(user: IUserDetails) {
    return await axios.patch("/root/users", user);
}

