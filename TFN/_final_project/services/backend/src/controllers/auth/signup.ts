import { Request, Response, NextFunction } from 'express';
import { validationResult } from "express-validator";

import { hashPassword } from "../../utils/password";
import { pool } from "../../utils/db";

export const signup = async (req: Request, res: Response, next: NextFunction) => {
  // fields validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next({
      status: 422,
      message: "User input error",
      data: errors.mapped(),
    });
  }

  let { email, password, firstname, lastname, gender, credit_card_type, credit_card_number } = req.body;

  try {
    // check duplicate email
    const [[currentUser]]: any = await pool.execute(
      "SELECT email FROM users WHERE email = ?",
      [email]
    );

    if (currentUser !== undefined) {
      return next({ status: 400, message: "Email address already exists" });
    }

    // hash password
    password = await hashPassword(password);

    // create new user
    pool.execute('INSERT INTO `users` (`email`, `password`, `firstname`, `lastname`, `gender`, `credit_card_type`, `credit_card_number`) VALUES (?,?,?,?,?,?,?)',
      [email, password, firstname, lastname, gender, credit_card_type, credit_card_number]);

    return res.status(201).json({ type: "success" });
  } catch (error) {
    next(error);
  }
};
