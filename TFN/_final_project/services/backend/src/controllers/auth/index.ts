const { fetchCurrentUser } = require("./fetchCurrentUser");
const { login } = require("./login");
const { logout } = require("./logout");
const { signup } = require("./signup");

export {
  login,
  signup,
  logout,
  fetchCurrentUser,
};
