import { Request, Response, NextFunction } from 'express';

export const fetchCurrentUser = async (req: Request, res: Response, next: NextFunction) => {
  return res.status(200).json({
    type: "success",
    message: "Fetch current user",
    data: {
      id: req.session.user?.id,
      email: req.session.user?.email,
      isAdmin: req.session.user?.isAdmin
    },
  });
};