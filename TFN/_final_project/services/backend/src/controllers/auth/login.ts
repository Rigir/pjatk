import { Request, Response, NextFunction } from 'express';
const { validationResult } = require("express-validator");

import { pool } from "../../utils/db";
import { checkPassword } from "../../utils/password";

export const login = async (req: Request, res: Response, next: NextFunction) => {
  // return api fields validation errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next({
      status: 422,
      message: "User input error",
      data: errors.mapped(),
    });
  }

  const { email, password } = req.body;
  try {
    const [[currentUser]]: any = await pool.execute(
      "SELECT id, email, password, isAdmin FROM users WHERE email = ?",
      [email]
    );

    // verify email
    if (currentUser === undefined) {
      return next({ status: 400, message: "Incorrect email address" });
    }

    // verify password
    const matchPassword = await checkPassword(password, currentUser.password);
    if (!matchPassword) {
      return next({ status: 400, message: "Incorrect password" });
    }
    delete currentUser.password;
    req.session.user = {
      id: currentUser.id,
      email: currentUser.email,
      isAdmin: currentUser.isAdmin
    }

    res.status(200).json({
      type: "success",
      message: "You have loggedin successfully",
      data: {
        id: currentUser.id,
        email: currentUser.email,
        isAdmin: currentUser.isAdmin
      },
    });
  } catch (error) {
    next(error);
  }
};
