import { Request, Response, NextFunction } from 'express';

export const logout = async (req: Request, res: Response, next: NextFunction) => {
  try {
    res.clearCookie("tfn_pid", { path: '/' })
    req.session.destroy;

    return res.status(200).json({
      type: "success",
      message: "You have logout successfully",
      data: null,
    });
  } catch (error) {
    next(error);
  }
};
