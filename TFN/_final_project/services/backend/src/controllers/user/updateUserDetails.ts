import { Request, Response, NextFunction } from 'express';
import { validationResult } from "express-validator";
import { pool } from "../../utils/db";

export const updateUserDetails = async (req: Request, res: Response, next: NextFunction) => {
    // fields validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next({
            status: 422,
            message: "User input error",
            data: errors.mapped(),
        });
    }

    let { email, firstname, lastname, gender, credit_card_type, credit_card_number } = req.body;

    try {
        // Update data
        pool.execute("UPDATE users SET email=?, firstname=?, lastname=?, gender=?, credit_card_type=?, credit_card_number=? WHERE id = ?",
            [email, firstname, lastname, gender, credit_card_type, credit_card_number, req.session.user?.id])

        return res.status(201).json({ type: "success" });
    } catch (error) {
        next(error);
    }
};
