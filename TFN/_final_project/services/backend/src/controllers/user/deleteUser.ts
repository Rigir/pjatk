import { Request, Response, NextFunction } from 'express';
import { pool } from "../../utils/db";

export const deleteUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await pool.execute(
      "DELETE FROM users WHERE id=?",
      [req.session.user?.id]
    );

    res.clearCookie("tfn_pid", { path: '/' })
    req.session.destroy;

    return res.status(200).json({
      type: "success",
      message: "Account deleted successfully",
      data: null,
    });
  } catch (error) {
    next(error);
  }
};
