import { Request, Response, NextFunction } from 'express';
import { pool } from "../../utils/db";

export const fetchUserDetails = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const [[currentUser]]: any = await pool.execute(
      "SELECT * FROM users WHERE id = ?",
      [req.session.user?.id]
    );

    delete currentUser.id;
    delete currentUser.isAdmin;
    delete currentUser.password;

    return res.status(200).json({
      type: "success",
      message: "Fetch user details",
      data: {
        ...currentUser
      },
    });
  } catch (error) {
    next(error);
  }
};
