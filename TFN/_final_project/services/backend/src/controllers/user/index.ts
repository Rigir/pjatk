const { fetchUserDetails } = require("./fetchUserDetails");
const { deleteUser } = require("./deleteUser");
const { updateUserDetails } = require("./updateUserDetails");

export {
  deleteUser,
  fetchUserDetails,
  updateUserDetails,
};
