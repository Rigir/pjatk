const { fetchUsers } = require("./fetchUsers");
const { deleteUser } = require("./deleteUser");
const { updateUser } = require("./updateUser");

export {
  fetchUsers,
  deleteUser,
  updateUser
};
