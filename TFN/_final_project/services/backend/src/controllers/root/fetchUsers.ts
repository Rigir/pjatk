import { Request, Response, NextFunction } from 'express';
import { pool } from "../../utils/db";

export const fetchUsers = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const [allUsers]: any = await pool.execute("SELECT * FROM users WHERE id!=?", [req.session.user?.id]);
    return res.status(200).json({
      type: "success",
      message: "Fetch users",
      data: [
        ...allUsers
      ],
    });
  } catch (error) {
    next(error);
  }
};
