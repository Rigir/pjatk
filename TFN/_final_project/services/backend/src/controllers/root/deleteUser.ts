import { Request, Response, NextFunction } from 'express';
import { pool } from "../../utils/db";
import { validationResult } from 'express-validator';

export const deleteUser = async (req: Request, res: Response, next: NextFunction) => {
  console.log(req.body);
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next({
      status: 422,
      message: "User input error",
      data: errors.mapped(),
    });
  }

  const { id } = req.body;

  try {
    await pool.execute(
      "DELETE FROM users WHERE id=?",
      [id]
    );

    return res.status(200).json({
      type: "success",
      message: "Account deleted successfully",
      data: null,
    });
  } catch (error) {
    next(error);
  }
};
