import { Request, Response, NextFunction } from 'express';

export const checkAuth = async (req: Request, res: Response, next: NextFunction) => {
    if (req.session.user?.id) next();
    else next({
        status: 404,
        message: "Session is missing",
    });
};

export const checkAuthRoot = async (req: Request, res: Response, next: NextFunction) => {
    if (req.session.user?.isAdmin) next();
    else next({
        status: 404,
        message: "Session is missing",
    });
};