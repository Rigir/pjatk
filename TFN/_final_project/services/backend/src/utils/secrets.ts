import * as _ from "lodash";

export const ENVIRONMENT = _.defaultTo(process.env.APP_ENV, "development");
export const IS_PRODUCTION = ENVIRONMENT === "production";

export const ORIGIN = _.defaultTo(process.env.ORIGIN, `["http://localhost:3000"]`);
export const SESSION_SECRET = _.defaultTo(process.env.SESSION_SECRET, "itssecret");
export const APP_PORT = _.defaultTo(process.env.APP_PORT, 3000);
export const DB = {
    DATABASE: _.defaultTo(process.env.DB_DATABASE, "db"),
    HOST: _.defaultTo(process.env.DB_HOST, "localhost"),
    PORT: _.defaultTo(Number(String(process.env.DB_PORT)), 27017),
    USER: _.defaultTo(process.env.DB_USER, "root"),
    PASSWORD: _.defaultTo(process.env.DB_USER_PWD, "secret"),
}