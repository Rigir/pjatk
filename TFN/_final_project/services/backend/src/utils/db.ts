import mysql from "mysql2/promise";

import { DB } from "./secrets";

export const pool = mysql.createPool({
    host: DB.HOST,
    user: DB.USER,
    password: DB.PASSWORD,
    database: DB.DATABASE,
    port: DB.PORT,
    waitForConnections: true
});
