import bcrypt from "bcrypt";

// match plain password and hashed password
export const checkPassword = async (password: string, hashedPassword: string) => {
  const matchPassword = await bcrypt.compare(password, hashedPassword);
  return matchPassword;
};

// hash plain password into hashed password
export const hashPassword = async (password: string) => {
  const hashed = await bcrypt.hash(password, 12);
  return hashed;
};