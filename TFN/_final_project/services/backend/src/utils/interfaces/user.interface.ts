export interface IUserSession {
    id: string;
    email: string;
    isAdmin: boolean;
}
