import { Router } from 'express';
import { body } from 'express-validator';

const { checkAuth } = require("../utils/middlewares/auth.middleware");
const { deleteUser, fetchUserDetails, updateUserDetails } = require("../controllers/user");
const router: Router = Router();

const detailsValidation = [
    body("email").isString()
        .not().isEmpty().withMessage("Email address must be required")
        .isEmail().withMessage("Incorrect email address"),
    body("firstname").isString().not().isEmpty(),
    body("lastname").isString().not().isEmpty(),
    body("gender").isString().not().isEmpty(),
    body("credit_card_type").isString().not().isEmpty(),
    body("credit_card_number").isString().not().isEmpty()
];

router.get("/details", checkAuth, fetchUserDetails);
router.patch("/details", checkAuth, detailsValidation, updateUserDetails);
router.delete("/me", checkAuth, deleteUser);

export = router;
