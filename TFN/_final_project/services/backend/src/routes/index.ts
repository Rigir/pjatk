import { Express } from 'express';

import authRoutes from "./auth.route";
import userRoutes from "./user.route";
import rootRoutes from "./root.route";

export const registerRoutes = (app: Express) => {
  app.use("/auth", authRoutes);
  app.use("/user", userRoutes);
  app.use("/root", rootRoutes);
};
