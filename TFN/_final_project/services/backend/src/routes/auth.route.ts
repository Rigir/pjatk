import { Router } from 'express';
import { body } from 'express-validator';

const router: Router = Router();

const { checkAuth } = require("../utils/middlewares/auth.middleware");
const { login, logout, signup, fetchCurrentUser } = require("../controllers/auth");

const loginValidation = [
    body("email").isString()
        .not().isEmpty().withMessage("Email address must be required")
        .isEmail().withMessage("Incorrect email address"),
    body("password").isString()
        .not().isEmpty().withMessage("Password must be required"),
];

const signupValidation = [
    body("email").isString()
        .not().isEmpty().withMessage("Email address must be required")
        .isEmail().withMessage("Incorrect email address"),
    body("password").isString()
        .not().isEmpty().withMessage("Password must be required"),
    body("password_confirm").isString(),
    body("firstname").isString().not().isEmpty(),
    body("lastname").isString().not().isEmpty(),
    body("gender").isString().not().isEmpty(),
    body("credit_card_type").isString().not().isEmpty(),
    body("credit_card_number").isString().not().isEmpty()
];

router.get("/me", checkAuth, fetchCurrentUser);
router.post("/login", loginValidation, login);
router.post("/signup", signupValidation, signup);
router.patch("/logout", checkAuth, logout);

export = router;