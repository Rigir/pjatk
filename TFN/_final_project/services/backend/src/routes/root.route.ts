import { Router } from 'express';
import { body } from 'express-validator';

const { checkAuthRoot } = require("../utils/middlewares/auth.middleware");
const { fetchUsers, deleteUser, updateUser } = require("../controllers/root");
const router: Router = Router();

const userValidation = [
    body("id").isNumeric().not().isEmpty(),
    body("email").isString()
        .not().isEmpty().withMessage("Email address must be required")
        .isEmail().withMessage("Incorrect email address"),
    body("firstname").isString().not().isEmpty(),
    body("lastname").isString().not().isEmpty(),
    body("gender").isString().not().isEmpty(),
    body("credit_card_type").isString().not().isEmpty(),
    body("credit_card_number").isString().not().isEmpty()
];

router.get("/users", checkAuthRoot, fetchUsers);
router.post("/users", checkAuthRoot, deleteUser);
router.patch("/users", checkAuthRoot, userValidation, updateUser);

export = router;
