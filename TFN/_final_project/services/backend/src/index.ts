import express, { Express, Request, Response } from 'express';
import cors from 'cors';

import session from "express-session";
const MySQLStore = require('express-mysql-session')(session);
import { pool } from './utils/db';

import { registerRoutes } from './routes';
import { notFoundErrorHandler, globalErrorHandler } from './utils/middlewares/error.middleware';
import { IS_PRODUCTION, APP_PORT, ORIGIN, SESSION_SECRET } from "./utils/secrets";
import { IUserSession } from './utils/interfaces/user.interface';

const app: Express = express();


// logging in development environment
if (!IS_PRODUCTION) {
    const morgan = require("morgan");
    app.use(morgan("dev"));
}

// allow cors for frontend to access api routes
app.use(cors({
    origin: JSON.parse(ORIGIN),
    credentials: true,
}));

// config session
declare module "express-session" {
    interface SessionData {
        user: IUserSession;
    }
}

app.use(session({
    name: "tfn_pid",
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore({}, pool),
    cookie: {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
        secure: IS_PRODUCTION ? true : "auto",
        sameSite: IS_PRODUCTION ? "strict" : "none",
    }
}));

// parse incomming request into json
app.use(express.json());

// server health check end point
app.get('/', (req: Request, res: Response) => {
    res.status(200).json({
        type: "success",
        message: "server is up and running",
        data: null,
    });
});

// Other routers
registerRoutes(app);

// Error handling
app.use("*", notFoundErrorHandler);
app.use(globalErrorHandler);

app.listen(APP_PORT, () => {
    console.log(`⚡️[server]: Listening on port: ${APP_PORT}`);
});