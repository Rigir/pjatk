USE TFN;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    gender ENUM('male', 'female', 'other') NOT NULL,
    credit_card_type ENUM('rewards', 'travel', 'cash', 'starter', 'business', 'co-branded') NOT NULL,
    credit_card_number VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    isAdmin BOOLEAN DEFAULT false,
    PRIMARY KEY (id)
);