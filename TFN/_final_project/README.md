👤Person CRUD
=========

🛠 Main features:
---------
- Admin and user dashboard
- User login and registration system with two side validation
- Other requirement's are specified in mics/Punktacja_Crud_Osoba.pdf 

🛠 Technology stack:
---------
- Back-end:
  - express
  - express-session
  - dotenv
  - mysql2
- Front-end:
  - React
  - SCSS
  - Vite
  - Axios Formik, yup
- Other:
  - Docker
  - Typescript

📄 License:
---------
PJAIT © All Rights Reserved 
