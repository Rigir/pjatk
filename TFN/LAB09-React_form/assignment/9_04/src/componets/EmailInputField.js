import React from 'react';

export default function EmailInputField({
  handleValidation,
  handleInputChange,
  fieldValue,
  fieldError,
}) {
  return (
    <>
      <div className="form-group my-3">
        <input
          type="email"
          value={fieldValue}
          onChange={handleInputChange}
          onKeyUp={handleValidation}
          name="email"
          placeholder="Email"
          className="form-control"
        />
        <p className="text-danger">{fieldError}</p>
      </div>
    </>
  );
}
