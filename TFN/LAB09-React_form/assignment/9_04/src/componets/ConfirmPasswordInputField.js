import React from 'react';

export default function ConfirmPasswordInputField({
  handleValidation,
  handleInputChange,
  fieldValue,
  fieldError,
}) {
  return (
    <>
      <div className="form-group my-3">
        <input
          type="password"
          value={fieldValue}
          onChange={handleInputChange}
          onKeyUp={handleValidation}
          name="confirmPassword"
          placeholder="ConfirmPassword"
          className="form-control"
        />
        <p className="text-danger">{fieldError}</p>
      </div>
    </>
  );
}
