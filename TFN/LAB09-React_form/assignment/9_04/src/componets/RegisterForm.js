import React from 'react';
import { useState } from 'react/cjs/react.development';
import PasswordInputField from './PasswordInputField';
import ConfirmPasswordInputField from './ConfirmPasswordInputField';
import EmailInputField from './EmailInputField';
import LoginForm from './LoginForm';
import secret from '../data/secret.json';

export default function RegisterForm() {
  const [display, setDisplay] = useState(false);
  const [emailError, setEmailErr] = useState('');
  const [passwordError, setPasswordErr] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [formInput, setFormInput] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });

  const clearData = (e) => {
    setFormInput({
      email: '',
      password: '',
      confirmPassword: '',
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      emailError === '' &&
      passwordError === '' &&
      confirmPasswordError === ''
    ) {
      if (secret.find((user) => user.username !== formInput['email'])) {
        //nie da się po stronie clienta zapisać do pliku zadanie nie jest do wykonania ale załużmy, że tutaj wysyła się request do zapisu
        setDisplay(true);
      }
    }
  };

  const handleInputChange = (e) => {
    const inputValue = e.target.value.trim();
    const inputFieldName = e.target.name;
    const newInput = {
      ...formInput,
      [inputFieldName]: inputValue,
    };
    setFormInput(newInput);
  };

  const handleValidation = (e) => {
    const inputValue = e.target.value.trim();
    const inputFieldName = e.target.name;

    if (inputFieldName === 'password') {
      const uppercaseRegExp = /(?=.*?[A-Z])/;
      const lowercaseRegExp = /(?=.*?[a-z])/;
      const digitsRegExp = /(?=.*?[0-9])/;
      const specialCharRegExp = /(?=.*?[#?!@$%^&*-])/;
      const minLengthRegExp = /.{8,}/;
      let errMsg = '';

      if (inputValue.length === 0) {
        errMsg = 'Password is empty';
      } else if (!uppercaseRegExp.test(inputValue)) {
        errMsg = 'At least one Uppercase';
      } else if (!lowercaseRegExp.test(inputValue)) {
        errMsg = 'At least one Lowercase';
      } else if (!digitsRegExp.test(inputValue)) {
        errMsg = 'At least one digit';
      } else if (!specialCharRegExp.test(inputValue)) {
        errMsg = 'At least one Special Characters';
      } else if (!minLengthRegExp.test(inputValue)) {
        errMsg = 'At least minumum 8 characters';
      } else {
        errMsg = '';
      }
      setPasswordErr(errMsg);
    }

    if (
      inputFieldName === 'confirmPassword' ||
      (inputFieldName === 'password' && formInput.confirmPassword.length > 0)
    ) {
      if (formInput.confirmPassword !== formInput.password) {
        setConfirmPasswordError('Confirm password is not matched');
      } else {
        setConfirmPasswordError('');
      }
    }

    if (inputFieldName === 'email') {
      let errMsg = '';

      if (inputValue.length === 0) {
        console.log(inputValue);
        errMsg = 'Field is empty';
      }
      setEmailErr(errMsg);
    }
  };

  const form = (
    <div className="row">
      <div className="col-sm-4">
        <form onSubmit={handleSubmit}>
          <div>
            <input
              name="firstName"
              type="text"
              placeholder="firstName"
              value={formInput.firstName || ''}
              onChange={handleInputChange}
              required
            />
          </div>
          <div>
            <input
              name="lastName"
              type="text"
              placeholder="lastName"
              value={formInput.lastName || ''}
              onChange={handleInputChange}
              required
            />
          </div>
          <EmailInputField
            handleInputChange={handleInputChange}
            handleValidation={handleValidation}
            fieldValue={formInput.email}
            fieldError={emailError}
          />
          <PasswordInputField
            handleInputChange={handleInputChange}
            handleValidation={handleValidation}
            fieldValue={formInput.password}
            fieldError={passwordError}
          />
          <ConfirmPasswordInputField
            handleInputChange={handleInputChange}
            handleValidation={handleValidation}
            fieldValue={formInput.confirmPassword}
            fieldError={confirmPasswordError}
          />
          <div>
            <span>Terms and conditions:</span>
            <input
              name="accept"
              type="checkbox"
              checked={formInput.accept || false}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <button type="submit">Submit</button>
            <button type="button" onClick={clearData}>
              Reset
            </button>
          </div>
        </form>
      </div>
    </div>
  );

  return <>{display ? <LoginForm /> : form}</>;
}
