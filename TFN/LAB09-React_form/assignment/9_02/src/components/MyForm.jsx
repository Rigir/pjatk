import React, { useState } from 'react';

export default function MyForm() {
  const [display, setDisplay] = useState(false);
  const [data, setData] = useState({});

  const updateData = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const clearData = (e) => {
    setData({});
    setDisplay(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setDisplay(true);
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <p>firstName: </p>
          <input
            name="firstName"
            type="text"
            value={data.firstName || ''}
            onChange={updateData}
          />
        </div>
        <div>
          <p>lastName: </p>
          <input
            name="lastName"
            type="text"
            value={data.lastName || ''}
            onChange={updateData}
          />
        </div>
        <div>
          <span>Subtitle: </span>
          <input
            name="subtitle"
            type="radio"
            checked={data.subtitle || false}
            onChange={updateData}
          />
        </div>
        <div>
          <span>Sound:</span>
          <select
            name="sound"
            value={data.sound || 'default'}
            defaultValue={'default'}
            onChange={updateData}
          >
            <option value={'default'} disabled>
              Choose an option
            </option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </select>
        </div>
        <div>
          <span>Terms and conditions:</span>
          <input
            name="accept"
            type="checkbox"
            checked={data.accept || false}
            onChange={updateData}
          />
        </div>
        <div>
          <button type="submit">Submit</button>
          <button type="button" onClick={clearData}>
            Reset
          </button>
        </div>
      </form>

      {display ? (
        <table border="1">
          <td>
            {Object.keys(data).map((key, i) => (
              <tr>{key}</tr>
            ))}
          </td>
          <td>
            {Object.keys(data).map((key, i) => (
              <tr>{data[key]}</tr>
            ))}
          </td>
        </table>
      ) : (
        ''
      )}
    </>
  );
}
