import React from 'react';
import './style.css';

import MyForm from './components/MyForm';

export default function App() {
  return (
    <div>
      <MyForm />
    </div>
  );
}
