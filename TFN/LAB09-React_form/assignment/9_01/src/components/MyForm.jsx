import React, { useRef, useState } from 'react';

export default function MyForm() {
  const [numbers, setNumbers] = useState([]);
  const number = useRef();

  const submit = (e) => {
    e.preventDefault();
    const x = number.current.value;
    setNumbers([...numbers, parseInt(x)]);
  };

  const arr_sum = (arr) => {
    return arr.length > 0 ? arr.reduce((a, b) => a + b, 0) : 0;
  };

  const arr_avg = (arr) => {
    return arr.length > 0 ? arr_sum(arr) / arr.length : 0;
  };

  return (
    <>
      <form onSubmit={submit}>
        <input ref={number} type="number" placeholder="number" required />
        <button>Calculate</button>
      </form>
      <p>
        <span>SUM:{arr_sum(numbers)}</span>
        <br />
        <span>AVG:{arr_avg(numbers)}</span>
      </p>
    </>
  );
}
