import React from 'react';
import { useState } from 'react/cjs/react.development';
import PasswordInputField from './PasswordInputField';
import EmailInputField from './EmailInputField';
import secret from '../data/secret.json';

export default function LoginForm() {
  const [display, setDisplay] = useState(false);
  const [emailError, setEmailErr] = useState('');
  const [passwordError, setPasswordErr] = useState('');
  const [formInput, setFormInput] = useState({
    email: '',
    password: '',
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      secret.find(
        (user) =>
          user.username === formInput['email'] &&
          user.password == formInput['password']
      )
    ) {
      setDisplay(true);
    }
  };

  const handleInputChange = (e) => {
    const inputValue = e.target.value.trim();
    const inputFieldName = e.target.name;
    const newInput = {
      ...formInput,
      [inputFieldName]: inputValue,
    };
    setFormInput(newInput);
  };

  const handleValidation = (e) => {
    const inputValue = e.target.value.trim();
    const inputFieldName = e.target.name;

    if (inputFieldName === 'password') {
      let errMsg = '';

      if (inputValue.length === 0) {
        errMsg = 'Password is empty';
      }
      setPasswordErr(errMsg);
    }

    if (inputFieldName === 'email') {
      let errMsg = '';

      if (inputValue.length === 0) {
        console.log(inputValue);
        errMsg = 'Field is empty';
      }
      setEmailErr(errMsg);
    }
  };

  const form = (
    <div className="row">
      <div className="col-sm-4">
        <form onSubmit={handleSubmit}>
          <EmailInputField
            handleInputChange={handleInputChange}
            handleValidation={handleValidation}
            fieldValue={formInput.email}
            fieldError={emailError}
          />
          <PasswordInputField
            handleInputChange={handleInputChange}
            handleValidation={handleValidation}
            fieldValue={formInput.password}
            fieldError={passwordError}
          />
          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  );

  return (
    <>
      {display ? (
        <table border="1">
          <td>
            {Object.keys(formInput).map((key, i) => (
              <tr>{key}</tr>
            ))}
          </td>
          <td>
            {Object.keys(formInput).map((key, i) => (
              <tr>{formInput[key]}</tr>
            ))}
          </td>
        </table>
      ) : (
        form
      )}
    </>
  );
}
