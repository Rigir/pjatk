import React from 'react';

import Counter from './components/Counter.js';

export default function App() {
  return (
    <div>
      <Counter btnValues={[-1, 1]} />
      <Counter btnValues={[-3, 3]} />
    </div>
  );
}
