import React from 'react';

export default function CounterButtons({ btnValues, func }) {
  return (
    <div>
      {btnValues.map((value, i) => (
        <button key={i} onClick={() => func(value)}>
          {value}
        </button>
      ))}
    </div>
  );
}
