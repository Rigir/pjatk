import React, { useState } from 'react';

import CounterOutput from './CounterOutput.js';
import CounterButtons from './CounterButtons.js';

export default function Counter({ btnValues }) {
  const [counter, setCounter] = useState(0);

  const increase = (value) => {
    setCounter((count) => count + value);
  };

  return (
    <div>
      <CounterOutput value={counter}/>
      <CounterButtons btnValues={btnValues} func={increase} />
    </div>
  );
}
