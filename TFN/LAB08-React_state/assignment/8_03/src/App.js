import React from 'react';

import Gallery from './components/Gallery.js';

export default function App() {
  return (
    <div>
      <Gallery />
    </div>
  );
}
