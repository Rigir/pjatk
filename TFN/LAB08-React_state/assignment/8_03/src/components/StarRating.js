import React, { useState } from 'react';

import Star from './Star.js';

export default function StarRating() {
  const [rating, setRating] = useState(0);
  const [ratings, setRatings] = useState([]);

  const createArray = (size, funcValue) =>
    [...Array(size)].map((_) => funcValue());

  const averageSum = (arr) => {
    if (!arr.length) {
      return 0;
    }
    let result = arr.reduce((prev, curr) => prev + curr) / arr.length;
    return result.toPrecision(2);
  };

  const handleRating = () => {
    if (rating !== 0) {
      setRatings((result) => [...result, rating]);
      setRating(0);
    }
  };

  return (
    <div >
      Score: {averageSum(ratings)}
      <br />
      {createArray(rating, () => 1)
        .concat(createArray(5 - rating, () => 0))
        .map((value, index) => (
          <Star key={index} {...{ value, index, setRating }} />
        ))}
      <button onClick={handleRating}>Rate</button>
    </div>
  );
}
