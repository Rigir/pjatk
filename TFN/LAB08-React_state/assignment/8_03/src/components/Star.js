import React from 'react';
import { FaStar } from 'react-icons/fa';

export default function Star({ index, value, setRating }) {
  return (
    <FaStar
      color={value ? 'orange' : 'grey'}
      onClick={() => setRating(index + 1)}
    />
  );
}
