import React from 'react';

import { FaThumbsDown, FaThumbsUp } from 'react-icons/fa';

export default function Thumb({ func }) {
  return (
    <div>
      <FaThumbsUp onClick={() => func(1)} /> &nbsp;
      <FaThumbsDown onClick={() => func(-1)} />
    </div>
  );
}
