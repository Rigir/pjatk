import React, { useState } from 'react';

import GalleryItem from './GalleryItem.js';

import pictures_list from '../data/pictures_list.json';

export default function Gallery() {
  const [gallery, updateGallery] = useState(pictures_list);

  const sortImagesByDate = () => {
    const sortedData = [...gallery].sort(
      (a, b) => new Date(b.date) - new Date(a.date)
    );
    updateGallery(sortedData);
  };

  const sortImagesByTitle = () => {
    const sortedData = [...gallery].sort((a, b) =>
      b.title.localeCompare(a.title)
    );
    updateGallery(sortedData);
  };

  const sortImagesByRandom = () => {
    const sortedData = [...gallery].sort(() => 0.5 - Math.random());
    updateGallery(sortedData);
  };

  const handleRemoveItem = (id) => {
    console.log(gallery);
    updateGallery((arr) => arr.filter((item) => item.id !== id));
  };

  return (
    <>
      <button onClick={sortImagesByTitle}>SortByTitle</button>
      <button onClick={sortImagesByDate}>SortByDate</button>
      <button onClick={sortImagesByRandom}>SortRandom</button>
      <ul>
        {gallery.map((item) => (
          <GalleryItem item={item} onRemove={handleRemoveItem} />
        ))}
      </ul>
    </>
  );
}
