import React, { useState } from 'react';

import Thumb from './Thumb.js';

export default function ThumbRating() {
  const [counter, setCounter] = useState(0);

  const increase = (value) => {
    setCounter((count) => count + value);
  };

  return (
    <div>
      <div>Score: {counter}</div>
      <Thumb func={increase} />
    </div>
  );
}
