import React, { useState } from 'react';

import StarRating from './StarRating.js';
import ThumbRating from './ThumbRating.js';

export default function GalleryItem({ item, onRemove }) {
  return (
    <li key={item.id}>
      <div>
        {item.title} | {item.date} |-
        <button onClick={() => onRemove(item.id)}>Remove</button>
      </div>
      <img src={item.url} alt={item.title}></img>
      <StarRating />
      <ThumbRating />
    </li>
  );
}
