const app = ReactDOM.createRoot(document.getElementById('root'));

const items = [
  {
    link: 'https://google.com',
    description: 'Link to google.com',
  },
  {
    link: 'https://bing.com',
    description: 'Link to bing.com',
  },
];

const list = React.createElement(
  'ul',
  {
    className: 'nav',
  },
  items.map((items) =>
    React.createElement(
      'li',
      { className: 'nav-item' },
      React.createElement(
        'a',
        { className: 'nav-link', href: items.link },
        items.description
      )
    )
  )
);

app.render(list);
