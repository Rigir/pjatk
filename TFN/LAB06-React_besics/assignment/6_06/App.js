const app = ReactDOM.createRoot(document.getElementById('root'));

const data = {
  image: "https://ucarecdn.com/f8cf81eb-3bab-4bba-9431-668884eab174/-/resize/300x/",
  cardTitle: "Bob Dylan",
  cardDescription: "Bob Dylan (born Robert Allen Zimmerman, May 24, 1941) is an American singer/songwriter, author, and artist who has been an influential figure in popular music and culture for more than five decades.",
  button: {
    url: "https://en.wikipedia.org/wiki/Bob_Dylan",
    label: "Go to wikipedia"
  }
};

​
class OneData extends React.Component {
  render() {
    return  React.createElement(
      'div',
      {className: "card m-5"},
      React.createElement('img', {
        className: 'card-img-top',
        src: data.image,
        alt: 'Card image cap',
      }),
      React.createElement(
        'div',
        { className: 'card-body' },
        React.createElement('h5', { className: 'card-title' }, data.cardTitle),
        React.createElement('p', { className: 'card-text' }, data.cardDescription),
        React.createElement(
          'a',
          { className: 'btn btn-primary', href: data.button.url },
          data.button.label
        )
      )
    );
  }
}

app.render(React.createElement(OneData, { data }, null));
