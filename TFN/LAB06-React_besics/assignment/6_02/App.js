const app = ReactDOM.createRoot(document.getElementById('root'));

const customer = {
  first_name: 'Bob',
  last_name: 'Dylan',
};

const div = React.createElement(
  'div',
  null,
  React.createElement('h1', null, `My name is ${customer.first_name}`),
  React.createElement('h2', null, `My last name is ${customer.last_name}`)
);

app.render(div);
