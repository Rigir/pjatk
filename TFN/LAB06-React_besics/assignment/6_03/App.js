const app = ReactDOM.createRoot(document.getElementById('root'));

const data = {
  image: 'https://i.ytimg.com/vi/kHjzuqq3b44/maxresdefault.jpg',
  cardTitle: 'Bob Dylan',
  cardDescription:
    'Bob Dylan (born Robert Allen Zimmerman, May 24, 1941) is an American singer/songwriter, author, and artist who has been an influential figure in popular music and culture for more than five decades.',
  button: {
    url: 'https://en.wikipedia.org/wiki/Bob_Dylan',
    label: 'Go to wikipedia',
  },
};

const container = React.createElement(
  'div',
  null,
  React.createElement('img', {
    className: 'card-img-top',
    src: data.image,
    alt: 'Card image cap',
  }),
  React.createElement(
    'div',
    { className: 'card-body' },
    React.createElement('h5', { className: 'card-title' }, data.cardTitle),
    React.createElement('p', { className: 'card-text' }, data.cardDescription),
    React.createElement(
      'a',
      { className: 'card-text', href: data.button.url },
      data.button.label
    )
  )
);

app.render(container);
