const app = ReactDOM.createRoot(document.getElementById('root'));

const items = [
  {
    link: 'https://google.com',
    description: 'Link to google.com',
  },
  {
    link: 'https://bing.com',
    description: 'Link to bing.com',
  },
];

class Link extends React.Component {
  render() {
    return React.createElement(
      'ul',
      {
        className: 'nav',
      },
      items.map((items, i) =>
        React.createElement(
          'li',
          { className: 'nav-item', key: i },
          React.createElement(
            'a',
            { className: 'nav-link', href: items.link },
            items.description
          )
        )
      )
    );
  }
}

app.render(React.createElement(Link, { items }, null));
