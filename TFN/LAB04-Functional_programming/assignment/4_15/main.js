const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function binarySearch(list, start, stop, search) {
  return start > stop
    ? "Brak elementu w ciągu!!!"
    : list[Math.floor((start + stop) / 2)] === search
    ? Math.floor((start + stop) / 2)
    : list[Math.floor((start + stop) / 2)] > search
    ? binarySearch(list, start, Math.floor((start + stop) / 2) - 1, search)
    : binarySearch(list, Math.floor((start + stop) / 2) + 1, stop, search);
}

rl.on("line", (line_1) => {
  rl.on("line", (line_2) => {
    var list = line_1.split(" ").map((x) => parseInt(x));
    console.log(binarySearch(list, 0, list.length - 1, parseInt(line_2)));
  });
});
