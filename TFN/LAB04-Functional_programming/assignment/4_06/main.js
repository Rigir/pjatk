const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const sumNum = (arr) => {
  return arr
    .split(" ")
    .map(parseFloat)
    .reduce((x, y) => x + y);
};

rl.on("line", (line) => {
  console.log(sumNum(line));
});
