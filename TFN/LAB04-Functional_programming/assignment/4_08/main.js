const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const countWords = (str) => {
  return str.split(" ").reduce((arr, res) => {
    return { ...arr, [res]: (arr[res] || 0) + 1 };
  }, {});
};

rl.on("line", (line) => {
  console.log(countWords(line));
});
