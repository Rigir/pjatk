const myUsers = [
  { name: "shark", likes: "ocean" },
  { name: "turtle", likes: "pond" },
  { name: "otter", likes: "fish biscuits" },
];

const formatMyUser = (obj) => {
  return obj.map((x) =>
    JSON.parse(`{"${x.name}":"${x.likes}", "age":${x.name.length * 10}}`)
  );
};

console.log(formatMyUser(myUsers));
