const students = [
  { name: "Quincy", grade: 96 },
  { name: "Jason", grade: 84 },
  { name: "Alexis", grade: 100 },
  { name: "Sam", grade: 65 },
  { name: "Katie", grade: 90 },
];

const filterStudents = (obj) => {
  return Object.values(obj).filter((x) => x.grade >= 84 && x.grade <= 95);
};

console.log(filterStudents(students));
