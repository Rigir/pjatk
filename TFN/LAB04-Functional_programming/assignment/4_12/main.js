function getValue(object, path) {
  return (
    `${path.split(".").reduce((acc, item) => {
      return acc[item] === undefined ? "" : acc[item];
    }, object)}`.length || null
  );
}

const obj = {
  person: {
    address: {
      street: "Wiejska",
    },
    name: "Tomasz",
  },
};

console.log(getValue(obj, "person.address.street")); // => Wiejska = 7
console.log(getValue(obj, "person.name")); // => Tomasz = 6
console.log(getValue(obj, "person.lastName")); // => null = NaN/Undefined
