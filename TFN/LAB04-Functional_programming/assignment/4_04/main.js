const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const findEven = (arr) => {
  return arr
    .split(" ")
    .filter((x) => x % 2 == 0)
    .join(" ");
};

rl.on("line", (line) => {
  console.log(findEven(line));
});
