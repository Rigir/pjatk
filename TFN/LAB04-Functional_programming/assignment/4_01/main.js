const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const strToIntArr = (base) => {
  return base.split(" ").map((x) => parseInt(x));
};

rl.on("line", (line) => {
  console.log(strToIntArr(line));
});
