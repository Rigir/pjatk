const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function my_print([start, end], result = []) {
  result.push(start);
  return start >= end ? result : my_print([start + 1, end], result);
}

rl.on("line", (line) => {
  console.log(my_print(line.split(" ").map((x) => parseInt(x))).join(" "));
});
