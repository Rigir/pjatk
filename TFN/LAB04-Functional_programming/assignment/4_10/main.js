const func = (arr, func) => {
  return arr.find(func) || undefined;
};

let element = func(["Ala", "Kot", "Pies"], (y) => y === "Ala");
let notFound = func(["Ala", "Kot", "Pies"], (y) => y === "Jakub");

console.log(element); // ‘Ala’
console.log(notFound);
