const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function min(arr) {
  return arr.length === 1 || arr[0] < min(arr.slice(1))
    ? arr[0]
    : min(arr.slice(1));
}

rl.on("line", (line) => {
  console.log(min(line.split(" ")));
});
