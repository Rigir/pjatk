const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const formatInput = (obj) => {
  return obj.reduce((ret, { id, name }) => {
    return { ...ret, [id]: { id, name } };
  }, {});
};

console.log(
  formatInput([
    {
      id: "abc",
      name: "Ala",
    },
    {
      id: "def",
      name: "Tomek",
    },
    {
      id: "ghi",
      name: "Jan",
    },
  ])
);
