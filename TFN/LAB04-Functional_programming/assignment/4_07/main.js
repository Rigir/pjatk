const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const findPower = (arr) => {
  return arr
    .split(" ")
    .map((x) => (x ** 2).toFixed(2))
    .filter((x) => x % 1 == 0);
};

rl.on("line", (line) => {
  console.log(findPower(line));
});
