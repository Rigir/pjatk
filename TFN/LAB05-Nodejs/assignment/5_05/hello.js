const sum = process.argv.slice(2).map(x => parseInt(x)).reduce((x, y) => {
    return x + y;
}, 0);

console.log(sum);
