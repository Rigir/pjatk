let readline = require('readline');
  
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const isValidUrl = urlString => {
    try { 
        return Boolean(new URL(urlString)); 
    }
    catch(e){ 
        return false; 
    }
}

rl.on('line', (line) => {
    console.log(isValidUrl(line));
});