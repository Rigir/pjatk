var map = require("through2-map")

require("http").createServer((req, res) => {
    if (req.method === 'POST') {
        req.pipe(map((chunk) => {
            return chunk.toString().toUpperCase();
        })).pipe(res)
    }
}).listen(9393);

//curl -X POST -d "xyz" 127.0.0.1:9393     
