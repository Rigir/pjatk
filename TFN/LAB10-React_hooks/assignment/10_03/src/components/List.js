import React from 'react';

import ListElement from './ListElement.js';

export default function List({ arr, funcToRemove }) {
  return (
    <ul>
      {arr.map((item, i) => (
        <ListElement value={item} id={i} onRemove={funcToRemove} />
      ))}
    </ul>
  );
}
