import React, { useState } from 'react';

function dec2bin(dec) {
  return (dec >>> 0).toString(2);
}

export default function ListElement({ value, id, onRemove }) {
  const [isBinary, setIsBinary] = useState(true);

  const toggleIsBinary = () => {
    setIsBinary((prevCheck) => !prevCheck);
  };

  return (
    <li key={id}>
      <div>{isBinary ? value : dec2bin(value)}</div>
      <button
        onClick={() => {
          setIsBinary(true);
          onRemove(id);
        }}
      >
        Remove
      </button>
      <button onClick={() => toggleIsBinary()}>dec2bin2dec</button>
    </li>
  );
}
