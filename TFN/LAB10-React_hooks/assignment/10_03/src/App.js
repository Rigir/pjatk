import React, { useState, useEffect } from 'react';

import List from './components/List.js';

export default function App() {
  const [numbers, updateNumbers] = useState([]);

  const increaseArray = (value) => {
    updateNumbers((arr) =>
      arr.concat(
        Array(value)
          .fill()
          .map(() => Math.round(Math.random() * 9))
      )
    );
  };

  const handleRemoveItem = (id) => {
    updateNumbers((arr) => arr.filter((_, i) => i !== id));
  };

  useEffect(() => {
    increaseArray();
  }, []);

  return (
    <div>
      <button onClick={() => increaseArray(10)}>Add 10 items</button>
      <List arr={numbers} funcToRemove={handleRemoveItem} />
    </div>
  );
}
