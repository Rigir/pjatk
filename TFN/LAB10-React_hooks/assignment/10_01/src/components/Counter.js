import React, { useState, useEffect } from 'react';

import CounterOutput from './CounterOutput.js';

export default function Counter({ btnValues }) {
  const [counter, setCounter] = useState(0);

  const increase = (value) => {
    setCounter((count) => count + value);
  };

  return (
    <div
      onClick={(e) => {
        e.preventDefault();
        increase(btnValues[0]);
      }}
      onContextMenu={(e) => {
        e.preventDefault();
        increase(btnValues[1]);
      }}
      style={{ height: '100vh' }}
    >
      <CounterOutput value={counter} />
    </div>
  );
}

//Witch method ?
// useEffect(() => {
//   function handleMouseClick(event) {
//     if (event.button === 0) {
//       setCounter(counter + 1);
//     } else if (event.button === 2) {
//       setCounter(counter - 1);
//     }
//   }

//   window.addEventListener('mousedown', handleMouseClick);

//   return () => {
//     window.removeEventListener('mousedown', handleMouseClick);
//   };
// }, [counter]);
