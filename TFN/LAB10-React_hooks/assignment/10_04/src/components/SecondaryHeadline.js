import React, { useContext } from 'react';

import { MAIN_CONTEXT } from '../contextes/main';

export default function Paragraph() {
  const context = useContext(MAIN_CONTEXT);

  return <h2 style={context}>text</h2>;
}
