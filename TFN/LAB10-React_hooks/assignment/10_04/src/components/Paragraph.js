import React, { useContext } from 'react';

import { MAIN_CONTEXT } from '../contextes/main';

export default function Paragraph() {
  const context = useContext(MAIN_CONTEXT);

  return <p style={context}>text</p>;
}
