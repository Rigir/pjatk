import { createContext } from 'react';

export const MAIN_CONTEXT = createContext({
  background: '#741313',
  color: '#FFF',
});
