import React, { createContext } from 'react';

import PrimaryHeadline from './components/PrimaryHeadline';
import SecondaryHeadline from './components/SecondaryHeadline';
import Paragraph from './components/Paragraph';

export const MAIN_CONTEXT = createContext();

export default function App() {
  return (
    <MAIN_CONTEXT.Provider>
      <PrimaryHeadline />
      <SecondaryHeadline />
      <Paragraph />
    </MAIN_CONTEXT.Provider>
  );
}
