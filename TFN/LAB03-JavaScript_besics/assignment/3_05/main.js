var data = {
  Country: "USA",
  Title: "Zielona Mila",
  Director: "Frank Darabont",
  Year: 1999,
  Genre: "Dramat",
};

const { Country, Year, Genre, ...result } = data;

console.log(result);
