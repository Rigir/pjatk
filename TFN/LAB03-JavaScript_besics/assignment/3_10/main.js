const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on("line", (line) => {
  let num = line.split()[0];
  let result = Array.from(num)
    .map((x) => parseInt(x))
    .map((x) => x ** num.length)
    .reduce((sum, x) => sum + x, 0);

  if (result.toString() != num) console.log("false");
  else console.log("true");
});
