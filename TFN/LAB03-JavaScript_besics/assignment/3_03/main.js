const multiplyAsync = (x, y) => {
  return new Promise((resolve, reject) => {
    if (Number.isInteger(x) && Number.isInteger(y)) {
      return resolve(x * y);
    }
    return reject("Error: NaN");
  });
};

const displayString = (string) => {
  console.log(string);
};

multiplyAsync(3, 9).then((value) => {
  displayString(value);
});
