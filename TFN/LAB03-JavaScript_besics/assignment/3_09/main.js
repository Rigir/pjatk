const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on("line", (line) => {
  let numbers = line.split(" ").map((n) => parseInt(n));
  console.log(numbers[0] + numbers[1]);
});
