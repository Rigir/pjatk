const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function countYears(time, planet) {
  switch (planet) {
    case "Merkury":
      time /= 0.2408467;
      break;
    case "Wenus":
      time /= 0.61519726;
      break;
    case "Mars":
      time /= 1.8808158;
      break;
    case "Jowisz":
      time /= 11.862615;
      break;
    case "Saturn":
      time /= 29.447498;
      break;
    case "Uran":
      time /= 84.016846;
      break;
    case "Neptun":
      time /= 164.79132;
      break;
    case "Ziemia":
      break;
    default:
      return time.toFixed(2);
  }
  return (time / 31557600).toFixed(2);
}
rl.on("line", (time) => {
  rl.on("line", (planet) => {
    console.log(countYears(parseFloat(time), planet));
    rl.close();
  });
});
