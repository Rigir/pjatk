const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Udalo sie!");
  }, 5000);
});

const displayString = (string) => {
  console.log(string);
};

myPromise.then((value) => {
  displayString(value);
});
