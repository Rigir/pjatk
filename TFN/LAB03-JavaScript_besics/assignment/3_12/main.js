const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on("line", (line) => {
  let num = parseInt(line);

  if (Number.isInteger(num)) {
    for (let i = 0; i < num; i++) {
      for (let j = -1; j < i; j++) {
        process.stdout.write("*");
      }
      process.stdout.write("\n");
    }

    for (let i = num; i > 0; i--) {
      for (let j = 0; j < i; j++) {
        process.stdout.write("*");
      }
      process.stdout.write("\n");
    }

    for (let i = 0; i < num; i++) {
      for (let j = 0; j < num; j++) {
        if (i > j) process.stdout.write(" ");
        else process.stdout.write("*");
      }
      process.stdout.write("\n");
    }

    for (let i = num - 1; i > -1; i--) {
      for (let j = 0; j < num; j++) {
        if (i > j) process.stdout.write(" ");
        else process.stdout.write("*");
      }
      process.stdout.write("\n");
    }
  }
});
