const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function camelize(input) {
  let arr = input.split(" ");
  let out = "";

  arr.forEach((word) => {
    out += word.charAt(0).toUpperCase() + word.slice(1);
  });

  return out;
}

rl.on("line", (line) => {
  console.log(camelize(line));
});
