let Fraction = class {
  constructor(numerator, denumerator) {
    this.checkInput(numerator, denumerator);
    if (denumerator == 0) {
      throw new Error("Zero in denumerator");
    }
    this.numerator = numerator;
    this.denumerator = denumerator;
  }

  checkInput(...arr) {
    arr.forEach((num) => {
      if (Number.isNaN(num)) throw new Error("Input is NaN");
    });
  }

  multiplyBy(x) {
    this.checkInput(x);
    if (x instanceof Fraction) {
      this.numerator *= x.numerator;
      this.denumerator *= x.denumerator;
    } else {
      this.numerator *= x;
      this.denumerator *= x;
    }
  }

  static multiply(x, y) {
    if (x instanceof Fraction && y instanceof Fraction) {
      return new Fraction(
        (x.numerator *= y.numerator),
        (x.denumerator *= y.denumerator)
      );
    }
  }

  print() {
    return `${this.numerator}/${this.denumerator}`;
  }
};

let f1 = new Fraction(1, 2);
f1.multiplyBy(2);
console.log(f1.print());
let f2 = new Fraction(4, 3);
f2.multiplyBy(-2);
console.log(f2.print());
let f3 = Fraction.multiply(f1, f2);
console.log(f3.print());
