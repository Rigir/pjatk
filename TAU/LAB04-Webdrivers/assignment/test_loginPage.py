import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

@pytest.fixture(params=["firefox", "chrome", "safari"])
def webdriver(request):
    if request.param == "firefox":
        driver = webdriver.Firefox(executable_path=r'/path/to/driver')
    elif request.param == "chrome":
        driver = webdriver.Chrome(executable_path=r'/path/to/driver')
    elif request.param == "safari":
        driver = webdriver.Safari(executable_path=r'/path/to/driver')
    else:
        raise ValueError("Invalid browser name")
    
    yield driver
    driver.quit()

class TestSauceDemo():
  class TestLoadSiteScenario(object):
      def setup_method(self, webdriver):
          self.driver = webdriver

      def teardown_method(self):
          self.driver.quit()

      def load_webside(self):
          self.driver.get("https://www.saucedemo.com/")
          self.driver.set_window_size(870, 803)

      def test_LoadSiteScenario(self):
          self.load_webside()
          elements = self.driver.find_elements(By.CSS_SELECTOR, ".login_logo")
          assert len(elements) > 0
          self.driver.close()

  class TestLoginPageScenario():
      CORRECT_USERNAME = "standard_user"
      CORRECT_PASSWORD = "secret_sauce"
      TEST_SPECIAL_CHARACTERS = "!@#$%^&*()_+{/}"
      TEST_SQLINJECTIONON = "‘OR 1=1 --"
      INCORRECT_INPUT = "test"
      EMPTY_INPUT = ""

      def setup_method(self, webdriver):
          self.driver = webdriver
          self.load_webside()

      def teardown_method(self):
          self.driver.quit()

      def load_webside(self):
          self.driver.get("https://www.saucedemo.com/")
          self.driver.set_window_size(870, 803)

      def login_form(self, login, password):
          self.driver.find_element(By.ID, "user-name").send_keys(login)
          self.driver.find_element(By.ID, "password").send_keys(password)
          self.driver.find_element(By.ID, "login-button").click()

      def test_emptyInputs(self):
          self.login_form(self.EMPTY_INPUT, self.EMPTY_INPUT)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username is required\')]")
          assert len(elements) > 0

      def test_emptyUsernameGoodPassword(self):
          self.login_form(self.EMPTY_INPUT, self.CORRECT_PASSWORD)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username is required\')]")
          assert len(elements) > 0

      def test_goodUsernameEmptyPassword(self):
          self.login_form(self.CORRECT_USERNAME, self.EMPTY_INPUT)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Password is required\')]")
          assert len(elements) > 0

      def test_goodUsernameGoodPassword(self):
          self.login_form(self.CORRECT_USERNAME, self.CORRECT_PASSWORD)
          self.driver.find_element(By.CSS_SELECTOR, ".app_logo").click()

      def test_goodUsernameWrongPassword(self):
          self.login_form(self.CORRECT_USERNAME, self.INCORRECT_INPUT)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username and password do not match any user in this service\')]")
          assert len(elements) > 0

      def test_SQLInjectiononPassword(self):
          self.login_form(self.INCORRECT_INPUT, self.TEST_SQLINJECTIONON)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username and password do not match any user in this service\')]")
          assert len(elements) > 0

      def test_SQLInjectiononUsername(self):
          self.login_form(self.TEST_SQLINJECTIONON, self.INCORRECT_INPUT)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username and password do not match any user in this service\')]")
          assert len(elements) > 0

      def test_wrongUsernameWrongPassword(self):
          self.login_form(self.INCORRECT_INPUT, self.INCORRECT_INPUT)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username and password do not match any user in this service\')]")
          assert len(elements) > 0

      def test_SpecialCharactersInUsername(self):
          self.login_form(self.TEST_SPECIAL_CHARACTERS, self.INCORRECT_INPUT)
          elements = self.driver.find_elements(By.XPATH, "//h3[contains(.,\'Epic sadface: Username and password do not match any user in this service\')]")
          assert len(elements) > 0

  class TestShoppingCartScenario():
      CORRECT_USERNAME = "standard_user"
      CORRECT_PASSWORD = "secret_sauce"

      def setup_method(self, webdriver):
          self.driver = webdriver
          self.load_webside()

      def teardown_method(self):
          self.driver.quit()

      def load_webside(self):
          self.driver.get("https://www.saucedemo.com/")
          self.driver.set_window_size(870, 803)

      def add_items_to_cart(self):
          self.driver.find_element(By.ID, "add-to-cart-sauce-labs-backpack").click()
          self.driver.find_element(By.ID, "add-to-cart-sauce-labs-bike-light").click()

      def login_form(self, login, password):
          self.driver.find_element(By.ID, "user-name").send_keys(login)
          self.driver.find_element(By.ID, "password").send_keys(password)
          self.driver.find_element(By.ID, "login-button").click()

      def test_addingItemsToCart(self):
          self.login_form(self.CORRECT_USERNAME, self.CORRECT_PASSWORD)
          self.add_items_to_cart()
          element = self.driver.find_element(By.CSS_SELECTOR, ".shopping_cart_badge")
          assert int(element.text) == 2