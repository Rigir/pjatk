import unittest
from unittest import mock
from mealdb import MealDB

class TestMealDB(unittest.TestCase):
    @mock.patch("mealdb.MealDB")
    def test_search_meals(self, mock_mealdb_class):
        mock_mealdb = mock_mealdb_class.return_value

        mock_results = [
            {
                "idMeal": "1",
                "strMeal": "Spaghetti Bolognese",
                "strMealThumb": "https://www.themealdb.com/images/media/meals/sqpqtp1517054816.jpg",
            },
            {
                "idMeal": "2",
                "strMeal": "Chicken Tikka Masala",
                "strMealThumb": "https://www.themealdb.com/images/media/meals/wyxwsp1486979827.jpg",
            },
        ]
        mock_mealdb.search_meals.return_value = mock_results

        mealdb = MealDB()
        results = mealdb.search_meals("chicken")

        expected_results = [
            {
                "idMeal": "2",
                "strMeal": "Chicken Tikka Masala",
                "strMealThumb": "https://www.themealdb.com/images/media/meals/wyxwsp1486979827.jpg",
            }
        ]
        self.assertEqual(results, expected_results)

    @mock.patch("mealdb.MealDB")
    def test_get_meal_by_id(self, mock_mealdb_class):
        mock_mealdb = mock_mealdb_class.return_value

        mock_result = {
            "idMeal": "1",
            "strMeal": "Spaghetti Bolognese",
            "strMealThumb": "https://www.themealdb.com/images/media/meals/sqpqtp1517054816.jpg",
            "strInstructions": "Cook spaghetti according to instructions on packet...",
        }
        mock_mealdb.get_meal_by_id.return_value = mock_result

        mealdb = MealDB()
        result = mealdb.get_meal_by_id("1")

        expected_result = {
            "idMeal": "1",
            "strMeal": "Spaghetti Bolognese",
            "strMealThumb": "https://www.themealdb.com/images/media/meals/sqpqtp1517054816.jpg",
            "strInstructions": "Cook spaghetti according to instructions on packet...",
        }
        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()