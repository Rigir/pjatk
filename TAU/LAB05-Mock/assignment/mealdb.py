class MealDB:
    def __init__(self):
        self.data = {
            "meals": [
            {
                "idMeal": "1",
                "strMeal": "Spaghetti Bolognese",
                "strMealThumb": "https://www.themealdb.com/images/media/meals/sqpqtp1517054816.jpg",
                "strInstructions": "Cook spaghetti according to instructions on packet...",
                "strArea": "Italian",
                "strCategory": "Pasta",
                "strTags": "Meat",
                "strIngredient1": "Spaghetti",
                "strIngredient2": "Beef mince",
                "strIngredient3": "Tomatoes",
                "strIngredient4": "Onion",
                "strIngredient5": "Garlic",
                "strIngredient6": "Olive oil",
                "strIngredient7": "Salt",
                "strIngredient8": "",
                "strIngredient9": "",
                "strIngredient10": "",
                "strIngredient11": "",
                "strIngredient12": "",
                "strIngredient13": "",
                "strIngredient14": "",
                "strIngredient15": "",
                "strIngredient16": "",
                "strIngredient17": "",
                "strIngredient18": "",
                "strIngredient19": "",
                "strIngredient20": "",
            },
            {
                "idMeal": "2",
                "strMeal": "Chicken Tikka Masala",
                "strMealThumb": "https://www.themealdb.com/images/media/meals/wyxwsp1486979827.jpg",
                "strInstructions": "Mix all of the marinade ingredients together in a bowl...",
                "strArea": "Indian",
                "strCategory": "Chicken",
                "strTags": "Spicy",
                "strIngredient1": "Chicken",
                "strIngredient2": "Yogurt",
                "strIngredient3": "Lemon juice",
                "strIngredient4": "Ginger",
                "strIngredient5": "Garlic",
                "strIngredient6": "Garam masala",
                "strIngredient7": "Cumin",
                "strIngredient8": "Paprika",
                "strIngredient9": "Salt",
                "strIngredient10": "Butter",
                "strIngredient11": "Onion",
                "strIngredient12": "Tomato",
                "strIngredient13": "Cream",
                "strIngredient14": "",
                "strIngredient15": "",
                "strIngredient16": "",
                "strIngredient17": "",
                "strIngredient18": "",
                "strIngredient19": "",
                "strIngredient20": "",
            },
            ]
        }

    def get_meal_by_id(self, meal_id):
        for item in self.data["meals"]:
            if item["idMeal"] == meal_id:
                result = {
                    "idMeal": item["idMeal"],
                    "strMeal": item["strMeal"],
                    "strMealThumb": item["strMealThumb"],
                    "strInstructions": item["strInstructions"],
                }
                return result
        return None
    
    def search_meals(self, query):
        results = []
        for item in self.data["meals"]:
            if query.lower() in item["strMeal"].lower():
                results.append(
                    {
                        "idMeal": item["idMeal"],
                        "strMeal": item["strMeal"],
                        "strMealThumb": item["strMealThumb"],
                    }
                )
        return results