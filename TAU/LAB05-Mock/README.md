![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
* Zaimplementować obsługę REST API (dowolna tematyka np. procesor płatności) i zamockować wszystkie dostępne odpowiedzi, które zwraca. UWAGA - nie implementować REST API, zakładamy że ono nie istnieje, dlatego mockujemy odpowiedzi z tego API (w domyśle taką wiedzę możemy uzyskać z dokumentacji API, ale w naszym przypadku przyjmujemy, że sami wymyślamy, jakie odpowiedzi zwraca to API). Nie implementujemy API, ale tworzymy dokumentację, w której odpowiedzi z API są opisane.