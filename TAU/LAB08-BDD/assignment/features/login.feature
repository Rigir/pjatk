Feature: Login to the Sauce Demo website

  Scenario: Valid username with valid password
    Given I am on the Sauce Demo login page
    When I enter a valid username
    And I enter a valid password
    And I click the login button
    Then I should be logged in successfully

  Scenario: Blank password with valid password 
    Given I am on the Sauce Demo login page
    When I enter an empty username
    And I enter a valid password
    And I click the login button
    Then I should see an error message

  Scenario: Incorrect login with blank password 
    Given I am on the Sauce Demo login page
    When I enter a valid username
    And I enter an empty password
    And I click the login button
    Then I should see an error message

  Scenario: Invalid username with invalid password
    Given I am on the Sauce Demo login page
    When I enter an invalid username
    And I enter an invalid password
    And I click the login button
    Then I should see an error message

  Scenario: Valid username with invalid password
    Given I am on the Sauce Demo login page
    When I enter a valid username
    And I enter an invalid password
    And I click the login button
    Then I should see an error message
