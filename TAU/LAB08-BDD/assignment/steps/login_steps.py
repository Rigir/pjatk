from behave import given, when, then
from selenium.webdriver.common.by import By


@given('I am on the Sauce Demo login page')
def step_impl(context):
    context.driver.get('https://www.saucedemo.com/')


@then('I should be logged in successfully')
def step_impl(context):
    product_label = context.driver.find_element(By.XPATH, '//span[text()="Products"]')
    assert product_label.is_displayed()


@then('I should see an error message')
def step_impl(context):
    error_message = context.driver.find_element(By.CSS_SELECTOR, '.error-message-container.error')
    assert error_message.is_displayed()

# Button

@when('I click the login button')
def step_impl(context):
    login_button = context.driver.find_element(By.ID, 'login-button')
    login_button.click()

# Username

@when("I enter a valid username")
def step_impl(context):
    username = context.driver.find_element(By.ID, 'user-name')
    username.send_keys('standard_user')


@when("I enter an invalid username")
def step_impl(context):
    username = context.driver.find_element(By.ID, 'user-name')
    username.send_keys('invalid_user')


@when("I enter an empty username")
def step_impl(context):
    username = context.driver.find_element(By.ID, 'user-name')
    username.send_keys('')

# Password

@when("I enter a valid password")
def step_impl(context):
    password = context.driver.find_element(By.ID, 'password')
    password.send_keys('secret_sauce')


@when("I enter an invalid password")
def step_impl(context):
    password = context.driver.find_element(By.ID, 'password')
    password.send_keys('invalid_password')


@when("I enter an empty password")
def step_impl(context):
    password = context.driver.find_element(By.ID, 'password')
    password.send_keys('')

