![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
* 1:  używając scenariusza z wcześniejszego zadania podzielić go na podscenariusze. Czyli jeśli mamy scenariusz, który przechodzi na stronę, później testuje logowanie, należy wydzielić dwa oddzielne scenariusze (np. w postaci funkcji), które oddzielnie można testować - scenariusz 1 to przejście na stronę, scenariusz drugi, to przejście do logowania. Następnie dopisać trzeci scenariusz, który będzie realizował inną/dodatkową rzecz, np. pobierał jakiś element ze strony lub szukał jakiegoś elementu, którego się spodziewamy. Dalej napisać testy dla tego scenariusza, np. jeśli znajdziemy dany element, to sprawdzamy czy posiada on konkretną wartość itp.
	Dalej należy zbudować 3 scenariusze, które będą realizowane na podscenariuszach, które wcześniej zostały opisane.
	Scenariusz 1 przechodzi na stronę i sprawdza, czy się załadowała
	Scenariusz 2 przechodzi na stronę (nie testuje już, czy strona się załadowała), następnie przechodzi do logowania i testuje logowanie (testy z poprzednich laboratoriów, odnośnie testowania co najmniej 8 warunków brzegowych)
	Scenariusz 3 przechodzi na stronę, przechodzi do logowania, realizuje dodatkową rzecz, która będzie miała zostać zrealizowana przez niego i przetestuje tę czynność, np. znajdowanie fragmentu tekstu i sprawdzenie, czy istnieje i czy ma odpowiednią zawartość.
