package org.example.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.example.unit_tests.StringUtils;

public class StringUtilsTest {
    @Test
    public void toUpperCaseWorksWithSmallCharacters() {
        Assertions.assertEquals("ABC", StringUtils.toUpperCase("abc"));
    }
    @Test
    public void toUpperCaseWorksWithSpaces() {
        Assertions.assertEquals("A BC  D", StringUtils.toUpperCase("a bc  d"));
    }
    @Test
    public void toUpperCaseWorksWithLatinCharacters() {
        Assertions.assertEquals("ĄĘ", StringUtils.toUpperCase("ąę"));
    }

    @Test
    public void toUpperCaseWithNumAndSpecialChar() {
        Assertions.assertEquals("1231A24@!@21", StringUtils.toUpperCase("1231a24@!@21"));
    }

    @Test
    public void removeSmallCharsWorksWithSmallCharacters() {
        Assertions.assertEquals("", StringUtils.removeSmallChars("abc"));
    }

    @Test
    public void removeSmallCharsWithLatinCharacters() {
        Assertions.assertEquals("", StringUtils.removeSmallChars("ąćęłńóśźż"));
    }

    @Test
    public void removeSmallCharsWithThreeSpaces() {
        Assertions.assertEquals("   ", StringUtils.removeSmallChars("a bc  d"));
    }

    @Test
    public void removeSmallWithNumAndSpecialChar() {
        Assertions.assertEquals(">%@321", StringUtils.removeSmallChars(">a%@321"));
    }

    @Test
    public void reverseStringSmallCharacters() {
        Assertions.assertEquals("cba", StringUtils.reverseString("abc"));
    }

    @Test
    public void reverseStringWithWithSpaces() {
        Assertions.assertEquals("d  cb a", StringUtils.reverseString("a bc  d"));
    }

    @Test
    public void reverseStringWithLatinCharacters() {
        Assertions.assertEquals("żźśóńłęćą", StringUtils.reverseString("ąćęłńóśźż"));
    }

    @Test
    public void reverseStringWithNumAndSpecialChar() {
        Assertions.assertEquals("12@!@421321", StringUtils.reverseString("123124@!@21"));
    }

}
