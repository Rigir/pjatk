package org.example.unit_tests;

public class StringUtils {
    static public String toUpperCase(String str){
        return str.toUpperCase();
    }

    static public String reverseString(String str){
        return new StringBuffer(str).reverse().toString();
    }

    static public String removeSmallChars(String str){
        return str.replaceAll("[a-ząćęłńóśźż]", "");
    }
}
