![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
* 1: Przy użyciu selenium przetestować na dowolnej stronie proces logowania, zwracając uwagę na corner case'y (rozważyć conajmniej 8 przypadków). Język programowania jest dowolny, używamy Selenium Web Drivera, dla dowolnej przeglądarki.
