import pytest
from game import Board, Game, PathFinder

@pytest.fixture(scope="module")
def board():
    board = Board(5, 5)
    board.generate_board()
    yield board
    del board

class TestBoard:
    def test_generate_board(self, board):
        assert len(board.obstacles) > 0

    def test_is_valid_move(self, board):
        obstacles = board.get_obstacles()
        valid_pos = [(x, y) for x in range(5) for y in range(5) if (x, y) not in obstacles]
        for pos in valid_pos:
            assert board.is_valid_move(pos[0], pos[1])
        for obs in obstacles:
            assert not board.is_valid_move(obs[0], obs[1])
        assert not board.is_valid_move(-1, 0)
        assert not board.is_valid_move(0, -1)
        assert not board.is_valid_move(5, 0)
        assert not board.is_valid_move(0, 5)

class TestGame:
    def setup_class(self):
        self.game = Game(10, 10)

    def teardown_class(self):
        self.game = None

    def test_make_move_valid(self):
        self.game.player_pos = (0, 0)
        if self.game.board.is_valid_move(1, 0):
            self.game.make_move('d')
            assert self.game.player_pos == (1, 0)
        else:
            pytest.xfail("The space is occupied")


    def test_make_move_invalid(self):
        self.game.player_pos = (0, 0)
        self.game.make_move('w')
        assert self.game.player_pos == (0, 0)

    def test_find_char(self):
        assert self.game.find_char('A') == self.game.start_pos
        assert self.game.find_char('B') == self.game.stop_pos

class TestPathFinder:
    def setup_method(self):
        self.pathfinder = PathFinder(Game(5, 5))

    def teardown_method(self):
        self.pathfinder = None

    def test_find_path_valid(self):
        path = self.pathfinder.find_path()
        if path is not None:
            assert path[0] == self.pathfinder.game.start_pos
            assert path[-1] == self.pathfinder.game.stop_pos
        else:
            pytest.xfail("There is no path from A to B.")


    @pytest.mark.skip(reason="This test is currently not working.")
    def test_non_working_test(self):
        pf = PathFinder(Game(5, 5))
        assert pf.find_path() == [(0, 0)]

    @pytest.mark.skipif(pytest.__version__ > "6.0", reason="This test requires pytest version 6.0 or higher.")
    def test_skip_if_pytest_version_below_6(self):
        pf = PathFinder(Game(5, 5))
        assert not pf.find_path() == [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0)]
        
@pytest.mark.parametrize("move", ['w', 's', 'a', 'd'])
def test_make_move_parametrized(move):
    game = Game(5, 5)
    game.player_pos = (0, 0)
    game.make_move(move)
    assert game.player_pos in [(1, 0), (0, 1), (0, 0)]