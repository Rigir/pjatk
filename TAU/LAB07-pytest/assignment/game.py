import random

class Board:
    def __init__(self, A, B, obstacle_prob=0.2):
        self.A = A
        self.B = B
        self.obstacle_prob = obstacle_prob
        self.fields = [['_' for x in range(A)] for y in range(B)]
        self.obstacles = []

    def generate_board(self):
        start_x, start_y = random.randint(0, self.A-1), random.choice([0, self.B-1])
        stop_x, stop_y = random.randint(0, self.A-1), random.choice([0, self.B-1])
        while (stop_x == start_x and stop_y == start_y) or (abs(stop_x - start_x) + abs(stop_y - start_y) <= 1):
            stop_x, stop_y = random.randint(0, self.A-1), random.choice([0, self.B-1])

        self.fields[start_y][start_x] = 'A'
        self.fields[stop_y][stop_x] = 'B'

        for x in range(self.A):
            for y in range(self.B):
                if (x, y) == (start_x, start_y) or (x, y) == (stop_x, stop_y):
                    continue
                elif random.random() < self.obstacle_prob:
                    self.fields[y][x] = 'X'
                    self.obstacles.append((x, y))

    def print_board(self):
        for row in self.fields:
            print(' '.join(row))

    def get_obstacles(self):
        return self.obstacles

    def is_valid_move(self, x, y):
        if x < 0 or x >= self.A or y < 0 or y >= self.B:
            return False
        elif self.fields[y][x] == 'X':
            return False
        else:
            return True
            
            
class Game:
    def __init__(self, A, B):
        self.board = Board(A, B)
        self.board.generate_board()
        self.obstacles = self.board.get_obstacles()
        self.start_pos = self.find_char('A')
        self.stop_pos =  self.find_char('B')
        self.player_pos = self.start_pos
        self.game_over = False

    def start(self):
        print("Witaj w grze Prosta Gra!")
        self.board.print_board()
        while not self.game_over:
            move = input("Podaj kierunek ruchu (w - góra, s - dół ,a - lewo, d - prawo): ")
            if move in ['w', 's', 'a', 'd']:
                self.make_move(move)
            else:
                print("Nieprawidłowy kierunek ruchu!")
            

    def make_move(self, move):
        dx, dy = 0, 0
        if move == 'w':
            dy = -1
        elif move == 's':
            dy = 1
        elif move == 'a':
            dx = -1
        elif move == 'd':
            dx = 1
        else:                
            self.board.fields[self.player_pos[1]][self.player_pos[0]] = 'O'
            self.board.print_board()
            print("Nieprawidłowy kierunek ruchu!")
            return
        new_x = self.player_pos[0] + dx
        new_y = self.player_pos[1] + dy
        if self.board.is_valid_move(new_x, new_y):
            if self.player_pos == self.start_pos:
                self.board.fields[self.player_pos[1]][self.player_pos[0]] = 'A'
            else:
                self.board.fields[self.player_pos[1]][self.player_pos[0]] = ' '
            self.player_pos = (new_x, new_y)
            if self.player_pos == self.stop_pos:
                print("Gratulacje, udało Ci się dotrzeć do końca!")
                self.game_over = True
            else:
                self.board.fields[self.player_pos[1]][self.player_pos[0]] = 'O'
                self.board.print_board()
        else:
            print("Nie można wykonać tego ruchu, spróbuj ponownie.")

    def find_char(self, char):
        for y, row in enumerate(self.board.fields):
            for x, val in enumerate(row):
                if val == char:
                    return (x, y)

import heapq

class PathFinder:
    def __init__(self, game):
        self.game = game
        self.board = game.board
        self.obstacles = self.board.get_obstacles()

    def find_path(self):
        start = self.game.start_pos
        end = self.game.stop_pos
        queue = [(0, start, [start])]
        visited = set()
        while queue:
            cost, current, path = heapq.heappop(queue)
            if current == end:
                return path
            if current in visited:
                continue
            visited.add(current)
            for neighbor in self.get_neighbors(current[0], current[1]):
                if neighbor not in visited and self.game.board.is_valid_move(neighbor[0], neighbor[1]):
                    new_cost = cost + 1
                    heapq.heappush(queue, (new_cost, neighbor, path + [neighbor]))
        return None

    def get_neighbors(self, x, y):
        neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
        return [(nx, ny) for (nx, ny) in neighbors if nx >= 0 and nx < self.board.A and ny >= 0 and ny < self.board.B and (nx, ny) not in self.obstacles]

    def print_path(self):
        path = self.find_path()
        if path is None:
            print("No path found!")
            return
        board_copy = self.board.fields.copy()
        for x, y in path[1:-1]:
            board_copy[y][x] = '*'
        
        for row in board_copy:
            print(' '.join(row))

            
if __name__ == '__main__':
    game = Game(5, 5)
    # game.board.print_board()
    
    path_finder = PathFinder(game)
    path = path_finder.find_path()
    path_finder.print_path()
    if path is not None:
        print("The path from A to B is:", path)
    else:
        print("There is no path from A to B.")