![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
* (Dodatkowe) - Bazy danych - w ramach zadania należy użyć projektu lub utworzyć projekt, który będzię umożliwiał komunikację z bazą danych. Należy przetestować CRUD, czyli cztery podstawowe rodzaje zapytań, dostępne w bazach danych. Dla każdego przygotować po 2 testy.