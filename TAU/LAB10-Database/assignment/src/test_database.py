import pytest
from database import (
    DatabaseConnection,
    create_movie,
    get_movies,
    update_movie_title,
    delete_movie
)

@pytest.fixture
def database():
    with DatabaseConnection() as cursor:
        cursor.execute("DROP TABLE IF EXISTS movies")
        cursor.execute("CREATE TABLE movies (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255), director VARCHAR(255), year INT)")

@pytest.fixture
def populate_database(database):
    movies_data = [
        ("Movie 1", "Director 1", 2021),
        ("Movie 2", "Director 2", 2022),
        ("Movie 3", "Director 3", 2023),
    ]
    with DatabaseConnection() as cursor:
        for movie in movies_data:
            cursor.execute("INSERT INTO movies (title, director, year) VALUES (%s, %s, %s)", movie)

def test_create_movie(database):
    create_movie("New Movie", "New Director", 2023)
    with DatabaseConnection() as cursor:
        cursor.execute("SELECT * FROM movies")
        movies = cursor.fetchall()
    assert len(movies) == 1
    assert movies[0][1] == "New Movie"
    assert movies[0][2] == "New Director"
    assert movies[0][3] == 2023

def test_get_movies(database, populate_database):
    movies = get_movies()
    assert len(movies) == 3

def test_update_movie_title(database, populate_database):
    movie_id = 2
    new_title = "Updated Movie"
    update_movie_title(movie_id, new_title)
    with DatabaseConnection() as cursor:
        cursor.execute("SELECT * FROM movies WHERE id = %s", (movie_id,))
        movie = cursor.fetchone()
    assert movie[1] == new_title

def test_delete_movie(database, populate_database):
    movie_id = 2
    delete_movie(movie_id)
    with DatabaseConnection() as cursor:
        cursor.execute("SELECT * FROM movies")
        movies = cursor.fetchall()
    assert len(movies) == 2