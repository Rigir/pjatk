import mysql.connector

class DatabaseConnection:
    def __enter__(self):
        self.conn = mysql.connector.connect(
            host="lab10-db",
            user="root",
            password="p@ssw0rd1",
            database="moviesdb"
        )
        self.cursor = self.conn.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()

def create_movie(title, director, year):
    with DatabaseConnection() as cursor:
        cursor.execute("INSERT INTO movies (title, director, year) VALUES (%s, %s, %s)", (title, director, year))

def get_movies():
    with DatabaseConnection() as cursor:
        cursor.execute("SELECT * FROM movies")
        movies = cursor.fetchall()
    return movies

def update_movie_title(movie_id, new_title):
    with DatabaseConnection() as cursor:
        cursor.execute("UPDATE movies SET title = %s WHERE id = %s", (new_title, movie_id))

def delete_movie(movie_id):
    with DatabaseConnection() as cursor:
        cursor.execute("DELETE FROM movies WHERE id = %s", (movie_id,))