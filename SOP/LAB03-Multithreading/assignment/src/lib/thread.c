#include "thread.h"

#include <string.h>
#include <sys/stat.h>
#include "process.h"

struct thread_t* bt_search(struct thread_t* root, long pid) {
  if (root == NULL || root->file->pid == pid)
    return root;
  else if (pid > root->file->pid)
    return bt_search(root->right_child, pid);
  else
    return bt_search(root->left_child, pid);
}

struct thread_t* bt_insert(struct thread_t* root, struct file_t* file) {
  if (root == NULL)
    return _new_thread_t(file);
  else if (file->pid > root->file->pid)
    root->right_child = bt_insert(root->right_child, file);
  else
    root->left_child = bt_insert(root->left_child, file);
  return root;
}

struct thread_t* bt_remove(struct thread_t* root, long pid) {
  if (root == NULL)
    return NULL;
  if (pid > root->file->pid)
    root->right_child = bt_remove(root->right_child, pid);
  else if (pid < root->file->pid)
    root->left_child = bt_remove(root->left_child, pid);
  else {
    if (root->left_child == NULL && root->right_child == NULL) {
      pthread_cancel(root->id);
      free(root->file->path);
      free(root);
      return NULL;
    } else if (root->left_child == NULL || root->right_child == NULL) {
      struct thread_t* temp;
      if (root->left_child == NULL)
        temp = root->right_child;
      else
        temp = root->left_child;
      pthread_cancel(root->id);
      free(root->file->path);
      free(root);
      return temp;
    } else {
      struct thread_t* temp = _find_minimum(root->right_child);
      root->file = temp->file;
      root->right_child = bt_remove(root->right_child, temp->file->pid);
    }
  }
  return root;
}

void bt_inorder(struct thread_t* root) {
  if (root != NULL) {
    bt_inorder(root->left_child);
    printf("PID %d ", root->file->pid);
    printf("Path: %s ", root->file->path);
    printf("LU: %d \n", root->file->lastUpdate);
    bt_inorder(root->right_child);
  }
}

void bt_deleteTree(struct thread_t* root) {
  if (root != NULL) {
    bt_deleteTree(root->left_child);
    bt_deleteTree(root->right_child);
    pthread_cancel(root->id);
    free(root->file->path);
    free(root);
  }
}

struct thread_t* _find_minimum(struct thread_t* root) {
  if (root == NULL)
    return NULL;
  else if (root->left_child != NULL)
    return _find_minimum(root->left_child);
  return root;
}

struct thread_t* _new_thread_t(struct file_t* file) {
  struct thread_t* p;
  p = malloc(sizeof(struct thread_t));
  p->file = file;
  p->left_child = NULL;
  p->right_child = NULL;

  pthread_create(&p->id, 0, fileListenig, p->file);
  return p;
}

void* fileListenig(void* value) {
  struct stat attrib;
  struct file_t* file = (struct file_t*)value;
  countChange(&_mutex);
  while (_shutdown_flag) {
    if (!stat(file->path, &attrib)) {
      if (attrib.st_mtim.tv_sec > file->lastUpdate.tv_sec) {
        file->lastUpdate = attrib.st_mtim;
        countChange(&_mutex);
      }
    } else {
      countChange(&_mutex);
      pthread_exit(file);
    }
    sleep(1);
  }
  pthread_exit(file);
}

void countChange(sem_t* mutex) {
  sem_wait(mutex);
  _counter++;
  sem_post(mutex);
}
