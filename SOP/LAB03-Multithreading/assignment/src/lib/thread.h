#ifndef THREAD_H
#define THREAD_H

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "file.h"

struct thread_t {
  pthread_t id;
  struct file_t* file;
  struct thread_t *right_child, *left_child;
};

struct thread_t* bt_search(struct thread_t* root, long pid);
struct thread_t* bt_insert(struct thread_t* root, struct file_t* file);
struct thread_t* bt_remove(struct thread_t* root, long pid);
void bt_inorder(struct thread_t* root);
void bt_deleteTree(struct thread_t* root);
void* fileListenig(void* value);
void countChange(sem_t* mutex);

struct thread_t* _new_thread_t(struct file_t* file);
struct thread_t* _find_minimum(struct thread_t* root);
#endif  // THREAD_H