#include "file.h"

#include <dirent.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

char* getPath(char* dirName, char* fileName) {
  char* result = malloc(strlen(dirName) + strlen(fileName) + 1);
  strcpy(result, dirName);
  strcat(result, "/");
  strcat(result, fileName);
  return result;
}

struct thread_t* handleFiles(char* path, struct thread_t* root) {
  DIR* dir;
  struct dirent* entry;
  if ((dir = opendir(path)) != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
        char* file_path = getPath(path, entry->d_name);
        struct stat attrib;
        if (!stat(file_path, &attrib)) {
          if (bt_search(root, (long)attrib.st_ino) == NULL) {
            // printf("!\n");
            struct file_t* file = malloc(sizeof(struct file_t));
            file->pid = (long)attrib.st_ino;
            file->path = file_path;
            file->lastUpdate = attrib.st_mtim;
            root = bt_insert(root, file);
          }
        } else {
          perror(file_path);
        }
      }
    }
    closedir(dir);
  } else {
    perror("");
  }
  return root;
}

struct thread_t* findDeletedFile(struct thread_t* root) {
  if (root == NULL) {
    return NULL;
  } else if (access(root->file->path, F_OK) != 0) {
    return root;
  } else {
    struct thread_t* x = findDeletedFile(root->left_child);
    if (x)
      return x;
    return findDeletedFile(root->right_child);
  }
}
