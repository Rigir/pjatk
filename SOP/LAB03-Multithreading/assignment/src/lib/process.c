#include "process.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

sem_t _mutex;
long _counter = 0;
volatile sig_atomic_t _shutdown_flag = 1;

void _print_event(int sig) {
  sem_wait(&_mutex);
  printf("\n counter: %d\n\n", _counter);
  fflush(stdout);
  sem_post(&_mutex);
  alarm(ALARM_TIME_S);
}

pid_t createProcess(char* path) {
  pid_t c_pid = fork();
  switch (c_pid) {
    case -1:
      perror("fork");
      exit(EXIT_FAILURE);
      break;

    case 0:
      printf("\n C: %d, P: %d F: %s\n", getpid(), getppid(), path);
      _prepareSigaction();
      sem_init(&_mutex, 0, 1);
      signal(SIGALRM, (void (*)(int))_print_event);
      alarm(ALARM_TIME_S);
      struct thread_t *head = NULL, *find = NULL;
      while (_shutdown_flag) {
        head = handleFiles(path, head);
        do {
          find = findDeletedFile(head);
          if (find != NULL) {
            pthread_join(find->id, NULL);
            head = bt_remove(head, find->file->pid);
            find = NULL;
          }
        } while (find != NULL);

        printf("==========\n");
        bt_inorder(head);
        printf("==========\n");
        sleep(5);
      }
      alarm(0);
      bt_deleteTree(head);
      exit(EXIT_SUCCESS);
      break;

    default:
      return c_pid;
      break;
  }
}

pid_t stopProcess(pid_t pid) {
  int ret, wstatus;
  if (pid != 0) {
    ret = kill(pid, SIGTERM);
    if (waitpid(pid, &wstatus, WUNTRACED | WCONTINUED) == -1) {
      perror("waitpid");
      exit(EXIT_FAILURE);
    }
  }
  return 0;
}

void _prepareSigaction() {
  struct sigaction sigterm_action;
  memset(&sigterm_action, 0, sizeof(sigterm_action));
  sigterm_action.sa_handler = &_cleanupRoutine;
  sigterm_action.sa_flags = 0;

  if (sigfillset(&sigterm_action.sa_mask) != 0) {
    perror("sigfillset");
    exit(EXIT_FAILURE);
  }

  if (sigaction(SIGTERM, &sigterm_action, NULL) != 0) {
    perror("sigaction SIGTERM");
    exit(EXIT_FAILURE);
  }
}

void _cleanupRoutine(int signal_number) {
  _shutdown_flag = 0;
}