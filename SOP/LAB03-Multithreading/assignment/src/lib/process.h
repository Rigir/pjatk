#ifndef PROCESS_H
#define PROCESS_H

#include <semaphore.h>
#include <signal.h>
#include <sys/types.h>
#include "thread.h"

#define ALARM_TIME_S 30

pid_t createProcess(char* path);
pid_t stopProcess(pid_t pid);

// void killAll(pid_t c_pid);
void _prepareSigaction();
void _print_event(int sig);
void _cleanupRoutine(int signal_number);
extern sem_t _mutex;
extern long _counter;
extern volatile sig_atomic_t _shutdown_flag;

#endif  // PROCESS_H