#ifndef FILE_H
#define FILE_H

#include <time.h>
#include "thread.h"

struct file_t {
  unsigned long int pid;
  char* path;
  struct timespec lastUpdate;
};

char* getPath(char* dirName, char* fileName);
struct thread_t* handleFiles(char* path, struct thread_t* root);
struct thread_t* findDeletedFile(struct thread_t* root);

#endif  // FILE_H