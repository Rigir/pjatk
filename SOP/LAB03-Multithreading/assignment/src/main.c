#include <dirent.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib/process.h"

void getInteger(unsigned int* value);

int main() {
  unsigned int value = 0;
  char input[100];
  pid_t pid = 0;

  while (1) {
    do {
      printf(
          "\nPress: \n1-Start listening | 2-Stop listening |  3 - Exit "
          "\nInput: ");
      getInteger(&value);
    } while (value < 1 || value > 3);

    switch (value) {
      case 1:
        if (pid == 0) {
          do {
            printf("Please enter a volid path: ");
            scanf("%s", input);
          } while (!opendir(input));
          pid = createProcess(input);

        } else {
          printf("Process already started!");
          printf("\n P: %d \n", pid);
        }
        break;
      case 2:
        pid = stopProcess(pid);
        break;
      default:
        pid = stopProcess(pid);
        return EXIT_SUCCESS;
    }
  }
}

void getInteger(unsigned int* value) {
  char input[5], *ptr = NULL;
  scanf("%s", input);
  (*value) = strtol(input, &ptr, 10);
}
