section .text
  global main
  extern malloc, free, printf

main:
  push input_msg
  call printf
  add esp, 4

  call read_num
  cmp word[num], 0
    je display_linked_list
  cmp word[num], 22836
    je clean_then_exit

  call create_new_node
  mov ebx, dword[head]
  jmp main

create_new_node:
  pusha
    push dword 8
    call malloc
    add esp, 4

    test eax, eax
    jz clean_then_exit
    
    mov ebx, dword[head]
    mov dword[eax], ebx
    mov ebx, 0
    mov bx, word[num]
    mov dword[eax+4], ebx
    mov dword[head], eax
  popa
  ret

display_linked_list:
    push output_msg_info
    call printf
    add esp, 4

    ; Check if head is not null
    mov eax, head 
    cmp dword[eax], 0
    je main

    mov ebp, dword[head]
    _display_linked_list_loop:
      push dword[ebp+4]
      push output_msg_number
      call printf
      add esp, 8

      cmp dword[ebp], 0
      je main

      mov ebp, dword[ebp]
      jmp _display_linked_list_loop

free_linked_list:
  pusha
    ; Check if head is not null
    mov eax, head 
    cmp dword[eax], 0
    je _free_linked_list_end

    mov eax, dword[head]
    _free_linked_list_loop:
      mov ebp, dword[eax]
      push eax
      call free
      add esp, 4

      cmp ebp, 0
      je _free_linked_list_end

      mov eax, ebp
      jmp _free_linked_list_loop
  _free_linked_list_end:
  popa
  ret

read_num:
  pusha
    mov word[num], 0
    mov eax, num
    read_loop:
      call read_char

      cmp byte[char], 0xa ; LF - Line Feed
        je end_read
      cmp byte[char], 0x30 
        jl read_loop
      cmp byte[char], 0x39 
        jg read_loop

      mov ax, word[num]
      mov bx, 10
      mul bx
      mov bl, byte[char]
      sub bl, 0x30
      mov bh, 0
      add ax, bx
      mov word[num], ax

      jmp read_loop
    end_read:
  popa
  ret

read_char: 
  pusha
    mov eax, 3    ; sys_read
    mov ebx, 0    ; File descriptor (stdin)
    mov ecx, char ; pointer to output
    mov edx, 1    ; size of buffer
    int 0x80
  popa
  ret

clean_then_exit:
  call free_linked_list
  call exit

exit:
  mov eax, 1  ; sys_exit
  mov ebx, 0  ; return code (success) 
  int 0x80    ; call kernel

;Uninitialized data
section .bss 
  char resb 1
  num resw 1
  head resd 1


;initialized data
section .data 
  input_msg db 'Podaj liczbe: ', 0xa, 0x0
  output_msg_info db 'Podane liczby: ', 0xa, 0x0
  output_msg_number db '%d', 0xa, 0x0
