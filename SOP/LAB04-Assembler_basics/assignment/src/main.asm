section .text
  global main
    
main:
  mov edx, input_len ; Set length of text
  mov ecx, input_msg ; set message start pointer
  call print_label
  call read_num
  cmp word[num], 22836
    jne main
  jmp exit


read_num:
  pusha
    mov word[num], 0
    mov eax, num
    read_loop:
      call read_char

      cmp byte[temp], 0xa ; LF - Line Feed
        je end_read
      cmp byte[temp], 0x30 
        jl read_loop
      cmp byte[temp], 0x39 
        jg read_loop

      mov ax, word[num]
      mov bx, 10
      mul bx
      mov bl, byte[temp]
      sub bl, 0x30
      mov bh, 0
      add ax, bx
      mov word[num], ax

      jmp read_loop
    end_read:
  popa
  ret

read_char: 
  pusha
    mov eax, 3    ; sys_read
    mov ebx, 0    ; File descriptor (stdin)
    mov ecx, temp ; pointer to output
    mov edx, 1    ; size of buffer
    int 0x80
  popa
  ret

print_label: ; edx - length of text, ecx - message start pointer
  mov eax, 4  ; sys_write
  mov ebx, 1  ; File descriptor (stdout)
  int 0x80    ; call kernel
  ret

exit:
  mov eax, 1  ; sys_exit
  mov ebx, 0  ; return code (success) 
  int 0x80    ; call kernel

;initialized data
section .data 
  input_msg db 'Podaj liczbe: ', 0xa
  input_len equ $ - input_msg

;Uninitialized data
section .bss 
  num resw 1
  temp resb 1