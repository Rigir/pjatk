#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lib/bobblesort.h"
#include "lib/quicksort.h"

void getInteger(unsigned int* value);
int* getArraySize(unsigned int* size);
void randomValues(int* arr, unsigned int size);
void displayArray(int* arr, unsigned int size);
void inputValuesFromUser(int* arr, unsigned int size);

int main() {
  unsigned int* arr = NULL;
  unsigned int value = 0;

  do {
    printf("\nPress: \n1-Type numbers | 2-Random numbers | 3-Exit \nInput:");
    getInteger(&value);
  } while (value < 1 || value > 3);

  switch (value) {
    case 1:
      arr = getArraySize(&value);
      inputValuesFromUser(arr, value);
      bubblesort(arr, value);
      break;
    case 2:
      arr = getArraySize(&value);
      randomValues(arr, value);
      quickSort(arr, 0, value - 1);
      break;
    default:
      break;
  }
  if (arr != NULL) {
    displayArray(arr, value);
    free(arr);
  }
  return 0;
}

void getInteger(unsigned int* value) {
  char input[5], *ptr = NULL;
  scanf("%s", input);
  (*value) = strtol(input, &ptr, 10);
}

int* getArraySize(unsigned int* size) {
  do {
    printf("\nEnter the size of array: ");
    getInteger(size);
  } while (size <= 0);
  return (int*)malloc(sizeof(int) * (*size + 3));
}

void inputValuesFromUser(int* arr, unsigned int size) {
  unsigned int input = 0;
  for (int i = 0; i < size; i++) {
    printf("\nEnter value: ");
    getInteger(&input);
    arr[i] = input;
  }
}

void randomValues(int* arr, unsigned int size) {
  srand((unsigned)time(NULL));
  for (int i = 0; i < size; i++) {
    int val = rand() % 100 + 1;
    arr[i] = val;
  }
}

void displayArray(int* arr, unsigned int size) {
  for (int i = 0; i < size; i++) {
    printf("\n%d: %ld", i, *(arr + i));
  }
}
