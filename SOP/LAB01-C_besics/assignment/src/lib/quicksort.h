#ifndef QUICKSORT_H
#define QUICKSORT_H

int partition(int *arr, int low, int high);
void quickSort(int *arr, int low, int high);

#endif // QUICKSORT_H