#ifndef BOBBLESORT_H
#define BOBBLESORT_H

void swap(int *x, int *y);
void bubblesort(int *arr, unsigned int size);

#endif  // BOBBLESORT_H