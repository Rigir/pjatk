#include "bobblesort.h"
#include "supporting.h"

void bubblesort(int *arr, unsigned int size)
{
    for (int i = 0; i < size - 1; i++)
        for (int j = 0; j < size - i - 1; j++)
            if (arr[j] > arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
}