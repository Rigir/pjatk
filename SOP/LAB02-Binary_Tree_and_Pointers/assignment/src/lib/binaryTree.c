#include "binaryTree.h"

struct node* bt_search(struct node* root, int x) {
  if (root == NULL || root->data == x)
    return root;
  else if (x > root->data)
    return bt_search(root->right_child, x);
  else
    return bt_search(root->left_child, x);
}

struct node* _find_minimum(struct node* root) {
  if (root == NULL)
    return NULL;
  else if (root->left_child != NULL)
    return _find_minimum(root->left_child);
  return root;
}

struct node* _new_node(int x) {
  struct node* p;
  p = malloc(sizeof(struct node));
  p->data = x;
  p->left_child = NULL;
  p->right_child = NULL;

  return p;
}

struct node* bt_insert(struct node* root, int x) {
  if (root == NULL)
    return _new_node(x);
  else if (x > root->data)
    root->right_child = bt_insert(root->right_child, x);
  else
    root->left_child = bt_insert(root->left_child, x);
  return root;
}

struct node* bt_remove(struct node* root, int x) {
  if (root == NULL)
    return NULL;
  if (x > root->data)
    root->right_child = bt_remove(root->right_child, x);
  else if (x < root->data)
    root->left_child = bt_remove(root->left_child, x);
  else {
    if (root->left_child == NULL && root->right_child == NULL) {
      free(root);
      return NULL;
    } else if (root->left_child == NULL || root->right_child == NULL) {
      struct node* temp;
      if (root->left_child == NULL)
        temp = root->right_child;
      else
        temp = root->left_child;
      free(root);
      return temp;
    } else {
      struct node* temp = _find_minimum(root->right_child);
      root->data = temp->data;
      root->right_child = bt_remove(root->right_child, temp->data);
    }
  }
  return root;
}

void bt_inorder(struct node* root) {
  if (root != NULL) {
    bt_inorder(root->left_child);
    printf(" %d ", root->data);
    bt_inorder(root->right_child);
  }
}

void bt_deleteTree(struct node* root) {
  if (root != NULL) {
    bt_deleteTree(root->left_child);
    bt_deleteTree(root->right_child);
    free(root);
  }
}
