#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <stdio.h>
#include <stdlib.h>

struct node {
  int data;
  struct node *right_child, *left_child;
};

struct node* bt_search(struct node* root, int x);
struct node* bt_insert(struct node* root, int x);
struct node* bt_remove(struct node* root, int x);
void bt_inorder(struct node* root);
void bt_deleteTree(struct node* root);

struct node* _new_node(int x);
struct node* _find_minimum(struct node* root);
#endif  // BINARY_TREE_H