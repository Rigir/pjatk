#include <stdio.h>
#include <stdlib.h>
#include "lib/binaryTree.h"

void getInteger(unsigned int* value);

int main() {
  struct node *root = NULL, *p_search_node = NULL;
  unsigned int value = 0;

  while (1) {
    bt_inorder(root);
    do {
      printf(
          "\nPress: \n1-Add node | 2-Delete node | 3-Find node | 4 - Exit "
          "\nInput:");
      getInteger(&value);
    } while (value < 1 || value > 4);

    switch (value) {
      case 1:
        printf("Please enter a value to insert: ");
        getInteger(&value);
        if (root == NULL) {
          root = _new_node(value);
        } else {
          root = bt_insert(root, value);
        }
        break;
      case 2:
        printf("Please enter a value to remove: ");
        getInteger(&value);
        root = bt_remove(root, value);
        break;
      case 3:
        printf("Please enter a value to find:  ");
        getInteger(&value);
        p_search_node = bt_search(root, value);
        if (p_search_node != NULL) {
          printf("\nFound node\n\n");
        } else {
          printf("\nNode not found\n\n");
        }
        break;
      default:
        free(p_search_node);
        bt_deleteTree(root);
        return 0;
    }
  }
}

void getInteger(unsigned int* value) {
  char input[5], *ptr = NULL;
  scanf("%s", input);
  (*value) = strtol(input, &ptr, 10);
}