create table hobbies (
    id   serial primary key,
    hobby varchar(32),
    ranking int
);

insert into hobbies (hobby, ranking) values ('Hiking', 4),('Backpacking', 2),('Camping', 9),('Hunting', 3),('Fishing', 7);