const express = require("express");
const app = express();

// postgres://postgres:postgres@db_4_03:5432/postgres
const { Pool } = require("pg");
const pool = new Pool({
  database: "postgres",
  user: "postgres",
  password: "postgres",
  host: "db_4_03",
  port: 5432,
});

app.listen(3000, "0.0.0.0", () => {
  console.log("Application listening at 0.0.0.0:3000");
});

app.get("/", async (req, res) => {
  res.send(`<div>
    <a href="/all">Wyświetl wszystkie dane w tabeli</a><br>
    <a href="/nth/4">Wyświetl n-ty wiersz w tabeli</a><br>
    <a href="/avg">Policzenie średniej z pola typu liczba całkowita</a>
    </div>`);
});

app.get("/all", async (req, res) => {
  const result = await pool.query("SELECT * FROM hobbies");
  res.send(result.rows);
});

app.get("/nth/:n", async (req, res) => {
  const result = await pool.query(
    "SELECT * FROM hobbies where id =  " + req.params["n"]
  );
  res.send(result.rows);
});

app.get("/avg", async (req, res) => {
  const result = await pool.query("SELECT avg(ranking) FROM hobbies");
  res.send(result.rows);
});
