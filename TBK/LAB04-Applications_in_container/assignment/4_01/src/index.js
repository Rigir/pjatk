const express = require("express");
const app = express();

app.listen(3000, "0.0.0.0", () => {
  console.log("Application listening at 0.0.0.0:3000");
});

app.get("/add/:x/:y", (req, res) => {
  let x = parseFloat(req.params.x);
  let y = parseFloat(req.params.y);

  res.send(`${x + y}`);
});

app.get("/mul/:x/:y", (req, res) => {
  let x = parseFloat(req.params.x);
  let y = parseFloat(req.params.y);

  res.send(`${x * y}`);
});

app.get("/div/:x/:y", (req, res) => {
  let x = parseFloat(req.params.x);
  let y = parseFloat(req.params.y);
  res.send(y === 0 ? `Error: div by zero` : `${x / y}`);
});
