![PJATK](../../.assets/banner.png)
=========

Komendy: 
- docker ls
  - Wyświetla listę kontenerów
- docker run
  - Odpala kontener
- docker rm -f
  - Usuwa kontener o danej nazwie
- docker exec
  - Pozwala odpalić w kontenerze dany program
- docker inspect
  - Wyświetla szczegółowe informacje dotyczące kontenera
- docker attach
  - Dołącza standardowe wejście i wyjście do terminala
- docker logs
  - Wyświetla logi danego kontenera 
- docker pull
  - Pobranie kontenera 
  
  
- docker start: 
  - uruchamia kontener bez przekierowania strumienia I/O.
- docker start -a 
  - uruchamia kontener z przekierowania strumienia I/O.
- docker rm 
  - Remove one or more containers
- docker system prune 
  - Remove all unused containers, networks, images (both dangling and unreferenced), and optionally, volumes