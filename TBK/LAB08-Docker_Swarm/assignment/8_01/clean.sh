docker container rm -f $(docker container ls -aq)
docker service rm numbers_api
docker service rm numbers_web
docker network rm numbers_net
docker swarm leave --force