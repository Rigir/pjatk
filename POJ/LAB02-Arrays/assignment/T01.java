package assignment;

import java.util.Scanner;

public class T01 {
    public static void main(String[] args) {
        String temp;
        int num;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Input: ");
        num = scanner.nextInt();

        char[] arr = new char[num];

        for (int i = 0; i < arr.length; i++) {
            System.out.format("Input %d: ", i);
            temp = scanner.next();
            arr[i] = temp.charAt(0);
        }

        for (char i : arr) {
            System.out.println(i);
        }
        scanner.close();
    }
}
