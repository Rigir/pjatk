package assignment;

public class T06 {
    public static void main(String[] args) {
        int nwd = nwd(33, 50000); // 5
        System.out.println(nwd);
    }

    private static int nwd(int a, int b) {
        if (b == 0)
            return a;
        return nwd(b, a % b);
    }
}
