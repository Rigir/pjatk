package assignment;

public class T05 {
    public static void main(String[] args) {
        int[][] arr1 = { { 15, 25, 35 }, { 45, 55, 65 } };
        int[][] arr2 = { { 12, 22, 32 }, { 55, 25, 85 } };
        System.out.println("Sum: " + sumArrays(arr1, arr2));
    }

    private static int sumArrays(int[][] arr1, int[][] arr2) {
        int result = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[0].length; j++) {
                result += arr1[i][j] + arr2[i][j];
            }
        }
        return result;
    }
}
