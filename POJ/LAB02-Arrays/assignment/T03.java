package assignment;

import java.util.Arrays;

public class T03 {
    public static void main(String[] args) {
        int[] arr1 = { 1, 2, 3, 4 };
        int[] arr2 = { 5, 6, 7, 8 };
        int[] err = { 9, 10, 11 };
        System.out.println("Diffrent size: " + Arrays.toString(sumArrays(arr1, err)));
        System.out.println("Same size: " + Arrays.toString(sumArrays(arr1, arr2)));
    }

    private static int[] sumArrays(int[] arr1, int[] arr2) {
        if (arr1.length != arr2.length)
            return new int[0];
        int[] temp = new int[arr1.length];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = arr1[i] + arr2[i];
        }
        return temp;
    }
}
