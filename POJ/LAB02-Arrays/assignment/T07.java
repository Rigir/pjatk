package assignment;

public class T07 {
    public static void main(String[] args) {
        int[] arr = { 1, -2, -3, 9 };
        System.out.println("Result: " + sumPositiveNumArrays(arr));
    }

    private static int sumPositiveNumArrays(int[] arr) {
        int result = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0)
                result += arr[i];
        }
        return result;
    }
}
