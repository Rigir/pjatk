package assignment;

import java.util.Arrays;
import java.util.Scanner;

public class T02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Prosze podac wielkosc tablicy");
        int size = scanner.nextInt();
        System.out.println("Podaj wartosc pierwszego elementu");
        int first = scanner.nextInt();

        // 5,1 -> [1,2,4,6,8]
        int[] array = new int[size];
        array[0] = first;
        int index = 1;
        // 5,1 -> [1 -> 1+5=6
        for (int i = first + 1; index < size; i++) {
            if (i % 2 == 0) {
                array[index] = i;
                index++;
            }
        }
        System.out.println(Arrays.toString(array));

        System.out.println(String.format("Min: %d", min(array)));
        System.out.println(String.format("Max: %d", max(array)));
        System.out.println(String.format("Suma : %d", sum(array)));
        System.out.println(String.format("Srednia : %.3f", avg(array)));
        System.out.println(String.format("Mediana : %.2f", med(array)));
        scanner.close();
    }

    private static double med(int[] array) {
        if (array.length % 2 == 0) {
            // [1,2,3,4] -> size/2=2(3) + size/2 -1 = 1(2) / 2 = 2.5
            return (array[array.length / 2] + array[array.length / 2 - 1]) / 2.0;
        } else {
            // [1,2,3] -> size/2 = 3/2->1(2)
            return array[array.length / 2];
        }
    }

    private static double avg(int[] array) {
        return sum(array) / (double) array.length;
    }

    private static int sum(int[] array) {
        int sum = 0;
        for (int number : array) {
            sum += number;
        }
        return sum;
    }

    private static int max(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    private static int min(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }
}
