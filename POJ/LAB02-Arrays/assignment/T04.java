package assignment;

import java.util.Arrays;

public class T04 {
    public static void main(String[] args) {
        int[][] array = new int[2][4];
        int number = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = number;
                number += 2;
            }
        }
        display(array);

        System.out.println(Arrays.toString(array[0]) + Arrays.toString(array[1]));
    }

    private static void display(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
