package assignment.T04;

public class Assignment {
    private int a, b, c, d, e;

    public Assignment(int a, int b, int c, int d, int e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }

    @Override
    public String toString() {
        return String.format("A: %s, B: %s, C: %s, D: %s, E: %s, ", getA(), getB(), getC(), getD(), getE());
    }

    private int getA() {
        return a;
    }

    private int getB() {
        return b;
    }

    private int getC() {
        return c;
    }

    private int getD() {
        return d;
    }

    private int getE() {
        return e;
    }
}
