package assignment.T02;

public class Main {
    public static void main(String[] args) {
        Bookshoop bookshoop = new Bookshoop();
        Bakery bakery = new Bakery();
        Shop shop = new Shop();

        System.out.println(bookshoop.getInformation());
        System.out.println(bakery.getInformation());
        System.out.println(shop.getInformation());
    }
}
