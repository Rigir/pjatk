package assignment.T02;

public class Bookshoop extends Shop {
    private String[] products = { "ksiazka", "olowek" };

    public Bookshoop() {
        super();
        setAddress("Babilon");
        setSize(15);
    }

    @Override
    public String getInformation() {
        String result = getAddress() + " " + getSize();
        for (String product : products) {
            result += " " + product;
        }
        return result;
    }
}
