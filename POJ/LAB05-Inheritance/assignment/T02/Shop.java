package assignment.T02;

public class Shop {
    private String address;
    private int size;

    public Shop() {
        setAddress("Warszawa");
        setSize(10);
    }

    public String getInformation() {
        return getAddress() + " " + getSize();
    }

    // GET
    public String getAddress() {
        return address;
    }

    public int getSize() {
        return size;
    }

    // SET
    public void setAddress(String address) {
        this.address = address;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
