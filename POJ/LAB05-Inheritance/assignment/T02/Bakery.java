package assignment.T02;

public class Bakery extends Shop {
    private String[] products = { "bulka", "ciasto" };

    public Bakery() {
        super();
        setAddress("Berlin");
        setSize(20);
    }

    @Override
    public String getInformation() {
        String result = getAddress() + " " + getSize();
        for (String product : products) {
            result += " " + product;
        }
        return result;
    }
}