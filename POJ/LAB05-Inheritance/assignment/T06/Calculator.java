package assignment.T06;

public class Calculator {
    public Double calculate(Double x) {
        return Math.pow(x, 2);
    }

    public Double calculate(Double x, Double y) {
        return x * y;
    }

    public Double calculate(Double x, Double y, Double z) {
        return x + y + z;
    }
}
