package assignment.T06;

public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        System.out.println(calc.calculate(2.0));
        System.out.println(calc.calculate(2.0, 3.0));
        System.out.println(calc.calculate(2.0, 3.0, 4.0));
    }
}
