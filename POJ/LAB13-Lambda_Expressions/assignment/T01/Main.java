package T01;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        System.out.println(nextOddRegular(100)); // 101
        System.out.println(nextOddRegular(103)); // 105

        System.out.println(nextOddLamba.apply(100L));
        System.out.println(nextOddLamba.apply(103L));

        System.out.println(doesContainA.apply("Ala"));
        System.out.println(doesContainA.apply("Kot"));
    }

    public static long nextOddRegular(long x) {
        return x % 2 == 0 ? x + 1 : x + 2; // 100 -> 100+1 || 101 -> 101+2
    }

    public static Function<Long, Long> nextOddLamba = x -> x % 2 == 0 ? x + 1 : x + 2;

    public static Function<String, Boolean> doesContainA = x -> x.contains("A") || x.contains("a");
}
