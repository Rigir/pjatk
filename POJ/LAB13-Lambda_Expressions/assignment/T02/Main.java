package T02;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        System.out.println(findLength.apply("123456789"));
        System.out.println(findLength.apply("123456"));
    }

    public static Function<String, Integer> findLength = x -> x.length();
}
