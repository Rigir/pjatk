package T06;

import java.util.stream.Stream;

public class Main {
        private static final String[] IMIONA = { "Anna", "Bogdan", "Karol", "Daniel", "Tomasz", "Beata", "Daniela",
                        "Tomasz", "Jan", "Helena", "Tymoteusz", "Karol", "Roman" };

        public static void main(String[] args) {
                long sum = Stream.of(IMIONA).filter(imie -> imie.length() > 6).peek(System.out::println)
                                .mapToInt(imie -> imie.length()).peek(System.out::println).summaryStatistics().getSum();
                System.out.println(sum);
        }
}
