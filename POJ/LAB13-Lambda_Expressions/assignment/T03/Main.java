package T03;

public class Main {
    public static void main(String[] args) {
        MathEquationInterface equation;

        int a = 2;
        int b = 3;
        int c = 4;

        equation = new MathEquationInterface() {
            @Override
            public int calculate(int x) {
                return a * x * x + b * x + c;
            }
        };

        System.out.println(equation.calculate(1));
        System.out.println(equation.calculate(10));

        MathEquationInterface equation2 = new MathEquationInterface() {
            @Override
            public int calculate(int x) {
                return 10 * x * x + b * x + c;
            }
        };

        System.out.println(equation2.calculate(1));
    }
}
