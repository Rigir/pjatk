package T03;

@FunctionalInterface
public interface MathEquationInterface {
    int calculate(int x);
}
