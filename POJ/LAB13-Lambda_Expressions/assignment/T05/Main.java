package T05;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

        private final static Integer[] liczby = { 1, 2, 3, 10, 222, 35, 47, 50, 88, 300, 11, 35, 22, 19 };

        public static void main(String[] args) {
                // A.
                AtomicInteger suma = new AtomicInteger();
                Arrays.stream(liczby).forEach(x -> suma.addAndGet(x));
                System.out.println("Suma wszystkich elementow = " + suma);

                // A. alternatywa
                long suma2 = Arrays.stream(liczby).mapToInt(Integer::intValue).summaryStatistics().getSum();
                System.out.println("Suma wszystkich elementow = " + suma2);

                // B.
                long sumaParzyste = Arrays.stream(liczby).filter(liczba -> liczba % 2 == 0).mapToInt(Integer::intValue)
                                .summaryStatistics().getSum();
                System.out.println("Suma parzystych elementow = " + sumaParzyste);

                // C.
                long sumaNieParzyste = Arrays.stream(liczby).filter(liczba -> liczba % 2 != 0)
                                .mapToInt(Integer::intValue).summaryStatistics().getSum();
                System.out.println("Suma nieparzystych elementow = " + sumaNieParzyste);

                // D.
                System.out.println("Posortowane, bez powtorzen");
                Arrays.stream(liczby).sorted().distinct().forEach(System.out::println);

                // E.
                double srednia = Arrays.stream(liczby).mapToInt(Integer::intValue).summaryStatistics().getAverage();
                System.out.println("Srednia wszystkich elementow = " + srednia);
        }
}
