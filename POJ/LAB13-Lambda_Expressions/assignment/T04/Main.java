package T04;

import java.util.stream.Stream;

public class Main {
        private static final String[] IMIONA = { "Anna", "Bogdan", "Karol", "Daniel", "Tomasz", "Beata", "Daniela",
                        "Tomasz", "Jan", "Helena", "Tymoteusz", "Karol", "Roman" };

        public static void main(String[] args) {
                // A.
                System.out.println("A) Tylko imiona Damskie");
                Stream.of(IMIONA).filter(imie -> imie.endsWith("a")).forEach(System.out::println);

                // B.
                System.out.println("B) Imiona meskie bez powtorzen");
                Stream.of(IMIONA).filter(imie -> !imie.endsWith("a")).distinct().forEach(System.out::println);

                // C.
                System.out.println("C) Wyświetl ilość imion, które mają mniej niż 6 liter – bez powtórzeń");
                Stream.of(IMIONA).filter(imie -> imie.length() < 6).distinct().forEach(System.out::println);

                // D.
                System.out.println("D) Wyświetl imiona męskie w kolejności alfabetycznej bez powtórzeń");
                Stream.of(IMIONA).filter(imie -> !imie.endsWith("a")).distinct().sorted().forEach(System.out::println);

                // E.
                System.out.println(
                                "E) Posortujwszystkie imiona alfabetycznie, wyfiltruj powtórzenia i pomiń pierwszych 5 imion. Następnie wyświetl pozostałe imiona");
                Stream.of(IMIONA).sorted().distinct().skip(5).forEach(System.out::println);
        }
}
