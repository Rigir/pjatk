module com.pjatk.minesweeper {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
    opens com.pjatk.minesweeper to javafx.fxml;
    exports com.pjatk.minesweeper.Controllers;
    opens com.pjatk.minesweeper.Controllers to javafx.fxml;
    exports com.pjatk.minesweeper;
}