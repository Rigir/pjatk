package com.pjatk.minesweeper.Controllers;

import com.pjatk.minesweeper.Events.FieldControllerEvent;
import com.pjatk.minesweeper.Views.MinefieldView;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.layout.StackPane;

import java.net.URL;
import java.util.*;

public class MinefieldController extends MinefieldView implements Initializable {

    @FXML
    private StackPane container;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        newGame();
        createEventFilters();
    }

    protected void newGame(){
        container.getChildren().clear();
        createMinefield();
        renderMinefield();
        container.getChildren().add(this);
    }

    protected void restartOnClick(Boolean state){
        StackPane canvas = displayGameState(state);
        canvas.setOnMouseClicked(e -> newGame());
        container.getChildren().add(canvas);
    }

    private void createEventFilters(){
        addEventFilter(FieldControllerEvent.GAMEOVER, this::handleGameOver);
        addEventFilter(FieldControllerEvent.REVILED, this::handleReviled);
        addEventFilter(FieldControllerEvent.ISWIN, this::handleIsWin);
    }

    private void handleReviled(FieldControllerEvent e) {
        getNeighbors(e.getFieldController()).forEach(FieldController::openField);
    }

    private void handleGameOver(FieldControllerEvent e) {
        restartOnClick(false);
    }

    private void handleIsWin(FieldControllerEvent e) {
        checkWin();
    }

    private void checkWin() {
        emptyFields.setValue(emptyFields.getValue() - 1);
        if (emptyFields.getValue() <= 0) {
            restartOnClick(true);
        }
    }
}
