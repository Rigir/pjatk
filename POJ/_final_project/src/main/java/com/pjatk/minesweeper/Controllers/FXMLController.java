package com.pjatk.minesweeper.Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.pjatk.minesweeper.Settings.MinefieldSettings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

public class FXMLController implements Initializable {

    @FXML
    private Label emptyFields;

    @FXML
    private ComboBox<String> difficulty;

    @FXML
    private MinefieldController minefieldController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setUpComboBox(difficulty, new String[]{"Easy", "Medium", "Hard"});
        emptyFields.textProperty().bind(minefieldController.getEmptyFields().asString());
    }

    public void mouseClicked(ActionEvent mouseEvent) {
        MinefieldSettings.setDifficulty(difficulty.getValue());
        minefieldController.newGame();
    }

    private void setUpComboBox(ComboBox<String> name, String[] values) {
        name.getItems().addAll(values);
        name.getSelectionModel().selectFirst();
    }
}
