package com.pjatk.minesweeper.Views;

import com.pjatk.minesweeper.Models.Field;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class FieldView extends Field  {
    private static final int TILE_SIZE = 30;
    private final Rectangle border = new Rectangle(TILE_SIZE, TILE_SIZE);
    private final Text text = new Text();

    public FieldView(int x, int y){
        super(x, y);
        text.setVisible(false);
        border.setFill(Color.LIGHTGRAY);
        getChildren().addAll(border, text);
    }

    public void setContents() {
        text.setText(selectContents());
    }

    protected boolean isTextEmpty() {
        return text.getText().isEmpty();
    }

    protected void displayFlagged(){
        if(isFlagged())
            border.setFill(Color.LIGHTCYAN);
        else border.setFill(Color.LIGHTGRAY);
    }

    protected void displayRevealed(){
        text.setVisible(true);
        border.setFill(Color.WHITE);
    }

    private String selectContents(){
        if(isBomb()) return "\uD83D\uDCA3";
        if(getNearbyMines() != 0)
            return String.valueOf(getNearbyMines());
        return "";
    }
}
