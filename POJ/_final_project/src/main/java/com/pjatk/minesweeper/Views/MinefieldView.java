package com.pjatk.minesweeper.Views;

import com.pjatk.minesweeper.Models.Minefield;
import javafx.scene.layout.StackPane;

public class MinefieldView extends Minefield {

    protected void renderMinefield(){
        setGridLinesVisible(false);
        getChildren().clear();
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                add(minefield[i][j],i,j);
            }
        }
        setGridLinesVisible(true);
    }

    protected static StackPane displayGameState(Boolean state){
        StackPane canvas = new StackPane();
        if(state)
            canvas.setStyle("-fx-background-color: rgba(166,191,80,0.40);");
        else
            canvas.setStyle("-fx-background-color: rgba(217,98,54,0.40);");
        return canvas;
    }
}
