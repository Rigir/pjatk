package com.pjatk.minesweeper.Models;

import javafx.scene.layout.StackPane;

public class Field extends StackPane {
    private boolean flagged = false;
    private boolean revealed = false;
    private boolean bomb = false;
    private int nearbyMines;
    private final int x;
    private final int y;

    public Field(int x, int y) {
        this.x = x;
        this.y = y;
    }

    protected boolean isFlagged() {
        return flagged;
    }

    protected void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    protected int getX() {
        return x;
    }

    protected int getY() {
        return y;
    }

    protected boolean isBomb() {
        return bomb;
    }

    protected void setBomb() {
        this.bomb = true;
    }

    protected void setNearbyMines(int nearbyMines) {
        this.nearbyMines = nearbyMines;
    }

    protected boolean isRevealed() {
        return revealed;
    }

    protected int getNearbyMines() {
        return nearbyMines;
    }

    protected void setRevealed() {
        this.revealed = true;
    }

    @Override
    public String toString() {
        return "Field{" + "isFlagged=" + flagged + ", isRevealed=" + revealed + ", isBomb=" + bomb + ", nearbyMines="
                + nearbyMines + ", x=" + x + ", y=" + y + '}';
    }
}
