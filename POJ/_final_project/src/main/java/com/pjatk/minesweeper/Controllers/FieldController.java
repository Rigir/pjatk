package com.pjatk.minesweeper.Controllers;

import com.pjatk.minesweeper.Events.FieldControllerEvent;
import com.pjatk.minesweeper.Views.FieldView;
import javafx.scene.input.MouseButton;

public class FieldController extends FieldView {

    public FieldController(int x, int y){
        super(x, y);
        setOnMouseClicked(e -> {
                if( e.getButton() == MouseButton.SECONDARY && !isRevealed()){
                    setFlagged(!isFlagged());
                    displayFlagged();
                } else if(!isFlagged()){
                    openField();
                }
            }
        );
    }

    protected void openField() {
        if (isRevealed())
            return;

        if(isFlagged())
            setFlagged(false);

        setRevealed();
        displayRevealed();

        if(isBomb()){
            fireEvent(new FieldControllerEvent(FieldControllerEvent.GAMEOVER, this));
            return;
        }

        fireEvent(new FieldControllerEvent(FieldControllerEvent.ISWIN, this));

        if (isTextEmpty()) {
            fireEvent(new FieldControllerEvent(FieldControllerEvent.REVILED, this));
        }
    }
}
