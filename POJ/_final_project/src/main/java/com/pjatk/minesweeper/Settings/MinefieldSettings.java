package com.pjatk.minesweeper.Settings;

public class MinefieldSettings {
    private static int COLUMNS = 8;
    private static int ROWS = 8;
    private static int MINES = 10;

    public static int getROWS() {
        return ROWS;
    }

    public static int getMINES() {
        return MINES;
    }

    public static int getCOLUMNS() {
        return COLUMNS;
    }

    public static void setDifficulty(String name) {
        switch (name) {
            case "Easy":
                COLUMNS = 8;
                ROWS = 8;
                MINES = 10;
                break;
            case "Medium":
                COLUMNS = 16;
                ROWS = 16;
                MINES = 40;
                break;
            case "Hard":
                COLUMNS = 30;
                ROWS = 16;
                MINES = 96;
                break;
        }
    }
}
