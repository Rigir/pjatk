package com.pjatk.minesweeper.Models;

import com.pjatk.minesweeper.Controllers.FieldController;
import com.pjatk.minesweeper.Settings.MinefieldSettings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Minefield extends GridPane {

    private final Random rand = new Random();
    protected FieldController[][] minefield;
    protected SimpleIntegerProperty emptyFields = new SimpleIntegerProperty();
    protected int columns;
    protected int rows;
    private int mines;

    public void createMinefield(){
        updateDependencies();
        clearMinefield();
        setMines();
        prepareDistancesToMines();
        setContent();
    }

    public SimpleIntegerProperty getEmptyFields() {
        return emptyFields;
    }

    protected List<FieldController> getNeighbors(FieldController field) {
        ArrayList<FieldController> neighbors = new ArrayList<>();
        for (int xOffset = -1; xOffset <= 1; xOffset++) {
            for (int yOffset = -1; yOffset <= 1; yOffset++) {

                int newX = field.getX() + xOffset;
                int newY = field.getY() + yOffset;

                if (newX >= 0 && newX < columns && newY >= 0 && newY < rows) {
                    neighbors.add(minefield[newX][newY]);
                }
            }
        }
        return neighbors;
    }

    private int countNearbyMines(FieldController field){
        return (int) getNeighbors(field).stream().filter(Field::isBomb).count();
    }

    private void updateDependencies(){
        columns = MinefieldSettings.getCOLUMNS();
        rows = MinefieldSettings.getROWS();
        mines = MinefieldSettings.getMINES();
        minefield = new FieldController[columns][rows];
        emptyFields.set( columns * rows - mines );
    }

    private void setContent() {
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                minefield[i][j].setContents();
            }
        }
    }

    private void clearMinefield() {
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                minefield[i][j] = new FieldController(i,j);
            }
        }
    }

    private void setMines() {
        int counter = 0;
        while (mines != counter){
            int i = rand.nextInt(columns);
            int j = rand.nextInt(rows);
            if(!minefield[i][j].isBomb()){
                minefield[i][j].setBomb();
                counter++;
            }
        }
    }

    private void prepareDistancesToMines(){
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                if(minefield[i][j].isBomb()) continue;
                int bombs = countNearbyMines(minefield[i][j]);
                minefield[i][j].setNearbyMines(bombs);
            }
        }
    }

    @Override
    public String toString() {
        return "Minefield{" +
                "rand=" + rand +
                ", emptyFields=" + emptyFields +
                ", columns=" + columns +
                ", rows=" + rows +
                ", mines=" + mines +
                '}';
    }
}
