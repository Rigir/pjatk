package com.pjatk.minesweeper.Events;

import com.pjatk.minesweeper.Controllers.FieldController;
import javafx.event.Event;
import javafx.event.EventType;

public class FieldControllerEvent extends Event {
    private static final EventType<FieldControllerEvent> FIELD_CONTROLLER = new EventType<>(Event.ANY,"ANY");
    public static final EventType<FieldControllerEvent> REVILED = new EventType<>(FieldControllerEvent.FIELD_CONTROLLER,"REVILED");
    public static final EventType<FieldControllerEvent> GAMEOVER = new EventType<>(FieldControllerEvent.FIELD_CONTROLLER,"GAMEOVER");
    public static final EventType<FieldControllerEvent> ISWIN = new EventType<>(FieldControllerEvent.FIELD_CONTROLLER,"ISWIN");

    private final FieldController fieldController;

    public FieldControllerEvent(EventType<? extends Event> eventType, FieldController fieldController){
        super(eventType);
        this.fieldController = fieldController;
    }

    public FieldController getFieldController(){
        return this.fieldController;
    }
}
