💣 Minesweeper
=========

![game](assets/banner.png)

A simple game written entirely in Java and JavaFx with the 
[Model-View-Controller](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)
architectural pattern implemented. 

🛠 Main features:
---------
- The use of recursion when discovering fields 
- The ability to use flags 
- Three difficulty levels 

📄 License:
---------
PJAIT © All Rights Reserved 