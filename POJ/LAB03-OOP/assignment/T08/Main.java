package assignment.T08;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Entity player = new Entity();
        Entity enemy = new Entity();

        try (Scanner scanner = new Scanner(System.in)) {
            boolean loop = true;
            int selected;
            while (loop) {
                System.out.println("\nMain menu\n 1: Arena \n 2: Your Inventory \n 3: Enemy Inventory \n 4: Exit\n");
                System.out.print("Enter an choise: ");
                selected = scanner.nextInt();
                switch (selected) {
                case 1:
                    player.arena(enemy);
                    player.heal();
                    enemy.heal();
                    break;
                case 2:
                    player.selectInventory(scanner);
                    break;
                case 3:
                    enemy.selectInventory(scanner);
                    break;
                case 4:
                    loop = false;
                    break;
                default:
                    System.out.println("ERROR: Out of index!");
                    break;
                }
            }
        }
    }

}
