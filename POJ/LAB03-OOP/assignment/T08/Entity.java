package assignment.T08;

import java.util.Random;
import java.util.Scanner;

public class Entity {
    private final int WITHOUT = 1, LETHER = 5, BOW = 5, PLATE = 10, SWORD = 10;
    int health, armor, weapon;

    public Entity() {
        this.health = 99;
        this.armor = 1;
        this.weapon = 1;
    }

    public void arena(Entity enemy) {
        int attack;
        while (true) {
            attack = this.attack();
            if (enemy.isDefeated(attack)) {
                System.out.println("=! You Win !=");
                break;
            }
            System.out.println("\nYou: " + this.getHealth() + "\t Hits: " + attack);

            attack = enemy.attack();
            if (this.isDefeated(attack)) {
                System.out.println("=! Foe Win !=");
                break;
            }
            System.out.println("Foe: " + enemy.getHealth() + "\t Hits: " + attack);

        }
    }

    public int attack() {
        int power = this.getPower();
        Random rand = new Random();
        if (power <= 10)
            return power + (rand.nextInt(10) + 5);
        return power + rand.nextInt(5);
    }

    public boolean isDefeated(int attack) {
        if (this.health > 0) {
            this.health -= attack;
            return false;
        } else
            return true;
    }

    public int getHealth() {
        return this.health;
    }

    public void heal() {
        this.health = 99;
    }

    public int getPower() {
        return this.armor + this.weapon;
    }

    public void selectInventory(Scanner scanner) {
        boolean loop = true;
        int selected = 0;
        while (loop) {
            System.out.println("\nInventory\n 1: Weapon \n 2: Armor \n 3: Status \n 4: Exit\n");
            System.out.print("Enter an choise: ");
            selected = scanner.nextInt();
            switch (selected) {
            case 1:
                this.setWeapon(scanner);
                break;
            case 2:
                this.setArmor(scanner);
                break;
            case 3:
                System.out.println("Health: " + this.getHealth());
                System.out.println("Power: " + this.getPower());
                break;
            case 4:
                loop = false;
                break;
            default:
                System.out.println("ERROR: Out of index!");
                break;
            }
        }
    }

    private void setWeapon(Scanner scanner) {
        boolean loop = true;
        int selected = 0;
        while (loop) {
            System.out.println("\n Weapons[Power] \n 1: Without[1] \n 2: Bow[5] \n 3: Sword[10] \n 4: Exit\n");
            System.out.print("Enter an choise: ");
            selected = scanner.nextInt();
            switch (selected) {
            case 1:
                this.weapon = WITHOUT;
                loop = false;
                break;
            case 2:
                this.weapon = BOW;
                loop = false;
                break;
            case 3:
                this.weapon = SWORD;
                loop = false;
                break;
            case 4:
                loop = false;
                break;
            default:
                System.out.println("ERROR: Out of index!");
                break;
            }
        }
    }

    private void setArmor(Scanner scanner) {
        boolean loop = true;
        int selected = 0;
        while (loop) {
            System.out.println("\n Armors[Power] \n 1: Without[1] \n 2: Lether[5] \n 3: Plate[10] \n 4: Exit\n");
            System.out.print("Enter an choise: ");
            selected = scanner.nextInt();
            switch (selected) {
            case 1:
                this.armor = WITHOUT;
                loop = false;
                break;
            case 2:
                this.armor = LETHER;
                loop = false;
                break;
            case 3:
                this.armor = PLATE;
                loop = false;
                break;
            case 4:
                loop = false;
                break;
            default:
                System.out.println("ERROR: Out of index!");
                break;
            }
        }
    }
}
