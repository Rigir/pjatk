package assignment.T07;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Account[] accounts = new Account[10];
        int selected;

        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account(i, 100);
        }

        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.print("Enter an id: ");
                selected = scanner.nextInt();
                if (selected >= 0 && selected < accounts.length) {
                    ATM _ATM = new ATM(accounts[selected]);
                    _ATM.menu(scanner);
                } else
                    System.out.println("ERROR: Out of index!");
            }
        }
    }
}
