package assignment.T07;

public class Account {
    int id;
    double balance;

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
    }

    public double checkBalance() {
        return this.balance;
    }

    public double depositBalance(double amount) {
        if (amount >= 0)
            this.balance += amount;
        return checkBalance();
    }

    public double withdrawBalance(double amount) {
        if (amount >= 0 && amount <= checkBalance())
            this.balance -= amount;
        return checkBalance();
    }
}
