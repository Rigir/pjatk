package assignment.T07;

import java.util.Scanner;

public class ATM {
    Account user;

    public ATM(Account user) {
        this.user = user;
    }

    public void menu(Scanner scanner) {
        boolean loop = true;
        int selected;
        double amount;
        while (loop) {
            System.out.println("\nMain menu\n 1: check balance\n 2: withdraw\n 3: deposit \n 4: exit\n");
            System.out.print("Enter an choise: ");
            selected = scanner.nextInt();
            switch (selected) {
            case 1:
                System.out.println("The valance is: " + user.checkBalance());
                break;
            case 2:
                System.out.print("Amount: ");
                amount = scanner.nextDouble();
                user.withdrawBalance(amount);
                break;
            case 3:
                System.out.print("Amount: ");
                amount = scanner.nextDouble();
                user.depositBalance(amount);
                break;
            case 4:
                loop = false;
                break;
            default:
                System.out.println("ERROR: Out of index!");
                break;
            }

        }

    }
}
