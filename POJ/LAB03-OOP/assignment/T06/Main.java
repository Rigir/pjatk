package assignment.T06;

public class Main {
    public static void main(String[] args) {
        Windmill w1 = new Windmill();
        Windmill w2 = new Windmill(3, 10, "green", true);
        w1.info();
        w2.info();
    }
}
