package assignment.T06;

public class Windmill {
    final int SLOW = 1, MEDIUM = 2, FAST = 3;
    int speed;
    double radius;
    String color;
    boolean on;

    public Windmill() {
        this.speed = SLOW;
        this.radius = 5;
        this.color = "blue";
        this.on = false;
    }

    public Windmill(int speed, double radius, String color, boolean on) {
        this.speed = speed;
        this.radius = radius;
        this.color = color;
        this.on = on;
    }

    public void info() {
        System.out.format("S: %d \nR: %.2f \nC: %s \nO: %b \n", this.speed, this.radius, this.color, this.on);
    }
}
