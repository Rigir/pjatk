package assignment.T03;

public class Main {
    public static void main(String[] args) {
        TV t = new TV();
        Remote r = new Remote(t);
        r.turnOn();
        r.changeChannel(0);
        r.changeChannel(20);
        r.changeChannel(21);
        r.changeVolume(0);
        r.changeVolume(5);
        r.changeVolume(11);
        r.status();
        r.turnOff();
    }
}
