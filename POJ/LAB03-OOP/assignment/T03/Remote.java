package assignment.T03;

public class Remote {
    TV tv;

    public Remote(TV t) {
        this.tv = t;
    }

    public void changeChannel(int num) {
        if (tv.on && num >= 1 && num <= 20) {
            tv.channel = num;
        }
    }

    public void changeVolume(int num) {
        if (tv.on && num >= 1 && num <= 10) {
            tv.volume = num;
        }
    }

    public void turnOn() {
        tv.on = true;
    }

    public void turnOff() {
        tv.on = false;
    }

    public void status() {
        System.out.println("on: " + tv.on);
        System.out.println("Volume: " + tv.volume);
        System.out.println("Channel: " + tv.channel);
    }

}
