package assignment.T05;

public class Main {
    public static void main(String[] args) {
        MyCircle c = new MyCircle(5);
        System.out.format("P: %.2f \n", c.perimeter());
        System.out.format("A: %.2f \n", c.area());
        System.out.format("D: %.2f \n", c.diameter());
    }
}
