package assignment.T05;

public class MyCircle {
    double radius;

    public MyCircle(int radius) {
        this.radius = radius;
    }

    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    public double area() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double diameter() {
        return 2 * radius;
    }

}
