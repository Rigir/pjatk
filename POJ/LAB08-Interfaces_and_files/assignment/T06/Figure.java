package assignment.T06;

public interface Figure {
    double calcField();

    double calcCircuit();
}
