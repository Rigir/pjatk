package assignment.T06;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2.0, 3.0);
        Square square = new Square(2.0);
        Circle circle = new Circle(5.0);

        Figure[] arr = new Figure[] { rectangle, square, circle };

        Arrays.sort(arr);

        for (Figure figure : arr) {
            System.out.println("P: " + figure.calcField());
        }
    }
}
