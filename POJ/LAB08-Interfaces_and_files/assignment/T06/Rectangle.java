package assignment.T06;

public class Rectangle implements Figure, Comparable<Figure> {
    private Double x, y;

    public Rectangle(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double calcField() {
        return x * y;
    }

    @Override
    public double calcCircuit() {
        return 2 * x + 2 * y;
    }

    @Override
    public int compareTo(Figure that) {
        return (int) this.calcField() - (int) that.calcField();
    }
}
