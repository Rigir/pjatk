package assignment.T06;

public class Circle implements Figure, Comparable<Figure> {
    private Double r;

    public Circle(Double r) {
        this.r = r;
    }

    @Override
    public double calcField() {
        return Math.PI * r * r;
    }

    @Override
    public double calcCircuit() {
        return 2 * Math.PI * r;
    }

    @Override
    public int compareTo(Figure that) {
        return (int) this.calcField() - (int) that.calcField();
    }
}
