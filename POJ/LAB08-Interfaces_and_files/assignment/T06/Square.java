package assignment.T06;

public class Square implements Figure, Comparable<Figure> {
    private Double x;

    public Square(Double x) {
        this.x = x;
    }

    @Override
    public double calcField() {
        return x * x;
    }

    @Override
    public double calcCircuit() {
        return 4 * x;
    }

    @Override
    public int compareTo(Figure that) {
        return (int) this.calcField() - (int) that.calcField();
    }
}
