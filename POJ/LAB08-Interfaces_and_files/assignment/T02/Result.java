package assignment.T02;

import java.io.File;

public class Result {
    File file;
    int count;

    public Result(File file, int count) {
        this.file = file;
        this.count = count;
    }

    public void compare(File file, int count) {
        if (count > this.count) {
            this.file = file;
            this.count = count;
        }
    }

    @Override
    public String toString() {
        return "\nResult: " + file + " ma " + count;
    }
}
