package assignment.T02;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        File[] paths = new File("assignment/T02/basedir").listFiles();
        String[] files = paths[0].list();
        Result result = new Result(paths[0], files.length);

        for (File dir : paths) {
            files = dir.list();
            result.compare(dir, files.length);
        }

        System.out.println(result.toString());
    }
}
