package assignment.T07;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student jan = new Student("Jan", "Kowalski", "s123456");
        Student beata = new Student("Beata", "Sz", "s123455");
        Student wieczny = new Student("Jan", "Wieczny", "s00001");
        Student nowak = new Student("Kazimierz", "Nowak", "s44444");

        Student[] studenci = new Student[] { jan, beata, wieczny, nowak };
        System.out.println(Arrays.toString(studenci));

        Arrays.sort(studenci);

        System.out.println(Arrays.toString(studenci));
    }
}
