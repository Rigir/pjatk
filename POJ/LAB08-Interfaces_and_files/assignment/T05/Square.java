package assignment.T05;

public class Square implements Figure {
    private Double x;

    public Square(Double x) {
        this.x = x;
    }

    @Override
    public double calcField() {
        return x * x;
    }

    @Override
    public double calcCircuit() {
        return 4 * x;
    }
}
