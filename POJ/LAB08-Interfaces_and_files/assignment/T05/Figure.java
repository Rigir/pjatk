package assignment.T05;

public interface Figure {
    double calcField();

    double calcCircuit();
}
