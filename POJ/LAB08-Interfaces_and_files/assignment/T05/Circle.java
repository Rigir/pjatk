package assignment.T05;

public class Circle implements Figure {
    private Double r;

    public Circle(Double r) {
        this.r = r;
    }

    @Override
    public double calcField() {
        return Math.PI * r * r;
    }

    @Override
    public double calcCircuit() {
        return 2 * Math.PI * r;
    }
}
