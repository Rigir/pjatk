package assignment.T05;

public class Rectangle implements Figure {
    private Double x, y;

    public Rectangle(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double calcField() {
        return x * y;
    }

    @Override
    public double calcCircuit() {
        return 2 * x + 2 * y;
    }
}
