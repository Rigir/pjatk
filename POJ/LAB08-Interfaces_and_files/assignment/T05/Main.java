package assignment.T05;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2.0, 3.0);
        Square square = new Square(2.0);
        Circle circle = new Circle(5.0);

        Figure[] arr = new Figure[] { rectangle, square, circle };

        arr.toString();

        for (Figure figure : arr) {
            System.out.println(figure);
            System.out.println("P: " + figure.calcField());
            System.out.println("O: " + figure.calcCircuit());
        }
    }
}
