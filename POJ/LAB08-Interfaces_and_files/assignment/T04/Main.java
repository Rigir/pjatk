package assignment.T04;

import java.util.Scanner;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        String pattern;
        try (Scanner scanner = new Scanner(System.in)) {
            pattern = scanner.nextLine();
        }
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        System.out.println(now.format(formatter));
    }
}
