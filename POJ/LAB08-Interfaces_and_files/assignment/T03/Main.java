package assignment.T03;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        LocalDate data = LocalDate.now();
        int roczniki = 0;

        while (roczniki < 100) {
            data = data.plusYears(1);
            if (data.isLeapYear()) {
                System.out.println(data.getYear());
                roczniki++;
            }
        }
    }
}
