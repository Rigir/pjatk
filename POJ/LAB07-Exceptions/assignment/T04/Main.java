package resources.T04;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String text;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Wpisz slowo: ");
            text = scanner.nextLine();
        }
        result(text);
    }

    public static void result(String text) {
        String letters = text.replaceAll("[1-9\\W\\s]", "");
        String vowels = letters.toLowerCase().replaceAll("[^aeyoiu]", "");

        System.out.format("\nPodany tekst '%s' zawiera: ", text);
        System.out.format("\n%d liter, w tym %d samoglosek oraz %d spolglosek zawiera: \n", letters.length(),
                vowels.length(), (letters.length() - vowels.length()));

        System.out.println("R:" + text.replaceAll("[\\S]", "").length());
        System.out.println("P:" + text.replaceAll("[^1-9]", "").length());
        System.out.println("S:" + text.replaceAll("[\\w\\s]", "").length());
    }
}
