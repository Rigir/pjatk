package resources.T05;

public class TransformacjaString {
    public static void main(String[] args) {
        String text = "123ABCabc !@#";
        Znak[] arr = Znak.values();
        for (Znak znak : arr) {
            System.out.println(text + " | " + znak);
            System.out.println("USU:" + usunZnaki(text, znak));
            System.out.println("POD:" + podmienZnaki(text, znak, "\\$"));
            System.out.println("POZ:" + pozostawZnaki(text, znak) + "\n");
        }
    }

    public static String usunZnaki(String tekst, Znak znak) {
        switch (znak) {
            case SPOLGLOSKA:
                tekst = tekst.replaceAll("[a-zA-Z&&[^aeyiouAEYIOU]]", "");
                break;
            case SAMOGLOSKA:
                tekst = tekst.replaceAll("[aeyoiuAEYOIU]", "");
                break;
            case SPACJA:
                tekst = tekst.replaceAll("[\\s]", "");
                break;
            case LICZBA:
                tekst = tekst.replaceAll("[1-9]", "");
                break;
            case ZNAK_SPECJALNY:
                tekst = tekst.replaceAll("[\\W&&[^\\s]]", "");
                break;
            default:
                System.out.println("Error: Nie ma takiej operacji");
                break;
        }
        return tekst;
    }

    public static String podmienZnaki(String tekst, Znak znak, String podmien) {
        switch (znak) {
            case SPOLGLOSKA:
                tekst = tekst.replaceAll("[a-zA-Z&&[^aeyiouAEYIOU]]", podmien);
                break;
            case SAMOGLOSKA:
                tekst = tekst.replaceAll("[aeyoiuAEYOIU]", podmien);
                break;
            case SPACJA:
                tekst = tekst.replaceAll("[\\s]", podmien);
                break;
            case LICZBA:
                tekst = tekst.replaceAll("[1-9]", podmien);
                break;
            case ZNAK_SPECJALNY:
                tekst = tekst.replaceAll("[\\W&&[^\\s]]", podmien);
                break;
            default:
                System.out.println("Error: Nie ma takiej operacji");
                break;
        }
        return tekst;
    }

    public static String pozostawZnaki(String tekst, Znak znak) {
        switch (znak) {
            case SPOLGLOSKA:
                tekst = tekst.replaceAll("[^a-zA-Z&&[^aeyiouAEYIOU]]", "");
                break;
            case SAMOGLOSKA:
                tekst = tekst.replaceAll("[^aeyoiuAEYOIU]", "");
                break;
            case SPACJA:
                tekst = tekst.replaceAll("[^\\s]", "");
                break;
            case LICZBA:
                tekst = tekst.replaceAll("[^1-9]", "");
                break;
            case ZNAK_SPECJALNY:
                tekst = tekst.replaceAll("[[^\\W][\\s]]", "");
                break;
            default:
                System.out.println("Error: Nie ma takiej operacji");
                break;
        }
        return tekst;
    }
}
