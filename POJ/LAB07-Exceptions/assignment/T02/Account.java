package resources.T02;

public class Account {
    private String accountName;
    private double balance;

    public Account() {
        this.accountName = "";
        this.balance = 0;
    }

    public Account(String accountName, double initialBalance) throws InsufficientFundsException {
        if (!accountName.isEmpty() && initialBalance >= 0) {
            this.accountName = accountName;
            this.balance = initialBalance;
        } else
            throw new InsufficientFundsException("Invoilid Data");
    }

    public void withdraw(int amount) throws InsufficientFundsException {
        if (balance >= amount && amount > 0)
            balance -= amount;
        else
            throw new InsufficientFundsException("Amount is invalid");
    }

    public String getAccountName() {
        return accountName;
    }

    public double getBalance() {
        return balance;
    }
}
