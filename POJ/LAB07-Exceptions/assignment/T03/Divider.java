package resources.T03;

import javax.swing.*;

public class Divider {
    public static void main(String[] args) {
        String number, divider;
        int result = 0;
        try {
            do {
                number = JOptionPane.showInputDialog(null, "Podaj liczbę");
                divider = JOptionPane.showInputDialog(null, "Podaj dzielnik");
            } while (!isNumeric(number, divider));

            if (convertToInteger(divider) != 0) {
                result = convertToInteger(number) / convertToInteger(divider);
                JOptionPane.showMessageDialog(null, String.format("Wynik dzielenia to %d", result));
            } else
                throw new ArithmeticException();
        } catch (ArithmeticException e) {
            JOptionPane.showMessageDialog(null, "Błąd, nie można dzielić przez zero");
        }

    }

    public static boolean isNumeric(String... arr) {
        try {
            for (String str : arr)
                if (!(str != null && str.matches("[-+]?\\d*\\.?\\d+")))
                    throw new Exception();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Błąd, nie wpisałeś liczby");
            return false;
        }
        return true;
    }

    private static int convertToInteger(String number) {
        return Integer.parseInt(number);
    }
}
