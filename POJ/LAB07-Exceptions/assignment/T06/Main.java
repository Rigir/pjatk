package resources.T06;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\b[aeyoiu]\\S+");
        String text = "";
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Wpisz slowo: ");
            text = scanner.nextLine();
        }

        Matcher matcher = pattern.matcher(text.toLowerCase());
        while (matcher.find())
            System.out.print(matcher.group() + " ");
    }
}
