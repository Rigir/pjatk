package assignment.T02;

public class Main {
    public static void main(String[] args) {
        Slownik<Integer, String> Slownik_IS = new Slownik<>(4);
        Slownik<String, Integer> Slownik_SI = new Slownik<>(10);

        // <Integer, String>
        System.out.println("Arr: " + Slownik_IS.toString());
        Slownik_IS.dodaj(1, "kot");
        Slownik_IS.dodaj(2, "pies");
        Slownik_IS.dodaj(3, "ryba");
        Slownik_IS.dodaj(4, "ptak");
        System.out.println("Test: " + Slownik_IS.toString());
        Slownik_IS.dodaj(5, "pajak"); // ArrayIndexOutOfBoundsException check
        System.out.println("Result: " + Slownik_IS.toString());
        Slownik_IS.dodaj(1, "ryba"); // Replace
        System.out.println("Replace: " + Slownik_IS.toString());
        System.out.println("Czy zawiera?:" + Slownik_IS.czyZawiera(1));
        System.out.println("Wielkość: " + Slownik_IS.wielkosc());

        // <String, Integer>
        System.out.println("\nArr: " + Slownik_SI.toString());
        Slownik_SI.dodaj("kot", 1);
        Slownik_SI.dodaj("pies", 2);
        System.out.println("Arr: " + Slownik_SI.toString());
        System.out.println("Czy zawiera?:" + Slownik_SI.czyZawiera("pies"));
        System.out.println("Wielkość: " + Slownik_SI.wielkosc());
    }
}
