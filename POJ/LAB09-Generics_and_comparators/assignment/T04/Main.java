package assignment.T04;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Zawodnik z1 = new Zawodnik("Jan", "Kowalski", 1, 60);
        Zawodnik z2 = new Zawodnik("Andrzej", "Piekarski", 2, 120);
        Zawodnik z3 = new Zawodnik("Roman", "Drwal", 3, 30);

        Zawodnik[] zawodnicy = new Zawodnik[] { z1, z2, z3 };
        System.out.println("Arr: " + Arrays.toString(zawodnicy));
        Arrays.sort(zawodnicy, new ImieComparator());
        System.out.println("Imie: " + Arrays.toString(zawodnicy));
        Arrays.sort(zawodnicy, new numerComparator());
        System.out.println("Numer: " + Arrays.toString(zawodnicy));
        Arrays.sort(zawodnicy, new maxPredkoscComparator());
        System.out.println("maxPredkosc: " + Arrays.toString(zawodnicy));
    }
}
