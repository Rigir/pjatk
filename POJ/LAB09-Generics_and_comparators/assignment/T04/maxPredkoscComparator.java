package assignment.T04;

import java.util.Comparator;

public class maxPredkoscComparator implements Comparator<Zawodnik> {
    @Override
    public int compare(Zawodnik z1, Zawodnik z2) {
        return z1.getMaxPredkosc().compareTo(z2.getMaxPredkosc());
    }
}
