package assignment.T04;

import java.util.Comparator;

public class ImieComparator implements Comparator<Zawodnik> {
    @Override
    public int compare(Zawodnik z1, Zawodnik z2) {
        return z1.getImie().compareTo(z2.getImie());
    }
}
