package assignment.T04;

import java.util.Comparator;

public class numerComparator implements Comparator<Zawodnik> {
    @Override
    public int compare(Zawodnik z1, Zawodnik z2) {
        return z1.getNumer().compareTo(z2.getNumer());
    }
}
