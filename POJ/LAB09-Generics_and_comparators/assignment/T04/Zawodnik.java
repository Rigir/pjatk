package assignment.T04;

public class Zawodnik {
    private String imie;
    private String nazwisko;
    private Integer numer;
    private Integer maxPredkosc;

    public Zawodnik(String imie, String nazwisko, Integer numer, Integer maxPredkosc) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numer = numer;
        this.maxPredkosc = maxPredkosc;
    }

    public String getImie() {
        return imie;
    }

    public Integer getMaxPredkosc() {
        return maxPredkosc;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public Integer getNumer() {
        return numer;
    }

    @Override
    public String toString() {
        return String.format("%s %s %d %d", imie, nazwisko, numer, maxPredkosc);
    }
}