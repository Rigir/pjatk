package assignment.T03;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        File file = new File("assignment/T03/words.txt");
        Slownik<String, String> dictionary = Slownik.wczytajSlownik(file);
        System.out.println(dictionary);
    }
}
