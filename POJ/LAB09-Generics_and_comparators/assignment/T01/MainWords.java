package assignment.T01;

public class MainWords {
    public static void main(String[] args) {
        Lista<String> slowa = new Lista<>(100);
        System.out.println(slowa);
        slowa.dodaj("Kot");
        System.out.println(slowa);
        slowa.dodaj("Pies");
        System.out.println(slowa);
        System.out.println(slowa.czyZawiera("Mysz"));// false
        System.out.println(slowa.czyZawiera("Pies"));// true
        slowa.dodaj("Kot");
        slowa.dodaj("Chomik");
        System.out.println(slowa.zwrocIndeks("Kot"));
        System.out.println(slowa.zwrocOstatniIndeks("Kot"));

        System.out.println(slowa.zwrocElement(1)); // pies
        System.out.println(slowa.zwrocElement(20)); // null
        System.out.println(slowa.zwrocElement(101)); // null

        System.out.println(slowa);
        System.out.println(slowa.podmienElement(0, "Ryba")); // true
        System.out.println(slowa.podmienElement(150, "Ryba")); // false
        System.out.println(slowa);

    }
}
