package assignment.T01;

public class MainNum {
    public static void main(String[] args) {
        Lista<Integer> liczby = new Lista<>(10);
        liczby.dodaj(2);
        liczby.dodaj(5);
        liczby.dodaj(10);
        liczby.dodaj(100);

        System.out.println(liczby);

        System.out.println(liczby.czyZawiera(100));
        System.out.println(liczby.zwrocIndeks(5));
        liczby.podmienElement(1, 10000);
        System.out.println(liczby);
    }
}
