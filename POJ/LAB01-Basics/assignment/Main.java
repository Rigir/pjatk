package assignment;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        // T01();
        // T02();
        // T03();
        // T04();
        // T05();
        // T06();
        // T07();
        // T08();
        // T09();
        T10();
    }

    public static void T01() {
        double result = (9.5 * 4.5 - 2.5 * 3) / (45.5 - 3.5);
        System.out.println("Result: " + result);
    }

    public static void T02() {
        double fahrenhait, celsius;

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input: ");
            celsius = scanner.nextDouble();
        }

        fahrenhait = (9.0 / 5.0) * celsius + 32;
        System.out.println("Result: " + fahrenhait + "F.");
    }

    public static void T03() {
        System.out.print("Miles \t Kilometers \n");
        for (int i = 1; i <= 10; i++) {
            System.out.format(" %d \t %.3f \n", i, i * 0.609);
        }
    }

    public static void T04() {
          int num, score = 0, tmp_score;
        String name = " ", tmp_name;

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("NumOfStudents: ");
            num = scanner.nextInt();
            for (int i = 0; i < num; i++) {
                System.out.print("Name: ");
                tmp_name = scanner.next();
                System.out.print("Score: ");
                tmp_score = scanner.nextInt();

                if (tmp_score > score) {
                    name = tmp_name;
                    score = tmp_score;
                }
            }
        }
        System.out.format("Result: %s %d \n", name, score);
    }

    public static void T05() {
        double a, b, c;

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input: ");
            a = scanner.nextDouble();
            b = scanner.nextDouble();
            c = scanner.nextDouble();
        }

        if (a + b > c && a + c > b && c + b > a) {
            System.out.println("Tak");
        } else {
            System.out.println("Nie");
        }
    }

    public static void T06() {
        int day;

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input [1-7]: ");
            day = scanner.nextInt();
        }

        switch (day) {
        case 1:
            System.out.println("Poniedzialek");
            break;
        case 2:
            System.out.println("Wtorek");
            break;
        case 3:
            System.out.println("Sroda");
            break;
        case 4:
            System.out.println("Czwartek");
            break;
        case 5:
            System.out.println("Piatek");
            break;
        case 6:
            System.out.println("Sobota");
            break;
        case 7:
            System.out.println("Niedziela");
            break;
        default:
            System.out.println("Error: Nie ma takiego dnia!");
            break;
        }
    }

    public static void T07() {
        char a, b;

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input: ");
            a = scanner.next().charAt(0);
            b = scanner.next().charAt(0);
        }

        if (a == b) {
            System.out.println("Sa takie same! \n");
        } else if (a > b) {
            System.out.format(" %c > %c \n", a, b);
        } else {
            System.out.format(" %c < %c \n", a, b);
        }
    }

    public static void T08() {
        Integer[] arr = new Integer[3];

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input: ");
            for (int i = 0; i < 3; i++) {
                arr[i] = scanner.nextInt();
            }
        }

        Arrays.sort(arr, Collections.reverseOrder());

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void T09() {
        double[] arr = new double[4];

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input (x|y)x2: "); // x y x y
            for (int i = 0; i < 4; i++) {
                arr[i] = scanner.nextInt();
            }
        }

        double d = Math.sqrt(Math.pow(arr[2] - arr[0], 2) + Math.pow(arr[3] - arr[1], 2));
        System.out.println("Results: " + d);
    }

    public static void T10() {
        Random rand = new Random();
        int player, com;

        com = rand.nextInt(3);

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Input (1-3): ");
            player = scanner.nextInt();
        }

        // 0 - Kamien, 1 - Papier, 2 - Nozyce
        System.out.println(com + " " + player);

        if (com == player) {
            System.out.println("Remis");
        } else if ((com == 0 && player == 1) || (com == 1 && player == 2) || (com == 2 && player == 0)) {
            System.out.println("Wygrales");
        } else {
            System.out.println("Przegrales");
        }
    }
}
