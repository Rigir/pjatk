package assignment.T06;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String text = "Ala ma kota, kot ma Ale";
        System.out.println(countTheOccurrenceOfWords(text));
    }

    public static Map<String, Integer> countTheOccurrenceOfWords(String text) {
        text = text.replaceAll("[\\W&&[^\\s]]", "").toLowerCase();
        Map<String, Integer> result = new HashMap<>();
        for (String word : text.split(" ")) {
            if (result.containsKey(word))
                result.put(word, result.get(word) + 1);
            else
                result.put(word, 1);
        }
        return result;
    }
}
