package assignment.T03;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HashSet<Double> set = new HashSet<>();
        Scanner sc = new Scanner(System.in);
        double liczba = sc.nextDouble();
        double liczba2 = sc.nextDouble();
        double liczba3 = sc.nextDouble();

        set.add(liczba);
        set.add(liczba2);
        set.add(liczba3);

        double suma = 0;
        Iterator<Double> iterator = set.iterator();
        while (iterator.hasNext()) {
            suma += iterator.next();
        }
        System.out.println(suma);
        sc.close();
    }
}
