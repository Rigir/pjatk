package assignment.T07;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        String text = "Hello";
        System.out.println(letterIndex(text).toString().replace("=", "->"));
    }

    public static Map<String, Set<Integer>> letterIndex(String text) {
        String[] letters = text.split("");
        Map<String, Set<Integer>> result = new HashMap<>();
        Set<Integer> temp = new HashSet<>();

        for (int i = 0; i < letters.length; i++) {
            temp = result.get(letters[i]);
            if (temp == null) {
                temp = new HashSet<Integer>();
                result.put(letters[i], temp);
            }
            temp.add(i);
            result.put(letters[i], temp);
        }
        return result;
    }
}
