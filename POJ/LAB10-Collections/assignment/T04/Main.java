package assignment.T04;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> lottery = new HashSet<Integer>();
        Set<Integer> user = new HashSet<Integer>();
        getSetOfRandomNumbers(lottery);
        getUserSet(user);

        System.out.println("Lottery: " + lottery);
        System.out.println("Your bet: " + user);
        System.out.println("You guessed: " + checkResult(lottery, user));

    }

    public static int checkResult(Set<Integer> lottery, Set<Integer> user) {
        Set<Integer> result = new HashSet<Integer>() {
            {
                addAll(lottery);
                addAll(user);
            }
        };
        return lottery.size() * 2 - result.size();
    }

    public static void getUserSet(Set<Integer> set) {
        Scanner scanner = new Scanner(System.in);
        boolean valid;
        int num = 0;
        while (set.size() != 6) {
            valid = false;
            while (!valid) {
                try {
                    System.out.format("\nInput %d/6: ", set.size() + 1);
                    num = scanner.nextInt();
                    if (num >= 1 && num <= 49) {
                        valid = true;
                    } else {
                        System.out.println("Out of range.");
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Error: It is not a number.");
                    scanner.next();
                }
            }
            set.add(num);
        }
        scanner.close();
    }

    public static void getSetOfRandomNumbers(Set<Integer> set) {
        while (set.size() != 6) {
            set.add(randNumber(1, 49));
        }
    }

    public static int randNumber(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
