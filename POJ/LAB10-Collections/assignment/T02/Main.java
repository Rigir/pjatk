package assignment.T02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 2, 3, 5, 5, 5));
        System.out.println("List:" + list);
        displayDuplicates(list);
        removeDuplicates(list);
        System.out.println("List:" + list);
    }

    public static void displayDuplicates(List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))) {
                    System.out.println(list.get(i));
                    j++;
                }
            }
        }
    }

    public static void removeDuplicates(List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))) {
                    list.remove(list.get(i));
                    j--;
                }
            }
        }
    }

}