package assignment.T01;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String[] tablicaSamoglosek = { "a", "e", "o", "u", "i", "y" };
        HashSet<String> samogloski = new HashSet<>(Arrays.asList(tablicaSamoglosek));
        int liczbaSamoglosek = 0;

        String[] tablicaSpolglosek = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t",
                "v", "w", "x", "z" };
        HashSet<String> spolgloski = new HashSet<>(Arrays.asList(tablicaSpolglosek));
        int liczbaSpolglosek = 0;

        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        String[] litery = input.split("");

        for (String litera : litery) {
            if (samogloski.contains(litera.toLowerCase())) {
                liczbaSamoglosek++;
            } else if (spolgloski.contains(litera.toLowerCase())) {
                liczbaSpolglosek++;
            }
        }
        System.out.println(String.format("Liczba spolglosek: %d", liczbaSpolglosek));
        System.out.println(String.format("Liczba samoglosek: %d", liczbaSamoglosek));
        scan.close();
    }
}
