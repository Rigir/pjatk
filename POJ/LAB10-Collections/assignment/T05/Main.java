package assignment.T05;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] tablica = { "Ala", "ma", "kota" };
        ArrayList<String> lista = new ArrayList<>(Arrays.asList(tablica));

        lista = removeOddStrings(lista);

        System.out.println(lista);
    }

    public static ArrayList<String> removeOddStrings(ArrayList<String> lista) {
        // Iterator<String> iterator = lista.iterator();
        // while(iterator.hasNext()) {
        // String element = iterator.next();
        // if(element.length() %2 != 0) {
        // lista.remove(element);
        // }
        // }
        ArrayList<String> temp = new ArrayList<>(lista);
        for (String element : lista) {
            if (element.length() % 2 != 0) {
                temp.remove(element);
            }
        }

        return temp;
    }
}
