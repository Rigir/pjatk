package assignment.T06;

public class Main {
    public static void main(String[] args) {
        System.out.println(removeWords(2, "Ala", "nie", "ma", "test", "kota i", "psa", "papuge"));
    }

    public static String removeWords(int num, String... words) {
        String result = "";
        int temp = num;
        for (int i = 0; i < words.length; i++) {
            if (temp - 1 != i)
                result += words[i] + " ";
            else
                temp += num;
        }
        return result;
    }
}
