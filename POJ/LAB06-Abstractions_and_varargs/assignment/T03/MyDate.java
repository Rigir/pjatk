package assignment.T03;

public class MyDate {
    private int day, month, year;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    // @Override
    // public String toString() { // A
    // return String.format("%d.%d.%d", this.day, this.month, this.year);
    // }

    // @Override
    // public String toString() { // B
    // if (this.day < 10)
    // return String.format("0%d.%d.%d", this.day, this.month, this.year);
    // else
    // return String.format("%d.%d.%d", this.day, this.month, this.year);
    // }

    @Override
    public String toString() { // C
        String[] months = { "sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paz", "lis", "gru" };
        return String.format("%d %s %d", this.day, months[this.month - 1], this.year);
    }
}
