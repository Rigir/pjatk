package assignment.T02;

public class Order {
    private Position[] positions;
    private int maxSize;

    public Order() {
        this.maxSize = 10;
        positions = new Position[this.maxSize];

    }

    public Order(int maxSize) {
        this.maxSize = maxSize;
        positions = new Position[this.maxSize];
    }

    public void addItem(String productName, int quantity, Double piecePrice) {
        for (int i = 0; i < positions.length; i++) {
            if (positions[i] == null) {
                positions[i] = new Position(productName, quantity, piecePrice);
                break;
            }
        }
    }

    private Double calculateValue() {
        Double result = 0.00;
        for (Position position : positions) {
            if (position != null) {
                result += position.calculateValue();
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "";
        for (Position position : positions) {
            if (position != null) {
                result += position.toString();
            }
        }
        result += "\nWynik: " + calculateValue();
        return result;
    }
}
