package assignment.T02;

public class Position {
    private String productName;
    private Double piecePrice;
    private int quantity;

    public Position() {
        this.productName = "Chleb";
        this.piecePrice = 2.00;
        this.quantity = 2;
    }

    public Position(String productName, int quantity, Double piecePrice) {
        this.productName = productName;
        this.piecePrice = piecePrice;
        this.quantity = quantity;
    }

    public Double calculateValue() {
        return this.piecePrice * this.quantity;
    }

    @Override
    public String toString() {
        return String.format("%s \t %.2f PLN \t %d pc. \t %.2f PLN \n", this.productName, this.piecePrice,
                this.quantity, calculateValue());
    }
}
