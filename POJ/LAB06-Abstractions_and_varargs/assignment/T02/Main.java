package assignment.T02;

public class Main {
    public static void main(String[] args) {
        Order order = new Order(3);
        order.addItem("Chleb", 2, 2.00);
        order.addItem("Banany", 1, 6.00);
        order.addItem("Woda", 3, 4.00);
        System.out.println(order.toString());
    }
}
