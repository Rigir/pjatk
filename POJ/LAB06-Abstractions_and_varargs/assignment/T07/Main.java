package assignment.T07;

public class Main {
    public static void main(String[] args) {
        int[] arr1 = { 1, 2, 3, 4, 5 };
        int[] arr2 = { 6, 7, 8, 9, 10 };
        System.out.println(sumArr(arr1, arr2));
    }

    public static int sumArr(int[]... arrays) {
        int result = 0;
        for (int[] array : arrays) {
            for (int i : array) {
                result += i;
            }
        }
        return result;
    }
}
