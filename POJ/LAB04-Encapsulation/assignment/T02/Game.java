package assignment.T02;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private int trials, min, max, hiddenNumber;

    public Game() {
        this.max = 1;
        this.min = 100;
    }

    public Game(int min, int max) {
        this.max = max;
        this.min = min;
    }

    public void play() {
        int number;
        setTrials(5);
        randHiddenNumber();
        System.out.println(this.hiddenNumber);
        try (Scanner scanner = new Scanner(System.in)) {
            do {
                System.out.print("\nEnter a guess: ");
                number = scanner.nextInt();
                if (guessNumber(number))
                    break;
            } while (checkTrials());
        }
    }

    private boolean guessNumber(int number) {
        if (number == getHiddenNumber()) {
            System.out.println("\nWell done, You have " + getTrials() + " tryes left!");
            return true;
        } else if (number > getHiddenNumber()) {
            System.out.println(" Too high! ");
        } else if (number < getHiddenNumber()) {
            System.out.println(" Too low! ");
        }
        return false;
    }

    private boolean checkTrials() {
        setTrials(getTrials() - 1);
        if (getTrials() > 0)
            return true;
        System.out.println("\nYou lost, the number you were looking for was: " + getHiddenNumber());
        return false;
    }

    private void randHiddenNumber() {
        Random rand = new Random();
        this.hiddenNumber = rand.nextInt((getMax() - getMin()) + 1) + getMin();
    }

    // SET
    private void setTrials(int trials) {
        this.trials = trials;
    }

    // GET
    private int getTrials() {
        return trials;
    }

    private int getHiddenNumber() {
        return hiddenNumber;
    }

    private int getMin() {
        return min;
    }

    private int getMax() {
        return max;
    }
}
