package assignment.T05;

public class Race {
    public static void main(String[] args) {
        Player[] players = { new Player("Robert", 2, 8), new Player("Wiktor", 4, 8), new Player("Dawid", 2, 12) };

        System.out.println("\n== Wyscig trzech zawodnikow ==");

        boolean flag = true;
        while (flag) {
            for (Player player : players) {
                System.out.println(player.toString());
                if (turn(player)) {
                    flag = false;
                    break;
                }
            }
        }
    }

    public static boolean turn(Player player) {
        player.run();
        if (player.getDistance() > 50) {
            player.introduceYourself();
            return true;
        }
        return false;
    }
}
