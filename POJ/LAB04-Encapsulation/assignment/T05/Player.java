package assignment.T05;

import java.util.Random;

public class Player {
    private String name;
    private int minSpeed, maxSpeed, distance;

    public Player(String name, int minSpeed, int maxSpeed) {
        this.name = name;
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
        this.distance = 0;
    }

    public void introduceYourself() {
        System.out.format("\nJestem: %s biegam z predkoscia %d-%dkm/h \n", name, minSpeed, maxSpeed);
    }

    public void run() {
        Random rand = new Random();
        distance += rand.nextInt((maxSpeed - minSpeed) + 1) + minSpeed;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return name + " " + distance;
    }
}
