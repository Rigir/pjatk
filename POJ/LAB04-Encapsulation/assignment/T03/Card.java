package assignment.T03;

public class Card {
    Value value;
    Color color;

    public Card(Value value, Color color) {
        this.value = value;
        this.color = color;
    }

    public String toString() {
        return String.format("%s %s", value, color);
    }
}
