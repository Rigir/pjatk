package assignment.T03;

public class Main {
    public static void main(String[] args) {
        Value[] value = Value.values();
        Color[] color = Color.values();
        Card[] cards = new Card[52];

        int counter = 0;
        for (int i = 0; i < color.length; i++) {
            for (int j = 0; j < value.length; j++) {
                cards[counter] = new Card(value[j], color[i]);
                counter++;
            }

        }

        for (Card card : cards) {
            System.out.println(card.toString());
        }
    }
}
