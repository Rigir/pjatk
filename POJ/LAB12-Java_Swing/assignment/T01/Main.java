package T01;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Main {
    public static void main(String[] args) {
        JFrame jf = new JFrame("Zad01");
        jf.setLayout(new GridLayout(2, 1));

        JLabel label = new JLabel("From celsius to fahrenheit");
        JTextField textField = new JTextField();
        textField.setHorizontalAlignment(SwingConstants.CENTER);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        textField.setPreferredSize(new Dimension(100, 200));

        textField.addActionListener(l -> {
            label.setText(String.valueOf((9.0 / 5.0) * Double.parseDouble(textField.getText()) + 32));
        });

        jf.setSize(500, 200);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.add(label);
        jf.add(textField);
        jf.setVisible(true);
        jf.setResizable(false);
    }
}