package T05;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class Calculator extends JPanel implements ActionListener {
    private GridBagConstraints gbc = new GridBagConstraints();
    private JButton[] bottons = new JButton[16];
    private JPanel displayKeyboard;
    private JPanel displayLabel;
    private JLabel label;

    private char[] operators = { '*', '/', '+', '-' };
    private Character operator = null;
    private Integer result = null;
    private String number = "";

    public Calculator() {
        setLayout(new GridLayout(2, 1));
        displayKeyboard = new JPanel(new GridBagLayout());
        displayLabel = new JPanel(new GridBagLayout());
        displayLabel.setBackground(Color.LIGHT_GRAY);

        // Display
        label = new JLabel("", SwingConstants.CENTER);
        label.setHorizontalAlignment(SwingConstants.CENTER);

        // Buttons
        char[] buttonsNames = { '7', '8', '9', operators[0], '4', '5', '6', operators[1], '1', '2', '3', operators[2],
                'C', '0', '=', operators[3] };

        int counter = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                gbc = new GridBagConstraints();
                bottons[counter] = new JButton(Character.toString(buttonsNames[counter]));
                bottons[counter].addActionListener(this);
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.gridy = i;
                gbc.gridx = j;
                displayKeyboard.add(bottons[counter], gbc);
                counter++;
            }
        }

        displayLabel.add(label);
        add(displayLabel);
        add(displayKeyboard);
    }

    private void clear() {
        operator = null;
        result = null;
        number = "";
        update();
    }

    private void update() {
        // System.out.println("R: " + result + " O: " + operator + " N: " + number);
        if (operator == null) {
            label.setText(number);
        } else {
            label.setText(Integer.toString(result) + operator + number);
        }
    }

    private boolean calculate() {
        if (result != null && operator != null && !number.isEmpty()) {
            switch (operator) {
                case '+':
                    result += Integer.parseInt(number);
                    break;

                case '-':
                    result -= Integer.parseInt(number);
                    break;

                case '/':
                    if (Integer.parseInt(number) != 0) {
                        result /= Integer.parseInt(number);
                    } else
                        result = 0;
                    break;

                case '*':
                    result *= Integer.parseInt(number);
                    break;

                default:
                    result = 0;
                    break;
            }
            number = "";
            return true;
        }
        return false;
    }

    private boolean numbersValidation(char input) {
        if (result == null && number.isEmpty()) {
            if (input == '0')
                return false;
        }
        if (!number.isEmpty()) {
            if (Integer.parseInt(number) == 0) {
                number = String.valueOf(input);
                return false;
            }
        }
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // System.out.println("Request:" + e.getActionCommand());
        // System.out.println("R: " + result + " O: " + operator + " N: " + number);
        char input = e.getActionCommand().charAt(0);
        if (input == 'C') {
            clear();
            return;
        }

        if (input == '=') {
            if (calculate()) {
                number = String.valueOf(result);
                result = null;
                operator = null;
                update();
            }
            return;
        }

        if (Character.isDigit(input)) {
            if (numbersValidation(input)) {
                number += input;
            }
        } else {
            if (!number.isEmpty() && result == null) {
                result = Integer.parseInt(number);
                operator = input;
                number = "";
            }

            if (operator != null && number.isEmpty())
                operator = input;

            if (calculate())
                operator = input;
        }

        update();
    }
}