package T04;

import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        JFrame jframe = new JFrame("Calendar");
        jframe.setSize(300, 300);
        jframe.setLayout(new BorderLayout());
        JTextField jTextField = new JTextField();
        JButton button = new JButton();
        button.setText("check");
        JLabel label = new JLabel();
        label.setPreferredSize(new Dimension(100, 50));

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Calendar calendar = Calendar.getInstance();
                String format = "dd-MM-yyyy";
                SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                try {
                    Date date = dateFormat.parse(jTextField.getText());
                    calendar.setTime(date);
                    label.setText(String.valueOf(calendar.get(Calendar.WEEK_OF_YEAR)));
                } catch (ParseException e) {
                    label.setText("Zly format daty");
                }

            }
        });

        jframe.add(label, BorderLayout.NORTH);
        jframe.add(jTextField, BorderLayout.CENTER);
        jframe.add(button, BorderLayout.SOUTH);
        jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jframe.setVisible(true);
    }
}
