package T03;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
    JTextField jTextField;
    JLabel jLabel;

    public MyActionListener(JTextField jTextField, JLabel jLabel) {
        this.jTextField = jTextField;
        this.jLabel = jLabel;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String textFieldValue = jTextField.getText();
        StringBuilder stringBuilder = new StringBuilder(textFieldValue);
        String reversedString = stringBuilder.reverse().toString();
        jLabel.setText(reversedString);
    }
}
