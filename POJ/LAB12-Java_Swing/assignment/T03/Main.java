package T03;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame jframe = new JFrame("String Reverse");
        jframe.setSize(300, 300);
        jframe.setLayout(new BorderLayout());
        JTextField jTextField = new JTextField();
        JLabel label = new JLabel();
        label.setPreferredSize(new Dimension(100, 50));
        MyActionListener listener = new MyActionListener(jTextField, label);
        jTextField.addActionListener(listener);
        jframe.add(jTextField, BorderLayout.SOUTH);
        jframe.add(label, BorderLayout.CENTER);
        jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jframe.setVisible(true);
    }
}
