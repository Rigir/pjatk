package T02;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        JFrame jf = new JFrame("Zad02");
        jf.setLayout(new GridLayout(2, 1));

        Random rand = new Random();
        int guess = rand.nextInt(50);

        JTextField textField = new JTextField();
        JLabel label = new JLabel("From 0 to 50");

        textField.setPreferredSize(new Dimension(100, 200));
        textField.setHorizontalAlignment(SwingConstants.CENTER);

        label.setHorizontalAlignment(SwingConstants.CENTER);

        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Integer.parseInt(textField.getText()) == guess) {
                    label.setText("Zgadles!");
                } else if (Integer.parseInt(textField.getText()) < guess)
                    label.setText("Spróbuj wyżej");
                else
                    label.setText("Spróbuj niżej");
                textField.setText("");
            }
        };

        textField.addActionListener(actionListener);

        jf.setSize(500, 200);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.add(label);
        jf.add(textField);
        jf.setVisible(true);
        jf.setResizable(false);
    }
}
