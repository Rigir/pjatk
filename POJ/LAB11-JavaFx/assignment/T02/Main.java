package T02;

import javafx.scene.control.Button;
import javafx.application.Application;
import java.time.LocalDate;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        VBox vBox = new VBox();
        Label label = new Label();

        DatePicker date = new DatePicker();
        date.setValue(LocalDate.now());

        TextField name = new TextField();
        name.setPromptText("Podaj imie...");

        Button button = new Button();
        button.setText("Oblicz");

        primaryStage.setTitle("Zad02");
        primaryStage.setScene(new Scene(vBox, 400, 300));
        vBox.getChildren().addAll(name,date,button,label);
        primaryStage.show();

        button.setOnMouseClicked((MouseEvent event) -> {
            LocalDate now = LocalDate.now();
            LocalDate then = date.getValue();
            if(now.isAfter(then)){
                label.setText(name.getText() + " ma " +  then.until(now).getYears() + " lat");
            }
            else label.setText("Wprowadzono datę w przyszłości albo dzisiejszą");
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
