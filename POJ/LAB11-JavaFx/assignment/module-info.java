module POJ {
    requires javafx.fxml;
    requires javafx.controls;

    opens Zad01;
    opens Zad02;
    opens Zad03;
    opens Zad04;
}
