package T03;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class FormController implements Initializable{
    @FXML
    private ComboBox<String> VATRate;
    @FXML
    private Spinner<Integer> quantity;
    @FXML
    private ComboBox<String> taxScale;
    @FXML
    private TextField productPrice;
    @FXML
    private Label netValue;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        productPrice.setText("0");
        setUpComboBox(VATRate, new String[]{"0%", "23%"});
        setUpComboBox(taxScale, new String[]{"18%","19%","32%"});
        setUpSpinner(quantity,1,10);
    }

    private void setUpComboBox(ComboBox<String> name, String[] values){
        name.getItems().addAll(values);
        name.getSelectionModel().selectFirst();
    }

    private void setUpSpinner(Spinner<Integer> name, int nim, int max){
        SpinnerValueFactory<Integer> valueFactory =
                new SpinnerValueFactory.IntegerSpinnerValueFactory(nim, max);
        valueFactory.setValue(nim);
        name.setValueFactory(valueFactory);
    }

    public void calculateNet(Event actionEvent) {
        double price = Double.parseDouble(productPrice.getText().replaceAll("[^\\d+(\\.\\d{1,2})?]", ""));
        double VATRateValue = Double.parseDouble(removeLastChar(VATRate.getValue()));
        double taxScaleValue = Double.parseDouble(removeLastChar(taxScale.getValue()));
        double result = (price/(1+VATRateValue/100)) * (1-taxScaleValue/100) * quantity.getValue();
        DecimalFormat df = new DecimalFormat("#.00");
        netValue.setText(df.format(result) + " zł");
    }

    private String removeLastChar(String s) {
        return s.substring(0, s.length() - 1);
    }
}

