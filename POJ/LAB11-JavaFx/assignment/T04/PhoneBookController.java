package T04;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class PhoneBookController implements Initializable {
    public TableView<PersonData> tableView;
    public TableColumn<PersonData, String> nameColumn;
    public TableColumn<PersonData, String> surnameColumn;
    public TableColumn<PersonData, String> phoneColumn;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField surnameTextField;

    @FXML
    private TextField phoneTextField;

    @FXML
    private Button addButton;

    private final ObservableList<PersonData> phoneBookList = FXCollections.observableArrayList();
    private PersonData currentPerson = new PersonData();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addButton.setDisable(true);
        tableView.setItems(phoneBookList);

        nameColumn.setCellValueFactory(rowData -> rowData.getValue().nameProperty());
        surnameColumn.setCellValueFactory(row -> row.getValue().surnameProperty());
        phoneColumn.setCellValueFactory(rowW->rowW.getValue().phoneProperty());

        PersonData person1 = new PersonData("Jan", "Kowalski", "777-666-777");
        PersonData person2 = new PersonData("Beata", "Nowak", "999-888-777");

        phoneBookList.addAll(person1, person2);

        nameTextField.textProperty().bindBidirectional(currentPerson.nameProperty());
        surnameTextField.textProperty().bindBidirectional(currentPerson.surnameProperty());
        phoneTextField.textProperty().bindBidirectional(currentPerson.phoneProperty());

    }

    public void addButtonClicked(ActionEvent actionEvent) {
        try {
            phoneBookList.add(new PersonData(currentPerson.getName(), currentPerson.getSurname(), currentPerson.getPhone()));
            currentPerson.setName("");
            currentPerson.setSurname("");
            currentPerson.setPhone("");
            addButton.setDisable(true);
        } catch (IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Blad!");
            alert.setContentText(String.format("Wystapil blad !\n%s", e.getMessage()));
            alert.showAndWait();
        }
    }

    public void textChanged(KeyEvent keyEvent) {
        if (!nameTextField.getText().isEmpty() && !surnameTextField.getText().isEmpty() && !phoneTextField.getText().isEmpty()){
            addButton.setDisable(false);
        }else {
            addButton.setDisable(true);
        }
    }
}
