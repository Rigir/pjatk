package T04;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonData {
    private StringProperty name = new SimpleStringProperty();
    private StringProperty surname = new SimpleStringProperty();
    private StringProperty phone = new SimpleStringProperty();

    public PersonData(){
        // Aby zapobiec nullpointerom z prezentacji
        name.set("");
        surname.set("");
        phone.set("");
    }

    public PersonData(String name, String surname, String phone){
        if (!phone.matches("\\d{3}-\\d{3}-\\d{3}")){
            throw new IllegalArgumentException("Podano bledny format telefonu!");
        }
        this.name.set(name);
        this.surname.set(surname);
        this.phone.set(phone);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurname() {
        return surname.get();
    }

    public StringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getPhone() {
        return phone.get();
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }
}
