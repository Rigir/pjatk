![PJATK](../../.assets/banner.png)
=========

LAB01
```
Stworzyć plan zajęć na uczelnie. Przykładowe:
/plan?od=""&do=""
/student/:id/plan?od=""&do=""
/wykladowca/:id/plan?od=""&do=""
/sala/:id/plan?od=""&do=""

Powinno odsługiwać plan zajęć dla:
studenta:123    | dzień:13.10.2023r.
wykladowca:abc  | dni:16-20.10.2023r.
sali:509        | semestr:zima23/24
grupy:GCw21     | semestr:zima23/24
```

LAB02
```
Stworzyć plan zajęć na uczelnie przy użyciu GraphQL.
```

LAB03
```
Stworzyć plan zajęć na uczelnie przy użyciu gRPC.
client <=> REST <=> gRPC
```

LAB04
```
Add swagger to rest-api
```

LAB05
```
Move code base of rest-api and grpc-api to typescript
```
