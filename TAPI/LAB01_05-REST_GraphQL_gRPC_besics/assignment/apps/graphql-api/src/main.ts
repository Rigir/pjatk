// import express from 'express';
// import cors from 'cors';
// import * as grpc from '@grpc/grpc-js';
// import * as protoLoader from '@grpc/proto-loader';
// import { ApolloServer } from "@apollo/server";
// import { expressMiddleware } from "@apollo/server/express4"

// import {ProtoGrpcType} from 'packages/types/proto/scheduleRecipe';
// import {Students} from "packages/types/proto/Students"
// // import {ScheduleEntry} from "packages/types/proto/ScheduleEntry"
// // import {CourseEntry} from "packages/types/proto/CourseEntry"
// // import {StudentEntry} from "packages/types/proto/StudentEntry"



// const typeDefs = `#graphql
//     type Student {
//         id: String
//     }

//     type Lecturer {
//         id: String
//     }

//     type Room {
//         id: String
//     }

//     type Group {
//         id: String
//         students: [Student]
//     }

//     type Course {
//         id: String
//         name: String
//         lecturer: Lecturer
//         room: Room
//         group: Group
//     }

//     type Schedule {
//         date: String
//         courses: [Course]
//     }

//     type Query {
//         students: [Student]
//         student(id:String): Student

//         lecturers: [Lecturer]
//         lecturer(id:String): Lecturer

//         rooms: [Room]
//         room(id:String): Room

//         groups: [Group]
//         group(id:String): Group

//         courses: [Course]
//         course(id:String): Course

//         schedule: [Schedule]
//         scheduleByDate(date:String): Schedule
//     }
// `;

// const host = process.env.HOST ?? 'localhost';
// const port = process.env.PORT ? Number(process.env.PORT) : 6060;
// const grpcPort: number = process.env.GRPC_PORT
//   ? Number(process.env.GRPC_PORT)
//   : 9090;


// const scheduleProto = grpc.loadPackageDefinition(
//   protoLoader.loadSync('packages/proto/scheduleRecipe.proto')
// ) as unknown as ProtoGrpcType;

// const scheduleGrpc = new scheduleProto.ScheduleService(
//   `${host}:${grpcPort}`,
//   grpc.ChannelCredentials.createInsecure()
// );

// const resolvers = {
//     Query: {
//         students: () => scheduleGrpc.getStudents({}, (err: Error | null, response: Students) => {
//           if (err) {
//             console.error(err);
//             return;
//           }
//           return response.students
//         }
//       ),
//     //     student: (parent, args) => students.find(student => student.id === args.id),

//     //     lecturers: () => lecturers,
//     //     lecturer: (parent, args) => lecturers.find(lecturer => lecturer.id === args.id),

//     //     rooms: () => rooms,
//     //     room: (parent, args) => rooms.find(room => room.id === args.id),

//     //     groups: () => groups,
//     //     group: (parent, args) => groups.find(group => group.id === args.id),

//     //     courses: () => courses,
//     //     course: (parent, args) => courses.find(course => course.id === args.id),

//     //     schedule: () => schedule,
//     //     scheduleByDate: (parent, args) => schedule.find(entry => entry.date === args.date)

//     // },
//     // Course: {
//     //     lecturer: (parent) => lecturers.find(lecturer => lecturer.id === parent.lecturer.id),
//     //     room: (parent) => rooms.find(room => room.id === parent.room.id),
//     //     group: (parent) => groups.find(group => group.id === parent.group.id)
//     // },
//     // Schedule: {
//     //     courses: (parent) => parent.courses.map(course => courses.find(c => c.id === course.id))
//     // }
//   }
// };

// const server = new ApolloServer({ typeDefs, resolvers })
// await server.start();

// const app = express()
// app.use('/graphql', cors(), express.json(), expressMiddleware(server));
// app.listen(port, host, () => {
//   console.log(`[ ready ] http://${host}:${port}`);
// });
