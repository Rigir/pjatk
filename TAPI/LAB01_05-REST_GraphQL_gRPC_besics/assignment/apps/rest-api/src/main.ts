import express, { Request, Response, NextFunction } from 'express';
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import * as swaggerUi from 'swagger-ui-express';
import { readJsonFile } from '@nx/devkit';
import cors from 'cors';
import swaggerAutogen from "swagger-autogen"
import {Schedule} from "packages/types/proto/Schedule"
import {ScheduleEntry} from "packages/types/proto/ScheduleEntry"
import {CourseEntry} from "packages/types/proto/CourseEntry"
import {StudentEntry} from "packages/types/proto/StudentEntry"
import {ProtoGrpcType} from 'packages/types/proto/scheduleRecipe';

const swaggerAutogenClient = swaggerAutogen({ openapi: '3.0.0' });
const doc = {
  info: {
    title: 'My API',
    description: 'Description',
  },
  host: 'localhost:3000',
};
const outputFile = 'apps/rest-api/src/swagger-output.json';
const routes = ['apps/rest-api/src/main.ts'];
swaggerAutogenClient(outputFile, routes, doc);

const host: string = process.env.HOST ?? 'localhost';
const port: number = process.env.PORT ? Number(process.env.PORT) : 3000;
const grpcPort: number = process.env.GRPC_PORT
  ? Number(process.env.GRPC_PORT)
  : 9090;

const scheduleProto = grpc.loadPackageDefinition(
  protoLoader.loadSync('packages/proto/scheduleRecipe.proto')
) as unknown as ProtoGrpcType;

const scheduleGrpc = new scheduleProto.ScheduleService(
  `${host}:${grpcPort}`,
  grpc.ChannelCredentials.createInsecure()
);

let modificationDate: Date = new Date();
const setCache = function (req: Request, res: Response, next: NextFunction): void {
  const period = 60;
  if (req.method == 'GET') {
    res.set('Cache-control', `public, max-age=${period}, must-revalidate`);
    res.set('Last-Modified', modificationDate.toUTCString());
  } else {
    res.set('Cache-control', 'no-store');
  }
  next();
};

const app = express();
app.disable('etag');
app.use(cors());
app.use(setCache);
app.use(
  '/doc',
  swaggerUi.serve,
  swaggerUi.setup(readJsonFile('apps/rest-api/src/swagger-output.json'))
);

app.get('/schedule', (req: Request, res: Response) => {
  const { from, to } = req.query;

  scheduleGrpc.GetScheduleByDate(
    { from: from.toString() ||undefined , to: to.toString()||undefined },
    (err: Error | null, response: Schedule) => {
      if (err) {
        console.error(err);
        return;
      }
      modificationDate = new Date();
      res.send(response.schedule);
    }
  );
});

app.get('/students/:id/schedule', (req: Request, res: Response) => {
  const { id } = req.params;
  const { from, to } = req.query;
  scheduleGrpc.GetScheduleByDate(
    { from: from.toString() ||undefined , to: to.toString()||undefined },
    (err: Error | null, response: Schedule) => {
      if (err) {
        console.error(err);
        return;
      }
      res.send(
        response.schedule.filter((day: ScheduleEntry) =>
          day.courses.find((course: CourseEntry) =>
            course.group.students.find((student: StudentEntry) => student.id === id)
          )
        )
      );
    }
  );
});

app.get('/lecturers/:id/schedule', (req: Request, res: Response) => {
  const { id } = req.params;
  const { from, to } = req.query;
  scheduleGrpc.GetScheduleByDate(
    { from: from.toString() ||undefined , to: to.toString()||undefined },
    (err: Error | null, response: Schedule) => {
      if (err) {
        console.error(err);
        return;
      }
      res.send(
        response.schedule.filter((day: ScheduleEntry) =>
          day.courses.find((course: CourseEntry) => course.lecturer.id === id)
        )
      );
    }
  );
});

app.get('/rooms/:id/schedule', (req: Request, res: Response) => {
  const { id } = req.params;
  const { from, to } = req.query;
  scheduleGrpc.GetScheduleByDate(
    { from: from.toString() ||undefined , to: to.toString()||undefined },
    (err: Error | null, response: Schedule) => {
      if (err) {
        console.error(err);
        return;
      }
      res.send(
        response.schedule.filter((day: ScheduleEntry) =>
          day.courses.find((course: CourseEntry) => course.room.id === id)
        )
      );
    }
  );
});

app.get('/groups/:id/schedule', (req: Request, res: Response) => {
  const { id } = req.params;
  const { from, to } = req.query;
  scheduleGrpc.GetScheduleByDate(
    { from: from.toString() ||undefined , to: to.toString()||undefined },
    (err: Error | null, response: Schedule) => {
      if (err) {
        console.error(err);
        return;
      }
      res.send(
        response.schedule.filter((day: ScheduleEntry) =>
          day.courses.find((course: CourseEntry) => course.group.id === id)
        )
      );
    }
  );
});

app.listen(port, host, () => {
  console.log(`[ ready ] http://${host}:${port}`);
});
