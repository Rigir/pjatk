import {faker} from '@faker-js/faker';
faker.seed(0);

const rooms = Array.from({ length: 10 }, () => ({ id: faker.string.uuid() }));
const lecturers = Array.from({ length: 5 }, () => ({
  id: faker.string.uuid(),
}));
const students = Array.from({ length: 50 }, () => ({
  id: faker.string.uuid(),
}));
const groupSize = Math.floor(students.length / 5);
const groups = Array.from({ length: 5 }, (_, index) => {
  const startIndex = index * groupSize;
  const endIndex = startIndex + groupSize;
  return {
    id: faker.string.uuid(),
    students: students.slice(startIndex, endIndex),
  };
});

const courses = Array.from([
  'Mathematics',
  'History',
  'Biology',
  'Computer Science',
  'English Literature',
  'Physics',
  'Chemistry',
  'Art History',
  'Psychology',
  'Economics',
]).map((name) => {
  return {
    id: faker.string.uuid(),
    name: name,
    lecturer: lecturers[Math.floor(Math.random() * lecturers.length)],
    room: rooms[Math.floor(Math.random() * rooms.length)],
    group: groups[Math.floor(Math.random() * groups.length)],
  };
});

const generateFakeSchedule = () => {
  const schedule = [];
  for (let i = 0; i < 5; i++) {
    const date = faker.date.future().toISOString().split('T')[0];
    const numberOfEntries = Math.floor(Math.random() * 8) + 0;
    const entries = [];
    for (let j = 0; j < numberOfEntries; j++) {
      const course = courses[Math.floor(Math.random() * courses.length)];
      entries.push(course);
    }
    schedule.push({ date, courses: entries });
  }

  const sortedSchedule = schedule.sort((a, b) => a.date.localeCompare(b.date));
  return sortedSchedule;
};

const schedule = generateFakeSchedule();

export { rooms, lecturers, students, groups, schedule, courses };
