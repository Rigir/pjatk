import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import * as fake from './lib/scheduleData';
import {ProtoGrpcType} from 'packages/types/proto/scheduleRecipe';
import {ScheduleServiceHandlers} from 'packages/types/proto/ScheduleService';

const host = process.env.HOST ?? 'localhost';
const port = process.env.PORT ? Number(process.env.PORT) : 9090;

const scheduleProto = grpc.loadPackageDefinition(
  protoLoader.loadSync('packages/proto/scheduleRecipe.proto')
) as unknown as ProtoGrpcType;

const server = new grpc.Server();

server.addService(scheduleProto.ScheduleService.service, {
  GetStudentById: (call, callback) => {
    const student = fake.students.find(
      (student) => student.id === call.request.id
    );
    callback(null,  student);
  },
  GetStudents: (call, callback) => {
    callback(null, { students: fake.students });
  },
  GetLecturerById: (call, callback) => {
    const lecturer = fake.lecturers.find(
      (lecturer) => lecturer.id === call.request.id
    );
    callback(null,  lecturer );
  },
  GetLecturers: (call, callback) => {
    callback(null, { lecturers: fake.lecturers });
  },
  GetRoomById: (call, callback) => {
    const rooms = fake.rooms.find((rooms) => rooms.id === call.request.id);
    callback(null,  rooms );
  },
  GetRooms: (call, callback) => {
    callback(null, { rooms: fake.rooms });
  },
  GetGroupById: (call, callback) => {
    const group = fake.groups.find((group) => group.id === call.request.id);
    callback(null,  group );
  },
  GetGroups: (call, callback) => {
    callback(null, { groups: fake.groups });
  },
  GetCourseById: (call, callback) => {
    const course = fake.courses.find((course) => course.id === call.request.id);
    callback(null,  course );
  },
  GetCourses: (call, callback) => {
    callback(null, { courses: fake.courses });
  },
  GetScheduleByDate: (call, callback) => {
    const { from, to } = call.request;
    const schedule = fake.schedule.filter((entry) => {
      return (!from || entry.date >= from) && (!to || entry.date <= to);
    });
    callback(null, { schedule: schedule });
  },
  GetSchedule: (call, callback) => {
    callback(null, { schedule: fake.schedule });
  },
} as ScheduleServiceHandlers);

server.bindAsync(
  `${host}:${port}`,
  grpc.ServerCredentials.createInsecure(),
  (err, port) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(`Listening on port ${port}`);
    server.start();
  }
);
