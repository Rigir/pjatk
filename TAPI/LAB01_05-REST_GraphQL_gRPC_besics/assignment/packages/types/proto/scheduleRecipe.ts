import type * as grpc from '@grpc/grpc-js';
import type { MessageTypeDefinition } from '@grpc/proto-loader';

import type { ScheduleServiceClient as _ScheduleServiceClient, ScheduleServiceDefinition as _ScheduleServiceDefinition } from './ScheduleService';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  CourseEntry: MessageTypeDefinition
  Courses: MessageTypeDefinition
  DateRangeRequest: MessageTypeDefinition
  Empty: MessageTypeDefinition
  GroupEntry: MessageTypeDefinition
  Groups: MessageTypeDefinition
  IdRequest: MessageTypeDefinition
  LecturerEntry: MessageTypeDefinition
  Lecturers: MessageTypeDefinition
  RoomEntry: MessageTypeDefinition
  Rooms: MessageTypeDefinition
  Schedule: MessageTypeDefinition
  ScheduleEntry: MessageTypeDefinition
  ScheduleService: SubtypeConstructor<typeof grpc.Client, _ScheduleServiceClient> & { service: _ScheduleServiceDefinition }
  StudentEntry: MessageTypeDefinition
  Students: MessageTypeDefinition
}

