// Original file: packages/proto/scheduleRecipe.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { CourseEntry as _CourseEntry, CourseEntry__Output as _CourseEntry__Output } from './CourseEntry';
import type { Courses as _Courses, Courses__Output as _Courses__Output } from './Courses';
import type { DateRangeRequest as _DateRangeRequest, DateRangeRequest__Output as _DateRangeRequest__Output } from './DateRangeRequest';
import type { Empty as _Empty, Empty__Output as _Empty__Output } from './Empty';
import type { GroupEntry as _GroupEntry, GroupEntry__Output as _GroupEntry__Output } from './GroupEntry';
import type { Groups as _Groups, Groups__Output as _Groups__Output } from './Groups';
import type { IdRequest as _IdRequest, IdRequest__Output as _IdRequest__Output } from './IdRequest';
import type { LecturerEntry as _LecturerEntry, LecturerEntry__Output as _LecturerEntry__Output } from './LecturerEntry';
import type { Lecturers as _Lecturers, Lecturers__Output as _Lecturers__Output } from './Lecturers';
import type { RoomEntry as _RoomEntry, RoomEntry__Output as _RoomEntry__Output } from './RoomEntry';
import type { Rooms as _Rooms, Rooms__Output as _Rooms__Output } from './Rooms';
import type { Schedule as _Schedule, Schedule__Output as _Schedule__Output } from './Schedule';
import type { StudentEntry as _StudentEntry, StudentEntry__Output as _StudentEntry__Output } from './StudentEntry';
import type { Students as _Students, Students__Output as _Students__Output } from './Students';

export interface ScheduleServiceClient extends grpc.Client {
  GetCourseById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  GetCourseById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  GetCourseById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  GetCourseById(argument: _IdRequest, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  getCourseById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  getCourseById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  getCourseById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  getCourseById(argument: _IdRequest, callback: grpc.requestCallback<_CourseEntry__Output>): grpc.ClientUnaryCall;
  
  GetCourses(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  GetCourses(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  GetCourses(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  GetCourses(argument: _Empty, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  getCourses(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  getCourses(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  getCourses(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  getCourses(argument: _Empty, callback: grpc.requestCallback<_Courses__Output>): grpc.ClientUnaryCall;
  
  GetGroupById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  GetGroupById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  GetGroupById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  GetGroupById(argument: _IdRequest, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  getGroupById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  getGroupById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  getGroupById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  getGroupById(argument: _IdRequest, callback: grpc.requestCallback<_GroupEntry__Output>): grpc.ClientUnaryCall;
  
  GetGroups(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  GetGroups(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  GetGroups(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  GetGroups(argument: _Empty, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  getGroups(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  getGroups(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  getGroups(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  getGroups(argument: _Empty, callback: grpc.requestCallback<_Groups__Output>): grpc.ClientUnaryCall;
  
  GetLecturerById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  GetLecturerById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  GetLecturerById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  GetLecturerById(argument: _IdRequest, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  getLecturerById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  getLecturerById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  getLecturerById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  getLecturerById(argument: _IdRequest, callback: grpc.requestCallback<_LecturerEntry__Output>): grpc.ClientUnaryCall;
  
  GetLecturers(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  GetLecturers(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  GetLecturers(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  GetLecturers(argument: _Empty, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  getLecturers(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  getLecturers(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  getLecturers(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  getLecturers(argument: _Empty, callback: grpc.requestCallback<_Lecturers__Output>): grpc.ClientUnaryCall;
  
  GetRoomById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  GetRoomById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  GetRoomById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  GetRoomById(argument: _IdRequest, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  getRoomById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  getRoomById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  getRoomById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  getRoomById(argument: _IdRequest, callback: grpc.requestCallback<_RoomEntry__Output>): grpc.ClientUnaryCall;
  
  GetRooms(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  GetRooms(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  GetRooms(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  GetRooms(argument: _Empty, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  getRooms(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  getRooms(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  getRooms(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  getRooms(argument: _Empty, callback: grpc.requestCallback<_Rooms__Output>): grpc.ClientUnaryCall;
  
  GetSchedule(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  GetSchedule(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  GetSchedule(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  GetSchedule(argument: _Empty, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getSchedule(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getSchedule(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getSchedule(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getSchedule(argument: _Empty, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  
  GetScheduleByDate(argument: _DateRangeRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  GetScheduleByDate(argument: _DateRangeRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  GetScheduleByDate(argument: _DateRangeRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  GetScheduleByDate(argument: _DateRangeRequest, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getScheduleByDate(argument: _DateRangeRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getScheduleByDate(argument: _DateRangeRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getScheduleByDate(argument: _DateRangeRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  getScheduleByDate(argument: _DateRangeRequest, callback: grpc.requestCallback<_Schedule__Output>): grpc.ClientUnaryCall;
  
  GetStudentById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  GetStudentById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  GetStudentById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  GetStudentById(argument: _IdRequest, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  getStudentById(argument: _IdRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  getStudentById(argument: _IdRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  getStudentById(argument: _IdRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  getStudentById(argument: _IdRequest, callback: grpc.requestCallback<_StudentEntry__Output>): grpc.ClientUnaryCall;
  
  GetStudents(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  GetStudents(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  GetStudents(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  GetStudents(argument: _Empty, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  getStudents(argument: _Empty, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  getStudents(argument: _Empty, metadata: grpc.Metadata, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  getStudents(argument: _Empty, options: grpc.CallOptions, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  getStudents(argument: _Empty, callback: grpc.requestCallback<_Students__Output>): grpc.ClientUnaryCall;
  
}

export interface ScheduleServiceHandlers extends grpc.UntypedServiceImplementation {
  GetCourseById: grpc.handleUnaryCall<_IdRequest__Output, _CourseEntry>;
  
  GetCourses: grpc.handleUnaryCall<_Empty__Output, _Courses>;
  
  GetGroupById: grpc.handleUnaryCall<_IdRequest__Output, _GroupEntry>;
  
  GetGroups: grpc.handleUnaryCall<_Empty__Output, _Groups>;
  
  GetLecturerById: grpc.handleUnaryCall<_IdRequest__Output, _LecturerEntry>;
  
  GetLecturers: grpc.handleUnaryCall<_Empty__Output, _Lecturers>;
  
  GetRoomById: grpc.handleUnaryCall<_IdRequest__Output, _RoomEntry>;
  
  GetRooms: grpc.handleUnaryCall<_Empty__Output, _Rooms>;
  
  GetSchedule: grpc.handleUnaryCall<_Empty__Output, _Schedule>;
  
  GetScheduleByDate: grpc.handleUnaryCall<_DateRangeRequest__Output, _Schedule>;
  
  GetStudentById: grpc.handleUnaryCall<_IdRequest__Output, _StudentEntry>;
  
  GetStudents: grpc.handleUnaryCall<_Empty__Output, _Students>;
  
}

export interface ScheduleServiceDefinition extends grpc.ServiceDefinition {
  GetCourseById: MethodDefinition<_IdRequest, _CourseEntry, _IdRequest__Output, _CourseEntry__Output>
  GetCourses: MethodDefinition<_Empty, _Courses, _Empty__Output, _Courses__Output>
  GetGroupById: MethodDefinition<_IdRequest, _GroupEntry, _IdRequest__Output, _GroupEntry__Output>
  GetGroups: MethodDefinition<_Empty, _Groups, _Empty__Output, _Groups__Output>
  GetLecturerById: MethodDefinition<_IdRequest, _LecturerEntry, _IdRequest__Output, _LecturerEntry__Output>
  GetLecturers: MethodDefinition<_Empty, _Lecturers, _Empty__Output, _Lecturers__Output>
  GetRoomById: MethodDefinition<_IdRequest, _RoomEntry, _IdRequest__Output, _RoomEntry__Output>
  GetRooms: MethodDefinition<_Empty, _Rooms, _Empty__Output, _Rooms__Output>
  GetSchedule: MethodDefinition<_Empty, _Schedule, _Empty__Output, _Schedule__Output>
  GetScheduleByDate: MethodDefinition<_DateRangeRequest, _Schedule, _DateRangeRequest__Output, _Schedule__Output>
  GetStudentById: MethodDefinition<_IdRequest, _StudentEntry, _IdRequest__Output, _StudentEntry__Output>
  GetStudents: MethodDefinition<_Empty, _Students, _Empty__Output, _Students__Output>
}
