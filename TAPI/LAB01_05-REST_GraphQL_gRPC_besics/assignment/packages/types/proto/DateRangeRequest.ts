// Original file: packages/proto/scheduleRecipe.proto


export interface DateRangeRequest {
  'from'?: (string);
  'to'?: (string);
  '_from'?: "from";
  '_to'?: "to";
}

export interface DateRangeRequest__Output {
  'from'?: (string);
  'to'?: (string);
  '_from': "from";
  '_to': "to";
}
