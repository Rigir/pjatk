// Original file: packages/proto/scheduleRecipe.proto


export interface IdRequest {
  'id'?: (string);
}

export interface IdRequest__Output {
  'id': (string);
}
