// Original file: packages/proto/scheduleRecipe.proto

import type { ScheduleEntry as _ScheduleEntry, ScheduleEntry__Output as _ScheduleEntry__Output } from './ScheduleEntry';

export interface Schedule {
  'schedule'?: (_ScheduleEntry)[];
}

export interface Schedule__Output {
  'schedule': (_ScheduleEntry__Output)[];
}
