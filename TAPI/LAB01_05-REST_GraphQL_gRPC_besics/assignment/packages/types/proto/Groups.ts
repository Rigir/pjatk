// Original file: packages/proto/scheduleRecipe.proto

import type { GroupEntry as _GroupEntry, GroupEntry__Output as _GroupEntry__Output } from './GroupEntry';

export interface Groups {
  'groups'?: (_GroupEntry)[];
}

export interface Groups__Output {
  'groups': (_GroupEntry__Output)[];
}
