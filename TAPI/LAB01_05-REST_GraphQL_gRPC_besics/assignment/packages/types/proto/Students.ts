// Original file: packages/proto/scheduleRecipe.proto

import type { StudentEntry as _StudentEntry, StudentEntry__Output as _StudentEntry__Output } from './StudentEntry';

export interface Students {
  'students'?: (_StudentEntry)[];
}

export interface Students__Output {
  'students': (_StudentEntry__Output)[];
}
