// Original file: packages/proto/scheduleRecipe.proto

import type { LecturerEntry as _LecturerEntry, LecturerEntry__Output as _LecturerEntry__Output } from './LecturerEntry';
import type { RoomEntry as _RoomEntry, RoomEntry__Output as _RoomEntry__Output } from './RoomEntry';
import type { GroupEntry as _GroupEntry, GroupEntry__Output as _GroupEntry__Output } from './GroupEntry';

export interface CourseEntry {
  'id'?: (string);
  'name'?: (string);
  'lecturer'?: (_LecturerEntry | null);
  'room'?: (_RoomEntry | null);
  'group'?: (_GroupEntry | null);
}

export interface CourseEntry__Output {
  'id': (string);
  'name': (string);
  'lecturer': (_LecturerEntry__Output | null);
  'room': (_RoomEntry__Output | null);
  'group': (_GroupEntry__Output | null);
}
