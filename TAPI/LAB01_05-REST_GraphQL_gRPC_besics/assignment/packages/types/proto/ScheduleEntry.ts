// Original file: packages/proto/scheduleRecipe.proto

import type { CourseEntry as _CourseEntry, CourseEntry__Output as _CourseEntry__Output } from './CourseEntry';

export interface ScheduleEntry {
  'date'?: (string);
  'courses'?: (_CourseEntry)[];
}

export interface ScheduleEntry__Output {
  'date': (string);
  'courses': (_CourseEntry__Output)[];
}
