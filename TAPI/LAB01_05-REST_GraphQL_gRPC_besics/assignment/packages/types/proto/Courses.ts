// Original file: packages/proto/scheduleRecipe.proto

import type { CourseEntry as _CourseEntry, CourseEntry__Output as _CourseEntry__Output } from './CourseEntry';

export interface Courses {
  'courses'?: (_CourseEntry)[];
}

export interface Courses__Output {
  'courses': (_CourseEntry__Output)[];
}
