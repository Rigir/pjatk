// Original file: packages/proto/scheduleRecipe.proto

import type { RoomEntry as _RoomEntry, RoomEntry__Output as _RoomEntry__Output } from './RoomEntry';

export interface Rooms {
  'rooms'?: (_RoomEntry)[];
}

export interface Rooms__Output {
  'rooms': (_RoomEntry__Output)[];
}
