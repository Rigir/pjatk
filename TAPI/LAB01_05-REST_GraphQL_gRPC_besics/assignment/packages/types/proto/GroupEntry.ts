// Original file: packages/proto/scheduleRecipe.proto

import type { StudentEntry as _StudentEntry, StudentEntry__Output as _StudentEntry__Output } from './StudentEntry';

export interface GroupEntry {
  'id'?: (string);
  'students'?: (_StudentEntry)[];
}

export interface GroupEntry__Output {
  'id': (string);
  'students': (_StudentEntry__Output)[];
}
