// Original file: packages/proto/scheduleRecipe.proto

import type { LecturerEntry as _LecturerEntry, LecturerEntry__Output as _LecturerEntry__Output } from './LecturerEntry';

export interface Lecturers {
  'lecturers'?: (_LecturerEntry)[];
}

export interface Lecturers__Output {
  'lecturers': (_LecturerEntry__Output)[];
}
