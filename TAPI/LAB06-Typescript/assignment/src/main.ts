/*
    Utwórz typy dla studenta oraz nauczyciela.

    Student powinien mieć:
    - imię
    - nazwisko
    - numer indeksu
    - listę zajęć na które uczęszcza
    - listę ocen (może nie posiadać żadnej)
    - informację czy studia skończone

    Nauczyciel powinien mieć:
    - imię
    - nazwisko
    - przedmiot
    - przygotowane egzaminy (których typu nie znamy ;) )
    - funkcję do przeprowadzania egzaminu

    Stwórz również typ dla tablicy, która może zawierać zarówno studentów jak i nauczycieli.
    Stwórz typ na podstawie typu studenta oraz nauczyciela, który pozwoli nam wypisać ich imiona oraz nazwiska.
*/

type Student = {
    name: string;
    surname: string;
    index: number;
    subjects: string[];
    grades: number[];
    isGraduated: boolean;
}

type Lecturer = {
    name: string;
    surname: string;
    subject: string;
    exams: unknown[];
    exam: () => void;
}

type Persons = [Student | Lecturer];
type Names = Pick<Student | Lecturer, 'name' | 'surname'>;

/*
    Stwórz funkcję która będzie mogła przyjmować wiele argumentów typu number lub string.
    Dopisz do tej funkcji ciało, które zwróci sumę wszystkich liczb.
    Jeśli podana liczba będzie stringiem należy ją sparsować do typu number.

    Dodaj do funkcji argument boolean, na podstawie któego zwracany wynik będzie typu number lub string.
*/

function fun(asString: boolean, ...args: (string | number)[]) {
    let value = args
        .map(arg => (typeof arg === "string" ? parseInt(arg) : arg))
        .filter((arg): arg is number => !isNaN(arg))
        .reduce((acc, v) => acc + v, 0);
    return (asString) ? value.toString() : value
}

console.log(fun(false, 1, "2", 3));

/*
    Utwórz typ tuple, który przyjmie numer indeksu studenta, oraz jego ocenę, i nic więcej.
    Stwórz przykładową tablicę takich tupli.
*/

type tuple = [string, number]

var studentAndMark: tuple = ["Steve", 1];

/*
    Przepisz podany niżej typ `Person` tak, aby funkcja `exam` działała poprawnie. i nie zwracała błędów.
*/

type NewStudent = {
    type: 'student';
    takeExam: () => void;
}

type NewTeacher = {
    type: 'teacher';
    prepareExam: () => void;
}

type NewPerson = NewStudent | NewTeacher;

const exam = (person: NewPerson) => {
    if (person.type === 'student') {
        person.takeExam();
    } else {
        person.prepareExam();
    }
}

/*
    Utwórz typ, zwracany przez funkcję `message`, który będzie aktualny niezależny od ciała funkcji `message`.
*/
type Message = ReturnType<typeof message>

const message = (t: unknown) => {
    if (typeof t === 'string') {
        return `message: ${t}`;
    } else if (typeof t === 'number') {
        return t;
    } else {
        return new Error('Invalid type');
    }
}