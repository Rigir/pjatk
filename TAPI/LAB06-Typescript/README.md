![PJATK](../../.assets/banner.png)
=========

Zad01: 
```
Utwórz typy dla studenta oraz nauczyciela.

Student powinien mieć:
- imię
- nazwisko
- numer indeksu
- listę zajęć na które uczęszcza
- listę ocen (może nie posiadać żadnej)
- informację czy studia skończone

Nauczyciel powinien mieć:
- imię
- nazwisko
- przedmiot
- przygotowane egzaminy (których typu nie znamy ;) )
- funkcję do przeprowadzania egzaminu

Stwórz również typ dla tablicy, która może zawierać zarówno studentów jak i nauczycieli.
Stwórz typ na podstawie typu studenta oraz nauczyciela, który pozwoli nam wypisać ich imiona oraz nazwiska.
```

Zad02
```
Stwórz funkcję która będzie mogła przyjmować wiele argumentów typu number lub string.
Dopisz do tej funkcji ciało, które zwróci sumę wszystkich liczb.
Jeśli podana liczba będzie stringiem należy ją sparsować do typu number.


Dodaj do funkcji argument boolean, na podstawie któego zwracany wynik będzie typu number lub string.
```

Zad03
```
Utwórz typ tuple, który przyjmie numer indeksu studenta, oraz jego ocenę, i nic więcej.
Stwórz przykładową tablicę takich tupli.
```

Zad04
```
Przepisz podany niżej typ `Person` tak, aby funkcja `exam` działała poprawnie. i nie zwracała błędów.
```
Zad05
```
Utwórz typ, zwracany przez funkcję `message`, który będzie aktualny niezależny od ciała funkcji `message`.
```
