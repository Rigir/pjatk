/*
 * Copyright 2002-2008 Guillaume Cottenceau, 2015 Aleksander Denisiuk
 *
 * This software may be freely redistributed under the terms
 * of the X11 license.
 *
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define PNG_DEBUG 3
#include <png.h>

#define OUT_FILE "initials.png"
#define WIDTH 600
#define HEIGHT 600
#define COLOR_TYPE PNG_COLOR_TYPE_RGB
#define BIT_DEPTH 8

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep *row_pointers;

// Funcions headres
void create_png_file(void);
void process_file(void);
void write_png_file(char *file_name);
void abort_(const char *s, ...);

int *move_polygon(int n, int *arr, int moveBy);
void fill_background(png_byte cr, png_byte cg, png_byte cb);
void fill_space(int x, int y,
                png_byte fill_cr, png_byte fill_cg, png_byte fill_cb,
                png_byte boundary_cr, png_byte boundary_cg, png_byte boundary_cb);

png_byte *read_pixel(int x, int y);
void write_pixel(int x, int y, png_byte cr, png_byte cg, png_byte cb);
void bresenham_polygon(int n, int *x, int *y, png_byte cr, png_byte cg, png_byte cb);
void bresenham_line(int x1, int y1, int x2, int y2, png_byte cr, png_byte cg, png_byte cb);

void bresenham_circle(int x0, int y0, int r, png_byte cr, png_byte cg, png_byte cb);
void draw_circle(int xc, int yc, int x, int y, png_byte cr, png_byte cg, png_byte cb);

// Program starts here
int main(int argc, char **argv)
{
  create_png_file();
  process_file();
  write_png_file(OUT_FILE);

  return 0;
}

void fill_space(int x, int y,
                png_byte fill_cr, png_byte fill_cg, png_byte fill_cb,
                png_byte boundary_cr, png_byte boundary_cg, png_byte boundary_cb)
{
  png_byte *color = read_pixel(x, y);
  if ((color[0] != fill_cr && color[1] != fill_cg && color[2] != fill_cb) &&
      (color[0] != boundary_cr && color[1] != boundary_cg && color[2] != boundary_cb))
  {
    write_pixel(x, y, fill_cr, fill_cg, fill_cb);
    fill_space(x - 1, y, fill_cr, fill_cg, fill_cb, boundary_cr, boundary_cg, boundary_cb);
    fill_space(x, y - 1, fill_cr, fill_cg, fill_cb, boundary_cr, boundary_cg, boundary_cb);
    fill_space(x + 1, y, fill_cr, fill_cg, fill_cb, boundary_cr, boundary_cg, boundary_cb);
    fill_space(x, y + 1, fill_cr, fill_cg, fill_cb, boundary_cr, boundary_cg, boundary_cb);
  }
}

void process_file(void)
{
  fill_background(38, 47, 45);
  bresenham_circle(300, 300, 220, 254, 175, 59);

  // Write 'R'
  int x_r_outline[] = {231, 195, 195, 172, 172, 231, 260, 272, 285, 272, 257, 272, 312, 286, 253};
  int y_r_outline[] = {319, 319, 395, 395, 213, 213, 222, 230, 264, 300, 310, 320, 395, 395, 332};
  bresenham_polygon(15, x_r_outline, y_r_outline, 0, 0, 0);

  int x_r_inside[] = {195, 195, 231, 253, 262, 253, 231};
  int y_r_inside[] = {300, 234, 234, 242, 264, 290, 300};
  bresenham_polygon(7, x_r_inside, y_r_inside, 0, 0, 0);

  // Write 'P'
  bresenham_polygon(11, move_polygon(11, x_r_outline, 175), y_r_outline, 0, 0, 0);
  bresenham_polygon(7, move_polygon(7, x_r_inside, 175), y_r_inside, 0, 0, 0);

  fill_space(173, 394, 238, 233, 193, 0, 0, 0);
  fill_space(348, 394, 238, 233, 193, 0, 0, 0);
}

void bresenham_circle(int xc, int yc, int r, png_byte cr, png_byte cg, png_byte cb)
{
  int y = r;
  int d = 3 - 2 * r;
  for (int x = 0; y >= x; x++)
  {
    draw_circle(xc, yc, x, y, cr, cg, cb);
    if (d < 0)
    {
      d += 4 * x + 6;
    }
    else
    {
      y--;
      d += 4 * (x - y) + 10;
    }
  }
}

void draw_circle(int xc, int yc, int x, int y, png_byte cr, png_byte cg, png_byte cb)
{
  bresenham_line(xc - x, yc + y, xc + x, yc + y, cr, cg, cb);
  bresenham_line(xc - x, yc - y, xc + x, yc - y, cr, cg, cb);
  bresenham_line(xc - y, yc + x, xc + y, yc + x, cr, cg, cb);
  bresenham_line(xc - y, yc - x, xc + y, yc - x, cr, cg, cb);
}

void bresenham_polygon(int n, int *x, int *y, png_byte cr, png_byte cg, png_byte cb)
{
  for (int i = 0; i < n - 1; i++)
  {
    bresenham_line(x[i], y[i], x[i + 1], y[i + 1], cr, cg, cb);
  }
  bresenham_line(x[n - 1], y[n - 1], x[0], y[0], cr, cg, cb);
}

void bresenham_line(int x1, int y1, int x2, int y2, png_byte cr, png_byte cg, png_byte cb)
{

  int incx = (x2 < x1) ? -1 : 1;
  int incy = (y2 < y1) ? -1 : 1;

  int dx = abs(x2 - x1);
  int dy = abs(y2 - y1);

  int x = x1;
  int y = y1;
  write_pixel(x, y, cr, cg, cb);
  if (dx > dy)
  {
    // Slope less than 1
    int err = 2 * dy - dx;
    for (int i = 0; i < dx; i++)
    {
      if (err >= 0)
      {
        y += incy;
        err += 2 * (dy - dx);
      }
      else
      {
        err += 2 * dy;
      }
      x += incx;
      write_pixel(x, y, cr, cg, cb);
    }
  }
  else
  {
    // Slope greater than 1
    int err = 2 * dx - dy;
    for (int i = 0; i < dy; i++)
    {
      if (err >= 0)
      {
        x += incx;
        err += 2 * (dx - dy);
      }
      else
      {
        err += 2 * dx;
      }
      y += incy;
      write_pixel(x, y, cr, cg, cb);
    }
  }
}

int *move_polygon(int n, int *arr, int moveBy)
{
  for (int i = 0; i < n; i++)
  {
    arr[i] += moveBy;
  }
  return arr;
}

void write_pixel(int x, int y, png_byte cr, png_byte cg, png_byte cb)
{
  png_byte *row = row_pointers[y];
  png_byte *ptr = &(row[x * 3]);
  ptr[0] = cr;
  ptr[1] = cg;
  ptr[2] = cb;
}

png_byte *read_pixel(int x, int y)
{
  png_byte *row = row_pointers[y];
  png_byte *ptr = &(row[x * 3]);
  return ptr;
}

void fill_background(png_byte cr, png_byte cg, png_byte cb)
{
  for (int y = 0; y < height; y++)
  {
    png_byte *row = row_pointers[y];
    for (int x = 0; x < width; x++)
    {
      png_byte *ptr = &(row[x * 3]);
      ptr[0] = cr;
      ptr[1] = cg;
      ptr[2] = cb;
    }
  }
}

void create_png_file(void)
{
  width = WIDTH;
  height = HEIGHT;
  bit_depth = BIT_DEPTH;
  color_type = COLOR_TYPE;

  row_pointers = (png_bytep *)malloc(sizeof(png_bytep) * height);
  for (int y = 0; y < height; y++)
    row_pointers[y] = (png_byte *)malloc(width * bit_depth * 3);
}

void write_png_file(char *file_name)
{
  /* create file */
  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    abort_("[write_png_file] File %s could not be opened for writing",
           file_name);

  /* initialize stuff */
  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!png_ptr)
    abort_("[write_png_file] png_create_write_struct failed");

  info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
    abort_("[write_png_file] png_create_info_struct failed");

  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during init_io");

  png_init_io(png_ptr, fp);

  /* write header */
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during writing header");

  png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, color_type,
               PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
               PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);

  /* write bytes */
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during writing bytes");

  png_write_image(png_ptr, row_pointers);

  /* end write */
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during end of write");

  png_write_end(png_ptr, NULL);

  /* cleanup heap allocation */
  for (int y = 0; y < height; y++)
    free(row_pointers[y]);
  free(row_pointers);

  fclose(fp);
}

void abort_(const char *s, ...)
{
  va_list args;
  va_start(args, s);
  vfprintf(stderr, s, args);
  fprintf(stderr, "\n");
  va_end(args);
  abort();
}