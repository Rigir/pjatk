DROP TABLE IF EXISTS wypozyczyl, egzemplarz, czytelnik, wydanie, napisal, ksiazka, autor;

CREATE TABLE autor (
    id              INTEGER PRIMARY KEY,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL
);

CREATE TABLE ksiazka (
    kod             INTEGER PRIMARY KEY,
    tytul           VARCHAR(255) NOT NULL
);

CREATE TABLE napisal (
    id              INTEGER PRIMARY KEY,
    id_autora       INTEGER NOT NULL,
    id_ksiazka      INTEGER NOT NULL,
    CONSTRAINT fk_autor FOREIGN KEY(id_autora) REFERENCES autor(id),
    CONSTRAINT fk_ksiazka FOREIGN KEY(id_ksiazka) REFERENCES ksiazka(kod)
);

CREATE TABLE wydanie (
    id              INTEGER PRIMARY KEY,
    wydawnictwo     VARCHAR(255) NOT NULL,
    cena            NUMERIC(255, 2) NOT NULL,
    rok             DATE NOT NULL,
    id_ksiazka      INTEGER NOT NULL,
    CONSTRAINT fk_ksiazka FOREIGN KEY(id_ksiazka) REFERENCES ksiazka(kod)
);

CREATE TABLE czytelnik (
    nr_karty        INTEGER PRIMARY KEY,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL
);

CREATE TABLE egzemplarz (
    nr_inw          INTEGER PRIMARY KEY,
    data            DATE,
    id_wydanie      INTEGER NOT NULL,
    id_czytelnik    INTEGER,
    data_od         DATE,
    CONSTRAINT fk_wydanie FOREIGN KEY(id_wydanie) REFERENCES wydanie(id),
    CONSTRAINT fk_czytelnik FOREIGN KEY(id_czytelnik) REFERENCES czytelnik(nr_karty)
);

CREATE TABLE wypozyczyl (
    id                  INTEGER PRIMARY KEY,
    data_od             DATE NOT NULL,
    data_do             DATE NOT NULL,
    id_egzemplarz       INTEGER NOT NULL, 
    id_czytelnik        INTEGER NOT NULL,
    CONSTRAINT fk_egzemplarz FOREIGN KEY(id_egzemplarz) REFERENCES egzemplarz(nr_inw),
    CONSTRAINT fk_czytelnik FOREIGN KEY(id_czytelnik) REFERENCES czytelnik(nr_karty)
);