DROP TABLE IF EXISTS ksiazka, czytelnik;

CREATE TABLE czytelnik (
    nr_karty        INTEGER PRIMARY KEY,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL
);

CREATE TABLE ksiazka (
    nr_inw          INTEGER PRIMARY KEY,
    tytul           VARCHAR(255) NOT NULL,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL,
    wydawnictwo     VARCHAR(255) NOT NULL,
    cena            NUMERIC(255, 2) NOT NULL,
    rok             DATE NOT NULL,
    wypozycza       INTEGER,
    data            DATE, 
    CONSTRAINT fk_czytelnik FOREIGN KEY(wypozycza) REFERENCES czytelnik(nr_karty)
);
