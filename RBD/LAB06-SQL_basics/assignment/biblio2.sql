DROP TABLE IF EXISTS egzemplarz, czytelnik, ksiazka;

CREATE TABLE ksiazka (
    nr_inw          INTEGER PRIMARY KEY,
    tytul           VARCHAR(255) NOT NULL,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL,
    wydawnictwo     VARCHAR(255) NOT NULL,
    cena            NUMERIC(255, 2) NOT NULL,
    rok             DATE
);

CREATE TABLE czytelnik (
    nr_karty        INTEGER PRIMARY KEY,
    imie            VARCHAR(255) NOT NULL,
    nazwisko        VARCHAR(255) NOT NULL
);

CREATE TABLE egzemplarz (
    nr_inw          INTEGER PRIMARY KEY,
    data            DATE,
    ma              INTEGER NOT NULL,
    wypozycza       INTEGER,
    CONSTRAINT fk_ksiazka FOREIGN KEY(ma) REFERENCES ksiazka(nr_inw),
    CONSTRAINT fk_czytelnik FOREIGN KEY(wypozycza) REFERENCES czytelnik(nr_karty)
);