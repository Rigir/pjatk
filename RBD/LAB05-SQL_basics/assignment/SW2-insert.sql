INSERT INTO nauczyciel (nr_leg, imie, nazwisko) VALUES 
	('L 001', 'Jan', 'Kowalski'),
	('L 002', 'Michał', 'Michalski'),
	('L 003', 'Anna', 'Nowak'),
	('L 004', 'Zuzanna', 'Przepiórkowska'),
	('L 005', 'Eleonora', 'Plichta');

INSERT INTO student (nr_ind, imie, nazwisko) VALUES 
	('S 001', 'Aleksander', 'Cyra'),
	('S 002', 'Hermenegilda', 'Kociubińska'),
	('S 003', 'Krzysztof', 'Kulikowski'),
	('S 004', 'Jerzy', 'Samp'),
	('S 005', 'Bronisław', 'Wstęp');

INSERT INTO przedmiot (kod, rodzaj, nazwa, godziny, nr_leg) VALUES
	(1, 'wyklad', 'Matematyka dyskretna', 2, 'L 004'),
	(2, 'wyklad', 'Programowanie w C', 2, 'L 002'),
	(3, 'laboratorium', 'Programowanie w C', 2, 'L 001'),
	(4, 'ćwiczenia', 'Matematyka dyskretna', 2, 'L 004'),
	(5, 'wykład', 'Filozofia', 2, 'L 003');

INSERT INTO termin (dzien_tyg, godzina, sala, kod) VALUES
	(1, 10, 'aula', 1),
	(2, 8, 's.110', 2),
	(3, 12, 'l.109', 3),
	(2, 12, 's.13', 4),
	(4, 13, 'aula', 5);

INSERT INTO jest_sluchaczem (nr_ind, kod) VALUES
	('S 001', 1), ('S 001', 4), ('S 001', 2), ('S 001', 3),
	('S 002', 1), ('S 003', 3), ('S 003', 4), ('S 004', 1), 
	('S 004', 5), ('S 005', 3);

