--SET client_encoding='utf-8';
-- złączenia:
-- sprawdź numery zamówień klientów
SELECT imie, nazwisko, zamowienie.nr AS zamowienie_nr 
   FROM klient, zamowienie
   WHERE klient.nr=zamowienie.klient_nr
   ORDER BY nazwisko
;

-- zmień dalsze zapytania używając INNER JOIN
SELECT imie, nazwisko, zamowienie.nr AS zamowienie_nr
   FROM klient INNER JOIN zamowienie 
   ON klient.nr=zamowienie.klient_nr
   ORDER BY nazwisko
;

-- sprawdź jacy klienci w ogóle złożyli zamówienia
SELECT imie, nazwisko
   FROM klient, zamowienie
   WHERE klient.nr=zamowienie.klient_nr
   ORDER BY nazwisko
;

-- zad P5: 
-- zmień powyższe zapytanie używając INNER JOIN:
SELECT imie, nazwisko
   FROM klient INNER JOIN zamowienie
   ON klient.nr=zamowienie.klient_nr
   ORDER BY nazwisko
;

-- wypisz towary wraz z numerami zamówień
SELECT towar.opis, zamowienie.nr AS zamowienie_nr 
   FROM ((pozycja
   INNER JOIN towar ON towar.nr = pozycja.towar_nr)
   INNER JOIN zamowienie ON zamowienie.nr = pozycja.zamowienie_nr)
   ORDER BY towar.opis
;

-- sprawdź jakie towary były w ogóle zamawiane
SELECT DISTINCT nr, towar.opis 
   FROM towar,pozycja 
   WHERE towar_nr=nr 
   ORDER BY towar.opis
;

-- użyj obu wersji zapytania