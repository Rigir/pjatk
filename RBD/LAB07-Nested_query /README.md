![PJATK](../../.assets/banner.png)
=========

Baza danych:
---
![instrucions](resources/_database.png)\
`Pliki: create.sql, drop.sql, insert.sql znajdują się w folderze "resources".`

Zadania:
---

* Plik pyt-01.sql
  *  Wyświetl zawartość powyższych tabel uporządkowanych odpowiednio według miast, opisów;
  *  Wyświetl zawartość tabeli zamówień uporządkowanej według kosztów wysyłki malejąco
  *  Wyświetl tabeli kodów kreskowych uporządkowanej według kodów (rosnąco).
* Plik pyt-02.sql
  *  Wyświetl nazwiska i pełne adresy klientów.
  *  Wyświetl numery i daty złożenie zamówień, zastosuj jakieś uporządkowanie.
* Plik pyt-03.sql
  *  Wyświetl dane o procentowym zysku w stosunku do kosztów.
  *  Wyświetl dane o czasie realizacji zamówień (różnica dat złożenia i wysyłki zamówienia).
* Plik pyt-04.sql
  *  Wyświetl w kolejnych zapytaniach: 
     *  Dane klientów spoza Gdańska,
     *  Klientów bez telefonu,
     *  Klientów spoza Gdańska i bez telefonu,
     *  Dane o układankach,
     *  Zamówienia niezrealizowane (bez daty wysyłki),
     *  Zrealizowane w lutym 2021,
     *  Dane o czasie realizacji zamówień już zrealizowanych
* Plik pyt-05.sql
  * Sprawdź numery zamówień klientów
  * Zmień powyższe zapytanie używając INNER JOIN:
  * Wypisz towary wraz z numerami zamówień
  * Sprawdź jakie towary były w ogóle zamawiane
  * Użyj obu wersji zapytania

