![PJATK](../../.assets/banner.png)
=========

Baza danych:
---
![instrucions](resources/_database.png)\
`Pliki: create.sql, drop.sql, insert.sql znajdują się w folderze "resources".`

Zadania:
---

* Plik pyt-06.sql
  * Sprawdź czy ceny towarów powtarzają się;
  * Wypisz opis i ceny towarów o powtarzających się cenach, to samo dla kosztu.
* Plik pyt-07.sql
  * Wypisz daty złożenia zamówień nie mających żadnych pozycji,
  * Wypisz opisy towarów niezamawianych,
  * Wypisz opisy towarów nie posiadających kodu kreskowego
* Plik pyt-08.sql
  * Znajdź klientów, którzy złożyli zamówienia,
  * Znajdź zamówienia puste, bez pozycji,
  * Znajdź klientów, którzy złożyli takie zamówienia.
* Plik pyt-09.sql
  * Uporządkuj powyższy wydruk w/g klientów odrzuć wiersze, dla których nie da się określić zysku.
* Plik pyt-10.sql
   * Z tabel klientów i zamówień oblicz minimalny, maksymalny i średni czas oczekiwania przez poszczególnych klientów (zamówienia niezrealizowane nie liczą się do średniej).
* Plik pyt-11.sql
  * Sprawdź zamówienia na chusteczki higieniczne, kto zamawiał, ile, jaki jest średni czas oczekiwania na realizację zamówienie na chusteczki.
* Plik pyt-12.sql
  * Zmodyfikuj podobnie zapytanie tak, by dotyczyło towarów zamiast pozycji.
* Plik pyt-13.sql
  * Wymień pary klientów mieszkających w tym samym mieście.
  * Wymień pary towarów mających tę samą cenę oraz pary z tym samym kosztem.
* Plik pyt-14.sql
  * Towarom nie mającym kody kreskowego dodaj do opisu słowa 'brak kodu', 
  * Towarom mającym kod dodaj słowo 'KOD'
* Plik pyt-15.sql
  * Usuń klientów z Sopotu,
  * Usuń klientów, których telefonu nie znamy(nie wykona się)
  * Usuń kody kreskowe nie odpowiadające żadnym towarom
  * Usuń klientów, którzy nie złożyli żadnego zamówienia
  * Usuń zamówienie, które nie posiada pozycji
  * Usuń towary nie posiadające kodu kreskowego (nie wykona się)
* Plik pyt-16.sql
  * Wstaw nowy kod kreskowy dla jakiegoś towaru
  * Wstaw dwa inne kody nie podając towaru, któremu odpowiadają.
  * Wstaw zamówienie, nie podając daty wysyłki, użyj formatu daty ISO
  * Wstaw klienta o nazwisku O'Hara.

