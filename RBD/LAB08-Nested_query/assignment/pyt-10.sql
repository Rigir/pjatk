--SET client_encoding='utf-8';
-- funkcje agregujące

-- wypisz zamówienia klientów o więcej niż jednej pozycji
SELECT imie, nazwisko, zamowienie.nr as zamowienie_nr, count(*) as "ile pozycji"
   FROM (( klient INNER JOIN zamowienie
             ON klient.nr = zamowienie.klient_nr
         ) INNER JOIN pozycja
             ON zamowienie.nr = pozycja.zamowienie_nr
        )
   GROUP BY imie, nazwisko, zamowienie.nr
   HAVING count(*)>1
;

-- zad P10: z tabel klientów i zamówień oblicz minimalny, maksymalny
-- i średni czas oczekiwania przez poszczególnych klientów (zamówienia
-- niezrealizowane nie liczą się do średniej).

SELECT imie, nazwisko, 
    ROUND(MIN(data_wysylki- data_zlozenia),2) as "MIN", 
    ROUND(MAX(data_wysylki- data_zlozenia),2) as "MAX", 
    ROUND(AVG(data_wysylki- data_zlozenia),2) as "AVG"
    FROM klient INNER JOIN zamowienie 
    ON klient.nr = zamowienie.klient_nr
    WHERE data_wysylki IS NOT NULL
    GROUP BY klient.nr, imie, nazwisko
;   