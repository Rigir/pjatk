--SET client_encoding='utf-8';
-- zapytania zagnieżdżone (zagnieżdżenia skorelowane)

-- sprawdź jacy klienci nie złożyli zamówień
SELECT imie, nazwisko FROM klient K
   WHERE NOT EXISTS (
   SELECT *
     FROM zamowienie Z     
     WHERE K.nr=Z.klient_nr 
);

-- sprawdź jacy klienci nie zamówili niczego
-- (nie złożyli zamówienia lub złożyli zamówienie puste t.j. bez pozycji)
SELECT imie, nazwisko FROM klient K
   WHERE NOT EXISTS (
   SELECT *
     FROM zamowienie Z INNER JOIN pozycja P 
     ON  K.nr=Z.klient_nr
     AND P.zamowienie_nr=Z.nr 
);

-- zad P8: 
-- znajdź klientów, którzy złożyli zamówienia,
SELECT imie, nazwisko FROM klient K
   WHERE EXISTS (
   SELECT *
     FROM zamowienie Z     
     WHERE K.nr=Z.klient_nr 
);

-- znajdź zamówienia puste, bez pozycji,
SELECT * FROM zamowienie Z
   WHERE NOT EXISTS (
   SELECT *
     FROM pozycja P     
     WHERE Z.nr=P.zamowienie_nr 
);

-- znajdź klientów, którzy złożyli takie zamówienia.
SELECT imie, nazwisko FROM klient K
  WHERE nr IN ( 
    SELECT klient_nr FROM zamowienie Z
      WHERE NOT EXISTS (
        SELECT * FROM pozycja P     
          WHERE Z.nr=P.zamowienie_nr
      ) 
);
