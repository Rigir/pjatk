--SET client_encoding='utf-8';
-- ZAŁOŻENIE: nie ma problemów w integralnością referencyjną
-- tzn. klucze obce mają dodane ON DELETE CASCADE
-- albo usuwamy elementy nie będące adresatem klucza obcego

-- usuń z tabeli towarów wszelkie donice, ale tylko jeśli nie są 
-- zamawiane (innych nie da się usunąć mając zamówienia)
DELETE FROM towar
  WHERE opis LIKE 'donica%' AND nr NOT IN (
    SELECT towar_nr FROM pozycja
    )
;

-- usuń towary niezamawiane
DELETE FROM towar
  WHERE nr NOT IN (
    SELECT towar_nr FROM pozycja
    )
;

-- usuń towary o nieznanej cenie
-- (przedtem być może warto usunąć wszelkie pozycje faktur
--  DELETE from pozycja;)
DELETE FROM towar
  WHERE cena IS NULL
;

-- zad 15: 
-- usuń klientów z Sopotu,
DELETE FROM klient WHERE miasto LIKE 'Sopot';

-- usuń klientów, których telefonu nie znamy(nie wykona się)
DELETE FROM klient WHERE telefon IS NULL;

-- By móc usunąć najpierw wpisujemy.
-- DELETE FROM zamowienie WHERE klient_nr 
-- IN (SELECT nr FROM klient WHERE telefon IS NULL);

-- usuń kody kreskowe nie odpowiadające żadnym towarom
DELETE FROM kod_kreskowy Where towar_nr IS NULL

-- usuń klientów, którzy nie złożyli żadnego zamówienia
DELETE FROM klient 
WHERE nr NOT IN (SELECT klient_nr from zamowienie );

-- usuń zamówienie, które nie posiada pozycji
DELETE FROM zamowienie 
WHERE nr NOT IN (SELECT zamowienie_nr from pozycja);

-- usuń towary nie posiadające kodu kreskowego (nie wykona się)
DELETE FROM towar WHERE nr NOT IN
(SELECT towar_nr FROM kod_kreskowy WHERE towar_nr IS NOT NULL);

-- By móc usunąć najpierw wpisujemy.
-- DELETE FROM pozycja WHERE towar_nr NOT IN
-- (SELECT towar_nr FROM kod_kreskowy WHERE towar_nr IS NOT NULL);