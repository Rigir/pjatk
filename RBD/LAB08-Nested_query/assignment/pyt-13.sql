--SET client_encoding='utf-8';
-- wymień pary zamówień pochodzących od tego samego klienta
SELECT Z1.nr AS zam1, Z2.nr AS zam2, Z2.klient_nr
FROM zamowienie AS Z1, zamowienie Z2
WHERE Z1.klient_nr=Z2.klient_nr
AND z1.nr<z2.nr;

-- zad P13: 
-- wymień pary klientów mieszkających w tym samym mieście.
SELECT K1.nr AS zam1, K2.nr AS zam2, K2.miasto
FROM klient K1, klient K2
WHERE K1.miasto = K2.miasto
AND K1.nr < K2.nr
ORDER BY miasto, K1.nr, K2.nr;

-- Wymień pary towarów mających tę samą cenę oraz pary z tym samym kosztem.
SELECT t1.nr AS zam1, t2.nr AS zam2,
CONCAT(t1.cena,' ',t2.cena) as "Cena", 
CONCAT(t1.koszt,' ',t2.koszt) as "Koszt"
FROM towar AS t1, towar t2
WHERE t1.nr < t2.nr AND ( 
t1.cena = t2.cena OR t1.koszt = t2.koszt
);

