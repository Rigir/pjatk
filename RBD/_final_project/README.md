💾 RTS Game
=========

What the structure of the database looks like:
![ER_diagram](resources/RTSGame.svg)

The project was created for educational purposes and most likely has no application in the real world. 

🛠 Meaning of individual files:
---------
* [create_db](src/create_db.sql)
  * Creates tables in the database.
* [drop_db](src/drop_db.sql)
  * Deletes the entire project.
* [insert_db](src/insert_db.sql)
  * Inserts mockup data.
* [select_db](src/select_db.sql)
  * Example queries.
* [trigger](src/trigger.sql)
  * Trigger definition.
* [trigger-ex](src/trigger-ex.sql)
  * Trigger example.

📄 License:
---------
PJAIT © All Rights Reserved 