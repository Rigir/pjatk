-- Wyzwalacz sprawdz_czy_wolne() pozwala na zmianę statusu rozgrywki.
--      Jeśli użytkownik zaktualizuje wartość kolumny uczestniczy na
--      inną wartość niż null to w kolumnie czy_wolne w encji rozgrywka
--      wartość zmieni się na false, aby odwrócić tą wartość wystarczy
--      zaktualizować wartość kolumny uczestniczy na null.

-- Przed zmianami
SELECT * FROM druzyna WHERE Druzyna_id IN(2,3);
SELECT * FROM rozgrywka WHERE czy_wolne IS FALSE; 

-- Zmiana statusu kolumy czy_wolne na False.
UPDATE Druzyna SET uczestniczy = 3, data_od = NOW() WHERE Druzyna_id IN(2,3);
SELECT * FROM druzyna WHERE Druzyna_id IN(2,3);
SELECT * FROM rozgrywka WHERE czy_wolne IS FALSE; 


-- Zmiana statusu kolumy czy_wolne na true.
UPDATE Druzyna SET uczestniczy = NULL, data_od = NULL WHERE Druzyna_id IN(2,3);
SELECT * FROM druzyna WHERE Druzyna_id IN(2,3);
SELECT * FROM rozgrywka WHERE czy_wolne IS FALSE; 

