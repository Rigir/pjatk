CREATE TABLE Zdjecie (
    Zdjecie_id serial PRIMARY KEY,
    url varchar(64) NOT NULL UNIQUE
);

CREATE TABLE Ranga (
    Ranga_id int PRIMARY KEY,
    nazwa varchar(64) NOT NULL UNIQUE,
    prog_punktowy int NOT NULL UNIQUE
);

CREATE TABLE Mapa (
    Mapa_id serial PRIMARY KEY,
    nazwa varchar(64) NOT NULL UNIQUE,
    url varchar(64) NOT NULL UNIQUE
);

CREATE TABLE Budowla (
    Budowla_id serial PRIMARY KEY,
    nazwa varchar(64) NOT NULL UNIQUE,
    koszt_budowy int NOT NULL,
    url varchar(64) NOT NULL UNIQUE
);

CREATE TABLE Rozgrywka (
    Rozgrywka_id serial PRIMARY KEY,
    adres_ip varchar(64) NOT NULL UNIQUE,
    czy_wolne boolean NOT NULL,
    -- Foreign keys
    uzywa int NOT NULL,
    CONSTRAINT fk_mapa FOREIGN KEY (uzywa) REFERENCES Mapa (Mapa_id)
);

CREATE TABLE Druzyna (
    Druzyna_id serial PRIMARY KEY,
    nazwa varchar(64) NOT NULL,
    -- Foreign keys
    uzywa int NOT NULL,
    data_od date,
    uczestniczy int,
    CONSTRAINT fk_zdjecie FOREIGN KEY (uzywa) REFERENCES Zdjecie (Zdjecie_id),
    CONSTRAINT fk_rozgrywka FOREIGN KEY (uczestniczy) REFERENCES Rozgrywka (Rozgrywka_id)
);

CREATE TABLE Gracz (
    Gracz_id serial PRIMARY KEY,
    nazwa varchar(64) NOT NULL,
    email varchar(64) NOT NULL UNIQUE,
    punktacja int,
    -- Foreign keys
    ma int NOT NULL,
    uzywa int NOT NULL,
    posiada int NOT NULL,
    CONSTRAINT fk_druzyna FOREIGN KEY (ma) REFERENCES Druzyna (Druzyna_id),
    CONSTRAINT fk_ranga FOREIGN KEY (posiada) REFERENCES Ranga (Ranga_id),
    CONSTRAINT fk_zdjecie FOREIGN KEY (uzywa) REFERENCES Zdjecie (Zdjecie_id)
);

CREATE TABLE Uczestniczyl (
    Uczestniczyl_id serial PRIMARY KEY,
    wynik int NOT NULL,
    data_od date NOT NULL,
    data_do date NOT NULL,
    -- Foreign keys
    id_druzyna int NOT NULL,
    id_rozgrywka int NOT NULL,
    CONSTRAINT fk_druzyna FOREIGN KEY (id_druzyna) REFERENCES Druzyna (Druzyna_id),
    CONSTRAINT fk_rozgrywka FOREIGN KEY (id_rozgrywka) REFERENCES Rozgrywka (Rozgrywka_id)
);

CREATE TABLE Korzysta (
    -- Foreign keys
    id_budowla int NOT NULL,
    id_rozgrywka int NOT NULL,
    CONSTRAINT fk_budowla FOREIGN KEY (id_budowla) REFERENCES Budowla (Budowla_id),
    CONSTRAINT fk_rozgrywka FOREIGN KEY (id_rozgrywka) REFERENCES Rozgrywka (Rozgrywka_id)
);

CREATE TABLE Jednostka (
    Jednostka_id serial PRIMARY KEY,
    koszt_stworzenia int NOT NULL,
    wartosc_zycia int NOT NULL,
    wartosc_ataku int NOT NULL,
    url varchar(64) NOT NULL UNIQUE,
    -- Foreign keys
    tworzy int NOT NULL,
    CONSTRAINT fk_Budowla FOREIGN KEY (tworzy) REFERENCES Budowla (Budowla_id)
);