--SET client_encoding='utf-8';
SET DATESTYLE TO 'European';

INSERT INTO Zdjecie VALUES 
    (1, 'http://dummyimage.com/152x100.png/dddddd/000000'),
    (2, 'http://dummyimage.com/130x100.png/ff4444/ffffff'),
    (3, 'http://dummyimage.com/208x100.png/ff4444/ffffff'),
    (4, 'http://dummyimage.com/234x100.png/5fa2dd/ffffff'),
    (5, 'http://dummyimage.com/127x100.png/5fa2dd/ffffff'),
    (6, 'http://dummyimage.com/171x100.png/cc0000/ffffff'),
    (7, 'http://dummyimage.com/169x100.png/ff4444/ffffff'),
    (8, 'http://dummyimage.com/184x100.png/dddddd/000000'),
    (9, 'http://dummyimage.com/221x100.png/ff4444/ffffff'),
    (10, 'http://dummyimage.com/134x100.png/cc0000/ffffff'),
    (11, 'http://dummyimage.com/156x100.png/ff4444/ffffff'),
    (12, 'http://dummyimage.com/173x100.png/5fa2dd/ffffff');

INSERT INTO Ranga VALUES 
    (1, 'Szeregowy', 10), 
    (2, 'Kapral', 20), 
    (3, 'Sierżant', 30), 
    (4, 'St.Sierżant', 40), 
    (5, 'Chorąży', 50), 
    (6, 'St.Chorąży', 60), 
    (7, 'Podporucznik', 70), 
    (8, 'Porucznik', 80), 
    (9, 'Kapitan', 90), 
    (10, 'Major', 100), 
    (11, 'Podpułkownik', 110), 
    (12, 'Pułkownik', 120), 
    (13, 'Generał Brygady', 130), 
    (14, 'Generał Dywizji', 140), 
    (15, 'Generał Armii', 150);

INSERT INTO Mapa VALUES 
    (1, 'Little grebe', 'http://dummyimage.com/227x100.png/cc0000/ffffff'), 
    (2, 'Crested porcupine', 'http://dummyimage.com/233x100.png/ff4444/ffffff'), 
    (3, 'Porcupine, north american', 'http://dummyimage.com/130x100.png/5fa2dd/ffffff'), 
    (4, 'Heron, green-backed', 'http://dummyimage.com/250x100.png/ff4444/ffffff'), 
    (5, 'Antelope, roan', 'http://dummyimage.com/191x100.png/dddddd/000000'),
    (6, 'Dingo', 'http://dummyimage.com/230x100.png/dddddd/000000'),
    (7, 'Great cormorant', 'http://dummyimage.com/183x100.png/cc0000/ffffff'),
    (8, 'Grant''s gazelle', 'http://dummyimage.com/208x100.png/cc0000/ffffff'),
    (9, 'Ass, asiatic wild', 'http://dummyimage.com/197x100.png/5fa2dd/ffffff'), 
    (10, 'Hottentot teal', 'http://dummyimage.com/193x100.png/ff4444/ffffff'), 
    (11, 'Giant otter', 'http://dummyimage.com/190x100.png/ff4444/ffffff'), 
    (12, 'Meerkat, red', 'http://dummyimage.com/146x100.png/ff4444/ffffff');

INSERT INTO Budowla VALUES 
    (1, 'Brassicaceae', 100, 'http://dummyimage.com/221x100.png/5fa2dd/ffffff'), 
    (2, 'Loasaceae', 200, 'http://dummyimage.com/234x100.png/ff4444/ffffff'), 
    (3, 'Poaceae', 300, 'http://dummyimage.com/160x100.png/cc0000/ffffff'), 
    (4, 'Campanulaceae', 400, 'http://dummyimage.com/178x100.png/ff4444/ffffff'), 
    (5, 'Orchidaceae', 500, 'http://dummyimage.com/242x100.png/ff4444/ffffff'), 
    (6, 'Cyperaceae', 600, 'http://dummyimage.com/130x100.png/dddddd/000000'), 
    (7, 'Lamiaceae', 700, 'http://dummyimage.com/149x100.png/5fa2dd/ffffff'), 
    (8, 'Fabaceae', 800, 'http://dummyimage.com/250x100.png/ff4444/ffffff'), 
    (9, 'Asteraceae', 900, 'http://dummyimage.com/127x100.png/dddddd/000000'), 
    (10, 'Cadczsure', 1000, 'http://dummyimage.com/119x100.png/cc0000/ffffff'), 
    (11, 'Scrophulariaceae', 1000, 'http://dummyimage.com/158x100.png/dddddd/000000'), 
    (12, 'Verrucariaceae', 1000, 'http://dummyimage.com/239x100.png/5fa2dd/ffffff');

INSERT INTO Rozgrywka VALUES 
    (1, '192.168.0.1', false, 1),
    (2, '192.168.0.2', true, 2),
    (3, '192.168.0.3', true, 3),
    (4, '192.168.0.4', false, 4),
    (5, '192.168.0.5', true, 5),
    (6, '192.168.0.6', false, 6),
    (7, '192.168.0.7', true, 7),
    (8, '192.168.0.8', true, 8),
    (9, '192.168.0.9', true, 9),
    (10, '192.168.0.10', true, 10),
    (11, '192.168.0.11', true, 10),
    (12, '192.168.0.12', true, 11);

INSERT INTO Druzyna VALUES 
    (1, 'EXPO: Magic of the White City', 4, '13/06/2020', 1),
    (2, 'Ruby in Paradise', 8, NULL, NULL),
    (3, 'Pocket Money', 3, NULL, NULL),
    (4, 'Pan Tadeusz', 8, NULL, NULL),
    (5, 'Ciel est à vous, Le', 10, NULL, NULL),
    (6, 'Rhinoceros', 7, NULL, NULL),
    (7, 'Sól', 8, '13/06/2020', 1),
    (8, 'Rhinoceros', 6, '24/06/2020', 6),
    (9, 'Love Is All There Is', 12, '24/08/2020', 6),
    (10, 'Last Dragon, The', 1, '01/05/2021', 4),
    (11, 'History of Kim Skov (Historien om Kim Skov)', 12, '01/05/2021', 4),
    (12, 'Tale of Sweeney Todd, The', 3, NULL, NULL);

INSERT INTO Gracz VALUES
    (1, 'cvittle0', 'smaclachlan0@dell.com', 117, 2, 10, 11),
    (2, 'cvittle0', 'bstreatfield1@artisteer.com', 20, 2, 1, 9),
    (3, 'tpirrey2', 'mburling2@mapquest.com', 109, 3, 7, 4),
    (4, 'dtrytsman3', 'aguilliland3@twitpic.com', 32, 8, 1, 11),
    (5, 'echaimson4', 'haggs4@ycombinator.com', 38, 10, 7, 8),
    (6, 'rcounihan5', 'ffenech5@infoseek.co.jp', null, 11, 8, 12),
    (7, 'rcounihan5', 'gwitul6@odnoklassniki.ru', 118, 12, 7, 10),
    (8, 'epavlov7', 'cpalia7@twitpic.com', 109, 11, 1, 6),
    (9, 'nthurston8', 'railward8@163.com', 66, 5, 12, 6),
    (10, 'gfretson9', 'lhearmon9@imageshack.us', 131, 8, 8, 9),
    (11, 'hduggeta', 'jfrowdea@freewebs.com', 127, 2, 9, 10),
    (12, 'skynclb', 'fegerb@github.io', null, 8, 9, 6);

INSERT INTO Uczestniczyl VALUES
    (1, -10, '08/07/2020', '09/07/2020', 1, 1),
    (2, 10, '18/05/2020', '30/08/2020', 7, 1),
    (3, 20, '18/05/2020', '22/06/2020', 8, 6),
    (4, -20, '02/02/2020', '22/06/2020', 9, 6),
    (5, 4, '09/12/2020', '10/04/2021', 10, 4),
    (6, -4, '21/04/2021', '23/06/2021', 11, 4),
    (7, -10, '17/04/2021', '17/04/2021', 7, 10),
    (8, -3, '10/05/2020', '18/05/2020', 5, 3),
    (9, -2, '11/05/2020', '14/10/2020', 2, 10),
    (10, 10, '17/07/2020', '30/12/2020', 1, 3),
    (11, -1, '16/08/2020', '26/08/2020', 12, 3);

INSERT INTO Korzysta VALUES
    (3, 1),
    (7, 1),
    (7, 1),
    (3, 3),
    (4, 3),
    (10, 3),
    (9, 3),
    (5, 6),
    (4, 6),
    (12, 6);

INSERT INTO Jednostka VALUES
    (1, 298, 284, 139, 'http://dummyimage.com/222x100.png/cc0000/ffffff', 1),
    (2, 154, 199, 147, 'http://dummyimage.com/242x100.png/5fa2dd/ffffff', 1),
    (3, 70, 120, 98, 'http://dummyimage.com/189x100.png/5fa2dd/ffffff', 2),
    (4, 228, 116, 125, 'http://dummyimage.com/208x100.png/dddddd/000000', 3),
    (5, 147, 152, 109, 'http://dummyimage.com/209x100.png/cc0000/ffffff', 4),
    (6, 138, 173, 234, 'http://dummyimage.com/212x100.png/dddddd/000000', 5),
    (7, 225, 248, 162, 'http://dummyimage.com/137x100.png/ff4444/ffffff', 6),
    (8, 61, 209, 98, 'http://dummyimage.com/234x100.png/dddddd/000000', 7),
    (9, 94, 176, 55, 'http://dummyimage.com/136x100.png/ff4444/ffffff', 8),
    (10, 295, 209, 299, 'http://dummyimage.com/226x100.png/5fa2dd/ffffff', 9),
    (11, 13, 42, 79, 'http://dummyimage.com/100x100.png/5fa2dd/ffffff', 9),
    (12, 54, 75, 99, 'http://dummyimage.com/144x100.png/5fa2dd/ffffff', 10),
    (13, 94, 32, 19, 'http://dummyimage.com/233x100.png/5fa2dd/ffffff', 11);