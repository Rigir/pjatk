DROP VIEW IF EXISTS Uzywane_mapy;
DROP TRIGGER IF EXISTS sprawdz_czy_wolne ON Druzyna;
DROP FUNCTION IF EXISTS sprawdz_czy_wolne();
DROP TABLE IF EXISTS Jednostka, Korzysta, Uczestniczyl, Gracz, Druzyna, Rozgrywka, Budowla, Mapa, Ranga, Zdjecie;
