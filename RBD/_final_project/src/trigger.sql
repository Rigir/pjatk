-- DROP TRIGGER IF EXISTS sprawdz_czy_wolne ON Druzyna;
-- DROP FUNCTION IF EXISTS sprawdz_czy_wolne;

CREATE OR REPLACE FUNCTION sprawdz_czy_wolne()
    RETURNS TRIGGER 
AS $$
BEGIN
    IF ( NEW.uczestniczy IS NOT NULL ) THEN
        UPDATE Rozgrywka SET czy_wolne = false
        WHERE Rozgrywka.Rozgrywka_id = NEW.uczestniczy;
    ELSE
        UPDATE Rozgrywka SET czy_wolne = true
        WHERE Rozgrywka.Rozgrywka_id = OLD.uczestniczy;
    END IF;
    RETURN NULL;
END
$$
    LANGUAGE PLPGSQL
;

CREATE TRIGGER sprawdz_czy_wolne
    AFTER UPDATE ON Druzyna
    FOR EACH ROW
    EXECUTE PROCEDURE sprawdz_czy_wolne();

