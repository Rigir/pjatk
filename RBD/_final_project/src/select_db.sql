-- Obecność złączenia, zarówno w składni ze słowami INNER JOIN jak i bez niej.
--   Z INNER JOIN.
SELECT G.nazwa NICK, R.nazwa RANGA 
from gracz G INNER JOIN ranga R ON
g.posiada = R.ranga_id;

--   Bez INNER JOIN.
SELECT G.nazwa NICK, R.nazwa RANGA 
from gracz G, ranga R WHERE 
g.posiada = R.ranga_id;

-- Użycie grupowania i funkcji agregujących.
SELECT D.nazwa Druzyna, COUNT(G.posiada) Ile_Graczy  FROM
gracz G INNER JOIN druzyna D ON G.posiada = D.druzyna_id
GROUP By D.Druzyna_id;

-- Wyświetlanie wyniku operacji arytmetycznych na liczbach oraz na datach.
SELECT D.nazwa, SUM(U.data_do - U.data_od) grali_przez_dni 
from druzyna D, uczestniczyl U WHERE D.druzyna_id = U.id_druzyna
GROUP By D.Druzyna_id;

-- Warunki odwołujące się do wzorców napisów.
SELECT nazwa FROM gracz WHERE nazwa LIKE 'c%';

-- Użycie zagnieżdżenia, w obu odmianach (nieskorelowane i skorelowane). 
--   zagnieżdżenie nieskorelowane
SELECT nazwa from druzyna 
WHERE uczestniczy IN (
	SELECT rozgrywka_id FROM rozgrywka
) ORDER BY nazwa;

--   zagnieżdżenie skorelowane
SELECT nazwa from druzyna D
WHERE NOT EXISTS (
	SELECT * FROM rozgrywka R
  	WHERE D.uczestniczy = R.rozgrywka_id
) ORDER BY nazwa;

-- Sprawdzanie warunku NULL.
SELECT nazwa from gracz 
where punktacja ISNULL;

SELECT nazwa from druzyna 
where uczestniczy NOTNULL;

-- Zapytania negatywne, Co najmniej w dwu wersjach.
SELECT nazwa from druzyna D 
WHERE uczestniczy NOT IN (
    SELECT Rozgrywka_id FROM rozgrywka R
    WHERE D.uczestniczy = R.rozgrywka_id
) ORDER BY nazwa;

SELECT nazwa from druzyna D 
LEFT JOIN rozgrywka R ON
D.uczestniczy = R.rozgrywka_id
WHERE uczestniczy ISNULL;

-- Użycie widoku (perspektywy, view). 
CREATE VIEW Uzywane_mapy AS
SELECT nazwa FROM mapa M 
INNER JOIN rozgrywka R ON r.uzywa = m.mapa_id 
where r.czy_wolne=true;

-- Usuwanie z tabeli.
DELETE FROM gracz WHERE punktacja ISNULL;

-- Aktualizacja wierszy w tabeli. 
UPDATE jednostka set koszt_stworzenia = FLOOR(random()*(50)+100)  
WHERE koszt_stworzenia < 100;