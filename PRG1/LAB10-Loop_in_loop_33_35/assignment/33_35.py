import random


def zad_33():
    n = int(input(f'n: '))
    tab = createList_33(n)
    printList_33(tab)
    for x in list(tab):
        if 11 <= x <= 21:
            tab.remove(x)
    printList_33(tab)


def createList_33(n):
    return [random.randint(1, 50) for _ in range(n)]


def printList_33(tab):
    print(f'33_Lista: {tab}')


def zad_34():
    suma = 0
    n, m = map(int, input(f'n, m: ').split())
    tab = createM_34(n, m)
    printM_34(tab)
    for row in tab:
        for num in row:
            if num <= 5:
                suma += num
    print(f'34_Suma elementów macierzy: {suma}')


def createM_34(n, m):
    return [[random.randint(0, 9) for _ in range(m)] for _ in range(n)]


def printM_34(tab):
    print(f'34_Macierz: {tab}')


def zad_35():
    num = str(input(f'n: '))
    slownik = {'-': 'minus', '9': 'dziewięć', '8': 'osiem', '7': 'siedem', '6': 'szejść', '5': 'pięć', '4': 'cztery', '3': 'trzy',
               '2': 'dwa', '1': 'jeden', '0': 'zero'}
    for x in num:
        for znak in slownik:
            if x == znak:
                print(slownik[znak], end=' ')

if __name__ == "__main__":
    # zad_33()
    # zad_34()
    zad_35()