def zad_18_rand():
    import random
    p, q = random.randint(1, 6), random.randint(1, 6)
    if p % 2 == 0:
        if q in [2, 4, 5]:
            print(f' Wygrana 1: {p + 3 * q}')
        else:
            print(f' Wygrana 2: {2 * q}')
    else:
        if q in [1, 3, 6]:
            if p == q:
                print(f' Wygrana: {5 * p + 3}')
            else:
                print(f' Wygrana: {2 * q + p}')
        else:
            if p == 5 and q == 5:
                print(f' Wygrana: {min(p, q) + 9}')
            else:
                print(f' Wygrana: {min(p, q) + 4}')


def zad_18_input():
    p, q = map(int, input('Wpisz oba  rzuty  koscia p,q: ').split())
    if p % 2 == 0:
        if q in [2, 4, 5]:
            print(f' Wygrana 1: {p + 3 * q}')
        else:
            print(f' Wygrana 2: {2 * q}')
    else:
        if q in [1, 3, 6]:
            if p == q:
                print(f' Wygrana 3: {5 * p + 3}')
            else:
                print(f' Wygrana 4: {2 * q + p}')
        else:
            if p == 5 and q == 5:
                print(f' Wygrana 5: {min(p, q) + 9}')
            else:
                print(f' Wygrana 6: {min(p, q) + 4}')

if __name__ == "__main__":
    # zad_18_rand()
    zad_18_input()
