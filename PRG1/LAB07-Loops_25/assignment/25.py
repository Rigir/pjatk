def zad_25():
    sum, num = 0, int(input(f'Podaj liczbe: '))
    for x in range(1, num):
        if num % x == 0:
            sum += x
    if sum == num:
        print(f'Liczba jest ta doskonala')
    else:
        print(f'Liczba ta nie jest doskonala')


def zad_25_schemat_blokowy():
    import math
    sum, n = 0, int(input(f'Podaj liczbe: '))
    p = int(math.sqrt(n))
    while p > 1:
        if n % p == 0:
            sum += p
            p1 = n / p
            if p1 != p:
                sum += p1
        p -= 1
    else:
        sum += 1
        if sum == n:
            print('Tak')
        else:
            print('Nie')

if __name__ == "__main__":
    # zad_25()
    zad_25_schemat_blokowy()
