#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void srandSeed();
int randRange(int low, int high);
void avg(int *p_fist, int *p_last);

int main()
{
    srandSeed();
    int num, arr[20];
    int *p_arr = arr;

    do
    {
        cout << "Podaj rozmiar tablicy ( 0 < m <= 20): ";
        cin >> num;
    } while (num <= 0 || num > 20);

    // get random numbers
    for (int i = 0; i < num; i++)
    {
        *p_arr = randRange(0, 10);
        p_arr++;
    }

    cout << endl;
    // print
    p_arr = arr;
    for (int i = 0; i < num; i++)
        cout << p_arr + i << " | " << *(p_arr + i) << " " << endl;

    // find min and max
    p_arr = arr;
    int *p_min = arr, *p_max = arr;
    for (int i = 0; i < num; i++)
    {
        if (*p_arr > *p_max)
            p_max = p_arr;
        else if (*p_arr < *p_min)
            p_min = p_arr;
        p_arr++;
    }

    //Which address is smaller.
    if (p_max > p_min)
    {
        cout << "\nfrom: \t" << *p_min << " | " << p_min << endl;
        cout << "to: \t" << *p_max << " | " << p_max << endl;
        avg(p_min, p_max);
    }
    else
    {
        cout << "\nfrom: \t" << *p_max << " | " << p_max << endl;
        cout << "to: \t" << *p_min << " | " << p_min << endl;
        avg(p_max, p_min);
    }

    delete p_arr, p_min, p_max;
}

void avg(int *p_fist, int *p_last)
{
    int avg = 0, size = 0;
    p_fist++;
    cout << "Numbers to sum: ";
    while (p_fist != p_last)
    {
        cout << *p_fist << " ";
        avg += *p_fist;
        p_fist++;
        size++;
    }
    if (size != 0)
        cout << "\nResult: " << avg / size << endl;
    else
        cout << "\nArray compartment is empty: " << size << endl;
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << " srandSeed: " << seed << endl;
    srand(seed);
}