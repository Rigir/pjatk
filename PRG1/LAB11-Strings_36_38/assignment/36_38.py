
def zad_36(num):
    a, b, tab = 0, 1, []
    tab.extend((a, b))
    for i in range(1, num-1):
        a, b = b, a + b
        tab.append(b)
    return tab


def zad_37(napis):
    napis = str.lower(napis)
    return napis == napis[::-1]


def zad_38(napis):
    zlicz = {}
    napis = "".join(str.lower(napis).split())
    print(napis)
    for x in napis:
        if x in zlicz:
            zlicz[x] += 1
        else:
            zlicz[x] = 1
    return zlicz

if __name__ == "__main__":
    # print(zad_38(str(input(f'Wpisz napis: '))))
    # print(zad_36(int(input(f'Wypisz ciąg fib do danego wyrazu: '))))
    print(zad_37(str(input(f'Sprawcz czy jest to palindrom: '))))
