#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
void srandSeed();
int randRange(int low, int high);
void fillArr(int tab[][10], int size);
void printArr(int tab[][10], int size);
bool zad46(int tab[][10], int size);

int main()
{
    srandSeed();
    int tab[10][10];
    int n, m;

    do
    {
        cout << "Podaj rozmiar tablicy [NxM]: ";
        cin >> n >> m;
        if (n != m)
            cout << "Error: Macierz musi byc kwadratowa! \n";
    } while (n != m);

    fillArr(tab, n);
    printArr(tab, n);
    cout << endl;
    if (zad46(tab, n))
        cout << "Macierz jest trojkatna gorna";
    else
        cout << "Macierz nie jest trojkatna gorna";
}

bool zad46(int tab[][10], int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
            if (i > j && tab[i][j] != 0)
                return false;
    }
    return true;
}

void printArr(int tab[][10], int size)
{
    cout << "Tablica: \n";
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
            cout << tab[i][j] << " ";
        cout << "\n";
    }
}

void fillArr(int tab[][10], int size)
{
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            tab[i][j] = randRange(0, 9);
    // if(i > j) tab[i][j] = 0; // Można sprawdzić czy działa program
    // else tab[i][j] = randRange(0, 9);
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << " srandSeed: " << seed << endl;
    srand(seed);
}