#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
void srandSeed();
int randRange(int low, int high);
void fillArr(int tab[], int size, int a, int b);
void zad44(int tab[], int size);
void zad45(int tab[], int size);

int main()
{
    srandSeed();
    int tab[100];
    int n = 0, a = 0, b = 0;

    cout << "Podaj rozmiar tablicy: ";
    cin >> n;
    cout << "Podaj po spacji przedzial [a,b]: ";
    cin >> a >> b;

    fillArr(tab, n, a, b);
    cout << "Tablica: ";
    for (int i = 0; i < n; i++)
        cout << tab[i] << " ";

    cout << endl;
    zad44(tab, n);
    zad45(tab, n);
}

void zad45(int tab[], int size)
{
    int temp[size];
    temp[0] = tab[0];
    for (int i = 0; i < size - 1; i++)
    {
        if (i % 2 == 0)
            temp[i + 1] = temp[i] - tab[i + 1];
        else
            temp[i + 1] = temp[i] + tab[i + 1];
    }

    //display
    cout << "Zad 45: \n";
    for (int i = 0; i < size; i++)
        cout << temp[i] << " ";
}

void zad44(int tab[], int size)
{
    int ans, temp = 0;
    cout << "Zad44: \n";
    do
    {
        temp = tab[0];
        for (int i = 0; i < size - 1; i++)
            tab[i] = tab[i + 1];
        tab[size - 1] = temp;

        //display
        cout << "[1 Slide 2 left]: ";
        for (int i = 0; i < size; i++)
            cout << tab[i] << " ";

        cout << "\n Czy chcesz kontynolowac? 0 - Nie :";
        cin >> ans;
    } while (ans != 0);
}

void fillArr(int tab[], int size, int a, int b)
{
    for (int i = 0; i < size; i++)
        tab[i] = randRange(a, b);
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << " srandSeed: " << seed << endl;
    srand(seed);
}