#include <iostream>
#include <cmath>

using namespace std;

double trojkot(double a, double b, double c);
float prost(float dlug, float szer, float wys, float &objetosc);
bool pierwsza(int p);

main()
{
    cout << "Wynik 47_1: " << trojkot(3, 5, 9) << endl;   // Nie da sie
    cout << "Wynik 47_2: " << trojkot(1, 1.5, 2) << endl; // Da się
    float V = 0;
    cout << "Wynik 48_1 | Pb: " << prost(-10, 4, 5, V) << " V: " << V << endl; // Nie da sie
    cout << "Wynik 48_2 | Pb: " << prost(3, 4, 5, V) << " V: " << V << endl;   // Da się
    int n = 0;
    cout << "Wynik 49 |\n";
    do
    {
        cout << "Podaj liczbe z zakresu [2,n]: ";
        cin >> n;
    } while (n < 2);
    for (int i = 2; i <= n; i++)
    {
        if (pierwsza(i))
            cout << "P:\t " << i << endl; //pierwsza
        else
            cout << "NP:\t " << i << endl; // niepierwsza
    }
}

double trojkot(double a, double b, double c)
{
    if (a + b > c && a + c > b && b + c > a)
    {
        double S = 0, p = 0.5 * (a + b + c);
        return S = sqrt(p * (p - a) * (p - b) * (p - c));
    }
    else
        return -1;
}

float prost(float dlug, float szer, float wys, float &objetosc)
{
    if (dlug > 0 && szer > 0 && wys > 0)
    {
        objetosc = dlug * szer * wys;
        return 2 * (dlug * wys + szer * wys); // Pole powierzchni bocznej
    }
    else
        return -1;
}

bool pierwsza(int p)
{
    if (p < 2)
        return false;
    for (int i = 2; i * i <= p; i++)
        if (p % i == 0)
            return false;
    return true;
}