#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void srandSeed();
int randRange(int low, int high);

void dane(int *p_n, int *p_arr);
void wyswietl(int *p_n, int *p_arr);
int mnozenie(int *p_n, int *p_X, int *p_Y);

int main()
{
    srandSeed();

    int X[10], Y[10], n = 0;

    do
    {
        cout << " Enter, the size of array: ";
        cin >> n;
    } while (n <= 0 || n > 10);

    dane(&n, X);
    dane(&n, Y);
    cout << " X: ";
    wyswietl(&n, X);
    cout << "\n Y: ";
    wyswietl(&n, Y);

    cout << "\n Wynik: " << mnozenie(&n, X, Y);
}

int mnozenie(int *p_n, int *p_X, int *p_Y)
{
    int wynik = 0;
    for (int i = 0; i < *p_n; i++)
        wynik += *(p_X + i) * *(p_Y + i);
    return wynik;
}

void wyswietl(int *p_n, int *p_arr)
{
    for (int i = 0; i < *p_n; i++)
        cout << *(p_arr + i) << " ";
}

void dane(int *p_n, int *p_arr)
{
    for (int i = 0; i < *p_n; i++)
        *(p_arr + i) = randRange(1, 10);
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << " srandSeed: " << seed << endl;
    srand(seed);
}
