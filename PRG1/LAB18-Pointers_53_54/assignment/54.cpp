#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void srandSeed();
int randRange(int low, int high);

void dane(int *p_arr, int size);
void wyswietl(int *p_arr, int size);
int *odwracanie(int *p_arr, int n);

int main()
{
    srandSeed();

    int arr[10], size = 10, n = 0;

    do
    {
        cout << " Enter, the number: ";
        cin >> n;
    } while (n <= 0 || n > 10);

    dane(arr, size);
    cout << " Before: \t";
    wyswietl(arr, size);

    odwracanie(odwracanie(arr, n), n);
    cout << "\n Turn twice: \t";
    wyswietl(arr, size);

    odwracanie(arr, n);
    cout << "\n One: \t\t";
    wyswietl(arr, size);
}

int *odwracanie(int *p_arr, int n)
{
    for (int i = 0; i < n; i++)
    {
        swap(p_arr[i], p_arr[n - 1]);
        n--;
    }
    return p_arr;
}

void wyswietl(int *p_arr, int size)
{
    for (int i = 0; i < size; i++)
        cout << *(p_arr + i) << " ";
}

void dane(int *p_arr, int size)
{
    for (int i = 0; i < size; i++)
        *(p_arr + i) = randRange(1, 10);
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << " srandSeed: " << seed << endl;
    srand(seed);
}
