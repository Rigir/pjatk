import random


def zad_29(num):
    ten, five, two, one, change = num // 10, 0, 0, 0, 0
    change = num - ten * 10
    while change > 0:
        if change - 5 >= 0:
            change -= 5
            five += 1
        elif change - 2 >= 0:
            change -= 2
            two += 1
        elif change - 1 >= 0:
            change -= 1
            one += 1

    return f'Zad_29: {ten} szt.[10zł], {five} szt.[5zł], {two} szt.[2zł], {one} szt.[1zł],'


def zad_30(tab: list):
    maxNum = tab[0]
    for num in tab:
        if num > maxNum:
            maxNum = num
    return f'Zad_30: Najwieksza liczba to: {maxNum}'


def zad_31(tab: list):
    sumNum, temp = 0, 0
    for x in range(len(tab) - 1):
        temp = tab[x] + tab[x + 1]
        if temp > sumNum:
            sumNum = temp
    return f'Zad_31: Najwieksza suma to: {sumNum}'


def zad_32(tab: list):
    maxNum, secNum = 0, 0
    for num in tab:
        if num > maxNum:
            secNum = maxNum
            maxNum = num
        elif secNum < num < maxNum:
            secNum = num
    return f'Zad_32: Druga najwieksza liczba to: {secNum}'


if __name__ == "__main__":
    print(zad_29(int(input(f'Please, enter number: '))))

    a, b = map(int, input(f'Enter [a,b]: ').split())
    tab = [random.randint(a, b) for x in range(20)]
    print(tab)
    print(zad_30(tab))
    print(zad_31(tab))
    print(zad_32(tab))
