#include <iostream>

using namespace std;

//struct
struct Ulamek
{
    int licznik;
    int mianownik;
};

//headers
Ulamek mnozenie(Ulamek a, Ulamek b);

int main()
{
    Ulamek a{3, 4}, b{1, 2}, c;
    c = mnozenie(a, b);
    cout << c.licznik << "/" << c.mianownik;
}

Ulamek mnozenie(Ulamek a, Ulamek b)
{
    Ulamek temp;
    temp.licznik = a.licznik * b.licznik;
    temp.mianownik = a.mianownik * b.mianownik;
    return temp;
}