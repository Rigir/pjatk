#include <iostream>

using namespace std;

//struct
struct Trojkat
{
    float a;
    float b;
    float c;
};

//headers
float obwod(Trojkat t);

int main()
{
    Trojkat nowy{3, 4, 5};
    cout << obwod(nowy);
}

float obwod(Trojkat t)
{
    return t.a + t.b + t.c;
}
