#include <iostream>
#include <ctime>

using namespace std;

void zad50();
void zad51();

int main()
{
    zad50();
    //zad51();
}

void zad51()
{
    srand(time(NULL));
    int n, tab[10];
    int *wn = &n, *wtab = tab;

    cout << "Podaj rozmiar tablicy ( m <= 10): ";
    cin >> *wn;

    for (int i = 0; i < *wn; i++)
    {
        *wtab = rand() % 10;
        wtab++;
    }

    wtab = tab;
    for (int i = 0; i < *wn; i++)
        cout << *(wtab + i) << " ";

    int *min = tab, *temp = tab;
    for (int i = 0; i < *wn; i++)
    {
        if (*temp < *min)
            min = temp;
        temp++;
    }

    cout << endl
         << "Minimalny element tablicy to: " << *min;
    delete wn, wtab, min, temp;
}

void zad50()
{
    int n, k, sn, sk, snk;
    int *p_n = &n, *p_k = &k, *p_sn = &sn, *p_sk = &sk, *p_snk = &snk;
    *p_sn = 1;
    *p_sk = 1;
    *p_snk = 1;

    cout << " Podaj kolejno n i k: ";
    cin >> *p_n >> *p_k;

    cout << *p_n << " " << *p_k << endl;
    for (int i = 2; i <= *p_n; i++)
        *p_sn = (*p_sn) * i;
    for (int i = 2; i <= *p_k; i++)
        *p_sk = (*p_sk) * i;
    for (int i = 2; i <= (*p_n) - (*p_k); i++)
        *p_snk = (*p_snk) * i;

    cout << "Symbol Newtona n nad k wynosi: " << ((*p_sn) / ((*p_sk) * (*p_snk)));
    delete p_k, p_n, p_sn, p_sk, p_snk;
}