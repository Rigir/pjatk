def zad_16():
    # (1+a^2)x^2 + (2ab)x + b^2-r^2 = 0, S = (0,0)
    from math import fabs, sqrt, pow
    a, b, r = map(int, input("Podaj a,b,r: ").split())
    delta = fabs(b ** 2 - r ** 2) / \
        sqrt(pow(1 + a ** 2, 2) + pow(2 * a * b, 2))
    if delta == r:
        print('Prosta i okrąg są styczne.')
    elif delta < r:
        print('Prosta i okrąg mają dwa punkty wspólne.')
    else:
        print('Nie przecinają się.')


def zad_17():
    # a, b, c, d, e, f = 2, 3, 4, -5, 1, 13 # Dwa miejca zerowe (2,-1)
    # a, b, c, d, e, f = 1, 1, 1, 1, 1, 1 # Nieskończenie wiele
    # a, b, c, d, e, f = 1, 1, 0, 0, 1, 1 # Nie ma rozwwiązań
    a, b, c, d, e, f = map(int, input("Podaj a,b,c,d,e,f: ").split())
    W, Wx, Wy = (a * d) - (b * c), (e * d) - (b * f), (a * f) - (e * c)
    if W != 0:
        x, y = Wx / W, Wy / W
        print(f'Punkt jest w ({x}, {y})')
    elif W == 0:
        if Wx == 0 and Wy == 0:
            print("Jest nieskończenie wiele rozwiązań.")
        else:
            print("Rozwiązanie nie istnieje")


if __name__ == "__main__":
    # zad_16()
    zad_17()
