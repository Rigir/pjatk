def zad_11():
    import math
    num = int(input("11.Podaj liczbe: "))
    if num < 0:
        print("Liczba jest ujemna.")
    else:
        print(f'Wynik: {round(math.sqrt(num))}')


def zad_12():
    num = int(input("12.Podaj liczbe: "))
    if num % 2 == 0:
        print("Liczba jest parzysta.")
    else:
        print("Liczba jest nieparzysta.")


def zad_13():
    a, b = map(int, input("13.Podaj liczbe: ").split())
    if a % b == 0:
        print(f'liczba {a} jest podzielne przez {b}.')
    else:
        print(f'Liczba {a} nie jest podzielne przez {b}.')


def zad_14():
    a, b = map(int, input("14.Podaj liczbe: ").split())
    if a == b:
        print(f'liczba {a} i {b} są równe.')
    elif a > b:
        print(f'Liczba {a} jest większa od {b}.')
    else:
        print(f'Liczba {b} jest większa od {a}.')


def zad_15():
    num = int(input("15.Podaj liczbe: "))
    if num in range(1, 11):
        print(f'Liczba {num} znajduje się w przedzialne.')
    else:
        print(f'Liczba {num} NIE znajduje się w przedzialne.')


if __name__ == "__main__":
    # zad_11()
    # zad_12()
    # zad_13()
    # zad_14()
    zad_15()
