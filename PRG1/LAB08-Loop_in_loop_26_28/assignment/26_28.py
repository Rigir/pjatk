def zad26():
    h = int(input(f'Podaj liczbe: '))
    for x in range(0, h):
        # empty space on the left side
        for y in range(0, h-1-x):
            print(end=" ")
        # left side
        for y in range(0, x):
            print('X', end="")
        # middle
        print('X', end="")
        # right side
        for y in range(0, x):
            print('X', end="")
        print()


def zad27():
    h = int(input(f'Podaj liczbe: '))
    for x in range(h, 0, -1):
        sec = h + 1 - x
        for y in range(sec):
            print(sec*(y+1), end=' ')
        print()


def zad28():
    n = int(input(f'Podaj n od zakresu [2..n]: '))
    for x in range(0, n):
        if isPrime(x):
            print(x, end=' ')


def isPrime(num: int):
    if num > 1:
        for x in range(2, num):
            if num % x == 0:
                break
        else:
            return True
    return False

if __name__ == "__main__":
    # zad26()
    # zad27()
    zad28()
