#include <iostream>

using namespace std;

void zad41();
void zad42();
void zad43();

main()
{
    // zad41();
    // zad42();
    zad43();
}

void zad43()
{
    char kategoria;
    int godz = 0, nadgodziny = 0, wynik;

    cout << "Podaj liczbe godzin: ";
    cin >> godz;
    cout << "Podaj kategorie (od A do D): ";
    cin >> kategoria;

    if (godz > 40)
    {
        nadgodziny = godz - 40;
        godz -= nadgodziny;
    }

    switch (kategoria)
    {
    case 'A':
        wynik = godz * 15 + nadgodziny * 30;
        break;
    case 'B':
        wynik = godz * 25 + nadgodziny * 50;
        break;
    case 'C':
        wynik = godz * 30 + nadgodziny * 60;
        break;
    case 'D':
        wynik = godz * 35 + nadgodziny * 70;
        break;
    default:
        cout << "Nie ma takiej kategorii \n";
    }

    cout << "Brutto: " << wynik << endl;

    if (wynik <= 700)
        wynik -= (0.15 * wynik);
    else if (wynik > 700 && wynik < 1200)
        wynik -= (0.20 * wynik);
    else if (wynik > 1200)
        wynik -= (0.25 * wynik);

    cout << "Netto: " << wynik << endl;
}

void zad42()
{
    int godzina;

    cout << "Podaj godzine: ";
    cin >> godzina;

    switch (godzina)
    {
    case 8 ... 11:
        cout << " 8-11 \t Wyklady \n";
    case 12 ... 13:
        cout << " 12-13 \t Dyskusje \n";
    case 14:
        cout << " 14 \t Obiad \n";
    case 15 ... 18:
        cout << " 15-18 \t Prelekcje \n";
    case 19:
        cout << " 19 \t Obiad \n";
        break;
    default:
        cout << " Godziny tylko z zakresu od 8 do 19. \n";
        break;
    }
}

void zad41()
{
    float metry;
    int jednostka;

    cout << "Podaj odległosc w metrach: ";
    cin >> metry;
    cout << " 1-cal | 2-stopa | 3-jard | 4-pret | 5-furlong | 6-mila \n";
    cout << "Podaj na jake jednostke zamieniasz: ";
    cin >> jednostka;

    switch (jednostka)
    {
    case 1:
        cout << "Wynik to:" << metry * (39.37) << endl;
        break;
    case 2:
        cout << "Wynik to:" << metry * (3.281) << endl;
        break;
    case 3:
        cout << "Wynik to:" << metry * (1.094) << endl;
        break;
    case 4:
        cout << "Wynik to:" << metry * (5.0292) << endl;
        break;
    case 5:
        cout << "Wynik to:" << metry * (0.00497096953789867) << endl;
        break;
    case 6:
        cout << "Wynik to:" << metry * (0.000621371) << endl;
        break;
    default:
        cout << "Nie ma takiej jednostki" << endl;
    }
}