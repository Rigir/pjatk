#include <iostream>

using namespace std;

int min(int x, int y);
void zad39();
void zad40();

int main()
{
    //zad39();
    zad40();
}

int min(int x, int y)
{
    if (x < y)
        return x;
    else
        return y;
}

void zad39()
{
    int q, p, wynik;
    cout << "Wpisz rzuty kosci: ";
    cin >> p >> q;
    if (p % 2 == 0)
    {
        if (q == 2 || q == 4 || q == 5)
            wynik = p + 3 * p;
        else
            wynik = 2 * q;
    }
    else
    {
        if (q == 1 || q == 3 || q == 6)
        {
            if (p == q)
                wynik = 5 * p + 3;
            else
                wynik = 2 * q + p;
        }
        else
            wynik = min(p, q) + 4;
    }

    if (p == 5 && q == 5)
        wynik += 5;

    cout << "Wynik:" << wynik;
}

void zad40()
{
    int num, a;
    cout << "Wpisz liczbe: ";
    cin >> num;

    cout << "\n Petla for:      \t";
    for (int i = num; i > 0; i--)
        if (num % i == 0)
            cout << " " << num / i << " ";

    cout << "\n Petla while:    \t";
    int count = num;
    while (count > 0)
    {
        if (num % count == 0)
            cout << " " << num / count << " ";
        count--;
    }

    cout << "\n Petla do-while: \t";
    count = num;
    do
    {
        if (num % count == 0)
            cout << " " << num / count << " ";
        count--;
    } while (count > 0);
}