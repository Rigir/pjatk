def zad23():
    import random
    x, y, sum = random.randint(0, 20), 0, 0
    while x != y:
        sum += x
        print(f'Wprowadzona liczba: {x} Suma: {sum}')
        y, x = x, random.randint(0, 20)
    print(f'Wprowadzona liczba: {x} Suma: {sum}')


def zad24():
    num, sum = int(input(f'Podaj liczbe: ')), 0
    for x in range(num):
        sum += (x+1)/(x+2)
    print(f'Wynik: {round(sum,2)}')

if __name__ == "__main__":
    # zad23()
    zad24()
