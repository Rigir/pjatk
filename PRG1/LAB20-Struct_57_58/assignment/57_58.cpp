#include <iostream>
#include <string>
#include <cctype>
#include <algorithm>

using namespace std;

//struct
struct Ksiazka
{
    string autor, tytul;
    int rokWydania, liczbaStron;
    double cena;
    char wypozyczona;
};

//headers
void fillTab(Ksiazka k[], int n);
void findCheaperThen(Ksiazka k[], double value, int n);
bool letters(string n);
string autor();

//Validator
bool has_special_char(string const &str);
bool letters(string n);
string autor();
string tytul();
int wartosc();
double cena();
int rok();
char wyporzyczona();

int main()
{
    int n;
    do
    {
        cout << "N:";
        cin >> n;
    } while (n <= 0);
    Ksiazka arr[n];

    fillTab(arr, n);
    double value = 8;
    findCheaperThen(arr, value, n);
}

void findCheaperThen(Ksiazka k[], double value, int n)
{
    cout << "=============" << endl;
    for (int i = 0; i < n; i++)
    {
        if (k[i].cena < value)
        {
            cout << "A: " << k[i].autor << " T: " << k[i].tytul << en dl;
            cout << "C: " << k[i].cena << endl;
            cout << "LS: " << k[i].liczbaStron << " RW: " << k[i].rokWydania << endl;
            cout << "WS: " << k[i].wypozyczona << endl;
        }
    }
}

void fillTab(Ksiazka k[], int n)
{
    string temp;
    for (int i = 0; i < n; i++)
    {
        k[i].autor = autor();
        k[i].tytul = tytul();
        k[i].cena = cena();
        k[i].liczbaStron = wartosc();
        k[i].rokWydania = rok();
        k[i].wypozyczona = wyporzyczona();
    }
}

bool has_special_char(string const &str)
{
    return find_if(str.begin(), str.end(),
                   [](char ch) { return !(isalnum(ch) || ch == '_'); }) != str.end();
}

bool letters(string n)
{
    int i;
    for (i = 0; i < n.size(); i++)
    {
        if (!(isalpha(n[i])))
            return false;
        return true;
    }
    return true;
}

string autor()
{
    string temp;
    do
    {
        cout << "A:";
        cin >> temp;
    } while (!letters(temp));
    return temp;
}

string tytul()
{
    string temp;
    do
    {
        cout << "T:";
        cin >> temp;
    } while (has_special_char(temp));
    return temp;
}

double cena()
{
    double temp;
    do
    {
        cout << "C:";
        cin >> temp;
    } while (temp <= 0);
    return temp;
}

int wartosc()
{
    int temp;
    do
    {
        cout << "W:";
        cin >> temp;
    } while (temp <= 0);
    return temp;
}

int rok()
{
    int temp;
    do
    {
        cout << "R:";
        cin >> temp;
    } while (temp <= 0 || temp > 2021);
    return temp;
}

char wyporzyczona()
{
    char temp;
    do
    {
        cout << "Wy:";
        cin >> temp;
        if (temp == 'T' || temp == 'N')
            break;
    } while (true);
    return temp;
}