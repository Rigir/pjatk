def getNum():
    return int(input('Wpisz liczbę cakowita: '))


def zad_19():
    while getNum() < 0:
        print('ERROR: Wpisz liczbę dodatnią')
    print('Podales liczbe dodatnia!')


def zad_20_for():
    tab = []
    for x in range(100, 1000):
        if x % 13 == 0:
            tab.append(x)
    print(f'20.for: {tab}')


def zad_20_while():
    tab, x = [], 100
    while x < 1000:
        if x % 13 == 0:
            tab.append(x)
        x += 1
    print(f'20.while: {tab}')


def zad_21_for():
    tab, n = [], getNum()
    for x in range(1, n + 1):
        if n % x == 0:
            tab.append(x)
    print(f'21.for: {tab}')


def zad_21_while():
    tab, x, n = [], 1, getNum()
    while x < n + 1:
        if n % x == 0:
            tab.append(x)
        x += 1
    print(f'21.while: {tab}')


def zad_22_while():
    sum, ile, x = 0, 0, 1
    n = getNum()
    while ile < n:
        if x % 2 == 0:
            ile += 1
            sum += x
        x += 1
    print(f'22.while: {sum}')


def zad_22_for():
    sum, ile = 0, 0
    n = getNum()
    for x in range(2, 1000, 2):
        if ile == n:
            break
        if x % 2 == 0:
            ile += 1
            sum += x
    print(f'22.for: {sum}')

if __name__ == "__main__":
    zad_19()

    # zad_20_for()
    # zad_20_while()

    # zad_21_while()
    # zad_21_for()

    # zad_22_while()
    # zad_22_for()
