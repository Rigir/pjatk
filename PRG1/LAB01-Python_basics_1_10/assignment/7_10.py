import math


def zad7():
    print("7. Napisz program przeliczajacy kwote pieniedzy wyrazona w złotówkach na kwote dolarów. Biezacy kurs dolara znajdziesz w Internecie.")
    x = float(input("  Kwota w pln: "))
    print(f'  Przewalutowano na USD: {round(x * 0.25765, 2)}\n')


def zad8():
    print("8. Napisz program przeliczajacy wielkosc kata wyrazona w stopniach na radiany.")
    x = int(input("  Podaj wielkość kąta w stopniach: "))
    print(f'  {x} Stopnie = {round(math.radians(x), 4)} Radiany\n')


def zad9():
    print("9. Napisz program wyznaczajacy wartosc funkcji f(x) = x^5 + 3 cos x + e^(4x−7) + ln(x + 2) w zadanym punkcie.")
    x = int(input("  Podaj x: "))
    wynik = math.pow(x, 5) + 3*math.cos(x) + math.exp(4*x-7) + math.log(x+2)
    print(f'  Wyniki {round(wynik, 2)}\n')


def zad10():
    print("10. Napisz program wyznaczajacy wartosc funkcji f(x) = (ax + b) / (cx + 1)^2 w zadanym punkcie.")
    x, a, b, c = map(int, input(
        "  Podaj x, a, b, c w jednaje lini oddzielone spacją: ").split())
    print(f'  Wyniki {round((a*x + b) / (c*x + 1)**2, 2)}\n')


if __name__ == "__main__":
    # zad7()
    # zad8()
    # zad9()
    zad10()
