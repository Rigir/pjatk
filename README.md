# ![PJATK](/.assets/banner.png)

## 📑 Contents

Subject ( language/s, technology/es ) [ final project ]

- The first semester:
  - [PRG1](/PRG1) ( Python \ C++ )
- The second semester:
  - [RBD](/RBD) ( PostgreSQL ) [ [RTS Game](/RBD/_final_project) ]
  - [WPRG](/WPRG) ( PHP ) [ [Time Capsule](/WPRG/_final_project) ]
  - [POJ](/POJ) ( Java ) [ [Minesweeper](/POJ/_final_project) ]
- The third semester:
  - [ASD](/ASD)( Python ) [ [Time complexity / Compression](/ASD/_final_project) ]
  - [JAZ](/JAZ) ( Spring ) [ [Smart Home Weather Manager](/JAZ/_final_project) ]
  - [MPR](/MPR) ( Mockito \ Selenium \ JMeter )
  - [WIA2](/WIA2) ( Assembler )
- The fourth semester:
  - [GRK](/GRK) ( Blender \ OpenGL )
  - [TIN](/TIN) ( JS \ Bootstrap \ Less )
  - [SOP](/SOP) ( C \ Assembler )
- The fifth semester [ web applications ]:
  - [TFN](/TFN) ( React \ SASS )[ [Person CRUD](/TFN/_final_project/) ]
  - [BSI](/BSI) ( Python \ Linux )
  - [NAI](/NAI) ( Python \ C++ )
  - [ICK](/ICK) ( Figma \ Axure )
  - [TBK](/TBK) ( Docker )
- The sixth semester [ web applications ]:
  - [TAU](/TAU) ( pytest \ Selenium \ Cypress )
  - [SAI](/SAI) ( Python \ NLTK \ Snorkel )
  - [MAS](/MAS) ( PIPE )
- The seventh semester [ web applications ]:
  - [TAPI](/TAPI) ( REST, GraphQL, gRPC )
  - [SKL](/SKL) ( Scala )
  - [SAD](/SAD) ( R )

## 📝 Useful links

- Online database collection: <https://sqliteonline.com/>
- For testing redex: <https://regex101.com/>
- To generate lorem ipsum: <https://pl.lipsum.com/>
- Mock data (CSV, JSON, SQL, and Excel formats.) <https://www.mockaroo.com/>
- Markup Validation Service: <https://validator.w3.org/#validate_by_uri+with_options>
  
## 📄 License:

PJAIT © All Rights Reserved 
