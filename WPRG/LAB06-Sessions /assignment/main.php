<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Main</title>
    </head>
    <body>
    <form action="main.php" method="post">
        <select name='items[0][amount]'>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value='4'>4</option>
        </select><br>

        Imię: <input type='text' name='items[0][fName]' required><br>
        Nazwisko: <input type='text' name='items[0][lName]' required><br>
        Adres: <input type='text' name='items[0][address]'><br>
        E-mail: <input type='email' name='items[0][email]' required><br>
        Od: <input type='date'  min='2020-01-01' name='items[0][since]' required><br>
        Do: <input type='date' name='items[0][until]' required><br>
        Łóżka dla dziecka: <input type='checkbox' name='items[0][bedForKid]'><br>
        <br>Udogodnienia:<br>
        <select name='items[0][amenities][]' multiple>
            <option value='Klimatyzacja'>Klimatyzacja</option>
            <option value='Popielniczka'>Popielniczka</option>
        </select><br>
        <br><button type="submit">Wyślij</button>
    </form>
    </body>
</html>

<?php 
if(isset($_SESSION['items']) == null){
    if(isset($_POST["items"])){
        
        $items = $_POST["items"];
        
        $amount = $items[0]["amount"];
        for ($i=0; $i < $amount; $i++) { 
            if( isset($items[$i]['fName']) && isset($items[$i]['lName']) && isset($items[$i]['address']) && isset($items[$i]['email']) 
                && isset($items[$i]['since']) && isset($items[$i]['until']) && isset($items[$i]['amenities'])
            )
            if($items[$i]['until'] > $items[$i]['since'] && $items[$i]['since'] > date("Y-m-d")){
                $_SESSION['items'] = $_POST["items"];
                header("Refresh:0");
            } else echo "Error: Wrong range of days entered ";
        }
    } 
}
else header("Location: completeForm.php");


?>