<?php
    /* 
        Zadanie 1.5
        Kalkulator pól powierzchni (używając switch).
        - program zapytuje, jaką figurę chcemy obliczyć (trójkąt, prostokąt, trapez)
        - w zależności od wybranej figury program uruchamia odpowiednią funkcję
        - każda figura ma mieć swoją osobną funkcję, która zapyta o wymiary i policzy pole
    */
    
    $figure = "trójkąt";
    $a = 3;
    $b = 4;
    $c = 5;

    if(isset($figure)){
        echo "Wynik: ";
        switch($figure){
            case 'trójkąt':
                echo triangleArea($a,$b);
                break;
            case 'prostokąt':
                echo rectangleArea($a,$b);
                break;
            case 'trapez':
                echo trapezoidArea($a,$b,$c);
                break;
            default:
                echo "ERROR: Nie mamy takiej figury!"; 
                break;
        }
    }

    function triangleArea($a, $h){
        return ($a * $h)/2;
    }

    function rectangleArea($a, $b){
        return $a * $b;
    }

    function trapezoidArea($a, $b, $h){
        return (($a + $b) * $h)/2;
    }

?>
