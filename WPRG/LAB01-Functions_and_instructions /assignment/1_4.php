<?php
    /* 
        Zadanie 1.4
        Napisz funkcję, która z podanego numeru PESEL odczyta datę urodzenia i zwróci ją w
        formacie dd-mm-rr.
    */
    
    $PESEL_A = "72122206102";
    $PESEL_B = "19311206102";

    echo "Before 2000: " .  dateFromPESEL($PESEL_A) . "<br>";
    echo "After 2000: " .  dateFromPESEL($PESEL_B);

    function dateFromPESEL($PESEL){
        $year = (int)substr($PESEL,0,2);
        $month = (int)substr($PESEL,2,2);
        $day = (int)substr($PESEL,4,2);
        if($month <= 12)
            return $day . "-" . $month . "-" . $year;
        else return $day . "-" . ($month-20) . "-" . $year; 
       
    }
?>
