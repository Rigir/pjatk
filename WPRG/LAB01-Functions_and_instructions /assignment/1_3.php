<?php
    /* 
        Zadanie 1.3
        Program do cenzurowania.
        Napisz funkcję, która zastąpi wszystkie niepożądane słowa gwiazdkami (*).
        Funkcja ma zawierać w sobie tablicę niepożądanych słów. Zdanie do ocenzurowania
        powinna otrzymać w parametrze.
    */

    $dic = array("banan", "woda", "mango");
    $words = array("Banan", "kawa", "WoDa");

    foreach ($words as $key => $value) {
        echo censorship($value, $dic)  . "\n";
    }

    function censorship($word, $dic){
        $temp = strtolower($word);
        if(in_array($temp, $dic)){
            for( $i=0; $i<strlen($temp); $i++) { 
                echo "*";
            }
        }else echo $word;
    }

?>