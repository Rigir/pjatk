<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <title>Biuro podróży</title>
    </head>
    <body>
        <form action="3_3.php" method="post">
            Ilość pasarzerów: 
            <select name="amount">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select><br>
            <br><button type="submit">Wyślij</button>
        </form>
    </body>
</html>

<?php
    $amount = $_POST['amount'];
    $item = $_POST['item'];

    if(isset($amount)){
        ob_clean();
        displayForm($amount);
    }
    if(isset($item)){
        ob_clean();
        for ($i=0; $i < $amount; $i++) { 
            if( isset($item[$i]['fName']) && isset($item[$i]['lName']) && isset($item[$i]['address']) && isset($item[$i]['email']) 
                && isset($item[$i]['since']) && isset($item[$i]['until']) && isset($item[$i]['amenities'])
            )
            if($item[$i]['until'] > $item[$i]['since'] && $item[$i]['since'] > date("Y-m-d")){
            diplayData(
                $item[$i]['fName'],
                $item[$i]['lName'],
                $item[$i]['address'],
                $item[$i]['email'],
                $item[$i]['since'],
                $item[$i]['until'],
                $item[$i]['bedForKid'],
                $item[$i]['amenities'],
            );
            } else echo "Error: Wrong range of days entered ";
        }
    }
    
    function displayForm($amount){
        echo " <form action='3_3.php' method='post'> ";
        echo "<input type='hidden' name='amount' value='{$amount}' style=''";
        for ($i=0; $i < $amount; $i++) { 
            echo " <br> Form ${i}! <br>
                Imie: <input type='text' name='item[${i}][fName]' required><br>
                Nazwisko: <input type='text' name='item[${i}][lName]' required><br>
                Adres: <input type='text' name='item[${i}][address]'><br>
                E-mail: <input type='email' name='item[${i}][email]' required><br>
                Od: <input type='date'  min='2020-01-01' name='item[${i}][since]' required><br>
                Do: <input type='date' name='item[${i}][until]' required><br>
                Łóżka dla dziecka: <input type='checkbox' name='item[${i}][bedForKid]'><br>
                <br>Udogodnienia:<br>
                <select name='item[${i}][amenities][]' multiple>
                    <option value='Klimatyzacja'>Klimatyzacja</option>
                    <option value='Popielniczka'>Popielniczka</option>
                </select><br>
                <br>
            ";
        }
        echo "<button type='submit'>Wyślij</button></form>";
      
    }

    function diplayData($fName, $lName, $address, $email, $since, $until, $bedForKid, $amenities){
        echo " <br>   
        <table style='border: 1px solid black;'>
            <tr>
                <td> 
                    Imię:  ${fName} <br>
                    Nazwisko:  ${fName}
                </td>
            </tr>
            <tr>
                <td>
                    Adres: ${address} <br>
                    E-mail: ${email}
                </td>
            </tr>
                <td>
                    Pobyt: <br> od ${since} do ${until} 
                </td>
            </tr>
            </tr>
                <td>

                    Udogodnienia: <br>"; 
                    echo "Łóżko dla dziecka: ";
                    if($bedForKid) echo "Tak <br>";
                    else echo "Nie <br>";
                    echo "Inne: ";
                    foreach ($amenities as $key => $value) {
                        echo $value . " ";
                    }
        echo "
                    </td>
                </tr>
            </table>
        ";
    } 
?>