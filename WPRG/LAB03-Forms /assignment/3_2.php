<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <title>Biuro podróży</title>
    </head>
    <body>
        <form action="3_2.php" method="post">
            Ilość pasarzerów: 
            <select name="amount">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select><br>
            Imię: <input type="text" name="fName" required><br>
            Nazwisko: <input type="text" name="lName" required><br>
            Adres: <input type="text" name="address"><br>
            E-mail: <input type="email" name="email" required><br>
            Od: <input type="date"  min="2020-01-01" name="since" required><br>
            Do: <input type="date" name="until" required><br>
            Łóżka dla dziecka: <input type="checkbox" name="bedForKid"><br>
            <br>Udogodnienia:<br>
            <select name="amenities[]" multiple>
                <option value="Klimatyzacja">Klimatyzacja</option>
                <option value="Popielniczka">Popielniczka</option>
            </select><br>

            <br><button type="submit">Wyślij</button>
        </form>
    </body>
</html>

<?php
    $amount = $_POST['amount'];
    $fName = $_POST['fName']; $lName = $_POST['lName']; 
    $address = $_POST['address']; $email = $_POST['email']; 
    $since = $_POST['since']; $until = $_POST['until']; 
    $bedForKid = $_POST['bedForKid']; $amenities = $_POST['amenities'];
    
    if(isset($fName) && isset($lName) && isset($address) && isset($email) && isset($since) && isset($until) && isset($amount)){
        if($until > $since && $since > date("Y-m-d")){
            ob_clean();
            diplayForm($fName, $lName, $address, $email, $since, $until, $bedForKid, $amenities, $amount);
        } else echo "Error: Wrong range of days entered ";
    }

    function diplayForm($fName, $lName, $address, $email, $since, $until, $bedForKid, $amenities, $amount){
        echo "
        <!doctype html>
        <html>
            <head>
                <meta charset='UTF-8' />
                <title>Podsumowanie</title>
            </head>
            <body>      
            <table style='border: 1px solid black;'>
                <tr>
                    <td> 
                        Imię:  ${fName} <br>
                        Nazwisko:  ${fName}
                    </td>
                </tr>
                <tr>
                    <td>
                        Adres: ${address} <br>
                        E-mail: ${email} <br>
                        Ilość osób: ${amount} <br>
                    </td>
                </tr>
                    <td>
                        Pobyt: <br> od ${since} do ${until} 
                    </td>
                </tr>
                </tr>
                    <td>
                        Udogodnienia: <br>"; 
                        echo "Łóżko dla dziecka: ";
                        if($bedForKid) echo "Tak <br>";
                        else echo "Nie <br>";
                        echo "Inne: ";
                        foreach ($amenities as $key => $value) {
                            echo $value . " ";
                        }
        echo "
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ";
    } 
?>