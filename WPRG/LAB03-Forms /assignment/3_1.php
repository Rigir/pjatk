<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <title>Prosty kalkulator</title>
    </head>
    <body>
        <form action="3_1.php" method="post">
            Num1: <input type="number" name="x" required><br>
            Num2: <input type="number" name="y" required><br>
            Operacja:<select name="operation">
                <option value="add">Dodawanie</option>
                <option value="sub">Odejmowanie</option>
                <option value="multi">Mnożenie</option>
                <option value="div">Dzielenie</option>
            </select><br>
            <button type="submit">Sprawdź</button>
        </form>
    </body>
</html>

<?php
    $x = $_POST['x']; $y = $_POST['y'];  $operation = $_POST['operation'];
    switch ($operation) {
        case 'add':
            echo "Wynik: " . $x + $y;
            break;
        case 'sub':
            echo "Wynik: " . $x - $y;
            break;
        case 'multi':
            echo "Wynik: " . $x * $y;
            break;
        case 'div':
            if($y != 0) echo "Wynik: " . $x / $y;
            else echo "Error: You can't divide number by zero!";
            break;
        default:
            echo "Error: Operation doesn't exist!";
            break;
    }
?>