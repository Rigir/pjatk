<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <title>Liczba pierwsza</title>
    </head>
    <body>
        <form action="3_2.php" method="post">
            IsPrime: <input type="number" name="num" required>
            <button type="submit">Sprawdź</button>
        </form>
    </body>
</html>

<?php
    $num = $_POST['num'];

    if(isset($num) && $num > 0) echo "Liczba: " . $num . " ". primeCheck($num);
    else echo "Error: You did not enter the number or it is less than one."; 
    
    function primeCheck($number){ 
        $count = 1; 
        if ($number == 1) 
            return "nie jest pierwsza | Literacje: " . $count; 
        for ($i = 2; $i <= sqrt($number); $i++){ 
            if ($number % $i == 0) 
                return "nie jest pierwsza | Literacje: " . $count; 
            $count++;
        } 
        return "jest pierwsza | Literacje: " . $count; 
    } 
?>

