<?php
    /* 
        Zadanie 2.2
        Napisz program “jakiej jestem narodowości” z użyciem tablic asocjacyjnych. 
        Programpowinien przyjmować nazwę kraju, a następnie w zawartej w nim tablicy 
        sprawdzić, jaknazywa się odpowiednia narodowość - i tę narodowość zwrócić.
    */
    
    $country = 'Polska';
    echo getNationality($country);

    function getNationality($country){
        $nationality = array(
            'Polska' => 'Polak/Polka',
            'Wielka Brytania' => 'Brytyjczyk/Brytyjka',
            'Anglia' => 'Anglik/Angielka',
            'Szkocja' => 'Szkot/Szkotka',
            'Walia' => 'Walijczyk/Walijka'
        );

        return $nationality[$country];
    }

?>