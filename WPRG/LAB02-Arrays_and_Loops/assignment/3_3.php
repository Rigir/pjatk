<?php
    /* 
        Zadanie 3.3
        Napisz funkcję, która wyświetli w konsoli tabliczkę mnożenia w formie kwadratu o boku podanym jako parametr.
    */
    
    $num = 4;
    echo multiplicationTable($num);

    function multiplicationTable($num){
        for ($i=1; $i <= $num; $i++){
            for($j=1; $j <= $num; $j++)
                echo $i * $j . "\t";
            echo "<br>";
        }
    }

?>