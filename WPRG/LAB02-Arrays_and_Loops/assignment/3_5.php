<?php
/* 
    Zadanie 3.5
    Prosta gra “kółko i krzyżyk” (praca w grupach 2-3 os.).
        -Planszą jest tablica 3x3-Program działa na zasadzie pętli
        -Za każdą iteracją, program pyta użytkownika gdzie postawić kółko/krzyżyk (numerwiersza i kolumny)
        -Następnie program wyświetla w konsoli aktualny stan planszy
        -Program sprawdza, czy gra się już powinna zakończyć (ktoś wygrał, albo zajętowszystkie pola) i informuje o wyniku gdy rzeczywiście jest koniec gry.
*/
    session_start();

    //Global variables
    $COLUMNS = 3; $ROWS = 3; $PLAYER = "X";
    $rowMove = (int)$_POST['x']; $columnMove = (int)$_POST['y'];
    
    //debug
    //echo var_dump($_SESSION['BoardSquers']) . "<br>";
    //echo var_dump($rowMove) . " " . var_dump($columnMove) . "<br>";
    
    //Main
    if($_SESSION['BoardSquers'] == NULL) clearBoard();
    else $BoardSquers = $_SESSION['BoardSquers'];
    $nextPlayer = $BoardSquers[$ROWS][0];

    if(($rowMove >= 1 && $rowMove <= $ROWS) && ($columnMove >= 1 && $columnMove <= $COLUMNS) && $BoardSquers[$rowMove-1][$columnMove-1] == '_'){
        playersMove();
        displayBoard();
        
        if(!winner('X') && !winner('O') && !isFill()){
            header("Refresh:0");
        }else clearBoard();
    }
    else {
        displayBoard();
    } 

    //Functions
    function playersMove(){
        GLOBAL $BoardSquers, $rowMove, $columnMove, $nextPlayer, $ROWS;
        $BoardSquers[$rowMove-1][$columnMove-1] = $nextPlayer;
        $BoardSquers[$ROWS][0] = ( $nextPlayer == 'X' ) ? 'O' : 'X';
        $_SESSION['BoardSquers'] = $BoardSquers;
    }

    function isFill(){
        GLOBAL $BoardSquers, $ROWS, $COLUMNS;
        for($i=0; $i<$ROWS; $i++)
            for($j=0; $j<$COLUMNS; $j++)
                if($BoardSquers[$i][$j] == '_')
                    return FALSE; 
        displayBoard();
        echo "DRAW";     
        unset($_SESSION['BoardSquers']);
        return TRUE;
    }

    function winner($nextPlayer){
        if(checkWinner($nextPlayer)){
            displayBoard();
            echo " Player: " . $nextPlayer . " wins! ";
            return true;
        }
        return false;  
    }

    function checkWinner($nextPlayer){
        GLOBAL $BoardSquers, $ROWS, $COLUMNS;
        $win = false;

        //Rows
        for($i=0; $i<$ROWS; $i++)
            for($j=0; $j<$COLUMNS-2; $j++)
                if(( $BoardSquers[$i][$j] == $nextPlayer) && ($BoardSquers[$i][$j+1] == $nextPlayer) && ($BoardSquers[$i][$j+2] == $nextPlayer)) 
                    $win = true;
    
        //Columns
        for($i=0; $i<$ROWS-2; $i++)
            for($j=0; $j<$COLUMNS; $j++)
                if(( $BoardSquers[$i][$j] == $nextPlayer) && ($BoardSquers[$i+1][$j] == $nextPlayer) && ($BoardSquers[$i+2][$j] == $nextPlayer)) 
                    $win = true;
    
        //Diagonals
        for($i=0; $i<$ROWS-2; $i++) //from left to right
            for($j=0; $j<$COLUMNS-2; $j++)
                if(( $BoardSquers[$i][$j] == $nextPlayer) && ($BoardSquers[$i+1][$j+1] == $nextPlayer) && ($BoardSquers[$i+2][$j+2] == $nextPlayer)) 
                    $win = true;
        
        for($i=0; $i<$ROWS-2; $i++) //from right to left
            for($j=0; $j<$COLUMNS-2; $j++)
                if(( $BoardSquers[$i][$j+2] == $nextPlayer) && ($BoardSquers[$i+1][$j+1] == $nextPlayer) && ($BoardSquers[$i+2][$j] == $nextPlayer)) 
                    $win = true;
    
        return $win;
    }

    function clearBoard(){
        GLOBAL $BoardSquers, $ROWS, $COLUMNS, $PLAYER;
        unset($_SESSION['BoardSquers']);
        for($i=0; $i<$ROWS; $i++)
            for($j=0; $j<$COLUMNS; $j++)
                $BoardSquers[$i][$j] = '_';
        $BoardSquers[$ROWS][0] = $PLAYER;
        $_SESSION['BoardSquers'] = $BoardSquers;
    }

    function displayBoard(){
        GLOBAL $BoardSquers, $ROWS, $COLUMNS;
        ob_clean();
        $diplay = "<table>";
        for($i=0; $i < $ROWS; $i++){
            $diplay .= "<tr>";
            for($j=0; $j < $COLUMNS; $j++){
                $diplay .= "<th>" . $BoardSquers[$i][$j] . "</th>";
            }
            $diplay .= "</tr>";
        }
        $diplay .= "</table>";
        echo $diplay . displayInput();
    }

    function displayInput(){
        GLOBAL $nextPlayer;
        return ' <br>
        <form action="3_5.php" method="post">
            Player: '.$nextPlayer.'<br>
            col:  <input type="number" name="y"><br>
            row: <input type="number" name="x"><br>
            <input type="submit">
        </form>
        ';
    }



?>
