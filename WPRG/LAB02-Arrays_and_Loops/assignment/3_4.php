<?php
    /* 
        Zadanie 3.4
        Napisz funkcję, która sprawdzi, czy dana liczba jest liczbą pierwszą.W swoim programie umieść zmienną, 
        która policzy wszystkie iteracje pętli, potrzebne dowykonania obliczeń. Spróbuj tak zmodyfikować program, 
        by było potrzeba jak najmniejiteracji (przy zachowaniu prawidłowego działania).
    */
    
    $num = 12907;
    echo primeCheck($num) . "<br>";
    $num = 10;
    echo primeCheck($num);

    function primeCheck($number){ 
        $count = 1; 
        if ($number == 1) 
            return "Nie jest pierwsza | " . $count; 
        for ($i = 2; $i <= sqrt($number); $i++){ 
            if ($number % $i == 0) 
                return "Nie jest pierwsza | " . $count; 
            $count++;
        } 
        return "Jest pierwsza | " . $count; 
    } 

?>