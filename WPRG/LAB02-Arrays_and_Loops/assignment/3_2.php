<?php
    /* 
        Zadanie 3.2
        Zmodyfikuj funkcję z zadania 1.1, by przyjmowała argument - liczbę rzutów kostką.
        I zwracała tablicę wyników.
    */
    
    $num = 4;
    $log = implode(",", rollADice($num));
    echo $log;

    function rollADice($num){
        $arr = [];
        for ($i=0; $i < $num; $i++) { 
            $arr[$i] = rand(1,6);
        }
        return $arr;
    }
?>