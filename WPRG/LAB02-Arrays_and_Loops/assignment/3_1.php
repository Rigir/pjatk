<?php
    /* 
        Zadanie 3.1
        Napisz funkcję, zwracającą maksymalny element tablicy losowych liczb 
        (bez używaniagotowych funkcji PHP) w 4 wersjach: for, while, do while, foreach.
    */

    $numbers = getArrayOdfNums();
    $log = implode(",", $numbers);
    echo $log . "<br>";

    echo "w_Result: " . findMaxValue_while($numbers) . "<br>";
    echo "dw_Result: " . findMaxValue_doWhile($numbers) . "<br>";
    echo "f_Result: " . findMaxValue_for($numbers) . "<br>";
    echo "fe_Result: " . findMaxValue_forEach($numbers) . "<br>";

    function findMaxValue_while($numbers){
        $temp = 0; $i = 0;
        while( $i < count($numbers)){
            if( $numbers[$i] > $temp)
                $temp = $numbers[$i];
            $i++;
        }
        return $temp;
    }

    function findMaxValue_doWhile($numbers){
        $temp = 0; $i = 0;
        do{
            if( $numbers[$i] > $temp)
                $temp = $numbers[$i];
            $i++;
        } while( $i < count($numbers));
        return $temp;
    }

    function findMaxValue_for($numbers){
        $temp = 0;
        for ($i=0; $i < count($numbers); $i++) { 
            if( $numbers[$i] > $temp)
                $temp = $numbers[$i];
        }
        return $temp;
    }
    
    function findMaxValue_forEach($numbers){
        $temp = 0;
        foreach ($numbers as $key => $value) {
            if( $value > $temp)
                $temp = $value;
        }
        return $temp;
    }

    function getArrayOdfNums(){
        $temp = [];
        for ($i=0; $i < 10; $i++)
            $temp[$i] = rand(0,10);
        return $temp;
    }
?>