<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <title>Biuro podróży</title>
    </head>
    <body>
        <form action="6.php" method="post">
            <?php echo formStrusture(0)?>
            <br><button type="submit">Wyślij</button>
        </form>
    </body>
</html>

<?php
    $items = setNewCookie("items");

    if(isset($_COOKIE['items'])){
        $amount = $items[0]["amount"];
        ob_clean();
        if(!isset( $items[$amount-1] )) displayForm($amount);
        else for ($i=0; $i < $amount; $i++) { 
            if( isset($items[$i]['fName']) && isset($items[$i]['lName']) && isset($items[$i]['address']) && isset($items[$i]['email']) 
                && isset($items[$i]['since']) && isset($items[$i]['until']) && isset($items[$i]['amenities'])
            )
            if($items[$i]['until'] > $items[$i]['since'] && $items[$i]['since'] > date("Y-m-d")){
            diplayData(
                $items[$i]['fName'],
                $items[$i]['lName'],
                $items[$i]['address'],
                $items[$i]['email'],
                $items[$i]['since'],
                $items[$i]['until'],
                $items[$i]['bedForKid'],
                $items[$i]['amenities'],
            );
            } else echo "Error: Wrong range of days entered ";
        }
    }

    function setNewCookie($name){
        if(isset($_COOKIE["${name}"])){
            $temp = json_decode($_COOKIE["${name}"], true);
            $temp += $_POST[$name];
        } else $temp = $_POST[$name];

        if(isset($temp)){
            setcookie("${name}", json_encode($temp), time()+3600);
            $data = json_decode($_COOKIE["${name}"], true);
        }
        return $data;
    }

    function displayForm($amount){
        echo " <form action='6.php' method='post'> ";
        for ($i=1; $i < $amount; $i++) echo formStrusture($i);
        echo "<button type='submit'>Wyślij</button></form>";
    }

    function formStrusture($i){
        if( $i == 0) echo "
            <select name='items[${i}][amount]'>
                <option value='1'>1</option>
                <option value='2'>2</option>
                <option value='3'>3</option>
                <option value='4'>4</option>
            </select><br>
        ";
        return "Imie: <input type='text' name='items[${i}][fName]' required><br>
                Nazwisko: <input type='text' name='items[${i}][lName]' required><br>
                Adres: <input type='text' name='items[${i}][address]'><br>
                E-mail: <input type='email' name='items[${i}][email]' required><br>
                Od: <input type='date'  min='2020-01-01' name='items[${i}][since]' required><br>
                Do: <input type='date' name='items[${i}][until]' required><br>
                Łóżka dla dziecka: <input type='checkbox' name='items[${i}][bedForKid]'><br>
                <br>Udogodnienia:<br>
                <select name='items[${i}][amenities][]' multiple>
                    <option value='Klimatyzacja'>Klimatyzacja</option>
                    <option value='Popielniczka'>Popielniczka</option>
                </select><br><br>
        ";
    }

    function diplayData($fName, $lName, $address, $email, $since, $until, $bedForKid, $amenities){
        echo " <br>   
        <table style='border: 1px solid black;'>
            <tr>
                <td> 
                    Imię:  ${fName} <br>
                    Nazwisko:  ${fName}
                </td>
            </tr>
            <tr>
                <td>
                    Adres: ${address} <br>
                    E-mail: ${email}
                </td>
            </tr>
                <td>
                    Pobyt: <br> od ${since} do ${until} 
                </td>
            </tr>
            </tr>
                <td>

                    Udogodnienia: <br>"; 
                    echo "Łóżko dla dziecka: ";
                    if($bedForKid) echo "Tak <br>";
                    else echo "Nie <br>";
                    echo "Inne: ";
                    foreach ($amenities as $key => $value) {
                        echo $value . " ";
                    }
        echo "
                    </td>
                </tr>
            </table>
        ";
    } 
?>
