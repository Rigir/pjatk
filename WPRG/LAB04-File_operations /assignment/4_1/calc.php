<?php
    function addition($x, $y){
        return $x + $y;
    }
    function subtraction($x, $y){
        return $x - $y;
    }
    function multiplication($x, $y){
        return $x * $y;
    }
    function division($x, $y){
        if($y != 0) return $x / $y;
        else echo "Error: You can't divide number by zero!";
    }
?>
