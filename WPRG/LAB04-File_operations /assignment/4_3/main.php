<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <title>Biuro podróży</title>
    </head>
    <body>
        <form action="main.php" method="post">
            Ilość pasarzerów: 
            <select name="amount">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select><br>
            Imię: <input type="text" name="fName" required><br>
            Nazwisko: <input type="text" name="lName" required><br>
            Adres: <input type="text" name="address"><br>
            E-mail: <input type="email" name="email" required><br>
            Od: <input type="date"  min="2020-01-01" name="since" required><br>
            Do: <input type="date" name="until" required><br>
            Łóżka dla dziecka: <input type="checkbox" name="bedForKid"><br>

            <br><button type="submit">Wyślij</button>
        </form>
        <form action="main.php" method="post">
            <br><button type="submit" name="read" value="RUN">Wczytaj</button>
        </form>
    </body>
</html>

<?php
    $file = 'temp.csv';
    $amount = $_POST['amount'];
    $fName = $_POST['fName']; $lName = $_POST['lName']; 
    $address = $_POST['address']; $email = $_POST['email']; 
    $since = $_POST['since']; $until = $_POST['until']; 
    $bedForKid = $_POST['bedForKid'];
    
    if(isset($fName) && isset($lName) && isset($address) && isset($email) && isset($amount) && isset($since) && isset($until) && isset($bedForKid)){
        if($until > $since && $since > date("Y-m-d")){
            ob_clean();
            diplayForm($fName, $lName, $address, $email, $amount, $since, $until, $bedForKid);

            if(file_exists($file))
                $fd = fopen($file , 'a') or die("Can't create file");
            else {
                $fd = fopen($file , 'w') or die("Can't create file");
                $fields = array('fName', 'lName', 'address', 'email', 'amount', 'since', 'until','bedForKid');
                fputcsv($fd, $fields, ";");
            }
            
            $data = array($fName, $lName, $address, $email, $amount, $since, $until, $bedForKid);
            fputcsv($fd, $data, ";");

            fclose($fd); 
        } else echo "Error: Wrong range of days entered ";
    }

    if(array_key_exists('read',$_POST)){
        if(file_exists($file))
            $fd = fopen($file , "r") or die("File does not exist or you lack permission to open it");
        
        echo "<br><table style='border: 1px solid black;'>";
        while (($row = fgetcsv($fd, 0, ";")) !== FALSE) {
            echo "<tr>";
            foreach ($row as $key => $value) {
                echo "<td>" . $value . "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
        fclose($fd);
    }


    function diplayForm($fName, $lName, $address, $email, $amount, $since, $until, $bedForKid){
        echo "    
            <table style='border: 1px solid black;'>
                <tr>
                    <td> 
                        Imię:  ${fName} <br>
                        Nazwisko:  ${fName}
                    </td>
                </tr>
                <tr>
                    <td>
                        Adres: ${address} <br>
                        E-mail: ${email} <br>
                        Ilość osób: ${amount} <br>
                    </td>
                </tr>
                    <td>
                        Pobyt: <br> od ${since} do ${until} 
                    </td>
                </tr>
                </tr>
                    <td>
                        Udogodnienia: <br>"; 
                        echo "Łóżko dla dziecka: ";
                        if($bedForKid) echo "Tak <br>";
                        else echo "Nie <br>";
        echo "
                    </td>
                </tr>
            </table>
        ";
    } 
?>