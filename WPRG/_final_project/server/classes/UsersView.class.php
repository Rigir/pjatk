<?php
/*
    The View is used to show the data.
*/

class UsersView extends UsersModel{
    protected function showUserData($name){
        $result = $this -> getUserData($name, $name);
        return " username: " . $result['username'].
             " email: " . $result['email'];
    }

    protected function showTweets($arr){ 
        $result = " <div class='tweets'> ";
        foreach ($arr as &$row) {
            $result .= "
                <div>
                    <span>@{$row['username']}</span>
                    <p>{$row['tweet']}</p>
                </div>
            ";
        }  
        if(sizeof($arr) < 3){
            $temp =  $this -> randomTweets();
            for ($i=sizeof($arr); $i < 3; $i++) { 
                $result .= "
                <div>
                    <span>@{$temp[$i]['username']}</span>
                    <p>{$temp[$i]['tweet']}</p>
                </div>
                ";
            }
        }
        return $result . "</div>";
    }

    protected function randomTweets(){
        return array( 
            0 => array(
                "username" => "Mae West",
                "tweet" => "You only live once, but if you do it right, once is enough.",
            ),  
            1 => array(
                "username" => "Robert Frost",
                "tweet" => "In three words I can sum up everything I've learned about life: it goes on.",
            ),
            2 => array(
                "username" => "Oscar Wilde",
                "tweet" => "To live is the rarest thing in the world. Most people exist, that is all.",
            ),
        ); 
  
    }

    protected function formError($error){
        return "
            <div class='error'>
                <span>{$error}.</span>
            </div>
        ";
    }

    protected function changeHeader($path){
        header("location: ../client/{$path}");
        exit();
    }
}