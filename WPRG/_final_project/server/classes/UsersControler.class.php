<?php
/*
    The controller is used to pass the data to be modified.
*/

class UsersControler extends UsersView{
    
    public function signup($form){
        if($this -> checkEmptyInputForm($form) !== false)
            return $this -> formError("The form is empty");

        if($this -> invalidusername($form["username"]) !== false) 
            return $this -> formError("Invalid username");

        if($this -> invalidEmail($form["email"]) !== false) 
            return $this -> formError("Invalid email address");

        if($this -> pwdMatch($form["pwd"], $form["pwd_rpt"]) !== false) 
            return $this -> formError("Passwords do not match");

        if($this -> usernameExists($form["username"], $form["email"]) !== false) 
            return $this -> formError("Username or email address already exists");

        $this -> createUser($form["username"], $form["email"], $form["pwd"]);
        $this -> changeHeader('login.php');
    }

    public function login($form){
        if($this -> checkEmptyInputForm($form) !== false)
            return $this -> formError("The form is empty");
        if($this -> usernameExists($form["username"], $form["username"]) === false) 
            return $this -> formError("Wrong login");
        if($this -> checkPass($form["username"], $form["pwd"]) === false){
            return $this -> formError("Wrong password");
        }
        $this -> createSession($form["username"]);
        $this -> createCookie($form["username"]);
        $this -> changeHeader('main.php');
    }

    public function tweet($form, $username){
        if($this -> checkEmptyInputForm($form) !== false)
            return $this -> formError("The form is empty");
        $this -> updateTweet($username, $form["message"]);
        $this -> changeHeader('main.php');
    }

    public function tweets(){
        return $this -> showTweets($this->getTweets());
    }

    public function logout(){
        $this -> deleteSession();
        $this -> changeHeader('main.php');
    }

    public function removeAccount($form){
        $this -> deleteUser($form["delete"], $form["delete"]);
        $this -> logout();
    }

    public function test(){
        var_dump("HelloWorld");
    }
}