<?php
/*
    The model is responsible for data processing.
*/

class UsersQuery extends Database{
    protected function getUserData($username, $email){
        $sql = "SELECT * FROM {$this -> tableName} WHERE username = ? OR email = ?;";
        $stmt = $this -> conn -> prepare($sql);
        $stmt -> bind_param('ss', $username, $email); 
        $stmt -> execute(); 
        $result = $stmt -> get_result();
        return $result -> fetch_assoc();
    }

    protected function createUser($username, $email, $pwd){
        $sql = "INSERT INTO {$this -> tableName}(username,email,pwd,created) VALUES (?, ?, ?, ?);";
        $stmt = $this -> conn -> prepare($sql);
        $pwd = password_hash($pwd, PASSWORD_DEFAULT);   
        $stmt -> bind_param('ssss', $username, $email, $pwd, date("Y-m-d")); 
        $stmt -> execute();
    }

    protected function deleteUser($username, $email){
        $sql = "DELETE FROM {$this -> tableName} WHERE username = ? OR email = ?;";
        $stmt = $this -> conn -> prepare($sql);
        $stmt -> bind_param('ss', $username, $email); 
        $stmt -> execute(); 
    }
    
    protected function getTweets(){
        $sql = "SELECT username, tweet FROM {$this -> tableName} WHERE tweet IS NOT NULL ORDER BY RAND() LIMIT 3;";
        $stmt = $this -> conn -> prepare($sql);
        $stmt -> execute(); 
        $result = $stmt -> get_result();
        $arr = array();
        while ($row = $result -> fetch_assoc()) {
            array_push($arr,$row);
        }
        return $arr;
    }

    protected function setTweet($username, $email, $tweet){
        $sql = "UPDATE {$this -> tableName} SET tweet = ? WHERE username = ? OR email = ?;";
        $stmt = $this -> conn -> prepare($sql);
        $stmt -> bind_param('sss', $tweet, $username, $email); 
        $stmt -> execute(); 
    }
}