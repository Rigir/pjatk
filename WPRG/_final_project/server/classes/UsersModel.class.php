<?php
/*
    The model is responsible for data processing.
*/

class UsersModel extends UsersQuery{
    
    protected function createSession($username){
        $user = $this->getUserData($username, $username);
        session_start();
        $_SESSION["id"] = $user["id"];
        $_SESSION["username"] = $user["username"];
        $_SESSION["tweet"] = $user["tweet"]; 
    }

    protected function updateTweet($username, $tweet){
        $user = $this->setTweet($username, $username, $tweet);
        session_start();
        $_SESSION["tweet"] = $tweet;
    }

    protected function createCookie($cookie_value){
        setcookie("username", $cookie_value, time() + (86400 * 30), "/");
    }


    protected function deleteSession(){
        session_start();
        session_unset();
        session_destroy();
    }

    //Validation 
    protected function checkEmptyInputForm($form){
        foreach ($form as $key)
            if(empty($key)) return true;
        return false;          
    }

    function invalidusername($username) {
        if(!preg_match("/^[a-zA-Z0-9]*$/", $username))
            return true;
        return false;
    }
    
    function invalidEmail($email) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            return true;
        return false;
    }
    
    function pwdMatch($pwd, $pwd_rpt) {
        if($pwd !== $pwd_rpt)
            return true;
        return false;
    }

    function usernameExists($username, $email){
        if((bool)$this->getUserData($username, $email))
            return true;
        return false;
    }

    public function checkPass($username, $pwd){
        $user = $this->getUserData($username, $username);
        return password_verify($pwd, $user["pwd"]);
    }
}