<?php

class Database {
    public $conn;
    protected $tableName = "Users";

    private $databaseName = "WPRG_db";
    private $serverName = "localhost";
    private $userName = "root";
    private $passCode = "";
    
    
    function __construct(){
        /*
            MYSQLI_REPORT_ERROR 	Report errors from mysqli function calls
            MYSQLI_REPORT_STRICT 	Throw mysqli_sql_exception for errors instead of warnings 
        */
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        try {
            $this -> createConnection();
        } catch (mysqli_sql_exception $ex) {
            die("Can't connect to the database! \n" . $ex);
        }
        try {
            $this -> createDatabase();
        } catch (mysqli_sql_exception $ex) {
            die("Can't create database! \n" . $ex);
        }
        try {
            mysqli_select_db($this -> conn, $this -> databaseName);
            $this -> createTable();
        } catch (mysqli_sql_exception $ex) {
            die("Can't create table! \n" . $ex);
        }
        
        mysqli_select_db($this -> conn, $this -> databaseName);
    }

    function __destruct() {
        try {
            $this -> conn -> close();
        } catch (mysqli_sql_exception $ex) {
            die("Can't disconnect from the database! \n" . $ex);
        }
    }

    private function createConnection(){
        $this -> conn = new mysqli($this -> serverName, $this -> userName, $this -> passCode);
    }

    private function createDatabase(){
        $sql = "CREATE DATABASE IF NOT EXISTS {$this -> databaseName}";
        mysqli_query($this -> conn , $sql);
    }

    private function createTable(){
        $sql = " CREATE TABLE IF NOT EXISTS {$this -> tableName} (
            id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
            username varchar(10) NOT NULL,
            email varchar(32) NOT NULL,
            created DATE NOT NULL,
            tweet varchar(128),
            pwd varchar(128) NOT NULL
        );";
        mysqli_query($this -> conn , $sql);
    }
}