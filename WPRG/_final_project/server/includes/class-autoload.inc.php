<?php
/*
    Is used to import class from the classes folder. 
*/
spl_autoload_register(function($className){
    $path = "../server/classes/";
    $extencion = ".class.php";
    $fileName = $path . $className . $extencion;
    if(!file_exists($fileName)){
        return false;
    }
    include_once $path . $className . $extencion;
});

