⏳ Time Capsule 
=========

![app](assets/banner.png)

A simple application written entirely in PHP with the 
[Model-View-Controller](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)
architectural pattern implemented. 

🛠 Main features:
---------
- User login and registration system 
- Create and edit your own tweet and view random tweets from other users 
- Simple data validation system 

📑 Pages I used:
---------
- For front:
  - [Getwaves.io](https://getwaves.io/)
  - [coolsymbol.com](https://coolsymbol.com/)
- For backend:
  - [php - manual](https://www.php.net/manual/en/)
  - [Wikipedia - MVC ](https://en.wikipedia.org/wiki/Model-View-Controller)
  - [Wikipedia - CRUD ](https://pl.wikipedia.org/wiki/CRUD)
  - [YT - "MVC Explained in 4 Minutes" ](https://www.youtube.com/watch?v=DUg2SWWK18I)
  - [YT - "Create A Login System In PHP" ](https://www.youtube.com/watch?v=gCo6JqGMi30)

## 📄 License:

PJAIT © All Rights Reserved 