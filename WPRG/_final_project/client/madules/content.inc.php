<?php 
    if(isset($_SESSION["id"])){
        if(empty($_GET)){
            echo "
            <div class='introduction'>   
                <h2>Hi, {$_SESSION['username']}!</h2>
                Who will you meet today?  
            </div>
            ";
            echo "<div>{$UsersControler -> tweets()}</div>";
        }
        if(isset($_GET["writer"])){
            echo "
                <div class='form-header'>
                    <h2> Write a tweet! </h2>
                    What will you write today? 
                </div>
                <div class='form-main'>
                <form action='' method='post'>";
                if(isset($_POST["message"]))
                    echo $UsersControler -> tweet($_POST, $_SESSION["username"]);
                echo"<textarea name = 'message' rows = '5' cols='30' maxlength='128' placeholder='The limit is 128 characters!'></textarea>
                        <button type='submit'>Send</button>
                    </form>
                </div>
            ";
        }
        if(isset($_GET["account"])){
            if(isset($_POST["delete"]))
                $UsersControler -> removeAccount($_POST);
            echo "
                <div class='form-header'>
                    <h2> Your Account </h2>
                </div>
                <div class='form-main'>
                    <form action='' method='post'>
                        <p>Username: {$_SESSION['username']} </p>
                        <p>Your last message:</p>
                        <textarea rows = '5' cols='30' placeholder='Are you still wondering?' disabled>{$_SESSION['tweet']}</textarea>
                        <button type='submit' name='delete' value='{$_SESSION['username']}'>
                            Delete Account
                        </button>
                    </form>
                </div>
            ";
        }
    }
    else{
        echo "
            <div>   
                <h2>Welcome!</h2>
                Who will you meet today?  
            </div>
            <div>
                {$UsersControler -> tweets()}
            </div>
        ";
    }
?>