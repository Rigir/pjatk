<?php
    session_start();
    include "../server/includes/class-autoload.inc.php"; 
    $UsersControler;
    try {
        $UsersControler = new UsersControler();
    } catch (Throwable $th) {
        die("class-autoload is not included!" . $th);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Time Capsule</title>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>