<?php 
    include_once 'madules/header.inc.php';
?>

<div class = "container">
    <div class="wave">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 330">
            <path fill="#90be5c" fill-opacity="1" d="M0,160L80,181.3C160,203,320,245,480,234.7C640,224,800,160,960,128C1120,96,1280,96,1360,96L1440,96L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z"></path>
            <path fill="#a0d466" fill-opacity="1" d="M0,256L30,256C60,256,120,256,180,224C240,192,300,128,360,106.7C420,85,480,107,540,122.7C600,139,660,149,720,128C780,107,840,53,900,64C960,75,1020,149,1080,181.3C1140,213,1200,203,1260,213.3C1320,224,1380,256,1410,272L1440,288L1440,0L1410,0C1380,0,1320,0,1260,0C1200,0,1140,0,1080,0C1020,0,960,0,900,0C840,0,780,0,720,0C660,0,600,0,540,0C480,0,420,0,360,0C300,0,240,0,180,0C120,0,60,0,30,0L0,0Z"></path>
        </svg>
    </div>
    <main>
        <div class="form-header">
            <h2> Create Account </h2>
            The page is not secured!
        </div>
        <?php  
            if(isset($_POST['username']))
                echo $UsersControler -> signup($_POST);
        ?>
        <div class="form-main">
            <form action="" method="post">
                <input type="text" name="username" placeholder="Username" maxlength="10">
                <input type="text" name="email" placeholder="Email address" maxlength="32"> 
                <input type="password" name="pwd" placeholder="Password" maxlength="64">
                <input type="password" name="pwd_rpt" placeholder="Repeat password" maxlength="64">
                <button type="submit">Sign Up</button>
            </form>
        </div>
        <div class="form-footer">
            <a href="login.php">LogIn</a>
            <a href="main.php">Close</a>
        </div>
    </main>
</div>

<?php 
    include_once 'madules/footer.inc.php';
?>