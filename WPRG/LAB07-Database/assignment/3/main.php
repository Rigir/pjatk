<?php

$servername = "localhost";
$username = "root";
$password = "";
$database = "test";
$table = "user";
$conn = null;

// Create connection
$conn = new mysqli($servername, $username, $password);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = " CREATE DATABASE IF NOT EXISTS {$database};";
$conn -> query($sql);

// Selects database
$conn -> select_db($database);

// Create table
$sql = " CREATE TABLE IF NOT EXISTS {$table} (
    id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    username varchar(128) NOT NULL,
    email varchar(128) NOT NULL,
    pwd varchar(128) NOT NULL   
);";
$conn -> query($sql);

// insert data
$sql = " INSERT INTO {$table} VALUES
    (1,'mati','mati@gmail.com','123'),
    (2,'tomek','tomek@gmail.com','1@')
;";
$conn -> query($sql);

//Query
$sql = "SELECT * FROM {$table};";

// SELECT by mysqli_fetch_row:
echo "<b> mysqli_fetch_row </b><br>";
if ($result = $conn -> query($sql)) {
  while ($row = mysqli_fetch_row($result)) {
    printf ("%s | %s <br>", $row[1], $row[2]);
  }
  $result -> free_result();
}

// SELECT by mysqli_fetch_array:
// Numeric array
echo "<br><b>  mysqli_fetch_array | Numeric </b><br>"; 
if ($result = $conn -> query($sql)) {
    while ($row = mysqli_fetch_array($result, MYSQLI_NUM)) {
        printf ("%s | %s <br>", $row[1], $row[2]);
    }
    $result -> free_result();
}
// Associative array
echo "<br><b>  mysqli_fetch_array | Associative </b><br>"; 
if ($result = $conn -> query($sql)) {
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        printf ("%s | %s <br>", $row["username"], $row["email"]);
    }
    $result -> free_result();
}

// SELECT by mysqli_num_rows:
echo "<br><b>  mysqli_num_rows </b><br>"; 
if ($result = $conn -> query($sql)) {
    $rowcount = mysqli_num_rows($result);
    printf("Result set has %d rows.\n",$rowcount);
    $result -> free_result();
}

$conn->close();