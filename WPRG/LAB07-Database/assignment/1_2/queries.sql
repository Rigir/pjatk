-- CREATE TABLE
CREATE TABLE IF NOT EXISTS users(
    id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    username varchar(128) NOT NULL,
    email varchar(128) NOT NULL,
    pwd varchar(128) NOT NULL  
);

-- INSERT INTO (dodać dwa rekordy)
INSERT INTO users VALUES
(1,'mati','mati@gmail.com','123'),
(2,'tomek','tomek@gmail.com','1@')
;

-- UPDATE
UPDATE users SET pwd = '!@!' WHERE username = 'mati';
;

-- DELETE (usunąć jeden rekord)
DELETE FROM users WHERE username LIKE 'tomek';