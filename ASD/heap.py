def max_heapify(A, i):
    global heapsize
    l = 2 * i + 1
    r = 2 * i + 2
    if l <= heapsize and A[l] > A[i]:
        largest = l
    else:
        largest = i
    if r <= heapsize and A[r] > A[largest]:
        largest = r
    if largest != i:
        A[i], A[largest] = A[largest], A[i]
        max_heapify(A, largest)


def build_max_heap(A):
    global heapsize
    heapsize = len(A) - 1
    n = (len(A) - 1) // 2
    for i in range(n, -1, -1):
        max_heapify(A, i)


def heap_sort(A):
    global heapsize
    build_max_heap(A)
    for i in range(len(A) - 1, 0, -1):
        A[0], A[i] = A[i], A[0]
        heapsize -= 1
        max_heapify(A, 0)


if __name__ == "__main__":
    a = [4, 1, 3, 2, 16, 9, 10, 14, 8, 7]
    heapsize = 0

    print(f'Before: \t {a}')

    build_max_heap(a)
    print(f'BuildMaxHeap: \t {a}')

    heap_sort(a)
    print(f'HeapSort: \t {a}')

"""
The task is to implement pseudocode.
Symbols: ≠ ⟷ ≤ ≥

left(i) = 2i + 1
right(i) = 2i + 2

MAX-HEPIFY(A, i)
    l = left(i)
    r = right(i)
    if l ≤ heap.size[A] and A[l] > A[i]
        than largest = l
        else largest = i 
    if r ≤ heap.size[A] and A[r] > A[largest]
        than largest = r
    if largest ≠ i 
        than A[i] ⟷ A[largest]
        MAX-HEPIFY(A, largest)

BUILD-MAX-HEAP(A)
    heap.size[A] = length[A]
    for i = [(length[A]-1)/2] downto 0
        do MAX-HEPIFY(A, i)

HEAPSORT(A)
    BUILD-MAX-HEAP(A)
    for i = length[A] downto 1
        do A[0] ⟷ A[i]
            heap.size[A] = heap.size[A] - 1
            MAX-HEPIFY(A, 0)
"""
