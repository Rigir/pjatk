def counting_sort_words(A, B, k):
    for i in range(len(A[0])-1, -1, -1):
        C = [0 for _ in range(k)]
        for j in range(0, len(A)):
            C[ord(A[j][i])] += 1
        for j in range(1, k):
            C[j] += C[j - 1]
        for j in range(len(A)-1, -1, -1):
            B[C[ord(A[j][i])]-1] = A[j]
            C[ord(A[j][i])] = C[ord(A[j][i])] - 1
        for j in range(0, len(A)):
            A[j] = B[j]


def counting_sort_numbers(A, B, k):
    c = [0 for _ in range(k)]
    for j in range(0, len(A)):
        c[A[j]] += 1
    for i in range(1, k):
        c[i] += c[i - 1]
    for j in range(len(A)-1, -1, -1):
        B[c[A[j]]-1] = A[j]
        c[A[j]] = c[A[j]] - 1


if __name__ == "__main__":
    words = ["COW", "DOG", "SEA", "RUG", "ROW", "MOB",
             "BOX", "TAB", "BAR", "EAR", "FOX", "TEA"]
    numbers = [2, 5, 3, 0, 2, 3, 0, 3]
    result = []

    print(f'Before: \t\t\t {words}')
    result = ["" for _ in range(len(words))]
    counting_sort_words(words, result, ord("z")+1)
    print(f'counting_sort_words:     \t {result}\n')

    print(f'Before: \t\t\t {numbers}')
    result = [0 for _ in range(len(numbers))]
    counting_sort_numbers(numbers, result, max(numbers)+1)
    print(f'counting_sort_numbers: \t\t {result}')

"""
The task is to implement pseudocode.
Symbols: ≠ ⟷ ≤ ≥

COUNTING-SORT(A, B, k)
    new array C[0...k]
    for i = 0 to k
        C[i] = 0
    for j = 1 to A.length
        C[A[j]] = C[A[j]] + 1
    for i = 1 to k
        C[i] = C[i] + C[i -1]
    for j = A.length downto 1
        B[C[A[j]]] = A[j]
        C[A[j]] = C[A[j]] - 1
"""
