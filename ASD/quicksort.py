def partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r, 1):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i + 1], A[r] = A[r], A[i+1]
    return i + 1


def quicksort(A, p, r):
    q = 0
    if p < r:
        q = partition(A, p, r)
        quicksort(A, p, r - 1)
        quicksort(A, q + 1, r)


if __name__ == "__main__":
    a = [16, 4, 14, 10, 7, 2, 6, 11, 50, 2, 10]

    print(f'Before: \t {a}')

    quicksort(a, 0, len(a)-1)
    print(f'Quicksort: \t {a}')

"""
The task is to implement pseudocode.
Symbols: ≠ ⟷ ≤ ≥

QUICKSORT(A, p, r)
    if p < r
        q = PARTITION(A, p ,r)
        QUICKSORT(A, p, q - 1)
        QUICKSORT(A, q + 1, r)

PARTITION(A, p, r)
    x = A[r]
    i = p - 1
    for j = p to r - 1
        if A[j] ≤ c
            i = i + 1
            A[u] ⟷ A[j]
    A[i + 1] ⟷ A[r]
    return i + 1
"""
