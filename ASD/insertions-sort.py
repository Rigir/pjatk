def insertion_sort(A):
    for j in range(1, len(A)):
        key = A[j]
        i = j - 1
        while i > -1 and A[i] > key:
            A[i + 1] = A[i]
            i = i - 1
        A[i + 1] = key


if __name__ == "__main__":
    a = [16, 4, 14, 10, 7, 2, 6, 11, 50, 2, 10]

    print(f'Before: \t {a}')

    insertion_sort(a)
    print(f'insertionSort: \t {a}')

"""
The task is to implement pseudocode.
Symbols: ≠ ⟷ ≤ ≥

INSERTION-SORT(A)
for j = 2 to lenght[A] do
    key = A[j]
    i = j - 1
    while i > 0 and A[i] > key do
        A[i+1] = A[i]
        i = i - 1
    end while
    A[i+1] = key
end for 

"""
