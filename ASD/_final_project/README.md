# 📚 Compression

Implemetacion of Huffman coding.

## 📝 Useful links

* To check if structure of binary tree is alright.
  * https://resources.nerdfirst.net/huffman

# ⌛️ TimeComplexity

A program to check which sorting algorithm is the fastest.
```
Date: 28.11.2021
Array size: 500 000
     
Quicksort | Lomuto’s partition scheme
Random:          55.3659 sec
Sorted:          54.7356 sec
Reversed:        54.9295 sec

Heap_sort
Random:          3.4073 sec
Sorted:          3.0241 sec
Reversed:        3.3203 sec

Merge_sort
Random:          2.7198 sec
Sorted:          2.3318 sec
Reversed:        2.2372 sec

Counting_sort
Random:          0.0933 sec
Sorted:          0.0893 sec
Reversed:        0.0895 sec
```
📄 License:
---------
PJAIT © All Rights Reserved 