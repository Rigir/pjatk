import random
import sys
import time

from sortingAlgorithms.countingSort import counting_sort
from sortingAlgorithms.mergeSort import merge_sort
from sortingAlgorithms.quicksort import quicksort
from sortingAlgorithms.heapSort import heap_sort


def measure_time(foo, *args):
    begin = time.time()
    foo(*args)
    end = time.time()
    return round(end - begin, 4)


def measure_sort(foo, arr):
    calculations = arr.copy()
    print(f'Random: \t {measure_time(foo, calculations)} sec')
    print(f'Sorted: \t {measure_time(foo, calculations)} sec')
    print(f'Reversed: \t {measure_time(foo, calculations[::-1])} sec')


def prepere_random_numbers_array(size):
    return [random.randrange(100) for _ in range(size)]


if __name__ == "__main__":
    sys.setrecursionlimit(10 ** 6)
    numbers = prepere_random_numbers_array(500000)

    print(f'Sorted:   [0...n]')
    print(f'Reversed: [n...0]')

    print(f'\nQuicksort | Lomuto’s partition scheme')
    measure_sort(quicksort, numbers)

    print(f'\nHeap_sort')
    measure_sort(heap_sort, numbers)

    print(f'\nMerge_sort')
    measure_sort(merge_sort, numbers)

    print(f'\nCounting_sort')
    measure_sort(counting_sort, numbers)