import random


def partition(arr, pivot, min_v, max_v):
    arr[pivot], arr[min_v] = arr[min_v], arr[pivot]
    pivot = arr[min_v]
    i = min_v + 1

    for j in range(min_v + 1, max_v):
        if arr[j] < pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i = i + 1

    arr[min_v], arr[i - 1] = arr[i - 1], arr[min_v]
    return i - 1


def _quicksort(arr, min_v, max_v):
    if (max_v - min_v) < 2:
        return

    pivot = random.randrange(min_v, max_v - 1)
    sec_pivot = partition(arr, pivot, min_v, max_v)

    _quicksort(arr, min_v, sec_pivot)
    _quicksort(arr, sec_pivot + 1, max_v)


def quicksort(arr):  # Lomuto partition scheme
    return _quicksort(arr, 0, len(arr))
