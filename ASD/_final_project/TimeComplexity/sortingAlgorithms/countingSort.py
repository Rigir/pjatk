def counting_sort(arr):
    max_value = 0
    for i in range(len(arr)):
        if arr[i] > max_value:
            max_value = arr[i]
    buckets = [0] * (max_value + 1)
    for i in arr:
        buckets[i] += 1
    i = 0
    for j in range(max_value + 1):
        for a in range(buckets[j]):
            arr[i] = j
            i += 1

    return arr
