def merge_sort(arr):
    if len(arr) > 1:
        a = len(arr)//2
        left = arr[:a]
        right = arr[a:]

        merge_sort(left)
        merge_sort(right)

        b = c = d = 0
        while b < len(left) and c < len(right):
            if left[b] < right[c]:
                arr[d] = left[b]
                b += 1
            else:
                arr[d] = right[c]
                c += 1
            d += 1

        while b < len(left):
            arr[d] = left[b]
            b += 1
            d += 1

        while c < len(right):
            arr[d] = right[c]
            c += 1
            d += 1
