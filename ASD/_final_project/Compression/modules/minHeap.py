heapsize = 0


def min_heapify(arr, i):
    global heapsize
    left = 2 * i + 1
    right = 2 * i + 2
    if left <= heapsize and arr[left] < arr[i]:
        smallest = left
    else:
        smallest = i
    if right <= heapsize and arr[right] < arr[smallest]:
        smallest = right
    if smallest != i:
        arr[i], arr[smallest] = arr[smallest], arr[i]
        min_heapify(arr, smallest)


def build_min_heap(arr):
    global heapsize
    heapsize = len(arr) - 1
    n = (len(arr) - 1) // 2
    for i in range(n, -1, -1):
        min_heapify(arr, i)


def heap_dequeue(arr):
    global heapsize
    if heapsize < 1:
        return arr.pop()
    smallest, arr[0] = arr[0], arr.pop()
    heapsize -= 1
    min_heapify(arr, 0)
    return smallest


def heap_enqueue(arr, key):
    arr.append(key)
    build_min_heap(arr)
