from modules.minHeap import heap_enqueue, heap_dequeue


def createTree(queue):
    while len(queue) > 1:
        t1 = heap_dequeue(queue)
        t2 = heap_dequeue(queue)
        if t1[0] == t2[0] and not t1[1] == "":
            t1, t2 = t2, t1
        heap_enqueue(queue, (t1[0] + t2[0], "", t1, t2))
    return heap_dequeue(queue)

