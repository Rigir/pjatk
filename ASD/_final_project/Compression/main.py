import os

from modules.minHeap import heap_enqueue
from modules.binaryTree import createTree


def saveToFile(file_name, string):
    with open(file_name, "wb") as file:
        file.write(bytearray(int(string[x:x+8], 2) for x in range(0, len(string), 8)))


def readFromFile(file_name):
    with open(file_name, "r") as file:
        return file.read()


def countCharacters(text):
    occurrences = {}
    for char in text:
        if char in occurrences.keys():
            occurrences[char] += 1
        else:
            occurrences[char] = 1
    return occurrences


def createPriorityQueue(dictionery):
    queue = []
    for c in dictionery.keys():
        heap_enqueue(queue, (dictionery.get(c), c))
    return queue


def encodeValues(n, code, txt):
    global encodeValuesCode
    if n[1] != "":
        encodeValuesCode += code
        print((n[1], code))
        txt = txt.replace(n[1], code)
        return txt
    txt = encodeValues(n[2], code + "0", txt)
    txt = encodeValues(n[3], code + "1", txt)
    return txt

if __name__ == '__main__':
    encodeValuesCode = ""
    raw_data = readFromFile("resources/input.txt")
    queue_of_data = createPriorityQueue(countCharacters(raw_data))
    print(queue_of_data)
    root = createTree(queue_of_data)
    print(root)
    saveToFile("resources/output.bin", encodeValues(root, "", encodeValuesCode + raw_data))
    print(f'Input:     \t{os.path.getsize("resources/input.txt")} bytes')
    print(f'Output:    \t{os.path.getsize("resources/output.bin")} bytes')


