def lcs_length(X, Y):
    m, n = len(X) + 1, len(Y) + 1
    c = [[0 for _ in range(n)] for _ in range(m)]
    b = [['' for _ in range(n)] for _ in range(m)]
    for i in range(1, m):
        for j in range(1, n):
            if X[i-1] == Y[j-1]:
                c[i][j] = c[i-1][j-1]+1
                b[i][j] = '↖'
            elif c[i-1][j] >= c[i][j-1]:
                c[i][j] = c[i-1][j]
                b[i][j] = '↑'
            else:
                c[i][j] = c[i][j-1]
                b[i][j] = '←'
    return c, b


if __name__ == "__main__":
    x = [1, 0, 0, 1, 0, 1, 0, 1]
    y = [0, 1, 0, 1, 1, 0, 1, 1, 0]

    print(f'LCS_length:  {lcs_length(x, y)}')

"""
The task is to implement pseudocode.
Symbols: ≠ ⟷ ≤ ≥

LCS-LENGTH(X, Y)
    m = X.length
    n = Y.length
    for i = 1 to m
        do c[i, 0] = 0
    for j = 0 to n 
        do c[0, j] = 0
    for i = 1 to m 
        do for j =1 to n
            do if x[i] = y[j]
                then c[i,j] = c[i-1, j-1]+1
                     b[i,j] = "↖"
            else if c[i-1, j] ≥ c[i, j-1]
                then c[i,j] = c[i-1, j]
                     b[i,j] = "↑"
            else 
                then c[i,j] = c[i,j-1]
                     b[i,j] = "←"
    return c i b
"""
