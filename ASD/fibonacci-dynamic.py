def fibonacci_dynamic(n):
    f = [0, 1]
    for i in range(2, n + 1):
        f.append(f[i - 1] + f[i - 2])
    return f[n]


if __name__ == "__main__":
    print(fibonacci_dynamic(9))

"""
The task is to implement pseudocode.
Symbols: ≠ ⟷ ≤ ≥

FIBONACCI_DYNAMIC:
    f = [0, 1]
    for i = 2 to n + 1:
        f.append(f[i - 1] + f[i - 2])
    return f[n]
"""
