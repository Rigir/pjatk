package car;

import client.Client;
import client.ClientRepository;
import exception.PaymentException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import payment.Payment;
import payment.PaymentRepository;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    @Mock
    private CarCreator carCreator;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private PaymentRepository paymentRepository;

    @Test
    public void registerClientCarShouldAddCarToClientAndReturnClient(){
        String make = "bmw";
        String color = "black";
        String plate = "WF Joker";
        Car car = new Car();
        car.setMake(make);
        car.setColor(color);
        car.setPlate(plate);

        String clientId = "1";
        Client client = new Client(clientId, true, null);

        when(carCreator.createCar(make,color,plate)).thenReturn(car);
        when(clientRepository.findClient(clientId)).thenReturn(client);

        CarService carService = new CarService(carCreator, clientRepository, paymentRepository);
        Client returnedClient = carService.registerClientCar(clientId, make, color, plate);

        verify(clientRepository, times(1)).saveClient(client);

        Assert.assertEquals(clientId, returnedClient.getId());
        Assert.assertEquals(make, returnedClient.getCar().getMake());
        Assert.assertEquals(color, returnedClient.getCar().getColor());
        Assert.assertEquals(plate, returnedClient.getCar().getPlate());
    }

    @Test
    public void registerClientCarShouldNotAddCarToClientWhenCarIsNull(){
        String make = "bmw";
        String color = "black";
        String plate = "WF Joker";
        Car car = new Car();
        car.setMake(make);
        car.setColor(color);
        car.setPlate(plate);

        String clientId = "1";
        Client client = new Client(clientId, true, null);

        when(carCreator.createCar(make,color,plate)).thenReturn(null);
        when(clientRepository.findClient(clientId)).thenReturn(client);

        CarService carService = new CarService(carCreator, clientRepository, paymentRepository);
        Client returnedClient = carService.registerClientCar(clientId, make, color, plate);

        verify(clientRepository, times(0)).saveClient(client);

        Assert.assertEquals(clientId,returnedClient.getId());
        Assert.assertNull(returnedClient.getCar());
    }

    @Test
    public void savePaymentShouldThrowPaymentExceptionWhenAmountIsZero(){
        int paymentAmount = 0;
        Payment payment = new Payment();
        payment.setAmount(paymentAmount);

        CarService carService = new CarService(carCreator, clientRepository, paymentRepository);
        try {
            carService.savePayment(payment);
            Assert.fail();
        }catch (PaymentException e){
            Assert.assertEquals(new PaymentException().getCause(), e.getCause());
        }
    }

    @Test
    public void savePaymentShouldSavePaymentToTheClient(){
        int paymentAmount = 1;
        Payment payment = new Payment();
        payment.setAmount(paymentAmount);

        CarService carService = new CarService(carCreator, clientRepository, paymentRepository);
        carService.savePayment(payment);

        verify(paymentRepository, times(1)).savePayment(payment);
    }
}
