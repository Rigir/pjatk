package client;

import exception.ClientAlreadyInactiveException;
import exception.ClientNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientService clientService;

    @Test
    public void disableClientShouldReturnClientWhenAccountExistAndIsNotActive(){
        String clientId = "1";
        Client client = new Client(clientId, true, null);
        when(clientRepository.findClient(clientId)).thenReturn(client);

        clientService.disableClientAccount(clientId);
        Assert.assertFalse(client.isActive());
        verify(clientRepository).saveClient(client);
    }

    @Test
    public void disableClientAccountShouldThrowErrorWhenClientsAlreadyInactive(){
        String clientId = "1";
        Client client = new Client(clientId, false, null);
        when(clientRepository.findClient(clientId)).thenReturn(client);

        try {
            clientService.disableClientAccount(clientId);
            Assert.fail();
        }catch (ClientAlreadyInactiveException e){
            Assert.assertEquals(new ClientAlreadyInactiveException().getCause(), e.getCause());
        }
        verify(clientRepository).findClient(clientId);
    }

    @Test
    public void disableClientAccountShouldThrowErrorWhenClientCanNotBeFound(){
        String clientId = "1";
        when(clientRepository.findClient(clientId)).thenReturn(null);

        try {
            clientService.disableClientAccount(clientId);
            Assert.fail();
        }catch (ClientNotFoundException e){
            Assert.assertEquals(new ClientNotFoundException().getCause(), e.getCause());
        }
        verify(clientRepository).findClient(clientId);
    }

    @Test
    public void updateClientAccountIdShouldThrowErrorWhenClientIsNotFound(){
        String clientId = "1";
        String newClientId = "2";
        when(clientRepository.findClient(clientId)).thenReturn(null);

        try {
            clientService.updateClientAccountId(clientId, newClientId);
            Assert.fail();
        }catch (ClientNotFoundException e){
            Assert.assertEquals(new ClientNotFoundException().getCause(), e.getCause());
        }
        verify(clientRepository).findClient(clientId);
    }

    @Test
    public void updateClientAccountIdShouldCreateClientByDeletingTheOldReacord(){
        String clientId = "1";
        String newClientId = "2";
        Client client = new Client(clientId, true, null);
        when(clientRepository.findClient(clientId)).thenReturn(client);

        clientService.updateClientAccountId(clientId, newClientId);
        Assert.assertEquals(newClientId, client.getId());
        verify(clientRepository).findClient(clientId);
        verify(clientRepository).deleteClient(clientId);
        verify(clientRepository).saveClient(client);
    }
}
