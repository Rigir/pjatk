package com.example.LAB05.Spring.service;

import com.example.LAB05.Spring.Car;
import com.example.LAB05.Spring.repository.CarRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CarService {
    private CarRepository carRepository;

    public CarService(CarRepository carRepository){
        this.carRepository = carRepository;

        carRepository.save(new Car("fiat",2));
        carRepository.save(new Car("bmw",2));
        carRepository.save(new Car("audi",3));
        carRepository.save(new Car("kia",6));
    }

    public Car getCar(Integer id){
        Car car = carRepository.findById(id);
        if (car == null) throw new CarNotFoundException();
        return car;
    }

    public List<Car> getDatabase() {
        return carRepository.findAll();
    }

    public void addCar(Car car) {
        carRepository.save(car);
    }

    public void deleteCar(Car car) {
        carRepository.delete(car);
    }

    public void updateCarName(Long id, String name) {
        Car car = this.getCar(Math.toIntExact(id));
        car.setName(name);
        carRepository.save(car);
    }
}
