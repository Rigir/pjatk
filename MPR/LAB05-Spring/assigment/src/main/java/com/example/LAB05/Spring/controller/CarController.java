package com.example.LAB05.Spring.controller;

import com.example.LAB05.Spring.Car;
import com.example.LAB05.Spring.service.CarNotFoundException;
import com.example.LAB05.Spring.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    private CarService carService;

    @Autowired
    public CarController(CarService carService){
        this.carService = carService;
    }

    @GetMapping("/car")
    public List<Car> getCar(){
        return carService.getDatabase();
    }

    @GetMapping("/car/{id}")
    public ResponseEntity<Car> getCar(@PathVariable Integer id){
        return new ResponseEntity<>(carService.getCar(id), HttpStatus.OK);
    }

    @PostMapping("/car/add/{car}")
    public ResponseEntity createCar(@RequestBody Car car){
        carService.addCar(car);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
