package com.example.LAB05.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab05SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab05SpringApplication.class, args);
	}
}