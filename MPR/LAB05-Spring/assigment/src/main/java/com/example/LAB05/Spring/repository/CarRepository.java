package com.example.LAB05.Spring.repository;

import com.example.LAB05.Spring.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

//JDBS
@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
     Car findById(Integer id);
     List<Car> findAll();
     @Override
     void delete(Car entity);
}
