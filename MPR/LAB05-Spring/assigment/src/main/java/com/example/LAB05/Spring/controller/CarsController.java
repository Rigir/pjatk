package com.example.LAB05.Spring.controller;

import com.example.LAB05.Spring.Car;
import com.example.LAB05.Spring.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/cars")
public class CarsController {
    CarService carService;

    @Autowired
    public CarsController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public String displayCars(Model model){
        List<Car> cars = carService.getDatabase();
        model.addAttribute("cars", cars);
        return "cars";
    }

    @GetMapping("/add")
    public String displayAddCarForm(Model model){
        model.addAttribute("car", new Car());
        return "addForm";
    }

    @PostMapping("/add")
    public String submitAddCarForm(@ModelAttribute Car car){
        carService.addCar(car);
        return "redirect:/cars";
    }

    @GetMapping("/del")
    public String displayDeleteCarForm(Model model){
        model.addAttribute("car", new Car());
        return "deleteForm";
    }

    @PostMapping("/del")
    public String deleteCarForm(@ModelAttribute Car car){
        carService.deleteCar(car);
        return "redirect:/cars";
    }

    @GetMapping("/upd")
    public String displayUpdateCarForm(Model model){
        model.addAttribute("car", new Car());
        return "updateForm";
    }

    @PostMapping("/upd")
    public String updateCarForm(@ModelAttribute Car car){
        carService.updateCarName(car.getId(), car.getName());
        return "redirect:/cars";
    }
}

