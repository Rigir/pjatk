package com.example.LAB05.Spring.controller;

import com.example.LAB05.Spring.model.Car;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/cars")
public class CarsController {
    final String API_URL = "http://localhost:8081";
    RestTemplate restTemplate = new RestTemplate();
    WebClient webClient = WebClient.create(API_URL);


    @GetMapping
    public String displayCars(Model model){
//        Car[] cars = restTemplate.getForObject(API_URL + "/cars", Car[].class);
        Car[] cars = webClient.get()
                .uri("/car")
                .retrieve()
                .bodyToMono(Car[].class)
                .block(); // <-- synchronization

        model.addAttribute("cars", Arrays.asList(cars));
        return "cars";
    }

    @GetMapping("/add")
    public String displayAddCarForm(Model model){
        model.addAttribute("car", new Car());
        return "addForm";
    }

    @PostMapping("/add")
    public String submitAddCarForm(@ModelAttribute Car car){
//        Long carId = restTemplate.postForObject(API_URL + "/car", car, Long.class);
        Mono<Long> carId = webClient.post()
                .uri("/car/add")
                .body(Mono.just(car), Car.class)
                .retrieve()
                .bodyToMono(Long.class);

        carId.subscribe(System.out::println);
        return "redirect:/cars";
    }


    @GetMapping("/del")
    public String displayDeleteCarForm(Model model){
        model.addAttribute("car", new Car());
        return "deleteForm";
    }

    @PostMapping("/del")
    public String deleteCarForm(@ModelAttribute Long id){
        Mono<Long> carId = webClient.delete()
                .uri("/car/del/" + id)
                .retrieve()
                .bodyToMono(Long.class);
        return "redirect:/cars";
    }

    @GetMapping("/upd")
    public String displayUpdateCarForm(Model model){
        model.addAttribute("car", new Car());
        return "updateForm";
    }

    @PostMapping("/upd")
    public String updateCarForm(@ModelAttribute Car car){

        return "redirect:/cars";
    }
}

