package com.example.LAB05.Spring.model;

import javax.persistence.*;

@Entity
public class Car {

    @Id
    private Long id;
    private String name;
    private Integer doors;

    public Car(){}

    public Car(Long id) {
        this.id = id;
    }


    public Car(Long id, String name, Integer doors) {
        this.id = id;
        this.name = name;
        this.doors = doors;
    }

    public String getName() {
        return name;
    }

    public Integer getDoors() {
        return doors;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
