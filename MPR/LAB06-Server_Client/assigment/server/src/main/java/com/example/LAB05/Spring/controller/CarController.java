package com.example.LAB05.Spring.controller;

import com.example.LAB05.Spring.model.Car;
import com.example.LAB05.Spring.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    private final CarService carService;

    @Autowired
    public CarController(CarService carService){
        this.carService = carService;
    }

    @GetMapping("/all")
    public List<Car> getCars(){
        return carService.getDatabase();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Car> getCar(@PathVariable Integer id){
        return new ResponseEntity<>(carService.getCar(id), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<?> createCar(@RequestBody Car car){
        carService.addCar(car);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/del/{id}")
    public ResponseEntity<?> deleteCar(@PathVariable Long id){
        carService.deleteCar(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/upd")
    public ResponseEntity<?> updateCar(@RequestBody Car car){
        carService.updateCarName(car.getId(), car.getName());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
