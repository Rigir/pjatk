package pl.pjatk.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.pjatk.unit_tests.Calculator;

public class CalculatorTest {
    @Test
    public void fiveToPowerOfThreeEquals() {
        Assert.assertEquals(125, Calculator.power(5, 3));
    }

    @Test
    public void negativesNumGivesPositiveResult() {
        Assert.assertEquals(25, Calculator.power(-5, 2));
    }

    @Test
    public void isNotDivByZero() {
        Assert.assertFalse(Calculator.isDivisible(4, 0));
    }

    @Test
    public void fourIsDivByTwo() {
        Assert.assertTrue(Calculator.isDivisible(4, 2));
    }

    @Test
    public void isGDWorking() {
        Assert.assertEquals(10, Calculator.findGCD(20, 30));
    }

    @Test
    public void whenZeroIsOneOfParamInGDC() {
        Assert.assertEquals(42, Calculator.findGCD(42, 0));
    }

    // BONUS
    private Calculator calculator;

    @Before
    public void createCalculator() {
        calculator = new Calculator();
    }

    @Test
    public void multiTwoByTwo() {
        Assert.assertEquals(4, calculator.multi(2, 2));
    }

    @Test
    public void multiByZero() {
        Assert.assertEquals(0, calculator.multi(2, 0));
    }

    @Test
    public void multiByNegativeNum() {
        Assert.assertEquals(-4, calculator.multi(2, -2));
    }

    @Test
    public void multiByTwoNegativesNum() {
        Assert.assertEquals(4, calculator.multi(-2, -2));
    }

}
