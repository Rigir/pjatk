package pl.pjatk.test;

import org.junit.Assert;
import org.junit.Test;
import pl.pjatk.unit_tests.StringUtils;

public class StringUtilsTest {
    @Test
    public void toUpperCaseWorks() {
        Assert.assertEquals("ALA MA KOTA", StringUtils.toUpperCase("ala ma kota"));
    }

    @Test
    public void toUpperCaseWithNumAndSpecialChar() {
        Assert.assertEquals("1231A24@!@21", StringUtils.toUpperCase("1231a24@!@21"));
    }

    @Test
    public void reverseStringWorks() {
        Assert.assertEquals("ćawomargorp eibul", StringUtils.reverseString("lubie programować"));
    }

    @Test
    public void reverseStringWithNumAndSpecialChar() {
        Assert.assertEquals(">pS a%@321", StringUtils.reverseString("123@%a Sp>"));
    }

    @Test
    public void multiplyStringWorks() {
        Assert.assertEquals("test test test ", StringUtils.multiplyString("test", 3));
    }

    @Test
    public void multiplyStringWithZeroAsParam() {
        Assert.assertEquals("", StringUtils.multiplyString("test", 0));
    }

    @Test
    public void removeSmallCharsWorks() {
        Assert.assertEquals("J K", StringUtils.removeSmallChars("Jan Kowalski"));
    }

    @Test
    public void removeSmallWithNumAndSpecialChar() {
        Assert.assertEquals(">S %@321", StringUtils.removeSmallChars(">pS a%@321"));
    }

    @Test
    public void removeBigCharsWorks() {
        Assert.assertEquals("an owalski", StringUtils.removeBigChars("Jan Kowalski"));
    }

    @Test
    public void removeBigCharsWithNumAndSpecialChar() {
        Assert.assertEquals(">p a%@321", StringUtils.removeBigChars(">pS a%@321"));
    }

    @Test
    public void removeAllButLettersWorks() {
        Assert.assertEquals("Test", StringUtils.removeAllButLetters("123Test!"));
    }

    @Test
    public void removeAllButLettersWithNumAndSpecialChar() {
        Assert.assertEquals("", StringUtils.removeAllButLetters(">%@321"));
    }
}
