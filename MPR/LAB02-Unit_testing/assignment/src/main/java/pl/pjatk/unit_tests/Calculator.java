package pl.pjatk.unit_tests;

public class Calculator {
    static public int power(int x, int y){
        int result = 1;
        for (int i = 0; i < y; i++) {
            result *= x;
        }
        return result;
    }

    static public boolean isDivisible(int num, int div){
        if(div == 0) return false;
        return num % div == 0;
    }

    static public int findGCD(int a, int b){
        if (b == 0)
            return a;
        return findGCD(b, a % b);
    }

    public int multi(int a, int b) {
        if (a == 0 || b == 0) {
            return 0;
        } else if (b > 0) {
            return a + multi(a, b - 1);
        }
        return -multi(a, -b);
    }
}
