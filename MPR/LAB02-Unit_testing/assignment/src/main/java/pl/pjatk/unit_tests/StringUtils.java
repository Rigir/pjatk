package pl.pjatk.unit_tests;

public class StringUtils {
    static public String toUpperCase(String str){
        return str.toUpperCase();
    }
    static public String reverseString(String str){
        StringBuffer stringBuffer = new StringBuffer(str);
        return stringBuffer.reverse().toString();
    }

    static public String multiplyString(String str, int num){
        String result = "";
        for (int i = 0; i < num; i++) {
            result += str + " ";
        }
        return result;
    }

    static public String removeSmallChars(String str){
        return str.replaceAll("[a-z]", "");
    }

    static public String removeBigChars(String str){
        return str.replaceAll("[A-Z]", "");
    }

    static public String removeAllButLetters(String str){
        return str.replaceAll("\\P{L}", "");
    }
}
