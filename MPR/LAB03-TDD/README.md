![PJATK](../../.assets/banner.png)
=========

![instrucion_1](resources/instructions.png)

BVA_ZADANIE:
- Za każde jednorazowo wydane 40 złotych sklep „Dobre wino” wydaje kupon upoważniający do odbioru darmowego wina. 
  Maksymalna liczba kuponów jakie przy pojedynczej transakcji może otrzymać klient wynosi 3 sztuk.
  1. Przygotuj klasy równoważności dla pojedynczej transakcji
  2. Opracuj przypadki testowe
  3. Utwórz metody testowe a następnie zaimplementuj algorytm
  4. Sklep wprowadził zniżki – 5% przy zakupie powyżej 50 zł oraz -10% przy zakupie powyżej 100zł a dodaj odpowiednie testy a nastepnie zaktualizuj kod
  5. ** Dodatkowy upust 3% dla stałych klientów niezależnie od kwoty zakupu (wymaganie: posiadanie co najmniej 10 kuponów) – upust sumuję się z pozostałymi zniżkami np. stały klient robiąc zakupy za 100 zł zapłaci tylko 87zł (100 – (10%+3%) = 87%)