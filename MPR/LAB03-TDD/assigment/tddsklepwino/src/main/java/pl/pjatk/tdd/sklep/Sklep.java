package pl.pjatk.tdd.sklep;

public class Sklep {
    public static void dokonajZakupu(KartaKlienta karta, double kwotaZakupu) {
        double bonusDiscount = 0;

        if (karta.pobierzIloscDostepnychKuponow() >= 10) {
            bonusDiscount = (kwotaZakupu * 0.03);
        }

        if (kwotaZakupu >= 100) {
            kwotaZakupu = kwotaZakupu - (kwotaZakupu * 0.1 + bonusDiscount);
        } else if (kwotaZakupu >= 50) {
            kwotaZakupu = kwotaZakupu - (kwotaZakupu * 0.05 + bonusDiscount);
        } else kwotaZakupu = kwotaZakupu - bonusDiscount;

        if (kwotaZakupu >= 40.00 && kwotaZakupu < 80) {
            karta.dodajKupony(1);
        } else if (kwotaZakupu >= 80.00 && kwotaZakupu < 120) {
            karta.dodajKupony(2);
        } else if (kwotaZakupu >= 120) {
            karta.dodajKupony(3);
        }
    }

    public static KartaKlienta wydajNowaKarte() {
        return new KartaKlienta();
    }
}
