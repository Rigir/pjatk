package pl.pjatk.tdd.test.sklep;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.pjatk.tdd.sklep.KartaKlienta;
import pl.pjatk.tdd.sklep.Sklep;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SklepTestDlaStalegoKlienta {

    @Parameterized.Parameter(value = 0)
    public double kwotaZakupu;

    @Parameterized.Parameter(value = 1)
    public int oczekiwanaIloscKuponow;

    private KartaKlienta karta;
    private final int liczbaKuponowDoZostaniaStalymKilientem = 10;

    @Parameterized.Parameters(name = "[{index}] Przy zakupie za kwotę {0} oczekiwana ilość kuponów powinna wynieść {1}")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][] {
                { 41.23, 0 },
                { 41.24, 1 },
                { 86.95, 1 },
                { 86.96, 2 },
                { 137.93, 2 },
                { 137.94, 3 }
        });
    }

    @Before
    public void setup() {
        karta = Sklep.wydajNowaKarte();
        karta.dodajKupony(liczbaKuponowDoZostaniaStalymKilientem);
    }

    @Test
    public void klientPowinienDostacOdpowiedniaIloscKuponow() {
        Sklep.dokonajZakupu(karta, kwotaZakupu);
        assertEquals(oczekiwanaIloscKuponow, karta.pobierzIloscDostepnychKuponow() - liczbaKuponowDoZostaniaStalymKilientem);
    }
}
