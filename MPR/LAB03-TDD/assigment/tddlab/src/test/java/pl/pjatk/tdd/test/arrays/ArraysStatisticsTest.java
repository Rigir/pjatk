package pl.pjatk.tdd.test.arrays;

import org.junit.Assert;
import org.junit.Test;
import pl.pjatk.tdd.arrays.ArraysStatistics;

public class ArraysStatisticsTest {
    @Test
    public void maximumShouldBePositiveNumber(){
        Assert.assertEquals(1, ArraysStatistics.max(new int[]{-10,-5,1,-2,-13}));
    }

    @Test
    public void maximumShouldBeNegativeNumber() {
         Assert.assertEquals(-13, ArraysStatistics.max(new int[]{-90, -100, -14, -13, -888}));
     }

    @Test
    public void minimumShouldBePositiveNumber(){
        Assert.assertEquals(1, ArraysStatistics.min(new int[]{10,5,1,2,13}));
    }

    @Test
    public void minimumShouldBeNegativeNumber() {
        Assert.assertEquals(-888, ArraysStatistics.min(new int[]{-90, -100, -14, -13, -888}));
    }

    @Test
    public void avgShouldBePositiveNumber(){
        Assert.assertEquals( 6.2, ArraysStatistics.avg(new int[]{10,5,1,2,13}), 0.01);
    }

    @Test
    public void avgShouldBeNegativeNumber() {
        Assert.assertEquals(-221, ArraysStatistics.avg(new int[]{-90, -100, -14, -13, -888}), 0.01);
    }

    @Test
    public void sumShouldBePositiveNumber(){
        Assert.assertEquals( 31, ArraysStatistics.sum(new int[]{10,5,1,2,13}));
    }

    @Test
    public void sumShouldBeNegativeNumber() {
        Assert.assertEquals(-1105, ArraysStatistics.sum(new int[]{-90, -100, -14, -13, -888}));
    }
}
