package pl.pjatk.tdd.test.strings;

import org.junit.Assert;
import org.junit.Test;
import pl.pjatk.tdd.strings.StringUtils;

public class StringUtilsTest {
    @Test
    public void sumOfNumbersInStringShouldBePositive(){
        Assert.assertEquals(7, StringUtils.sumStringNumbers("52"));
    }

    @Test
    public void sumOfNegativeNumberInStringShouldBePositive(){
        Assert.assertEquals(9, StringUtils.sumStringNumbers("-36"));
    }

    @Test
    public void palindromeFromOneWord(){
        Assert.assertTrue(StringUtils.isPalindrome("aga"));
    }

    @Test
    public void palindromeFromOneSentence(){
        Assert.assertTrue(StringUtils.isPalindrome("A kilku tu klika"));
    }
}
