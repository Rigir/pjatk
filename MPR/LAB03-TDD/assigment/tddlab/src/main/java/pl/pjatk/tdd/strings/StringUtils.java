package pl.pjatk.tdd.strings;

public class StringUtils {
    public static int sumStringNumbers(String str) {
        int result = 0;
        for (char i : str.toCharArray()) {
            if (i != '-')
                result += Character.getNumericValue(i);
        }
        return result;
    }

    public static boolean isPalindrome(String str) {
        str = str.toLowerCase().replaceAll("\\s+", "");
        int i = 0, j = str.length() - 1;
        while (i < j) {
            if (str.charAt(i) != str.charAt(j))
                return false;
            i++;
            j--;
        }
        return true;
    }
}
