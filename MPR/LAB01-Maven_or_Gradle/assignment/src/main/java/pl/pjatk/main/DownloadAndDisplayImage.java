package pl.pjatk.main;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

/*
    URLs I used to test application.
    You can paste as many URLs as you want, but all of them must end with image format.
    https://www.w3schools.com/html/pic_trulli.jpg
    https://www.w3schools.com/html/img_chania.jpg
    https://www.w3schools.com/css/img_5terre.jpg
    https://www.w3schools.com/css/img_mountains.jpg
*/

public class DownloadAndDisplayImage {
    private static ImagePlus imp;
    private static final String imagesPath = "images/";

    public static void main(String[] args) {
        ArrayList<URL> urls = new ArrayList<>();

        for (String arg: args) {
            try{
                urls.add(new URL(arg));
            } catch (MalformedURLException e){
                e.printStackTrace();
            }
        }

        for (URL url: urls) {
            downloadFromURL(url);
        }

        for (URL url: urls) {
            showImage(url);
            wait(5000);
        }

        IJ.run("Close");
    }

    public static void downloadFromURL(URL imageURL) {
        try {
            String filePath = imagesPath + FilenameUtils.getName(imageURL.getPath());
            File destination_file = new File(filePath);
            FileUtils.copyURLToFile(imageURL, destination_file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showImage(URL imageURL){
        String imageFilePath = imagesPath + FilenameUtils.getName(imageURL.getPath());
        if ( imp == null) {
            imp = IJ.openImage(imageFilePath);
        }
        else {
            imp.setImage(IJ.openImage(imageFilePath));
        }
        addBlurToImage();
        imp.show();
    }

    public static void addBlurToImage(){
        ImageProcessor improc = imp.getProcessor().duplicate();
        improc.blurGaussian(5);
        imp.setImage(new ImagePlus("Result - Blurred", improc));
    }

    public static void wait(int ms)
    {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}