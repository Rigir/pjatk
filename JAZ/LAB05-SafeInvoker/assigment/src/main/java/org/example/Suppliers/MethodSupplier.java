package org.example.Suppliers;

@FunctionalInterface
public interface MethodSupplier {
    void execute() throws Exception;
}
