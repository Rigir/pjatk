package org.example.caching.loaders;

public interface CacheLoader {
    void load();
}
