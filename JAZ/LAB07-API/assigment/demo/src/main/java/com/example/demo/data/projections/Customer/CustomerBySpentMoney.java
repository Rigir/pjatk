package com.example.demo.data.projections;

import java.math.BigDecimal;

public interface CustomerBySpentMoney {
    CustomerInfo getCustomer();
    BigDecimal getSpent();
}
