package com.example.demo.business.converters;

import com.example.demo.business.parsers.ChartType;
import org.springframework.core.convert.converter.Converter;

public class StringToEnumConverter implements Converter<String, ChartType> {
    @Override
    public ChartType convert(String source) {
        return ChartType.valueOf(source.toUpperCase());
    }
}