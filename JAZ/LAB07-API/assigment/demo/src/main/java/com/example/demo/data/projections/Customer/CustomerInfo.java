package com.example.demo.data.projections;

public interface CustomerInfo {
    int getCustomerId();
    String getFirstName();
    String getLastName();
}
