package com.pjwstk.client.web;

import com.pjwstk.data.DTOs.DataRecord;
import com.pjwstk.data.DTOs.SearchParameters;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Controller
@RequestMapping("/client")
public class ClientController {
    private final String API_URL = "http://localhost:8082/server";
    private final WebClient webClient = WebClient.create(API_URL);

    @RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
    public String dataSubmit(Model model, @ModelAttribute SearchParameters searchParameters){
        System.out.println(searchParameters);
        List<DataRecord> records = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/data")
                        .queryParam("page", searchParameters.getPage())
                        .queryParam("location", searchParameters.getLocation())
                        .queryParam("dateFrom", searchParameters.getDateFrom())
                        .queryParam("dateTo", searchParameters.getDateTo())
                        .build())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<DataRecord>>(){})
                .block();

        model.addAttribute("records", records);
        model.addAttribute("searchParameters", searchParameters);
        return "index";
    }
}

