package com.pjwstk.server.business;

import com.pjwstk.data.DTOs.DataRecord;
import com.pjwstk.data.DTOs.SearchParameters;
import com.pjwstk.data.projections.ValuesByDateAndLocation;
import com.pjwstk.data.repositories.LocationRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class WeatherApiServices {
    public final LocationRepository locationRepository;
    public ModelMapper modelMapper;

    public List<DataRecord> getDataOnPage(SearchParameters searchParameters){
        List<ValuesByDateAndLocation> values = locationRepository.getDataByDateAndLocation(PageRequest.of(searchParameters.getPage() , 10));
        return  values.stream()
                .map(value -> modelMapper.map(value, DataRecord.class))
                .filter(value -> value.filterOutLocation(searchParameters.getLocation()))
                .filter(value -> value.filterOutDataFrom(searchParameters.getDateFrom()))
                .filter(value -> value.filterOutDataTo(searchParameters.getDateTo()))
                .toList();
    }
}
