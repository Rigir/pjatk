package com.pjwstk.server.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pjwstk.data.DTOs.SearchParameters;
import com.pjwstk.server.business.WeatherApiServices;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;

@Controller
@AllArgsConstructor
@RequestMapping("/server")
public class WeatherApiController {
    private final WeatherApiServices weatherApiServices;
    private ModelMapper modelMapper;

    @GetMapping("/data")
    public ResponseEntity<?> getAllData(
            @RequestParam(required = false, defaultValue = "1") Integer page,
            @RequestParam(required = false) String location,
            @RequestParam(required = false) String dateFrom,
            @RequestParam(required = false) String dateTo
    ){

        return ResponseEntity.ok(weatherApiServices.getDataOnPage(new SearchParameters(page, location, dateFrom, dateTo)));
    }
}
