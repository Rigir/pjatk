package com.pjwstk.scheduler.business;

import com.pjwstk.scheduler.data.info.TimerInfo;
import com.pjwstk.scheduler.data.jobs.UpdateWeatherApiJob;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaygroundService {
    private final SchedulerServices scheduler;

    public void schedulerTimers(){
        scheduler.schedule(UpdateWeatherApiJob.class, TimerInfo.builder()
                .runForever(true)
                .repeatIntervalMs(30000)
                .initialOffsetMs(1000)
                .build());
    }

    public List<TimerInfo> getAllRunningTimers(){
        return scheduler.getAllRunningTimers();
    }

    public Boolean deleteTimer(final String timerId) {
        return scheduler.deleteTimer(timerId);
    }
}
