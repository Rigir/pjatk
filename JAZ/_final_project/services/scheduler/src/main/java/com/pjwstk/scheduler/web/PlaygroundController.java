package com.pjwstk.scheduler.web;

import com.pjwstk.scheduler.business.PlaygroundService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/scheduler")
public class PlaygroundController {
    private PlaygroundService service;

    @PostMapping("/start")
    public void startServices(){
        service.schedulerTimers();
    }

    @GetMapping("/running")
    public ResponseEntity<?> getAllRunningTimers(){
        return ResponseEntity.ok(service.getAllRunningTimers());
    }

    @DeleteMapping("/{timerId}")
    public ResponseEntity<?> deleteTimer(@PathVariable String timerId) {
        return ResponseEntity.ok(service.deleteTimer(timerId));
    }
}

