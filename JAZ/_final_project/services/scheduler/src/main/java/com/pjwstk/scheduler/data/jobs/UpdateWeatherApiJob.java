package com.pjwstk.scheduler.data.jobs;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class UpdateWeatherApiJob implements Job {
    private final String API_URL = "http://localhost:8084/updater";
    private final WebClient webClient = WebClient.create(API_URL);

    @Override
    public void execute(JobExecutionContext context) {
        webClient.get().uri("/reload").retrieve().toBodilessEntity().block();
        log.info("'{}'", context.getJobDetail().getJobClass());
    }
}
