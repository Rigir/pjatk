package com.pjwstk.data.repositories;

import com.pjwstk.data.model.Location;

import com.pjwstk.data.projections.ValuesByDateAndLocation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    @Query("SELECT DISTINCT l FROM Location l")
    List<Location> getAllLocations();

    @Query("SELECT l.name as name, h.date AS date, h.value AS humidity, p.value AS pressure " +
            "FROM Location l, Humidity h, Pressure p " +
            "WHERE l.id=h.location.id AND h.date=p.date " +
            "GROUP BY h.location.id, h.date, h.value, p.value")
    List<ValuesByDateAndLocation> getDataByDateAndLocation(Pageable pageable);

    Location findByName(String name);
}