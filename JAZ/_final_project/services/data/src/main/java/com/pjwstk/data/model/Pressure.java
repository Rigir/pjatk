package com.pjwstk.data.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
public class Pressure {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    private Float value;

    @Column
    private Timestamp date;

    @ManyToOne
    @JoinColumn(name = "location_id")
    Location location;
}
