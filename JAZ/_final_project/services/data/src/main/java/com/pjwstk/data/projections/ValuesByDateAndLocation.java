package com.pjwstk.data.projections;

import java.util.Date;

public interface ValuesByDateAndLocation {
    String getName();
    Date getDate();
    Long getHumidity();
    Long getPressure();
}
