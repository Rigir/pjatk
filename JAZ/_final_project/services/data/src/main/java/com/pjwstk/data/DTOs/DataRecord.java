package com.pjwstk.data.DTOs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.pjwstk.data.projections.ValuesByDateAndLocation;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.beans.ConstructorProperties;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DataRecord {
    private String name;
    private Date date;
    private Long humidity;
    private Long pressure;

    @JsonCreator
    @ConstructorProperties({"name", "date", "humidity", "pressure"})
    public DataRecord(String name, Date date, Long humidity, Long pressure) {
        this.name = name;
        this.date = date;
        this.humidity = humidity;
        this.pressure = pressure;
    }

    public Boolean filterOutLocation(String location){
        if (!empty(location))
            return name.matches(String.format("(?i).*%s.*", location));
        return true;
    }

    public boolean filterOutDataFrom(String dateFrom) {
        if (!empty(dateFrom))
            return date.after(purseStringToDate(dateFrom));
        return true;
    }

    public boolean filterOutDataTo(String dateTo) {
        if (!empty(dateTo))
            return date.before(purseStringToDate(dateTo));
        return true;
    }

    private Date purseStringToDate(String date){
        return Date.from(LocalDate.parse(date).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private boolean empty( final String s ) {
        return s == null || s.trim().isEmpty();
    }
}
