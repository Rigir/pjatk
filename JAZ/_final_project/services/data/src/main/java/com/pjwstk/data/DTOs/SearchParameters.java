package com.pjwstk.data.DTOs;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.context.annotation.Bean;
import org.springframework.format.annotation.DateTimeFormat;

import java.beans.ConstructorProperties;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class SearchParameters {

    @JsonAlias("page")
    private Integer page;

    @JsonAlias("location")
    private String location;

    @JsonAlias("dateFrom")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private String dateFrom;

    @JsonAlias("dateTo")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private String dateTo;

    @JsonCreator
    @ConstructorProperties({"page", "location", "dateFrom", "dateTo"})
    public SearchParameters(Integer page, String location, String dateFrom, String dateTo) {
        this.page = page;
        this.location = location;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }
}
