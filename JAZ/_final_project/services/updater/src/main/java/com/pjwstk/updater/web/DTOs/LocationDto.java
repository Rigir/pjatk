package com.pjwstk.updater.web.DTOs;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;

import java.beans.ConstructorProperties;
import java.io.Serializable;

@Getter
@Setter
@Data
public class LocationDto implements Serializable {
    private final String name;

    @JsonCreator
    @ConstructorProperties({"name"})
    public LocationDto(String name) {
        this.name = name;
    }
}
