package com.pjwstk.updater.business;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class StatusItem {
    private String name;
    private Date lastUpdate;
    private Boolean isReachable;

    public boolean isEqualByName(String string){
        return this.name.equalsIgnoreCase(string);
    }

    public void update(Date lastUpdate, Boolean isReachable){
        this.lastUpdate = lastUpdate;
        this.isReachable = isReachable;
    }
}
