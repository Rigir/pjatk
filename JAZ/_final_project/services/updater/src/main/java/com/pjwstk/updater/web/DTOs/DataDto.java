package com.pjwstk.updater.web.DTOs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@Data
public class DataDto implements Serializable {
    private final Float value;
    private final Timestamp date;

    @JsonCreator
    @ConstructorProperties({"value"})
    public DataDto(Float value) {
        this.value = value;
        this.date = new Timestamp(new Date().getTime());
    }
}
