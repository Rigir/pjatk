package com.pjwstk.updater.business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pjwstk.updater.web.DTOs.WeatherApiDto;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@Component
public class Status {
    @JsonIgnore
    private static final Status instance;
    static {
        instance = new Status();
    }

    private Status() {}

    public synchronized static Status getInstance(){
        return instance;
    }

    private boolean isWorking;
    private Timestamp reloadStarted;
    private Timestamp reloadEnded;

    private List<StatusItem> items = new ArrayList<>(List.of(
            new StatusItem("192.168.1.94", null, true),
            new StatusItem("red.local", null, true)
    ));

    public synchronized void setStartStatus() {
        Status.getInstance().setWorking(true);
        this.setReloadStarted(new Timestamp(new Date().getTime()));
    }

    public synchronized void setEndStatus() {
        this.setReloadEnded(new Timestamp(new Date().getTime()));
        this.setWorking(false);
    }

    public void updateItem(StatusItem statusItem, Object object) {
        if (items.contains(statusItem))
            Objects.requireNonNull(items.stream()
                    .filter(item -> item.isEqualByName(statusItem.getName()))
                    .findFirst()
                    .orElse(null)).update(new Timestamp(new Date().getTime()), !Objects.isNull(object));
    }

}
