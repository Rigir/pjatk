package com.pjwstk.updater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
@ComponentScan("com.pjwstk")
@EnableJpaRepositories("com.pjwstk")
public class UpdaterApplication {
	public static void main(String[] args) {
		SpringApplication.run(UpdaterApplication.class, args);
	}
}
