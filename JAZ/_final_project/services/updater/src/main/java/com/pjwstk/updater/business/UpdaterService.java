package com.pjwstk.updater.business;

import com.pjwstk.data.repositories.HumidityRepository;
import com.pjwstk.data.repositories.LocationRepository;
import com.pjwstk.data.repositories.PressureRepository;
import com.pjwstk.updater.business.handlers.Handler;
import com.pjwstk.updater.business.handlers.HumidityHandler;
import com.pjwstk.updater.business.handlers.LocationHandler;
import com.pjwstk.updater.business.handlers.PressureHandler;
import com.pjwstk.updater.web.DTOs.WeatherApiDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class UpdaterService {
    private final RestTemplate rest;
    private final ModelMapper modelMapper;
    private final Status status = Status.getInstance();

    private final LocationRepository locationRepository;
    private final HumidityRepository humidityRepository;
    private final PressureRepository pressureRepository;

    public void reload(){
        status.setStartStatus();

        Handler handler = new LocationHandler(modelMapper, locationRepository);
        handler.linkWith(new HumidityHandler(modelMapper,locationRepository, humidityRepository))
                .linkWith(new PressureHandler(modelMapper, locationRepository, pressureRepository));

        handler.check(status.getItems().stream()
                .filter(StatusItem::getIsReachable)
                .map(this::getEnvData)
                .filter(Objects::nonNull)
                .toList());

        status.setEndStatus();
    }

    private WeatherApiDto getEnvData(StatusItem statusItem){
        try{
            WeatherApiDto apiDto = rest.getForObject("http://" + statusItem.getName() + "/env", WeatherApiDto.class);
            status.updateItem(statusItem, apiDto);
            return apiDto;
        } catch (Exception e){
            status.updateItem(statusItem, null);
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
