package com.pjwstk.updater.business.handlers;

import com.pjwstk.updater.web.DTOs.WeatherApiDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public abstract class Handler {
    private Handler next;

    public Handler linkWith(Handler next) {
        this.next = next;
        return next;
    }

    public abstract boolean check(List<WeatherApiDto> weatherApiDtoList);

    protected boolean checkNext(List<WeatherApiDto> weatherApiDtoList) {
        return next == null || next.check(weatherApiDtoList);
    }
}
