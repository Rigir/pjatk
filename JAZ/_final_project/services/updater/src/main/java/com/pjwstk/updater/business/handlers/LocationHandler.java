package com.pjwstk.updater.business.handlers;

import com.pjwstk.data.model.Location;
import com.pjwstk.data.repositories.LocationRepository;
import com.pjwstk.updater.web.DTOs.WeatherApiDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class LocationHandler extends Handler{
    private ModelMapper modelMapper;
    private LocationRepository locationRepository;

    @Override
    public boolean check(List<WeatherApiDto> weatherApiDtoList) {
        for (WeatherApiDto weatherApiDto : weatherApiDtoList) {
            if (weatherApiDto.getLocation() != null) {
                List<Location> locations = locationRepository.getAllLocations();
                if (locations.stream().noneMatch(location ->
                        location.getName().equalsIgnoreCase(weatherApiDto.getLocation().getName())))
                    locationRepository.save(modelMapper.map(weatherApiDto.getLocation(), Location.class));
            }
        }
        return checkNext(weatherApiDtoList);
    }

}
