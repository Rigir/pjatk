package com.pjwstk.updater.web.DTOs;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.beans.ConstructorProperties;
import java.io.Serializable;

@Getter
@Setter
@Data
public class CurrentDto implements Serializable {
    private final DataDto pressure;
    private final DataDto humidity;

    @JsonCreator
    @ConstructorProperties({"pressure", "humidity"})
    public CurrentDto(DataDto pressure, DataDto humidity) {
        this.pressure = pressure;
        this.humidity = humidity;
    }
}