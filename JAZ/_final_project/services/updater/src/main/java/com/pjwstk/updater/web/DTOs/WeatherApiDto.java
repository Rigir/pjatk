package com.pjwstk.updater.web.DTOs;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;

import java.beans.ConstructorProperties;

@Getter
@Setter
@Data
public class WeatherApiDto {
    private final LocationDto location;
    private final CurrentDto current;

    @JsonCreator
    @ConstructorProperties({"location", "current"})
    public WeatherApiDto(LocationDto location, CurrentDto current) {
        this.location = location;
        this.current = current;
    }
}
