package com.pjwstk.updater.business.handlers;

import com.pjwstk.data.model.Pressure;
import com.pjwstk.data.repositories.HumidityRepository;
import com.pjwstk.data.repositories.LocationRepository;
import com.pjwstk.data.repositories.PressureRepository;
import com.pjwstk.updater.web.DTOs.DataDto;
import com.pjwstk.updater.web.DTOs.WeatherApiDto;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class PressureHandler extends Handler {
    private ModelMapper modelMapper;
    private LocationRepository locationRepository;
    private PressureRepository pressureRepository;

    @Override
    public boolean check(List<WeatherApiDto> weatherApiDtoList) {
        for (WeatherApiDto weatherApiDto : weatherApiDtoList) {
            DataDto dataDto = weatherApiDto.getCurrent().getPressure();
            if(dataDto != null){
                Pressure pressure = modelMapper.map(dataDto, Pressure.class);
                pressure.setLocation(locationRepository.findByName(weatherApiDto.getLocation().getName()));
                pressureRepository.save(pressure);
            }
        }
        return checkNext(weatherApiDtoList);
    }
}
