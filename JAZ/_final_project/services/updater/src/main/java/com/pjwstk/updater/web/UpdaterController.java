package com.pjwstk.updater.web;

import com.pjwstk.updater.business.Status;
import com.pjwstk.updater.business.UpdaterService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@AllArgsConstructor
@RequestMapping("/updater")
public class UpdaterController {
    private UpdaterService updaterService;

    @Async
    @GetMapping(value = "/reload")
    public ResponseEntity<?> getUpdate(){
        updaterService.reload();
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/status")
    public ResponseEntity<?> getStatus(){
        return ResponseEntity.ok(Status.getInstance());
    }
}


