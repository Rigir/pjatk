package com.pjwstk.updater.business.handlers;

import com.pjwstk.data.model.Humidity;
import com.pjwstk.data.repositories.HumidityRepository;
import com.pjwstk.data.repositories.LocationRepository;
import com.pjwstk.updater.web.DTOs.DataDto;
import com.pjwstk.updater.web.DTOs.WeatherApiDto;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class HumidityHandler extends Handler {
    private ModelMapper modelMapper;
    private LocationRepository locationRepository;
    private HumidityRepository humidityRepository;

    @Override
    public boolean check(List<WeatherApiDto> weatherApiDtoList) {
        for (WeatherApiDto weatherApiDto : weatherApiDtoList) {
            DataDto dataDto = weatherApiDto.getCurrent().getHumidity();
            if (dataDto != null) {
                Humidity humidity = modelMapper.map(dataDto, Humidity.class);
                humidity.setLocation(locationRepository.findByName(weatherApiDto.getLocation().getName()));
                humidityRepository.save(humidity);
            }
        }
        return checkNext(weatherApiDtoList);
    }
}
