#include "Arduino.h"

#include <ArduinoJson.h>
#include <ESPmDNS.h>
#include <WebServer.h>
#include <WiFi.h>

#include <cstdlib>
#include <ctime>
#include <string>

#include "../include/config.h"
#include "../include/debug.h"

WebServer server(80);
char buffer[1024];

using namespace std;

void setup_routing();
void connectToWiFi();
void initializeDNS();
DynamicJsonDocument createJsonDocument(int size);
void add_json_object(DynamicJsonDocument& doc,
                     string tag,
                     float value,
                     string unit);

void setup() {
  Serial.begin(115200);
  srand(time(NULL));

  connectToWiFi();
  initializeDNS();

  setup_routing();
  server.begin();
}

void loop() {
  server.handleClient();
}

int randRange(int low, int high) {
  return rand() % (high - low + 1) + low;
}

void setup_routing() {
  server.on("/humidity", HTTP_GET, []() {
    DynamicJsonDocument doc = createJsonDocument(1024);
    add_json_object(doc, "humidity", randRange(0, 100), "%");
    serializeJson(doc, buffer);
    server.send(200, "application/json", buffer);
  });

  server.on("/pressure", HTTP_GET, []() {
    DynamicJsonDocument doc = createJsonDocument(1024);
    add_json_object(doc, "pressure", randRange(0, 100), "mBar");
    serializeJson(doc, buffer);
    server.send(200, "application/json", buffer);
  });

  server.on("/env", HTTP_GET, []() {
    DynamicJsonDocument doc = createJsonDocument(1024);
    add_json_object(doc, "pressure", randRange(0, 100), "mBar");
    add_json_object(doc, "humidity", randRange(0, 100), "%");
    serializeJson(doc, buffer);
    server.send(200, "application/json", buffer);
  });

  server.onNotFound([]() {
    server.send(404, "text/plain", "Uri not found " + server.uri());
  });
}

void connectToWiFi() {
  serial_print(F("Connecting to "));
  serial_println(F(WIFI_SSID));

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    serial_print(F("."));
    delay(500);
  }

  serial_print(F("Connected. IP: "));
  serial_println(WiFi.localIP());
}

void initializeDNS() {
  if (MDNS.begin(DNS_NAME)) {
    serial_print(F("MDNS responder started: "));
    serial_println(F(DNS_NAME));
  }
}

DynamicJsonDocument createJsonDocument(int size) {
  DynamicJsonDocument doc(size);
  JsonObject obj = doc.createNestedObject("location");
  obj["name"] = GENERAL_LOCATION_NAME;
  return doc;
}

void add_json_object(DynamicJsonDocument& doc,
                     string tag,
                     float value,
                     string unit) {
  JsonObject current;
  if (doc.getMember("current").isNull())
    current = doc.createNestedObject("current");
  else
    current = doc.getMember("current");

  JsonObject obj = current.createNestedObject(tag);
  obj["value"] = value;
  obj["unit"] = unit;
}
