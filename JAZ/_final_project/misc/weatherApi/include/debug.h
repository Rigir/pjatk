#ifndef INCLUDE_DEBUG_H_
#define INCLUDE_DEBUG_H_

/**
 * Set this to false to disable Serial logging
 */
#define DEBUG true

/**
 * How to use it:
 * serial_print(F("string"));
 * serial_println(F("string"));
 */

#if DEBUG == true
#define serial_print(x) Serial.print(x)
#define serial_println(x) Serial.println(x)
#else
#define serial_print(x)
#define serial_println(x)
#endif

#endif // INCLUDE_DEBUG_H_