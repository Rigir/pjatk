#ifndef INCLUDE_CONFIG_H_
#define INCLUDE_CONFIG_H_
// ==! You need to rename file to 'config.h' !==

/**
 * General settings
 */

#define GENERAL_LOCATION_NAME "_EXAMPLE_NAME_"

/**
 * WiFi settings
 */
#define WIFI_SSID "_EXAMPLE_SSID_"
#define WIFI_PASSWORD "_EXAMPLE_PASSWORD_"

/**
 * DNS settings
 */
#define DNS_NAME "_EXAMPLE_NAME_"

#endif  // INCLUDE_CONFIG_H_