🌡️Smart Home Weather Manager
=========

It is an application based on microservices that systematically collects data from sesors placed around the house and displays them in the user's panel

To run the project, you must first run the services on the docker side, then you must run the individual modules located in the /services folder. The code for the microcontroller is in the /misc folder. `In my next project I fix this problem and all services run in one command. The problem was with containerized maven.`

```
Project is build on Spring Web version lower then 6.0.0 and there is a discovered vulnerability
```

📚  Main features:
---------
- User dashboard 
  - U can sort pages by location or date.
  - You can navigate through the different pages of the database

🛠 Technology stack:
---------
  - Platform.io
  - Spring boot
  - Thymeleaf
  - Quartz

📄 License:
---------
PJAIT © All Rights Reserved 