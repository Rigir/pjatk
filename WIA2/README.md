Interrupt Jump Table 
http://www.ctyme.com/intr/int.htm

INT 10,0 - Set Video Mode
https://stanislavs.org/helppc/int_10-0.html

 Add to the end of dosbox-[version].conf file
```
@echo off

#Programs
set PATH=%PATH%;C:\misc\nasm;C:\misc\insight.124;
#Aliases
set PATH=%PATH%;C:\misc\aliases;

#Path to project
mount c ~/path_to_WIA2_folder
c:
```
Added aliases:
- make [/d] filename - To build the project 
  - /d - to debug
- ls - Instead of dir

insight shortcuts
- ctrl + F2 - restart program 
- ctrl + F8 - next program
- ctrl + D - data program 


