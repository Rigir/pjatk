@REM DOS version 5.00 
@REM @Author: Rigir
@echo off
cls

set operator=%1
set fileName=%2
if [%2]==[] set fileName=%1
set fileOutputName=output.com

if not exist %fileName% goto Err
    nasm %fileName% -o %fileOutputName% 
    if not exist %fileOutputName% goto Err
        if ["%1"]==["/d"] goto Debug 
        %fileOutputName%
        del %fileOutputName%
goto End

:Debug
insight %fileOutputName%
del %fileOutputName%
goto End

:Err
echo "Error: File not found!"

:End