![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Przygotuj, korzystając z pozyskanej przed chwilą wiedzy, programik, który będzie rysował ASCII choinkę. Proponuję przygotować dwie funkcje: jedną, która będzie jako argumenty przyjmowała ilość poprzednią wywoływała w pętli określoną ilość razy. Wspaniale by było, jakby choinka była zielona.
2. Rozszerz poprzedni programik (jedną z jego funkcji wystarczy) o rysowanie bombek. Sposób i umiejscowienie bombek dowolne, ale nie powinny stanowić więcej niż 1/3 choinki.
3. Za rysowanie choinki symetrycznej i z pniem, a nie od krawędzi.