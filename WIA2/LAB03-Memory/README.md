![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Za pomocą przerwania 21h/AH=0Ah wczytaj ciąg znaków, a następnie wydrukuj trzeci znak tego stringa. Do druku można użyć teletype output (int10h/AH=0Eh).
2. Wydrukuj stringa, może być zdefiniowany w kodzie, ale do trzeciego znaku. Przerwanie INT 21/AH=09h drukuje stringa zaczynając od umieszczonego w DX adresu aż do napotkania znaku ‘$’, czyli wystarczy po trzecim znaku umieścić w pamięci $ i załatwione.
3. Napisz program, który przyjmie stringa od użytkownika (int 21h/AH=0Ah), przesunie gdzieś kursor (INT 10h/AH=02h) i wydrukuje tego stringa na terminalu. Program ma wykryć, jak długi był przyjęty od użytkownika string i postawić na jego końcu ‘$’.