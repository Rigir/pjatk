![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Napisz program, który przyjmie od użytkownika jeden znak. Jeśli będzie większy niż ‘_’ (5Fh) to wydrukuje ‘>’, jeśli mniejszy to wydrukuje ‘<’, jeśli równy, to wydrukuje ‘==’.
2. Zmodyfikuj programik tak, aby rozpoznawał wielkie i małe litery. Wielkie litery mają kody ASCII 41h-5Ah, a małe 61h-7Ah. Program powinien drukować informacje zwrotną, np. „wielka litera”, „mala litera”, „error”.
3. Napisz programik, który w pętli będzie przyjmował i od razu wypisywał przyjęte znaki, ale jeśli wprowadzony zostanie znak Q to się zakończy.