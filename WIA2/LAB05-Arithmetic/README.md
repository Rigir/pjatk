![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Napisz program który obliczy wzór:
   1.  𝑎 + 𝑏 + 𝑐
   2.  𝑎 * 𝑏 + 𝑐
   3.  2𝑎 + 2𝑏 - 2𝑐
   4.  a / b + c
   5.  a / (b + c)
   6.  (a * b) / c
   7.  𝑎^2 + 2𝑏 + 𝑐
   8.  (2a * b) / 2c -> (a * b) / c
   9.  (a * (b + a)) / c
   10. a/2 + b/3 + c/4