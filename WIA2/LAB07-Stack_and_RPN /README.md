![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Napisz program obliczający wzór:
   1. a*b+c
   2. 2a+2b-2c
   3. (a/b)+c
   4. a/(b+c)
   5. (a*b)/c
   6. a^2+2*b+c
   7. (2a*b)/(2c)