![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Napisz program, który wczyta od użytkownika 1 znak, doda do niego wartość 10h i wypisze znak powiększony o daną wartość.
2. Napisz program, który wczyta od użytkownika tekst i wyświetli go. Program ma sam wykryć jak długi jest wpisywany tekst i ustawić w tamtym miejscu symbol dolara.
3. Napisz program, który zamiast wyświetlać całego stringa ma wyświetlić jego połowę. W skrócie – postawić dolara pośrodku, zamiast na końcu stringa.