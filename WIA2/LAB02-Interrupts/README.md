![PJATK](../../.assets/banner.png)
=========

Zadanie:
---
1. Napisz program, który utworzy katalog o nazwie składającej się z twoich inicjałów i dwóch ostatnich cyfr indeksu. Potrzebne będzie przerwanie 21h. Spróbuj ustalić, czym jest ASCIIZ i wykombinuj, jak je zapisać w kodzie programu.
2. Skasuj ten katalog.
3. Skorzystaj z przerwania 16h aby poczekać na wciśnięcie klawisza.
4. Napisz program, który na środku ekranu wypisze twoje imię i nazwisko, a następnie ustawi kursor na górze ekranu i się zamknie. Przydatne będzie przerwanie 10h oraz polecenie cls wykonane przed uruchomieniem programu.
5. Korzystając z przerwania 10h napisz program, który zmieni tryb wyświetalnia na graficzny. Program ma działać następująco:
   1. Zmienić tryb graficzny na 640x480 i 2 kolory na piksel
   2. Poczeka na wciśnięcie klawisza (przerwanie do wyboru)
   3. Zmieni tryb na tekstowys
   4. Zamknie się

