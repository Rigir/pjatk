'''
python3 main.py IP PORT
'''
from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import os
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]
    

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(1024).decode("UTF-8")
    input_codes = re.findall(r'\"(.*?)\"', str(data))
    cmd_line(f'echo {input_codes[0]} > data.enc')
    output_code = cmd_line(f'openssl enc -aes-256-ecb -d -a -in data.enc -K {input_codes[1]}')
    result = output_code.decode("UTF-8").split()[-1]
    pyperclip.copy(result)
    print(result)
    os.remove("data.enc") 
    s.close()

