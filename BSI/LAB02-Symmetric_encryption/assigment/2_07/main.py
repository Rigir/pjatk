'''
python3 main.py address
'''

from subprocess import PIPE, Popen
import pyperclip
import sys
import re
import os

ADDRESS = str(sys.argv[1])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

page_object = cmd_line(f'curl -s {ADDRESS} | grep -o \'href=\"\(.*\?\)\"\' ').decode("UTF-8")
FILENAME = re.findall(r'\"(.*?)\"', str(page_object))[0]
cmd_line(f'wget {ADDRESS}/{FILENAME}')
cmd_line(f'zip2john {FILENAME} > john.pass')
cmd_line(f'john --wordlist=rockyou.txt john.pass')
file_pass = cmd_line(f'john --show john.pass')
password = re.findall(r'\:(.*?)\:', str(file_pass))[0]
cmd_line(f'unzip -P {password} {FILENAME}')
result = cmd_line(f'cat secret.txt').decode("UTF-8")
pyperclip.copy(result)
print(result)
os.remove(f'{FILENAME}')
os.remove(f'john.pass')
os.remove("secret.txt")

