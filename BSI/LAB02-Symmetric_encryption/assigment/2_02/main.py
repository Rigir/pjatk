'''
python3 main.py ADDRESS
'''
from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

ADDRESS = str(sys.argv[1])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]
    

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    cmd_line(f'wget {ADDRESS}/cs.out {ADDRESS}/pwd.pass')
    output_code = cmd_line(f'openssl enc -aes-256-cbc -d -in cs.out -kfile pwd.pass -pbkdf2')
    result = output_code.decode("UTF-8")
    pyperclip.copy(result)
    print(result)
    os.remove("cs.out") 
    os.remove("pwd.pass") 
    s.close()

