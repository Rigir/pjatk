# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Odszyfrowanie danych szyfrem AES** \
Posiadasz zaszyfrowaną wiadomość: "CODE01". \
Do szyfrowania użyty został algorytm AES-256-ECB. \
Sekret zaszyfrowany został za pomocą następującego klucza: "CODE02". \
Odszyfruj szyfrogram w celu uzyskania odpowiedzi do zadania.

**Zadanie 2: Odszyfrowanie pliku szyfrem AES** \
Na serwerze znajduje się strona WWW, na której dostępne są zaszyfrowany plik oraz plik z kluczem szyfrującym. \
Używając otrzymanego klucza, odszyfruj zaszyfrowany plik w celu uzyskania odpowiedzi do zadania, wiedząc, że: \
Użyty algorytm to AES-256-CBC, Funkcja generowania klucza to PBKDF2. Adres strony: "Adres"

**Zadanie 3: Szyfrowanie danych algorytmem 3DES** \
Posiadasz zaszyfrowaną wiadomość: "CODE01". \
Do szyfrowania użyty został program OpenSSL z użyciem algorytmu \
szyfrowania 3DES oraz algorytmem generowania klucza PBKDF2. \
Sekret zaszyfrowany został za pomocą następującego hasła: "CODE02". \
Odszyfruj szyfrogram w celu uzyskania odpowiedzi do zadania.

**Zadanie 4: Szyfrowanie z wykorzystaniem algorytmu PBKDF1** \
Posiadasz zaszyfrowaną wiadomość: "CODE01". \
Do szyfrowania użyty został program OpenSSL z użyciem algorytmu \
szyfrowania AES-256-ECB oraz algorytmem generowania klucza PBKDF1. \
Sekret zaszyfrowany został za pomocą następującego hasła: "CODE02". \
Oraz następującej ilości iteracji algorytmu PBKDF1: "CODE03". \
Odszyfruj szyfrogram w celu uzyskania odpowiedzi do zadania.

**Zadanie 5: Algorytm PBKDF2 z niestandardową liczbą iteracji** \
Posiadasz zaszyfrowaną wiadomość: "CODE01". \
Do szyfrowania użyty został program OpenSSL z użyciem algorytmu \
szyfrowania AES-256-CBC oraz algorytmem generowania klucza PBKDF2. \
Sekret zaszyfrowany został za pomocą następującego hasła: "CODE02". \
Oraz następującej ilości iteracji algorytmu PBKDF2: "CODE03". \
Odszyfruj szyfrogram w celu uzyskania odpowiedzi do zadania.

**Zadanie 6: Identyfikacja algorytmu szyfrującego** \
Posiadasz zaszyfrowaną wiadomość: "CODE01". Do szyfrowania użyty \
został program OpenSSL z użyciem jednego z algorytmów szyfrowania \
w trybie ECB dostępnego w narzędziu OpenSSL. Sekret zaszyfrowany \
został za pomocą następującego hasła: "CODE02" przy użyciu algorytmu \
klucza PBKDF2. Odszyfruj szyfrogram i odeślij jej nazwę (pisaną małymi \
literami i bez znaków specjalnych) w celu uzyskania odpowiedzi do zadania.

**Zadanie 7: Szyfrowany plik Zip** \
Na serwerze znajduje się strona WWW, na której umieszczono archiwum .zip \
zaszyfrowane jednym z popularnych haseł oraz zawierające odpowiedź do zadania. \
W celu uzyskania odpowiedzi zadania, wypakuj chronione hasłem archiwum .zip. \
Podpowiedź: Do złamania hasła użyj narzędzia takiego jak fcrackzip lub \
John The Ripper oraz pliku z najpopularniejszymi hasłami rockyou.txt. Adres strony: "ADDRESS" \

**Test sprawdzający**:

**Aby wygenerować skrót SHA2-256 dla ciągu "i8d6s9v4x6s1" należy wykonać polecenie:** \
echo -n i8d6s9v4x6s1 | shasum -a 256

**Atak typu "brute-force" na kryptosystem polega na:** \
próbie odszyfrowania szyfrogramu poprzez przegląd całej przestrzeni kluczy

**Skrót AES oznacza:** \
Advanced Encryption Standard

**Algorytm 3DES to:** \
odmiana algorytmu DES, w której szyfrowanie z wykorzystaniem DES wykonuje się trzykrotnie

**Dla klucza szyfrującego o rozmiarze 256 bitów przestrzeń kluczy zawiera:** \
2^256 elementów

**Wskaż, który z elementów nie decyduje o sile systemu kryptograficznego:** \
długość szyfrogramu

**Różnica w pracy algorytmu szyfrującego w trybie ECB i CBC polega na tym, że:** \
w trybie CBC inaczej niż w trybie ECB, każdy nowy blok danych sumowany jest modulo 2 z szyfrogramem poprzedzającego go bloku

**Poniżej przedstawiono fragment podręcznika systemowego dla polecenia "openssl". Szyfrowanie pliku data.txt algorytmem AES w trybie wiązania zaszyfrowanych bloków, z blokami o rozmiarze 256 bitów z hasłem "as34Dw9P2lsH" przedstawia polecenie:** \

```bash
openssl enc  -ciphername [-AadePpv] [-base64] [-bufsize number] [-debug] [-in file]
                  [-iter iterations] [-iv IV] [-K key] [-k password] [-kfile file]
                  [-md digest] [-none] [-nopad] [-nosalt] [-out file] [-pass arg]
                  [-pbkdf2] [-S salt] [-salt]
```

openssl enc -e -aes-256-cbc -in data.txt -out data.enc -k as34Dw9P2lsH

**Sparametryzowany koszt obliczeniowy (work factor) w funkcjach PBKDF ma na celu:** \
znaczące utrudnienie ataków typu "brute-force"

**W kryptografii symetrycznej do deszyfrowania wiadomości odbiorca używa:** \
tego samego klucza, którym nadawca zaszyfrował wiadomość

**Algorytm AES w trybie CBC do zaszyfrowania danych wymaga podania:** \
klucza szyfrującego oraz wektora inicjującego
