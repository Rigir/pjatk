'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]
    

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(1024).decode("UTF-8")
    input_codes = re.findall(r'\"(.*?)\"', str(data))
    output_code = cmd_line(f'echo -n "{input_codes[0]}" | openssl dgst -sha512 -hmac "{input_codes[1]}"')
    prepared_data = output_code.decode("UTF-8").split()[1]
    s.send(f'{prepared_data}\n'.encode())
    result = s.recv(1024).decode("UTF-8").split()[-1]
    pyperclip.copy(result)
    print(result)
    s.close()
