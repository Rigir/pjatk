'''
python3 main.py IP PORT
'''

import pyperclip
import hashlib
import socket
import sys
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(1024).decode("UTF-8")
    input_code = re.search(r'\"(.*?)\"', str(data)).group(1)
    h = hashlib.sha3_512()
    h.update(input_code.encode())
    prepared_data = h.hexdigest()
    s.send(f'{prepared_data}\n'.encode())
    result = s.recv(1024).decode("UTF-8").split()[-1]
    pyperclip.copy(result)
    print(result)
    s.close()
