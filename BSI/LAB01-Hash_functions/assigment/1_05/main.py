'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import hashlib
import socket
import sys
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def find_algorithm(search_string, search_hash):
    for algorithm in hashlib.algorithms_available:
        try:
            h = hashlib.new(algorithm)
            h.update(search_string)
            if h.hexdigest() == search_hash:
                 return algorithm
        except: 
            pass

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(1024).decode("UTF-8")
    input_codes = re.findall(r'\"(.*?)\"', str(data))
    output_hash = find_algorithm(input_codes[0].encode("UTF-8"), input_codes[1])
    s.send(f'{output_hash}\n'.encode())
    result = s.recv(1024).decode("UTF-8").split()[-1]
    pyperclip.copy(result)
    print(result)
    s.close()
