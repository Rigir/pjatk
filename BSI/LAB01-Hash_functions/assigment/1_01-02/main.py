'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re

# HASH_ALG = "md5sum" # EX01
# HASH_ALG = "shasum" # EX02

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(1024).decode("UTF-8")
    input_code = re.search(r'\"(.*?)\"', str(data)).group(1)
    output_code = cmd_line(f'echo -n "{input_code}" | {HASH_ALG}').split()[0]
    prepared_data = f'{output_code.decode("UTF-8")}\n'.encode()
    s.send(prepared_data)
    result = s.recv(1024).decode("UTF-8").split()[-1]
    pyperclip.copy(result)
    print(result)
    s.close()
