'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import sys

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])


def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

result = cmd_line(f'nc {HOST} {PORT} < MD5_collision.txt').decode("UTF-8").split()[-1:][0]
pyperclip.copy(result)
print(result)
