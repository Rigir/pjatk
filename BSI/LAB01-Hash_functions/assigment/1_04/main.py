'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(1024).decode("UTF-8")
    input_code = re.search(r'\"(.*?)\"', str(data)).group(1)
    cmd_line(f'echo -n {input_code} > pass.txt')
    cmd_line(f'john --wordlist=rockyou.txt --format=raw-md5 pass.txt')
    output_code = cmd_line(f'john --show --format=Raw-MD5 pass.txt').decode("UTF-8")
    prepared_data = re.search(r'\?:(.*?)\n', str(output_code)).group(1)
    s.send(f'{prepared_data}\n'.encode())
    result = s.recv(1024).decode("UTF-8").split()[-1]
    pyperclip.copy(result)
    print(result)
    os.remove("pass.txt") 
    s.close()
