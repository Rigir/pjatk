# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Funkcja skrótu MD5** \
Zakoduj ciąg znaków: "CODE" (bez znaków cytatu) za pomocą algorytmu MD5, \
a następnie wyślij wynik do tego serwisu w celu uzyskania odpowiedzi na zadanie. \
Sumę MD5 wyślij w formacie heksadecymalnym.

**Zadanie 2: Funkcja skrótu SHA1** \
Zakoduj ciąg znaków: "CODE" (bez znaków cytatu) za pomocą algorytmu SHA1, \
a następnie wyślij wynik do tego serwisu w celu uzyskania odpowiedzi na zadanie \
Sumę SHA1 wyślij w formacie heskadecymalnym.

**Zadanie 3: Funkcja skrótu SHA3** \
Zakoduj ciąg znaków: "CODE" (bez znaków cytatu) za pomocą algorytmu SHA3_512, \
a następnie wyślij wynik do tego serwisu w celu uzyskania odpowiedzi na zadanie \
Sumę SHA3_512 wyślij w formacie heskadecymalnym.

**Zadanie 4: Łamanie skrótu MD5 metodą słownikową** \
Posiadasz zakodowany hash MD5 w postaci heksadecymalnej: "CODE" (bez znaków cytatu). \
Używając programu do łamania hashy (takiego jak John The Ripper) oraz wykorzystując \
słownik najpopularniejszych haseł rockyou.txt, złam hash hasła, \
a następnie ciąg znaków wyślij do serwisu w celu uzyskania odpowiedzi na zadanie.

**Zadanie 5: Identyfikacja funkcji skrótu** \
Posiadasz string: "CODE01" (bez znaków cytatu). \
W efekcie użycia algorytmu hashującego ciąg znaków przybrał postać: "CODE02" (bez znaków cytatu). \
Odgadnij jakiego algorytmu użyto i wyślij w odpowiedzi jego nazwę.

Uwaga:

1. Nazwy algorytmów bazowane są na nazwach algorytmów z biblioteki python: hashlib.
2. Wielkość liter odpowiedzi nie ma znaczenia.
3. Znaki "-" oraz "\_" są wycinane z odpowiedzi (możesz udzielić odpowiedzi zarówno z tymi znakami jak i bez nich).
4. W przypadku złej odpowiedzi, serwis zostanie zablokowany i wymagany będzie restart zadania.

**Zadanie 6: Kod uwierzytelniający HMAC** \
Używając algorytmu HMAC_SHA512 zakoduj następującą wiadomość: "CODE01" za pomocą następującego klucza: \
"CODE02" oraz odeślij wynik w postaci heksadecymalnej w celu otrzymania odpowiedzi do zadania.

**Zadanie 7 Kolizja MD5:** \
Wyślij do serwisu 2 różne ciągi znaków, zakodowane w formacie heksadecymalnym, \
które tworzą kolizję hasha MD5 (tj. dwa ciągi znaków generują ten sam hash MD5). \
Ciągi znaków muszą być oddzielone znakiem nowej linii ("\n"). \
Przy podaniu poprawnych ciągów znaków, generujących kolizję MD5, otrzymasz odpowiedź do zadania.

**Test sprawdzający**:

**Aby wygenerować skrót SHA2-256 dla ciągu "i8d6s9v4x6s1" należy wykonać polecenie:** \
echo -n i8d6s9v4x6s1 | shasum -a 256

**Jaki ciąg zostanie wygenerowany po wykonaniu polecenia: echo "pasword" | shasum a 256?** \
398f71d9dfecdadbfafa5f638d87e40a6e604e1f8697c4e2baf8bcf772daff8b

**W przypadku jednokierunkowych funkcji skrótu kolizja oznacza, że:** \
dla określonego algorytmu dwa różne ciągi danych wejściowych generują tę samą wartość skrótu

**Algorytm MD5 generuje skrót o długości:** \
128 bitów

**Algorytm SHA2-256 generuje skrót o długości:** \
32 bajtów

**W bibliotece openssl do generowania skrótów wiadomości wykorzystuje się komendę:** \
openssl dgst

**Wśród wymienionych poniżej nazw algorytmów jednokierunkową funkcją skrótu nie jest:** \
AES

**Ciąg znaków "aasb763jjab" został zapisany do pliku "file.txt" za pomocą polecenia: echo "aasb763jjab" > file.txt. Aby wygenerować skrót sha-256 dla ciągu "aasb763jjab" należy wykonać polecenie:** \
echo -n "aasb763jjab" | sha256sum

**Kod HMAC oznacza:** \
kod MAC wykorzystujący funkcje haszujące

**Wykorzystanie funkcji skrótu pozwala na zachowanie:** \
integralności wiadomości
