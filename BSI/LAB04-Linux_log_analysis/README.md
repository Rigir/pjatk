# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Statystyki pliku z logami** \
Na serwerze, w pliku /var/log/fontconfig.log znajduje plik z rejestrem zdarzeń (logami), które wystąpiły w systemie.

Odpowiedź do zadania znajduje się w pliku codebook.txt (plik w Twoim katalogu domowym) w wierszu o numerze równym liczbie linii pliku /var/log/fontconfig.log.

**Zadanie 2: Analiza logów z konkretnej daty** \
Na serwerze, w pliku /var/log/audit.log znajduje rejestr zdarzeń (logi), które wystąpiły w systemie. Odpowiedź do zadania znajduje się we wpisie (linii) dodanym 3 października o godzinie 10:10:31.

**Zadanie 3: Analiza logów konkretnej długości** \
Na serwerze, w pliku /var/log/audit.log znajduje plik z rejestrem zdarzeń (logami), które wystąpiły w systemie. Odpowiedź do zadania znajduje się w tym wpisie (linii), który ma więcej niż 150, ale mniej niż 180 znaków (na końcu linii).

**Zadanie 4: Analiza logów z konkretnego zakresu czasowego** \
Na serwerze, w pliku /var/log/audit.log znajduje plik z rejestrem zdarzeń (logami), które wystąpiły w systemie. Odpowiedź do zadania znajduje się w tym wpisie (linii), który został dodany między 8:00, a 8:02 (na początku linii).

**Zadanie 5:  Analiza udanych prób logowania się na SSH** \
Na serwerze, w pliku /var/log/audit.log znajduje się rejestr zdarzeń, które wystąpiły w systemie. Odpowiedź do zadania znajduje się we wpisie (linii), który dotyczy usługi VSFTPD, FTP lub XINETD.

Dane potrzebne do zalogowania znajdziesz poniżej po naciśnięciu przycisku Start.

**Zadanie 6:  Analiza udanych prób logowania się na SSH** \
Na serwerze, w pliku /var/log/audit.log znajduje się rejestr zdarzeń, które dotyczą logowania przez SSH. Jeden z nich zawiera wpis o nieudanej próbie logowania.

Odpowiedzią do zadania jest nazwa użytkownika, na którego konto próbowano się zalogować.

Dane potrzebne do zalogowania znajdziesz poniżej po naciśnięciu przycisku Start.

**Zadanie 7:  Analiza udanych prób logowania się na SSH** \

Na serwerze, w pliku /var/log/audit.log znajduje się rejestr zdarzeń dotyczący logowania przez SSH. Jedno z nich to udana próba logowania, co oznacza, że intruz uzyskał dostęp do serwera.

Odpowiedź do zadania to nazwa użytkownika, na którego konto próbowano się zalogować.

Dane potrzebne do zalogowania znajdziesz poniżej po naciśnięciu przycisku Start.

**Zadanie 8: Analiza wielu nieudanych prób logowania się na SSH** \

Na serwerze, w pliku /var/log/audit.log znajduje się rejestr zdarzeń (logi), które dotyczą logowania przez SSH. Część z nich to zdarzenia polegające na nieudanej próbie logowania przez SSH na konta użytkowników o nazwie user_X, gdzie X przyjmuje różne wartości.

Odpowiedź do zadania to połączone przyrostki użytkowników, na których konta próbowano się zalogować, w kolejności od najstarszej do najnowszej próby.

Dane potrzebne do zalogowania znajdziesz poniżej po naciśnięciu przycisku Start.