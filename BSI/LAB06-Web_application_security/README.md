# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Podgląd nagłówków odpowiedzi** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Podejrzyj nagłówki odpowiedzi serwera w narzędziach programistycznych \
wybranej przez siebie przeglądarki w celu uzyskania odpowiedzi do zadania. \

**Zadanie 2: Manipulowanie parametrami HTTP** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Zmanipuluj parametrem ID aplikacji w taki sposób, aby dotrzeć do innych \
wpisów na stronie. Po dotarciu do odpowiedniego wpisu otrzymasz odpowiedź do zadania. \

**Zadanie 3: Uruchomienie i konfiguracja proxy w przeglądarce** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Podejrzyj nagłówki odpowiedzi serwera we wcześniej skonfigurowanym \
narzędziu http Proxy (takim jak Burp Proxy czy OWASP ZAP). \
Uwaga: Do rozwiązania zadania nie używaj narzędzi programistycznych wbudowanych w przeglądarkę. \

**Zadanie 4: Automatyczny skan aplikacji** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Użyj automatycznego skanu aplikacji (wbudowany skan aplikacji OWASP ZAP). \
Odpowiedź znajduje się w danych zwróconych przez automatyczny skaner. \

**Zadanie 5: Modyfikacja żądań HTTP** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Zmodyfikuj żądanie HTTP ustawiając wartość nagłówka User-Agent \
na CyberSkiller w celu uzyskania odpowiedzi na zadanie. \

**Zadanie 6: Powtarzanie żądania HTTP** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Zmodyfikuj żądanie HTTP ustawiając wartość nagłówka User-Agent \
na CyberSkiller w celu uzyskania odpowiedzi na zadanie. \

**Zadanie 6: Wyszukiwanie właściwej wartości parametru metodą siłową** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP. \
Użyj funkcji Intruder z Burp Suite lub Fuzzer z OWASP ZAP w celu \
przeprowadzenia ataku metodą siłową na parametr id aplikacji. \
W jednym z odkrytych wpisów znajdować będzie się odpowiedź do zadania. \

**Test sprawdzający**:

**Jak nazywamy parametry dodawane do adresu URL zaczynające się od znaku "?" oraz rozdzielane znakiem "&"?** \
łańuchami zapytania (query string)

**Jak nazywamy dane, które przesyłane są zawsze po pierwszej linii zapytania HTTP?** \
nagłówkami HTTP

**Oceń prawdziwość zdania. Serwer otrzymując zapytanie HTTP jest w stanie zweryfikować autentyczność zapytania.** \
Fałsz

**Wartości z jakiego zakresu może przyjąć kod statusu zwracany w odpowiedzi zapytania HTTP?** \
100-599

**Administrator serwera HTTP ukrył pod adresem znanym tylko przez niego plik. Czy zewnętrzni użytkownicy mogą uzyskać dostęp do tego pliku?** \
Tak

**Oceń prawdziość zdania. Ruch HTTPS nie może zostać przechwycony za pomocą serwera proxy.** \
Fałsz

**Która z poniższych odpowiedzi o serwerach proxy jest nieprawdziwa?** \
Serwer proxy wykorzystywany jest do przyspieszenia łącza internetowego.

**Która z poniższych odpowiedzi przedstawia informacje zawarte w pierwszej linii zapytania HTTP?** \
metoda HTTP, adres URL, wersja HTTP

**Jaki jest domyślny port serwera HTTP?** \
80

**Jak nazywamy ataki korzystające z metody siłowej?** \
Bruteforce attack
