'''
python3 main.py IP
'''

from subprocess import PIPE, Popen
import pyperclip
import sys
import re

HOST = str(sys.argv[1])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

output = cmd_line(f'curl --head {HOST}').decode("UTF-8")
print(output)
result = re.findall(r'CyberSkiller.*', output)[0].split(':')[-1]
pyperclip.copy(result)
print(result)
