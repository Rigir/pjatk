'''
python3 main.py IP
'''

from subprocess import PIPE, Popen
import pyperclip
import requests
import sys
import re

HOST = str(sys.argv[1])
HEADERS = {'User-Agent': 'CyberSkiller'}
       
response = requests.get(HOST, headers=HEADERS)
result = re.findall(r'CS[0-9]{10}', response.text)[0]
pyperclip.copy(result)
print(result)
