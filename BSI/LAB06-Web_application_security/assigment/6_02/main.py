'''
python3 main.py IP
'''

from subprocess import PIPE, Popen
import pyperclip
import sys
import re

HOST = str(sys.argv[1])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

output = cmd_line(f'curl {HOST}/?id=6').decode("UTF-8")
result = re.findall(r'CS[0-9]{10}', output)[0]
pyperclip.copy(result)
print(result)
