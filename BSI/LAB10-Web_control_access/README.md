# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Dostęp do ukrytych stron** \
Przeszukaj najpopularniejsze ścieżki serwerowe w celu odkrycia adresu panelu administratora.

```
dirb <ip>
```

**Zadanie 2: Luki w dostępie do API** \
Aplikacja posiada funkcjonalność wyszukiwania użytkownika w systemie. W kodzie strony zawarty jest link URL API używanego do pobierania danych użytkownika wykorzystując link oraz modyfikując jego parametry, skonstruuj zapytanie usuwające użytkownika w systemie w celu uzyskania odpowiedzi na zadanie. Odpowiedź ta będzie podana jako Response serwera w przypadku skonstruowania poprawnego zapytania.

```
<is>/api/?controller=user&action=delete&user=CyberSkiller
```

**Zadanie 3: Manipulowanie parametrami HTTP** \
Odpowiednio manipulując ukrytymi parametrami koszyka takimi jak ilość zakupionych produktów oraz ich cena doprowadź do sytuacji, w której wartość koszyka zakupów będzie ujemna. W takim przypadku serwer zwróci w koszyku odpowiedź do zadania.

```
devtools -> network -> przycisk 'checkout' -> na metodzie POST 'edit and resend' -> zmień cenę z 25 na -25 w obu przypadkach -> wyślij -> odpowiedz w response 
```

**Zadanie 4: Podatność typu Path Traversal** \
ykorzystując podatność typu Path Traversal dla wczytywanych plików do podglądu doprowadź do wczytania pliku secret.txt znajdującego się w folderze nadrzędnym do wywoływanego skryptu.

```
<ip>/?image=../../../secret.txt
view page source -> 
poszukaj:<img src="data: text/plain;base64,<code>" alt="" class="thumb">
echo <code> | base64 -d
```

**Zadanie 5: Podatność typu Insecure Direct Object Reference** \
Wykorzystując podatność typu Insecure Direct Object Reference doprowadź do wczytania pliku secret.txt.

```
Wejdz w jakikolwiek plik download file który nie jest 'secret.txt' i dopisz secret.txt na sam koniec:
<ip>/<katalog_dowolnego_pliku>/secret.txt
```

**Test sprawdzający**:

**Oceń prawdziwość zdania. Każda próba dostępu do funkcji administracyjnych powinna podlegać autoryzacji.** \
Prawda

**Która z poniższych odpowiedzi jest fałszywa? Narzędzie gobuster służy do:** \
Ataku siłowego na formularze logowania

**Na stronie HTTP udostępniony został skrypt umożliwiający pobieranie pliku podanego w parametrze. W jaki sposób możemy zabezpieczyć ten skrypt i stronę przed podatnością Insecure Direct Object Reference?** \
Należy dodać autoryzację - użytkownik pobierający plik powinien mieć odpowiedni dostęp

**Oceń prawdziwość zdania. Autoryzacja i uwierzytelnienie to synonimy.** \
Fałsz

**Serwer HTTP udostępnia możliwość pobierania plików, a nazwę pliku przyjmuje w parametrze. W jaki sposób za pomocą podatności Path Traversal możemy odwołać się do pliku secret.txt w katalogu /root, skoro wiemy, że pliki domyślnie pobierane są z folderu /home/user?** \
../../root/secret.txt

**Oceń prawdziwość zdania. Narzędzie gobuster nie pozwoli na wyszukiwanie zasobów dostępnych po zalogowaniu na serwer HTTP.** \
Fałsz





