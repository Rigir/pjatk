# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Hasło użytkownika o niskiej złożoności** \
Użyj narzędzi do automatycznych prób logowania takich jak Hydra w celu przetestowania konta administratora pod kątem stosowania słabych haseł konta. Gdy uzyskasz poprawne dane logowania do konta, zaloguj się do panelu administratora w celu uzyskania odpowiedzi do zadania.

```
hydra <ip> -s 80 -l admin -P 10k-most-common.txt -t 64 -T 64 http-post-form "/:username=^USER^&password=^PASS^&submit=Submit:failed"
```

**Zadanie 2: Identyfikator sesji o niskiej losowości** \
Twoim zadaniem jest manipulowanie ciasteczkiem sesji w taki sposób, aby uzyskać dostęp do konta administratora w portalu. Po podaniu poprawnych danych uwierzytelniających strona wyświetli panel administracyjny z odpowiedzią do zadania.

**Zadanie 3: Uwierzytelnianie po stronie klienta** \
Spróbuj znaleźć wszelkie luki w systemie, takie jak niezabezpieczone punkty końcowe lub walidacja danych po stronie klienta.

```
prawy myszek -> view source code -> <script> kopiuj wartość login_key -> na stronie login:admin hasło: login_key
```

**Zadanie 4: Nieprawidłowa obsługa przypomnienia hasła** \
Twoim zadaniem jest znaleźć link do resetowania hasła administratora, a następnie zresetować hasło administratora i zalogować się do panelu administratora, aby otrzymać odpowiedź do zadania.

**Zadanie 5: Enumeracja użytkowników na podstawie czasu odpowiedzi** \
Twoim zadaniem jest znalezienie poprawnych kont użytkowników w systemie. Aby to zrobić, możesz enumerować system pod kątem występowania najpopularniejszych nazw użytkowników, przyjmując zawartość pliku z najpopularniejszymi użytkownikami

```
?
```

**Test sprawdzający**:

**W jakim celu wykorzystuje się atak na czas odpowiedzi serwera?.** \
Atak służy do spowodowania wycieku danych

**Oceń prawdziwość zdania. Walidacja użytkownika w serwisach HTTP jest zawsze przeprowadzana po stronie klienta.** \
Fałsz

**W jaki sposób przeglądarka potwierdza autentyczność strony w protokole HTTPS?** \
Weryfikuje cyfrowy certyfikat przesyłany podczas nawiązywania połączenia ze stroną

**W jaki sposób serwery przekazują użytkownikowi dowód potwierdzający jego zalogowanie?** \
Użytkownik otrzymuje od serwera ciasteczka z numerem sesji

**Oceń prawdziwość zdania. Identyfikator sesji powinien być generowany tylko raz podczas pierwszego zalogowania użytkownika przy następnych logowaniach użytkownik powinien otrzymać ten sam identyfikator.** \
Fałsz

**Oceń prawdziwość zdania. Protokół HTTP jest protokołem bezstanowym.** \
Prawda

**Czym jest atak typu session fixation?** \
Atakiem przejęcia sesji

**Oceń prawdziwość zdania. Autoryzacja to proces potwierdzenia tożsamości.** \
Fałsz