# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Generowanie kluczy RSA** \
Wygeneruj 4096 bitową parę kluczy RSA i wyślij na serwer w formacie PEM w celu otrzymania odpowiedzi do zadania.
Uwaga! Serwer czeka na odpowiedź tylko 5 sekund.

Dane powinny być sformatowane w następujący sposób:

```
KLUCZ PUBLICZNY W FORMACIE PEM
<pusta linia>
KLUCZ PRYWATNY W FORMACIE PEM
```

**Zadanie 2: Generowanie kluczy na krzywych eliptycznych** \
Wygeneruj parę kluczy ECC NIST P-256 prime256v1 i wyślij na serwer w formacie PEM w celu otrzymania odpowiedzi do zadania.
Uwaga! Serwer czeka na odpowiedź tylko 5 sekund.

Dane powinny być sformatowane w następujący sposób:

```
KLUCZ PUBLICZNY W FORMACIE PEM
<pusta linia>
KLUCZ PRYWATNY W FORMACIE PEM
```

**Zadanie 3: Szyfrowanie kluczem publicznym** \
Posiadasz publiczny klucz RSA(CODE01):

```
-----BEGIN PUBLIC KEY-----
RSA-KEY
-----END PUBLIC KEY-----
```

Używając powyższego klucza zakoduj następujący ciąg znaków: "CODE02" z użyciem następującego schematu paddingu: OAEP oraz odeślij go do serwisu w formacie base64 w celu otrzymania odpowiedzi do zadania.
Uwaga! Odpowiedź powinna zawierać się w 1 linii.Serwer czeka na odpowiedź tylko 5 sekund.

**Zadanie 4: Certyfikat z zadanym polem Nazwa pospolita** \
Stwórz Urząd Certyfikacji (ang. Certificate Authority), a następnie wygeneruj certyfikat, który w swoim polu CN (Common Name) posiadał będzie następujący ciąg znaków:

"CODE01"

Odeslij certyfikat w formacie PEM w celu otrzymania odpowiedzi do zadania.
Uwaga! Serwer czeka na odpowiedź tylko 5 sekund.

**Zadanie 5: Certyfikat na podstawie zapytania** \

Stwórz Urząd Certyfikacji (ang. Certificate Authority), a następnie wygeneruj certyfikat na podstawie otrzymanego zapytania (CODE01):

```
-----BEGIN CERTIFICATE REQUEST-----
KEY
-----END CERTIFICATE REQUEST-----
```

Odeślij certyfikat w formacie PEM odpowiedzi do zadania.
Uwaga! Serwer czeka na odpowiedź tylko 5 sekund.

**Zadanie 6: Generowanie klucza PGP** \
Wygeneruj klucz PGP, który w polu imienia i nazwiska posiadał będzie następujący ciąg znaków: "CODE01".
Odeślij klucz publiczny jako blok kluczy w formacie PEM w celu otrzymania odpowiedzi do zadania.
Uwaga! Serwer czeka na odpowiedź tylko 5 sekund

**Zadanie 7: Szyfrowanie za pomocą klucza PGP** \
siadasz losowy ciąg znaków:"CODE01"
Zaszyfruj go za pomocą podanego klucza publicznego GPG(CODE02):

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
PGP-KEY
-----END PGP PUBLIC KEY BLOCK-----
```

Oraz odeślij szyfrogram w postaci armor w celu otrzymania odpowiedzi do zadania.
Uwaga! Serwer czeka na odpowiedź tylko 5 sekund.

**Zadanie 8: Odszyfrowanie za pomocą klucza PGP** \
Wykorzystując program GPG, wygeneruj zestaw kluczy (publiczny-prywatny) dla dowolnego użytkownika.
Następnie wyślij publiczny klucz GPG w formacie armor w celu otrzymania zaszyfrowanej wiadomości GPG
Odszyfruj wiadomość za pomocą klucza prywatnego w celu otrzymania odpowiedzi do zadania.
Uwaga! Serwer czeka na klucz tylko 5 sekund.

**Test sprawdzający**:

**Podpis złożony na wiadomości gwarantuje przede wszystkim jej:** \
integralność

**W kryptografii asymetrycznej używamy:** \
klucza prywatnego oraz wielu kluczy publicznych

**W kryptografii asymetrycznej:** \
wysyłane wiadomości szyfruje się kluczami publicznymi odbiorcy

**Bezpieczeństwo kryptosystemu RSA opera się na:** \
problemie faktoryzacji

**Celem stosowania GnuPG nie jest:** \
zachowanie rozliczalności

**Aby zaszyfrować plik data.txt o rozmiarze 4 kB wykorzystując klucz publiczny o rozmiarze 4 kb (zapisany w pliku pub.pem) należy:** \
wygenerować losowy klucz, zaszyfrować podany plik za pomocą algorytmu symetrycznego z wykorzystaniem wygenerowanego klucza, zaszyfrować kluczem publicznym klucz symetryczny, ewentualnie przesłać odbiorcy zaszyfrowany plik wraz z zaszyfrowanym kluczem

**W kryptosystemie RSA:** \
odbierane wiadomości deszyfruje się kluczami prywatnymi odbiorcy

**Poniżej przedstawiono:**

```
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn1HMolkYW7gJOZYBXTiw
8xDNX4zaMxTrYW7w6/i3pHbpPGPCo+bclJvPoVZohG+QnyNJV25ILW6oJ9rqv4qq
xesNQTTepKSguKwMAqPOlnSYwbFBJoNNuYIThNz6qaAOgIfTBFKohRbR2rECFoW7
inkO44rok+fAmEjQvykFgO/3l5fs3D1Cqq6fN+Qk6TGHmk/hVpX4s9zPPcpm9b3H
gvngBl3toxHPIkrHV+7xlzEAcYeZoiXofGIkWgMjGg+sOh/tPwgEviM3hFH5P0+V
oi/Txsvv1jQvkMAVZTUyuMg4gFyv777t0BFRbWB8M1er1Qr38D+atV5bZjtIcBzE
lwIDAQAB
-----END PUBLIC KEY-----
```

klucz publiczny zapisany z wykorzystaniem kodowania base64

**System GnuPG oparty jest na:** \
modelu zaufania typu "Web of Trust"

**Odbiorca B, w celu weryfikacji podpisanej przez nadawcę A wiadomości:** \
wykorzysta klucz publiczny A
