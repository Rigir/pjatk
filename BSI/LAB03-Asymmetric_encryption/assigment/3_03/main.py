'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])
PATTERN = r'(-----BEGIN PUBLIC KEY-----(\n|\r|\r\n)([0-9a-zA-Z\+\/=]{64}(\n|\r|\r\n))*([0-9a-zA-Z\+\/=]{1,63}(\n|\r|\r\n))?-----END PUBLIC KEY-----)|(-----BEGIN PRIVATE KEY-----(\n|\r|\r\n)([0-9a-zA-Z\+\/=]{64}(\n|\r|\r\n))*([0-9a-zA-Z\+\/=]{1,63}(\n|\r|\r\n))?-----END PRIVATE KEY-----)'

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

if __name__=="__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(1024).decode("UTF-8")
        input_rsa = max(re.findall(PATTERN, data)[0], key=len)
        input_code = re.search(r'\"(.*?)\"', data).group(1)
        cmd_line(f'echo "{input_rsa}" > pub.pem')
        cmd_line(f'echo "{input_code}" > data.txt')
        cmd_line(f'openssl pkeyutl -encrypt -in data.txt -inkey pub.pem -pubin -out data.enc -pkeyopt rsa_padding_mode:oaep')
        prepared_data = cmd_line(f'base64 -w0 data.enc').decode("UTF-8")
        s.send(f'{prepared_data}\n'.encode())
        result = s.recv(1024).decode("UTF-8").split()[-1]
        pyperclip.copy(result)
        print(result)
        os.remove("pub.pem")
        os.remove("data.txt")
        os.remove("data.enc") 
        s.close()
