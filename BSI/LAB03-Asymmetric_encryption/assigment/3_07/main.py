'''
python3 main.py IP PORT
need to clear gpg --list-keys with contact@cyberskiller.com address in it
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])
PATTERN_START = '-----BEGIN PGP PUBLIC KEY BLOCK-----'
PATTERN_END = '-----END PGP PUBLIC KEY BLOCK-----'

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

if __name__=="__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(2048).decode("UTF-8")
        input_codes = re.findall(r'\"(.*?)\"', str(data))
        pub_key = data[data.find(PATTERN_START):data.find(PATTERN_END)+len(PATTERN_END)]
        cmd_line(f'echo -n "{pub_key}" > pub.pem')
        cmd_line(f'gpg --import pub.pem')
        output_data=cmd_line(f'echo -n "{input_codes[0]}" | gpg --trust-model always --encrypt --armor --recipient "contact@cyberskiller.com"').decode("UTF-8")
        prepared_data = output_data[output_data.find('-----BEGIN PGP MESSAGE-----'):]
        s.send(f'{prepared_data}\n'.encode())
        result = s.recv(1024).decode("UTF-8").split()[-1]
        pyperclip.copy(result)
        print(result)
        os.remove("pub.pem") 
        s.close()
