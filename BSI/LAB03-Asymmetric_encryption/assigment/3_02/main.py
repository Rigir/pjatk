'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import os
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


if __name__=="__main__":
    cmd_line(f'openssl ecparam -name prime256v1 -genkey -out priv.pem')
    cmd_line(f'openssl ec -in priv.pem -out pub.pem -pubout')
    cmd_line(f'cat pub.pem > keys.pem')
    cmd_line(f'echo "" >> keys.pem')
    cmd_line(f'cat priv.pem >> keys.pem')
    result = cmd_line(f'nc {HOST} {PORT} < keys.pem').decode("UTF-8").split()[-1:][0]
    pyperclip.copy(result)
    print(result)
    os.remove("priv.pem") 
    os.remove("pub.pem")
    os.remove("keys.pem")
