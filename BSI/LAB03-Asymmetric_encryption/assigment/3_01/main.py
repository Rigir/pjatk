'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import os
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])
PATTERN = r'(-----BEGIN PUBLIC KEY-----(\n|\r|\r\n)([0-9a-zA-Z\+\/=]{64}(\n|\r|\r\n))*([0-9a-zA-Z\+\/=]{1,63}(\n|\r|\r\n))?-----END PUBLIC KEY-----)|(-----BEGIN PRIVATE KEY-----(\n|\r|\r\n)([0-9a-zA-Z\+\/=]{64}(\n|\r|\r\n))*([0-9a-zA-Z\+\/=]{1,63}(\n|\r|\r\n))?-----END PRIVATE KEY-----)'

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


if __name__=="__main__":
    cmd_line(f'openssl genrsa -out priv.pem 4096')
    cmd_line(f'openssl rsa -in priv.pem -pubout -out pub.pem')
    private_input = cmd_line(f'openssl rsa -in priv.pem -text').decode("UTF-8")
    public_input = cmd_line(f'openssl rsa -in pub.pem -pubin -text').decode("UTF-8")
    private_output = max(re.findall(PATTERN, public_input)[0], key=len)
    public_output =  max(re.findall(PATTERN, private_input)[0], key=len)
    cmd_line(f'echo -n "{private_output}\n\n{public_output}" > keys.txt')
    result = cmd_line(f'nc {HOST} {PORT} < keys.txt').decode("UTF-8").split()[-1:][0]
    pyperclip.copy(result)
    print(result)
    os.remove("priv.pem") 
    os.remove("pub.pem")
    os.remove("keys.txt")
