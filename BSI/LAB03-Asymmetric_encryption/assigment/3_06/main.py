'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

if __name__=="__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(1024).decode("UTF-8")
        input_codes = re.findall(r'\"(.*?)\"', str(data))
        cmd_line(f'gpg --quick-gen-key --batch --passphrase "..." "{input_codes[0]} <text@cyberskiller.com>"')
        prepared_data = cmd_line(f'gpg --export --armor "{input_codes[0]}"').decode("UTF-8")
        s.send(f'{prepared_data}\n'.encode())
        result = s.recv(1024).decode("UTF-8").split()[-1]
        pyperclip.copy(result)
        print(result)
        s.close()