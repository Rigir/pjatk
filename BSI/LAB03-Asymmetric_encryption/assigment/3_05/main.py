'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])
PATTERN = r'(-----BEGIN CERTIFICATE REQUEST-----(\n|\r|\r\n)([0-9a-zA-Z\+\/=]{64}(\n|\r|\r\n))*([0-9a-zA-Z\+\/=]{1,63}(\n|\r|\r\n))?-----END CERTIFICATE REQUEST-----)'

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

if __name__=="__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(2048).decode("UTF-8")
        input_rsa = max(re.findall(PATTERN, data)[0], key=len)
        cmd_line(f'openssl genrsa -out ca.key 2048')
        cmd_line(f'echo ".\n.\n.\n.\n.\ncyberskiller\n.\n" | openssl req -new -x509 -key ca.key -out ca.crt')
        cmd_line(f'echo "{input_rsa}" > client.crt')
        cmd_line(f'openssl x509 -req -CAkey ca.key -CA ca.crt -in client.crt -CAcreateserial -out client.crt')
        prepared_data = cmd_line(f'cat client.crt').decode("UTF-8")
        s.send(f'{prepared_data}\n'.encode())
        result = s.recv(1024).decode("UTF-8").split()[-1]
        pyperclip.copy(result)
        print(result)
        os.remove("ca.key") 
        os.remove("ca.crt")
        os.remove("client.crt") 
        s.close()
