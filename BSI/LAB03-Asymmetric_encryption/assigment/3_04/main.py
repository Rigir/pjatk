'''
python3 main.py IP PORT
'''
from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import os
import re

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]
    
if __name__=="__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(1024).decode("UTF-8")
        input_codes = re.findall(r'\"(.*?)\"', str(data))
        cmd_line(f'openssl genrsa -out ca.key 2048')
        cmd_line(f'echo ".\n.\n.\n.\n.\n{input_codes[0]}\n.\n" | openssl req -new -x509 -key ca.key -out ca.crt')
        prepared_data = cmd_line(f'cat ca.crt').decode("UTF-8")
        s.send(f'{prepared_data}\n'.encode())
        result = s.recv(1024).decode("UTF-8").split()[-1]
        pyperclip.copy(result)
        print(result)
        os.remove("ca.key") 
        os.remove("ca.crt")
        s.close()

