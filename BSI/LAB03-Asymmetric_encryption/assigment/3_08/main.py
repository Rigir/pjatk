'''
python3 main.py IP PORT
'''

from subprocess import PIPE, Popen
import pyperclip
import socket
import sys
import re
import os

HOST = str(sys.argv[1])
PORT = int(sys.argv[2])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

if __name__=="__main__":
    cmd_line(f'gpg --quick-gen-key --batch --passphrase "..." "test_cyberskiller <test@cyberskiller.com>"')
    cmd_line(f'gpg --export --armor "test_cyberskiller" > public.txt')
    data = cmd_line(f'nc {HOST} {PORT} < public.txt').decode("UTF-8")
    input_pgp = data[data.find('-----BEGIN PGP MESSAGE-----'):]
    cmd_line(f'echo -n "{input_pgp}" > data.enc')
    result = cmd_line(f'gpg --pinentry-mode=loopback --passphrase "..." --batch -d data.enc').decode("UTF-8")
    pyperclip.copy(result)
    print(result)
    os.remove("public.txt") 
    os.remove("data.enc") 

