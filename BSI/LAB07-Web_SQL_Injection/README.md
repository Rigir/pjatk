# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Klasyczna podatność SQL Injection** \
Aplikacja posiada prosty panel logowania użytkownika. Wykorzystując podatność typu SQL Injection oraz znając login użytkownika CyberSkiller zaloguj się do panelu w celu otrzymania odpowiedzi do zadania.

**Zadanie 2: Odczyt schematu bazy danych** \
Aplikacja posiada zaimplementowany prosty mechanizm wyszukiwarki. Wykorzystując podatność typu UNION SQL Injection przeprowadź atak, który zwróci nazwy tabel w bazie danych. Nazwa jednej z tabel jest odpowiedzią do zadania.

**Zadanie 3: Identyfikacja wersji serwera bazy danych** \
Aplikacja posiada zaimplementowany prosty mechanizm wyszukiwarki. Wykorzystując podatność typu UNION SQL Injection przeprowadź atak, który zwróci nazwy tabel w bazie danych. Nazwa jednej z tabel jest odpowiedzią do zadania.

**Zadanie 4: Podatność SQL Injection typu Blind** \
Aplikacja posiada zaimplementowany prosty mechanizm wyszukiwarki. Wykorzystując podatność typu Blind SQL Injection, wydobądź z bazy danych hasło użytkownika CyberSkiller.
Podpowiedź: Nazwa tabeli użytkowników to users.
Nazwy kolumn w tabeli users to id, username, password.
Hasło użytkownika stanowi odpowiedź do zadania i ma wartość zgodną z formatem flagi, czyli rozpoczyna się od wielkiej litery C, po której jest druga wielka litera i 10 cyfr.

**Zadanie 5:  Podatność SQL Injection typu Time Based** \
Posiadasz dostęp do web serwera obsługującego prostą stronę HTTP.
Aplikacja posiada zaimplementowany prosty mechanizm resetowania hasła użytkownika. Wykorzystując podatność Time Based SQL Injection, wydobądź z bazy danych hasło użytkownika CyberSkiller.
Podpowiedź: Nazwa tabeli użytkowników to users.
Nazwy kolumn w tabeli users to id, username, password.
Hasło użytkownika stanowi odpowiedź do zadania i ma wartość zgodną z formatem flagi, czyli rozpoczyna się od wielkiej litery C, po której jest druga wielka litera i 10 cyfr.
 

**Test sprawdzający**:

**Oceń prawdziwość zdania. Podatność SQL Injection pozwala na pobranie danych tylko i wyłącznie z jednej tabeli.** \
Fałsz

**Na czym polega podatność typu SQL Injection?** \
Na wstrzykiwaniu kodu SQL do zapytań baz danych

**Czym charakteryzuje się atak SQL Injection typu blind?** \
Atak zwraca pozytywny wynik w przypadku znalezienia pełnej wartości

**Które z poniższych zdań nie opisuje mechanizmu Prepared Statement w bazach SQL?** \
Mechanizm jest kompilowany przy każdym użyciu

**Jak nazywamy zbiór informacji o budowie bazy danych?** \
Schematem bazy danych

**Który z poniższych ciągów znaków służy do dołączenia komentarza do zapytania SQL?** \
--

**Oceń prawdziwość zdania. Ataki SQL Injection nie różnią się pomiędzy różnymi systemami zarządzania bazami danych.** \
Fałsz

**Oceń prawdziwość zdania. Mechanizm Preprared Statement jest podatny na wstrzyknięcie kodu ze względu na to, że jest skompilowany przed użyciem.** \
Fałsz