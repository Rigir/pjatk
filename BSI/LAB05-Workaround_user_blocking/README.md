# ![PJATK](../../.assets/banner.png)

**Zadanie 1: Automatyczne wylogowywanie** \
Na serwerze, w Twoim katalogu domowym, umieszczono plik secret.txt, w którym znajduje się odpowiedź do zadania. Jednakże po zalogowaniu użytkownik jest automatycznie wylogowywany (modyfikacja pliku .bash_rc).

Odczytaj zawartość pliku i uzyskaj odpowiedź do zadania.

- ssh alice@ip cat secret.txt

**Zadanie 2: Dostęp do plików w katalogu /root** \
Na serwerze znajduje się plik secret.txt w folderze /root, którego właścicielem jest root. Zadanie polega na odczytaniu zawartości tego pliku, w którym znajduje się odpowiedź. W celu rozwiązania zadania można skorzystać z jednego z plików w Twoim katalogu domowym.

**Zadanie 3: Dostęp do plików należących do użytkownika root** \
Na serwerze znajduje się plik secret.txt w folderze /root, którego właścicielem jest root. Zadanie polega na odczytaniu zawartości tego pliku, w którym znajduje się odpowiedź.

W celu rozwiązania zadania można skorzystać z jednego z plików w katalogu domowym użytkownika, który pozwala na odczytywanie plików z folderu /tmp i który podatny jest na atak Path Traversal.

Dane logowania znajdziesz poniżej po naciśnięciu przycisku Start.

**Zadanie 4: Podwyższenie uprawnień użytkownika** \
Na serwerze znajduje się plik secret.txt w folderze /root, którego właścicielem jest root. Zadanie polega na odczytaniu zawartości tego pliku, w którym znajduje się odpowiedź.

W celu rozwiązania zadania należy skorzystać z jednego z programów z uprawnieniami SUID, aby uzyskać dostęp do konta root.

**Zadanie 5: Inna powłoka niż Bash** \
Po zalogowaniu się przez SSH na serwer musisz zalogować się na konto użytkownika bob, jednakże po zalogowaniu się użytkownika bob uruchamiana jest jest inna powłoka niż Bash.

Zidentyfikuj, jaka to powłoka, wyjdź z niej do powłoki Bash i odczytaj plik z katalogu domowego użytkownika bob z odpowiedzią do zadania.

- ssh alice@ip 
- zmiejsz rozmiar okna
- ssh bob@localhost -i .ssh/bob_key.pem
- v - odpala vim
- :e secret.txt

**Zadanie 6: Usunięty plik** \
W Twoim katalogu domowym znajduje się program answer.py (napisany w języku Python), który odczytuje plik z odpowiedzią do zadania. Niestety, plik ten został usunięty, ale w tle wciąż działa program answer.py.

Twoje zadanie polega na odczytaniu zawartości pliku z odpowiedzią do zadania.
