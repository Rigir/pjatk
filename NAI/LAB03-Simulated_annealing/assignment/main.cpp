#include <math.h>
#include <string.h>

#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <optional>
#include <random>
#include <vector>
/**
 * @brief random number generator
 *
 * @param std::random_device
 * @return std::mt19937
 */
std::mt19937 mt_generator((std::random_device())());

/**
 * @brief goal function domain.
 */
using points_t = std::pair<double, double>;

/**
 * @brief full review method that will check every domain point
 *
 * @param f goal function
 * @param domain_generator the function that will generate consecutive points
 * from the domain and it will return empty when there are no more points to
 * check
 * @return domain_t the point where f has its minimum
 */
points_t brute_force_method(
    const std::function<double(points_t)> &f,
    const std::function<points_t(double, double)> &domain_generator,
    const int domain_min, const int domain_max, const int iterations) {
  points_t current_p, best_p;
  current_p = best_p = domain_generator(domain_min, domain_max);
  for (int i = 0; i < iterations; ++i) {
    if (f(current_p) < f(best_p)) {
      best_p = current_p;
    }
    current_p = domain_generator(domain_min, domain_max);
  }
  return best_p;
}

/**
 * @brief calculate minimum point using hill climbing algorithm
 *
 * @param f goal function
 * @param start_point the start point for calculations
 * @param get_close_points function generating neighbours
 * @param max_iterations number of iterations
 * @return domain_t the domain ponint where the function f has minimum
 */
points_t hill_climbing(
    const std::function<double(points_t)> &f,
    const std::function<points_t(double, double)> &domain_generator,
    const std::function<std::vector<points_t>(points_t, int, int)>
        &get_close_points,
    const int domain_min, const int domain_max, const int iterations) {
  points_t best_p = domain_generator(domain_min, domain_max);
  for (int i = 0; i < iterations; i++) {
    auto close_points = get_close_points(best_p, domain_min, domain_max);
    auto best_neighbour =
        *std::min_element(close_points.begin(), close_points.end(),
                          [f](auto a, auto b) { return f(a) > f(b); });
    if (f(best_neighbour) < f(best_p)) best_p = best_neighbour;
  }
  return best_p;
}

points_t annealing_method(
    const std::function<double(points_t)> &f,
    const std::function<points_t(double, double)> &domain_generator,
    const std::function<std::vector<points_t>(points_t, int, int)>
        &get_close_point,
    const int domain_min, const int domain_max, const int iterations) {
  std::uniform_real_distribution<double> uk_dist(0, 1);

  std::vector<points_t> annealing_list = {
      domain_generator(domain_min, domain_max)};
  points_t current_p = annealing_list.back();

  for (int i = 0; i < iterations; ++i) {
    double uk_value = uk_dist(mt_generator);
    std::vector<points_t> neighbors_point =
        get_close_point(current_p, domain_min, domain_max);
    auto tk = *std::min_element(neighbors_point.begin(), neighbors_point.end(),
                                [f](auto a, auto b) { return f(a) < f(b); });
    if (f(tk) <= f(current_p)) {
      current_p = tk;
      annealing_list.push_back(current_p);
    } else {
      if (uk_value < exp(-(abs(f(tk) - f(current_p)) / (1 / log(i))))) {
        current_p = tk;
        annealing_list.push_back(current_p);
      }
    }
  }

  return current_p;
}

auto isInDomain = [](const double value, const int domain_min,
                     const int domain_max) -> bool {
  return (domain_min <= value) && (value <= domain_max);
};

auto xy_generator = [](int x, int y) {
  std::uniform_real_distribution<> dis(x, y);
  return points_t(dis(mt_generator), dis(mt_generator));
};

auto get_close_point = [](points_t p, int domain_min,
                          int domain_max) -> std::vector<points_t> {
  std::uniform_real_distribution<> dis(domain_min, domain_max);
  double x = 0, y = 0;
  do {
    x = p.first + dis(mt_generator);
  } while (!isInDomain(x, domain_min, domain_max));
  do {
    y = p.first + dis(mt_generator);
  } while (!isInDomain(y, domain_min, domain_max));
  return {points_t(x, y)};
};

auto get_close_points = [](points_t p, const int domain_min,
                           const int domain_max) -> std::vector<points_t> {
  double precision = 1.0 / 128.0;
  std::vector<points_t> ret;
  for (int i = -1; i <= 1; i++) {
    for (int j = -1; j <= 1; j++) {
      points_t new_point = {p.first + (i * precision),
                            p.second + (j * precision)};
      if (isInDomain(new_point.first, domain_min, domain_max) &&
          isInDomain(new_point.second, domain_min, domain_max)) {
        ret.push_back(new_point);
      }
    }
  }
  return ret;
};

int main() {
  // <-512, 512>
  auto eggholder_f_v = [](std::pair<double, double> v) {
    return -(v.second + 47) * sin(sqrt(abs((v.first / 2) + (v.second + 47)))) -
           v.first * sin(sqrt(abs(v.first - (v.second + 47))));
  };

  // <-5, 5>
  auto ackley_f_v = [](std::pair<double, double> v) {
    return -20 * exp(-0.2 * sqrt(0.5 * (pow(v.first, 2) + pow(v.second, 2)))) -
           exp(0.5 * (cos(2 * M_PI * v.first) + cos(2 * M_PI * v.second))) +
           exp(1) + 20;
  };

  // <-10, 10>
  auto holder_table_f_v = [](std::pair<double, double> v) {
    return -abs(
        sin(v.first) * cos(v.second) *
        exp(abs(1 - (sqrt(pow(v.first, 2) + pow(v.second, 2)) / (M_PI)))));
  };

  const int iterations = 100000;
  auto test_fuction = [](const std::function<double(points_t)> &f,
                         const int domain_min, const int domain_max,
                         std::string header) {
    using namespace std::chrono;
    std::cout << header << std::endl;
    std::cout << "fuction_name\tx\ty\tresult\ttime(ms)\n";
    auto start = high_resolution_clock::now();
    points_t bf =
        brute_force_method(f, xy_generator, domain_min, domain_max, iterations);
    auto stop = high_resolution_clock::now();
    std::cout << "brute_force\t{ " << bf.first << " | " << bf.second << " | "
              << f(bf) << " | "
              << duration_cast<microseconds>(stop - start).count() << " } \n";
    start = high_resolution_clock::now();
    points_t hc = hill_climbing(f, xy_generator, get_close_points, domain_min,
                                domain_max, iterations);
    stop = high_resolution_clock::now();
    std::cout << "hill_climbing\t{ " << std::setprecision(3) << hc.first
              << " | " << hc.second << " | " << f(hc) << " | "
              << duration_cast<microseconds>(stop - start).count() << " } \n";
    start = high_resolution_clock::now();
    points_t an = annealing_method(f, xy_generator, get_close_point, domain_min,
                                   domain_max, iterations);
    stop = high_resolution_clock::now();
    std::cout << "annealing\t{ " << an.first << " | " << an.second << " | "
              << f(an) << " | "
              << duration_cast<microseconds>(stop - start).count() << " } \n";
  };
  test_fuction(eggholder_f_v, -512, 512,
               "\n\t Eggholder \t f(512,404.2319)=-959.6407");
  test_fuction(ackley_f_v, -5, 5, "\n\t Ackley \t f(0,0)=0");
  test_fuction(holder_table_f_v, -10, 10,
               "\n\t Holder_table \t f(±8.05502,±9.66459)=-19.2085");
  return 0;
}
