# ![PJATK](../../.assets/banner.png)

[Test_functions_for_optimization](https://en.wikipedia.org/wiki/Test_functions_for_optimization)

- Zadanie 1

  - Przygotuj uniwersalną metodę wypisywania zadania, oraz wyników dla zadania.

- Zadanie 2

  - Przygotuj 3 funkcje do optymalizacji (to w sumie masz). Upewnij się, że co najmniej dwie z nich mają wiele optimów lokalnych.

- Zadanie 3

  - Zaimplementuj algorytm symulowanego wyżarzania. Zobacz slajdy z wykładu/wykład (na końcu dzisiejszej notatki jest także opis tego algorytmu).

- Zadanie 4

  - Porównaj: Algorytm wspinaczkowy, kontra algorytm Symulowanego Wyżarzania, kontra metoda pełnego przeglądu. Sprawdź dla każdej z funkcji.

- Zadanie 5

  - (zapisz odpowiedź w pliku .md w repo z uzasadnieniem) Jaka metoda daje najlepsze wyniki?
    - W przypadku moich implemętacji najlepiej działa brute_force_method, ponieważ działa on na losowo wybieranych punktach z domeny.

- Zadanie 6

  - (zapisz odpowiedź w pliku .md w repo z uzasadnieniem) Jaka metoda jest najszybsza dla podobnej jakości wyników? Jaka metoda kompletnie się nie nadaje?
    - W przypadku moich implemętacji najszybciej działa brute_force_method, ponieważ działa on na losowo wybieranych punktach z domeny. Natomiast najwolniej działa hill_climbing i kompletnie się nie nadaje.

- Zadanie dodatkowe (na dodatkowy punkt)
  - Przeprowadź eksperyment który pozwoli na porównanie zaimplementowanych metod. Przygotuj wykresy krzywych zbieżności dla każdej metody. Pamiętaj - eksperyment powtarzamy co najmniej 25 razy aby uzyskać jakieś znaczące dane statystyczne.
