# ![PJATK](../../.assets/banner.png)

Zadanie:
- Rozpocznijmy przygodę z OpenCV. Na dziś zrobimy tylko instalację i proste demo z odbiciem lustrzanym.

1. Zainstaluj u siebie biblioteki do OpenCV - w zależności od systemu operacyjnego proces będzie wyglądał trochę inaczej. Najłatwiej jest pod Linuksem (apt install libopencv-dev). Na pracowni trzeba sobie zainstalować (a właściwie to skompilować) bibliotekę OpenCV (https://github.com/opencv/opencv)
2. Przygotuj projekt wykorzystujący bibliotekę OpenCV
3. Niech program który jest w Twoim repozytorium będzie wyświetlał odbicie lustrzane tego co widzi kamerka. Jeśli nie działa okno, to niech zamiast okna, wynik pojawi się w serii plików png.
4. Niech też program się zakończy w momencie wciśnięcia klawisza ESC.
5. Jeśli się udało daj znać prowadzącemu - dostaniesz plusa.

Zadanie dodatkowe
- To samo, ale w innym języku programowania. W obu wersjach proszę wykonać wyszukiwanie konturów i porównać czas między momentem po pobraniu ramki z kamery a wybraniem największego konturu.