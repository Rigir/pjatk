#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include <iostream>

// make && ./build/final_program
int main(int argc, char **argv)
{
    using namespace cv;
    using namespace std;
    bool capturing = true;
    VideoCapture cap(0);
    if (!cap.isOpened())
    {
        cerr << "error opening frames source" << endl;
        return -1;
    }
    cout << "Video size: " << cap.get(CAP_PROP_FRAME_WIDTH)
         << "x" << cap.get(CAP_PROP_FRAME_HEIGHT) << endl;
    do
    {
        Mat frame;
        if (cap.read(frame))
        {
            imshow("Orginal window", frame);
            Mat frame_flip;
            cv::flip(frame, frame_flip, 1);
            imshow("Flip", frame_flip);
        }
        else
        {
            // stream finished
            capturing = false;
        }
        // czekaj na klawisz, sprawdz czy to jest 'esc'
        if ((waitKey(1000.0 / 60.0) & 0x0ff) == 27)
            capturing = false;
    } while (capturing);
    return 0;
}