# ![PJATK](../../.assets/banner.png)


# Zadanie 1 - Question answering

Na podstawie artykułu o Janie Pawle II z wikipedii:
Znajdź na huggingface model który pozwala odpowiadać na pytania w języku polskim (ustaw filtr `tasks` i `languages`)
Zanaleźć odpowiedni pipeline 

Kożystając z modelu odpowiedz na następujące pytania:

* kiedy urodził się papież?
* kiedy zmarł papież?
* ile osób kanonizował?
* kiedy został kaninizowany?

```
import transformers
from transformers import pipeline

qa =  pipeline("question-answering", model='azwierzc/herbert-large-poquad', handle_impossible_answer=True)

questions = ["kiedy urodził się papież?", "kiedy zmarł papież?", "ile osób kanonizował?", "kiedy został kaninizowany?"]
answers = []

for q in questions:
  answers.append(qa({'context': text, 'question': q}))

print(*answers, sep = "\n")
```

# Zadanie 2 - visual question answering
Dany jest obraz przedstawiający 3 mężczyzn

Wyszukaj na huggingface model pozwalający odpowiedzieć na pytania na podstawie obrazu (zadanie nazywa się `visual-question-answering`) kożystając z modelu odpowiedz na następujące pytania:

* Ile ludzi jest na obrazie?
* Co robią?
* Gdzie się znajdują?
* Ile osób nosi okulary?

Uwaga - model prawdpodobnie trenowany jest na języku angielskim więc pytania musisz zadać po angielsku

```
from transformers import ViltProcessor, ViltForQuestionAnswering
import requests
from PIL import Image

# prepare image + question
url = "https://globalnews.ca/wp-content/uploads/2016/11/gettyimages-165240569.jpg?quality=85&strip=all&w=1200"
image = Image.open(requests.get(url, stream=True).raw)

questions = ["How many people are in the picture?", "What are they doing?", "Where are they located?", "How many people wear glasses?"]
answers = []

qa =  pipeline(model="dandelin/vilt-b32-finetuned-vqa")

for q in questions:
  answers.append(qa(image,q)[0])

print(*answers, sep = "\n")
```

# Zadanie 3 model klasyfikacji tekstu

Na podstawie tekstu

Określ jego sentyment (czy jest pozytywny czy negatywny)
Wyszkukaj w huggingface model to klasyfikacji tekstu dla języka polskiego określający sentyment (`sentiment`)

```
from transformers import pipeline

qa =  pipeline(model="Voicelab/herbert-base-cased-sentiment")
print(qa(text))
```