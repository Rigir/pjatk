#include <math.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char** argv) {
  map<string, function<float(vector<float>)>> operators;
  operators["sin"] = [](vector<float> numbers) { return sin(numbers.front()); };
  operators["add"] = [](vector<float> numbers) {
    return numbers.at(0) + numbers.at(1);
  };
  operators["mod"] = [](vector<float> numbers) {
    return (float)((int)numbers.at(0) % (int)numbers.at(1));
  };

  try {
    vector<float> numbers;
    transform(argv + 2, argv + argc, back_inserter(numbers),
              [](auto s) { return stof(s); });
    string posicion = string(argv[1]);
    function<float(vector<float>)> fun = operators.at(posicion);
    if (numbers.empty()) throw out_of_range("Please enter an more arguments.");
    cout << "\n Result of " << posicion << " is " << fun(numbers) << ".\n\n";
  } catch (invalid_argument err) {
    cout << "\n Err: Please enter an argument. Available: [";
    for (const auto& [k, v] : operators) cout << " " << k;
    cout << " ]\n";
    return 1;
  } catch (out_of_range err) {
    cout << "\n Err: Please enter an more arguments. \n\n";
    return 1;
  }
  return 0;
}