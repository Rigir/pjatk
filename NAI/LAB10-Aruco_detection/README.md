# ![PJATK](../../.assets/banner.png)

Zadanie
- Napisz program, który będzie oceniał jak daleko są od siebie dwa markery ArUco. Założenia upraszczające są takie, że oba markery są na płaskiej powierzchni i są fotografowane pod kątem 90°.
 

Zadanie dodatkowe
- Skalibruj swoją kamerę i rozwiń program tak, aby pozwalał na ocenę względnej odległości w przestrzeni 3D.