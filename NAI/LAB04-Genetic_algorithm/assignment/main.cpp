#include <functional>
#include <iostream>
#include <random>
#include <vector>

using chromosome_t = std::vector<int>;
using genotype_t = std::vector<chromosome_t>;
using point_t = std::pair<double, double>;

std::random_device rd;
std::mt19937 mt_generator(rd());

// <-5, 5>
auto ackley_f_v = [](point_t v)
{
  return -20 * exp(-0.2 * sqrt(0.5 * (pow(v.first, 2) + pow(v.second, 2)))) -
         exp(0.5 * (cos(2 * M_PI * v.first) + cos(2 * M_PI * v.second))) +
         M_E + 20;
};

auto genetic_algorithm = [](auto initial_population, auto fitness,
                            auto term_condition, auto selection,
                            double p_crossover, auto crossover,
                            double p_mutation, auto mutation)
{
  using namespace std;
  uniform_real_distribution<double> uniform(0.0, 1.0);
  auto population = initial_population;
  vector<double> population_fit = fitness(population);
  while (!term_condition(population, population_fit))
  {
    auto parents_indexes = selection(population_fit);
    decltype(population) new_population;
    for (int i = 0; i < parents_indexes.size(); i += 2)
    {
      decltype(initial_population) offspring = {population[i],
                                                population[i + 1]};
      if (uniform(mt_generator) < p_crossover)
      {
        offspring = crossover(offspring);
      }
      for (auto chromosome : offspring)
        new_population.push_back(chromosome);
    }
    for (auto &chromosome : new_population)
    {
      chromosome = mutation(chromosome, p_mutation);
    }
    population = new_population;
    population_fit = fitness(population);
  }
  return population;
};

genotype_t generate_init_population(int size)
{
  std::uniform_int_distribution<int> random(0, 1);
  genotype_t ret;
  for (int i = 0; i < size; i++)
  {
    chromosome_t chromosone(size);
    std::generate(chromosone.begin(), chromosone.end(), [&random]()
                  { return random(mt_generator); });
    ret.push_back(chromosone);
  }
  return ret;
};

point_t decode(chromosome_t chromosome)
{
  int half_of_vec = chromosome.size() / 2;
  double x = 0;
  double y = 0;

  for (int i = 1; i < half_of_vec; i++)
  {
    x += chromosome[i] * pow(2, -i);
  }

  for (int i = half_of_vec + 1; i < chromosome.size() - 1; i++)
  {
    y += chromosome[i] * pow(2, -(i - half_of_vec));
  }

  x *= chromosome[0] == 1 ? -5 : 5;
  y *= chromosome[half_of_vec] == 1 ? -5 : 5;

  std::cout << x << " " << y << " \n";
  return point_t(x, y);
}

std::vector<double> fintess_function(genotype_t population)
{
  std::vector<double> ret(population.size());
  for (auto &chromosome : population)
  {
    double fitness = 1.0 / (1.0 + std::abs(ackley_f_v(decode(chromosome))));
    ret.push_back(fitness);
  }
  // for (auto element : ret)
  // {
  //   std::cout << element << " \n";
  // }
  return ret;
}

std::vector<int> selection_empty(std::vector<double> fitnesses) { return {}; }
std::vector<chromosome_t> crossover_empty(std::vector<chromosome_t> parents)
{
  return parents;
}

chromosome_t mutation_empty(chromosome_t parents, double p_mutation)
{
  return parents;
}

int main()
{
  using namespace std;
  int my_magic_number = 100 + (22836 % 10) * 2;
  auto result = genetic_algorithm(
      generate_init_population(my_magic_number),
      fintess_function,
      [](auto a, auto b)
      { return true; },
      selection_empty,
      1.0, crossover_empty,
      0.01, mutation_empty);

  // for (chromosome_t chromosome : result)
  // {
  //   cout << "[";
  //   for (int p : chromosome)
  //   {
  //     cout << p;
  //   }
  //   cout << "]\n";
  // }
  // cout << endl;
  // return 0;
}