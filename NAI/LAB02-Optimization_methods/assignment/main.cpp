#include <math.h>

#include <functional>
#include <iostream>
#include <random>
#include <utility>
#include <vector>

/**
 * domain - generate domain points. Throws exception when all the points were
 * returned
 */

auto brute_force = [](auto f, auto domain, int limit) {
  auto current_p = domain();
  auto best_point = current_p;
  try {
    for (size_t i = 0; i < limit; i++) {
      if (f(current_p) < f(best_point)) {
        best_point = current_p;
      }
      current_p = domain();
    }
  } catch (std::exception &e) {
  }
  return best_point;
};

int main() {
  auto sphere_f = [](double x) { return x * x; };
  auto beale_f = [](std::pair<double, double> values) {
    return pow(values.first + 2 * values.second - 7, 2) +
           pow(2 * values.first + values.second - 5, 2);
  };
  auto matyas_f = [](std::pair<double, double> values) {
    return 0.26 * (pow(values.first, 2) + pow(values.second, 2)) -
           0.48 * values.first * values.second;
  };

  std::random_device rd;
  std::mt19937 mt_generator(rd());
  std::uniform_real_distribution dis(-10.0, 10.0);
  auto alone_generator = [&]() { return dis(mt_generator); };

  auto pair_generator = [&]() {
    return std::make_pair(dis(mt_generator), dis(mt_generator));
  };

  auto best_point_1 = brute_force(sphere_f, alone_generator, 10000);
  std::cout << "sphere_f(n)=0 \t\t result = " << best_point_1 << std::endl;

  auto best_point_2 = brute_force(beale_f, pair_generator, 10000);
  std::cout << "beale_f(3,0.5)=0  \t x: " << best_point_2.second
            << " \t y: " << best_point_2.first
            << " \t | result: " << beale_f(best_point_2) << std::endl;

  auto best_point_3 = brute_force(matyas_f, pair_generator, 10000);
  std::cout << "matyas_f(0,0)=0 \t x: " << best_point_3.first
            << " \t y: " << best_point_3.second
            << " \t | result: " << matyas_f(best_point_3) << std::endl;

  return 0;
}
