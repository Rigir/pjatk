#include <functional>
#include <iostream>
#include <random>
#include <vector>

using chromosome_t = std::vector<int>;
using genotype_t = std::vector<chromosome_t>;
using point_t = std::pair<double, double>;
using fitness_t = std::vector<double>;

std::random_device rd;
std::mt19937 mt_generator(rd());

std::ostream &operator<<(std::ostream &o, const chromosome_t chromosome)
{
  for (const int p : chromosome)
  {
    o << p;
  }
  return o;
}

std::ostream &operator<<(std::ostream &o, const genotype_t chromosome)
{
  for (chromosome_t chromosome : chromosome)
  {
    o << "[";
    for (int p : chromosome)
    {
      o << p;
    }
    o << "]\n";
  }

  return o;
};

void display_fitnes_stats(const fitness_t &fitness)
{
  double max_fitness = *max_element(fitness.begin(), fitness.end());
  double nim_fitness = *min_element(fitness.begin(), fitness.end());
  double avg_fitness = std::reduce(fitness.begin(), fitness.end()) / fitness.size();

  std::cout << "\nmax_fitness:\t" << max_fitness
            << "\nnim_fitness:\t" << nim_fitness
            << "\navg_fitness:\t" << avg_fitness << "\n";
}

// <-5, 5>
auto ackley_f_v = [](point_t v)
{
  return -20 * exp(-0.2 * sqrt(0.5 * (pow(v.first, 2) + pow(v.second, 2)))) -
         exp(0.5 * (cos(2 * M_PI * v.first) + cos(2 * M_PI * v.second))) +
         M_E + 20;
};

auto genetic_algorithm = [](auto initial_population, auto fitness,
                            auto term_condition, auto selection,
                            double p_crossover, auto crossover,
                            double p_mutation, auto mutation,
                            bool display_fitness, double fitness_end_at)
{
  using namespace std;

  uniform_real_distribution<double> uniform(0.0, 1.0);
  auto population = initial_population;
  fitness_t population_fit = fitness(population);

  while (!term_condition(population_fit, fitness_end_at))
  {
    decltype(population) new_population;
    for (int i = 0; i < population.size(); i += 2)
    {
      decltype(initial_population) offspring;
      offspring.push_back(population.at(selection(population_fit)));
      offspring.push_back(population.at(selection(population_fit)));
      if (uniform(mt_generator) < p_crossover)
      {
        offspring = crossover(offspring);
      }
      for (auto chromosome : offspring)
        new_population.push_back(chromosome);
    }
    for (auto &chromosome : new_population)
    {
      chromosome = mutation(chromosome, p_mutation, 2);
    }
    population = new_population;
    population_fit = fitness(population);
    if (display_fitness)
    {
      display_fitnes_stats(population_fit);
    }
  }
  return population;
};

genotype_t generate_init_population(int size)
{
  std::uniform_int_distribution<int> random(0, 1);
  genotype_t ret;
  for (int i = 0; i < size; i++)
  {
    chromosome_t chromosone(size);
    std::generate(chromosone.begin(), chromosone.end(), [&random]()
                  { return random(mt_generator); });
    ret.push_back(chromosone);
  }
  return ret;
};

point_t decode(chromosome_t chromosome)
{
  int half_of_vec = chromosome.size() / 2;
  double x = 0;
  double y = 0;

  for (int i = 1; i < half_of_vec; i++)
  {
    x += chromosome[i] * pow(2, -i);
  }

  for (int i = half_of_vec + 1; i < chromosome.size() - 1; i++)
  {
    y += chromosome[i] * pow(2, -(i - half_of_vec));
  }

  x *= chromosome[0] == 1 ? -5 : 5;
  y *= chromosome[half_of_vec] == 1 ? -5 : 5;

  // std::cout << x << " " << y << " \n";
  return point_t(x, y);
}

double fintess_function(chromosome_t chromosome)
{
  return 1.0 / (1.0 + std::abs(ackley_f_v(decode(chromosome))));
}

fitness_t fintess_population_function(genotype_t population)
{
  fitness_t ret;
  for (auto &chromosome : population)
  {
    ret.push_back(fintess_function(chromosome));
  }
  // for (auto element : ret)
  // {
  //   std::cout << element << " \n";
  // }
  return ret;
}

genotype_t crossover_empty(genotype_t parents) { return parents; }

genotype_t crossover_one_point(genotype_t parents)
{
  int genotype_size = parents.size();
  int chromosome_size = parents[0].size();
  std::uniform_int_distribution<int> distribution(0, chromosome_size);

  for (int i = distribution(mt_generator); i < chromosome_size; i++)
  {
    std::swap(parents[0][i], parents[1][i]);
  }

  return parents;
}

genotype_t crossover_two_points(genotype_t parents)
{
  int chromosome_size = parents[0].size() - 1;
  std::uniform_int_distribution<int> distribution(0, chromosome_size);
  int p1 = distribution(mt_generator);
  int p2 = distribution(mt_generator);

  if (p1 > p2)
  {
    std::swap(p1, p2);
  }

  genotype_t children = parents;

  for (int i = p1; i < p2; i++)
  {
    std::swap(children[0][i], children[1][i]);
  }

  return children;
}

int selection_empty(fitness_t fitnesses) { return {}; }

int selection_tournament(fitness_t fitnesses)
{
  std::uniform_int_distribution<int> uniform(0, fitnesses.size() - 1);
  int a = uniform(mt_generator);
  int b = uniform(mt_generator);
  return (fitnesses[a] > fitnesses[b]) ? a : b;
}

int selection_roulette(fitness_t fitnesses)
{
  double total_fitness = std::reduce(fitnesses.begin(), fitnesses.end());
  std::uniform_real_distribution<double> distribution(0, total_fitness);
  double random = distribution(mt_generator);

  double i = 0;
  while (random > 0)
  {
    random = random - fitnesses[i];
    i++;
  }

  return i - 1;
}

chromosome_t mutation_empty(chromosome_t parent, double p_mutation) { return parent; }

chromosome_t mutation_point_homogeneous(chromosome_t parent, double p_mutation)
{
  chromosome_t child = parent;
  for (int i = 0; i < child.size(); i++)
  {
    std::uniform_real_distribution<double> uni(0.0, 1.0);
    if (uni(mt_generator) < p_mutation)
    {
      child[i] = 1 - child[i];
    }
  }
  return child;
}

chromosome_t mutation_multi_point(chromosome_t parent, double p_mutation, int number)
{
  std::uniform_real_distribution<double> uni(0.0, 1.0);
  if (uni(mt_generator) >= p_mutation)
    return parent;

  chromosome_t child = parent;
  for (int i = 0; i < number; i++)
  {
    std::uniform_int_distribution<int> distribution(0, parent.size() - 1);
    auto l = distribution(mt_generator);
    child[l] = 1 - child[l];
  }

  return child;
}

int main(int argc, char *argv[])
{
  int population_size = 100 + (22836 % 10) * 2;
  int iterations = 10;
  double p_crossover = 1.0;
  double p_mutation = 0.01;
  bool display_fitness = false;
  double fitness_end_at = 0.0;

  int _iterations = 0;
  auto term_condition_iterations = [&](fitness_t &fitness, double fitness_end_at)
  {
    _iterations++;
    return (_iterations > iterations) ? true : false;
  };

  auto term_condition_standard_deviation = [&](fitness_t &fitness, double fitness_end_at)
  {
    double avg = std::reduce(fitness.begin(), fitness.end()) / fitness.size();
    double sum = 0.0;
    std::for_each(fitness.begin(), fitness.end(), [&](int num)
                  { sum += (num - avg) * (num - avg); });
    return sqrt(sum / fitness.size() < fitness_end_at);
  };

  auto chosen_lambda_term_condition = term_condition_iterations;
  if (argc > 1)
  {
    for (int count = 1; count < argc; count++)
    {
      std::string parameter(argv[count]);
      std::string argument = parameter.size() > 2 ? parameter.substr(2, parameter.size() - 1) : "";
      if (parameter != "")
      {
        switch (parameter[1])
        {
        case 'p':
          population_size = stoi(argument);
          break;
        case 'i':
          iterations = stoi(argument);
          break;
        case 'c':
          p_crossover = stod(argument);
          break;
        case 'm':
          p_mutation = stod(argument);
          break;
        case 'f':
          display_fitness = (argument[0] == 't');
          break;
        case 'e':
          auto chosen_lambda_term_condition = term_condition_standard_deviation;
          fitness_end_at = stod(argument);
          break;
        }
      }
    }
  }
  // std::cout << population_size << " "
  //           << iterations << " "
  //           << p_crossover << " "
  //           << p_mutation << " "
  //           << display_fitness << "\n";

  auto result = genetic_algorithm(
      generate_init_population(population_size),
      fintess_population_function,
      chosen_lambda_term_condition,
      selection_tournament,
      p_crossover,
      crossover_one_point,
      p_mutation,
      mutation_multi_point,
      display_fitness,
      fitness_end_at);

  point_t best_result = decode(*max_element(result.begin(), result.end(),
                                            [&](chromosome_t l, chromosome_t r)
                                            { return fintess_function(l) < fintess_function(r); }));

  std::cout << "Result: \n"
            << best_result.first << " | " << best_result.second << std::endl;

  fitness_t fitness;
  std::transform(result.begin(), result.end(), std::back_inserter(fitness),
                 [&](chromosome_t chromosome)
                 { return fintess_function(chromosome); });
  display_fitnes_stats(fitness);
  return 0;
}