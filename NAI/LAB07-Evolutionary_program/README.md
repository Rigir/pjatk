# ![PJATK](../../.assets/banner.png)

- Dzisiejsze zadanie jest wyjątkowo krótkie - dokończ algorytm genetyczny. Program powinien rozwiązywać zadany problem optymalizacyjny za pomocą algorytmu genetycznego.

- W stosunku do tego co było pokazane jako implementacja na wykładzie:

  1. niech program przyjmuje jako argumenty z linii komend:
     1. rozmiar populacji [s]
     2. liczbę iteracji [i]
     3. prawdopodobieństwo krzyżowania [c]
     4. prawdopodobieństwo mutacji [m]
  2. dodać funkcję która policzy średnią, maksymalną i minimalną wartości funkcji fitness dla populacji
  3. przełącznik jako argument wywołania programu - wypisanie co iterację statystyk z poprzedniego punktu jeśli użytkownik sobie zażyczy [f]

- Zadanie na dodatkowy punkt:
  - Dodaj alternatywny warunek zakończenia. Warunek będzie polegał na tym, że jeśli odchylenie standardowe wartości funkcji fitness populacji będzie mniejsze niż zadany próg, to wtedy kończymy. Oczywiście odpowiedni argument linii komend do tego.[e]
