# ![PJATK](../../.assets/banner.png)

Zadanie polega na tym, aby zaimplementować selekcję, krzyżowanie i mutację dla Twojej reprezentacji chromosomu.

- Plan minimum

  - krzyżowanie jednopunktowe
  - mutacja jednorodna
  - selekcja ruletkowa

- Plan na dodatkowy punkt ( plan minimum + )

  - krzyżowanie dwupunktowe
  - mutacja dowolna inna niż jednorodna
  - selekcja turniejowa

Oczywiście należy mieć możliwość weryfikacji, czy to działa - na przykład w funkcji main będzie wywołanie operatorów i wypisanie na ekranie wyników oraz jak wyglądają chromosomy
