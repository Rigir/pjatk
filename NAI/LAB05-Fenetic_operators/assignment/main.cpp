#include <functional>
#include <iostream>
#include <random>
#include <vector>

using chromosome_t = std::vector<int>;
using genotype_t = std::vector<chromosome_t>;
using point_t = std::pair<double, double>;
using fitness_t = std::vector<double>;

std::random_device rd;
std::mt19937 mt_generator(rd());

std::ostream &operator<<(std::ostream &o, const chromosome_t chromosome)
{
  for (const int p : chromosome)
  {
    o << p;
  }
  return o;
}

std::ostream &operator<<(std::ostream &o, const genotype_t chromosome)
{
  for (chromosome_t chromosome : chromosome)
  {
    o << "[";
    for (int p : chromosome)
    {
      o << p;
    }
    o << "]\n";
  }

  return o;
};

// <-5, 5>
auto ackley_f_v = [](point_t v)
{
  return -20 * exp(-0.2 * sqrt(0.5 * (pow(v.first, 2) + pow(v.second, 2)))) -
         exp(0.5 * (cos(2 * M_PI * v.first) + cos(2 * M_PI * v.second))) +
         M_E + 20;
};

auto genetic_algorithm = [](auto initial_population, auto fitness,
                            auto term_condition, auto selection,
                            double p_crossover, auto crossover,
                            double p_mutation, auto mutation)
{
  using namespace std;
  uniform_real_distribution<double> uniform(0.0, 1.0);
  auto population = initial_population;
  fitness_t population_fit = fitness(population);
  while (!term_condition(population, population_fit))
  {
    auto parents_indexes = selection(population_fit);
    decltype(population) new_population;
    for (int i = 0; i < parents_indexes.size(); i += 2)
    {
      decltype(initial_population) offspring = {population[i],
                                                population[i + 1]};
      if (uniform(mt_generator) < p_crossover)
      {
        offspring = crossover(offspring);
      }
      for (auto chromosome : offspring)
        new_population.push_back(chromosome);
    }
    for (auto &chromosome : new_population)
    {
      chromosome = mutation(chromosome, p_mutation);
    }
    population = new_population;
    population_fit = fitness(population);
  }
  return population;
};

genotype_t generate_init_population(int size)
{
  std::uniform_int_distribution<int> random(0, 1);
  genotype_t ret;
  for (int i = 0; i < size; i++)
  {
    chromosome_t chromosone(size);
    std::generate(chromosone.begin(), chromosone.end(), [&random]()
                  { return random(mt_generator); });
    ret.push_back(chromosone);
  }
  return ret;
};

point_t decode(chromosome_t chromosome)
{
  int half_of_vec = chromosome.size() / 2;
  double x = 0;
  double y = 0;

  for (int i = 1; i < half_of_vec; i++)
  {
    x += chromosome[i] * pow(2, -i);
  }

  for (int i = half_of_vec + 1; i < chromosome.size() - 1; i++)
  {
    y += chromosome[i] * pow(2, -(i - half_of_vec));
  }

  x *= chromosome[0] == 1 ? -5 : 5;
  y *= chromosome[half_of_vec] == 1 ? -5 : 5;

  // std::cout << x << " " << y << " \n";
  return point_t(x, y);
}

fitness_t fintess_function(genotype_t population)
{
  fitness_t ret(population.size());
  for (auto &chromosome : population)
  {
    double fitness = 1.0 / (1.0 + std::abs(ackley_f_v(decode(chromosome))));
    ret.push_back(fitness);
  }
  // for (auto element : ret)
  // {
  //   std::cout << element << " \n";
  // }
  return ret;
}

genotype_t crossover_empty(genotype_t parents) { return parents; }

genotype_t crossover_one_point(genotype_t parents)
{
  int genotype_size = parents.size();
  int chromosome_size = parents[0].size();
  std::uniform_int_distribution<int> distribution(0, chromosome_size);

  for (int i = distribution(mt_generator); i < chromosome_size; i++)
  {
    std::swap(parents[0][i], parents[1][i]);
  }

  return parents;
}

genotype_t crossover_two_points(genotype_t parents)
{
  int chromosome_size = parents[0].size() - 1;
  std::uniform_int_distribution<int> distribution(0, chromosome_size);
  int p1 = distribution(mt_generator);
  int p2 = distribution(mt_generator);

  if (p1 > p2)
  {
    std::swap(p1, p2);
  }

  genotype_t children = parents;

  for (int i = p1; i < p2; i++)
  {
    std::swap(children[0][i], children[1][i]);
  }

  return children;
}

int selection_empty(fitness_t fitnesses) { return {}; }

int selection_tournament(fitness_t fitnesses)
{
  std::uniform_int_distribution<int> uniform(0, fitnesses.size() - 1);
  int a = uniform(mt_generator);
  int b = uniform(mt_generator);
  return (fitnesses[a] > fitnesses[b]) ? a : b;
}

int selection_roulette(fitness_t fitnesses)
{
  double total_fitness = std::reduce(fitnesses.begin(), fitnesses.end());
  std::uniform_real_distribution<double> distribution(0, total_fitness);
  double random = distribution(mt_generator);

  double i = 0;
  while (random > 0)
  {
    random = random - fitnesses[i];
    i++;
  }

  return i - 1;
}

chromosome_t mutation_empty(chromosome_t parent, double p_mutation) { return parent; }

chromosome_t mutation_point_homogeneous(chromosome_t parent, double p_mutation)
{
  chromosome_t child = parent;
  for(int i = 0; i < child.size(); i++) {
    std::uniform_real_distribution<double> uni(0.0, 1.0);
    if (uni(mt_generator) < p_mutation)
    {
      child[i] = 1 - child[i];
    }
  }
  return child;
}

chromosome_t mutation_multi_point(chromosome_t parent, double p_mutation, int number)
{
  std::uniform_real_distribution<double> uni(0.0, 1.0);
  if (uni(mt_generator) >= p_mutation)
    return parent;

  chromosome_t child = parent;
  for (int i = 0; i < number; i++)
  {
    std::uniform_int_distribution<int> distribution(0, parent.size() - 1);
    auto l = distribution(mt_generator);
    child[l] = 1 - child[l];
  }

  return child;
}

int main()
{
  int my_magic_number = 100 + (22836 % 10) * 2;

  // auto result = genetic_algorithm(
  //     generate_init_population(my_magic_number),
  //     fintess_function,
  //     [](auto a, auto b)
  //     { return true; },
  //     selection_empty,
  //     1.0, crossover_empty,
  //     0.01, mutation_empty);

  chromosome_t base_first(10, 0);
  chromosome_t base_secound(10, 1);
  genotype_t base_genotype{base_first, base_secound};
  fitness_t base_fit_population = fintess_function(base_genotype);

  std::cout << "base_genotype \n"
            << base_genotype << std::endl;

  std::cout << "==========\n\n";

  std::cout << "crossover_one_point() \n"
            << crossover_one_point(base_genotype) << std::endl;

  std::cout << "selection_roulette() \n"
            << selection_roulette(base_fit_population) << std::endl;

  std::cout << "mutation_homogeneous() \n"
          << mutation_point_homogeneous(base_first, 1) << std::endl;

  std::cout << "==========\n\n";

  std::cout << "crossover_two_points() \n"
            << crossover_two_points(base_genotype) << std::endl;

  std::cout << "selection_tournament() \n"
            << selection_tournament(base_fit_population) << std::endl;

  std::cout << "mutation_multi_point() \n"
            << mutation_multi_point(base_first, 1, 2) << std::endl;

  return 0;
}