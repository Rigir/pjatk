# ![PJATK](../../.assets/banner.png)

- Zadanie 1 (1 pkt)

  - a. Zbalansuj zbiór nadpróbkowując mniej liczną klasę.
  - b. Zbalansuj zbiór podpróbkowując liczniejszą klasę.

- Zadanie 2 (3 pkt)

  - Stwórz dwa klasyfikatory typu Drzewo decyzyjne. Skorzystaj z biblioteki scikit-learn. Jeden klasyfikator wytrenuj na niezbalansowanym zbiorze, a drugi na zbalansowanym. Porównaj wyniki.

- Zadanie 3 (1 pkt)

  - Wyrysuj stworzone drzewo decyzyjne. Czy uważasz, że wybrana przez algorytm struktura jest optymalna? Czy daje dobre wyniki?

- Zadanie 4 (2 pkt)

- Jakie znasz metody na optymalizację struktury drzewa? Czemu pozwalają zapobiec?
  Wypisz tutaj te metody, a następnie wdróż je trenując nowy klasyfikator.

- Zadanie 5 (1 pkt)

  - Poeksperymentuj z parametrami związanymi z metodami, które wypisałeś_aś w poprzednim zadaniu. Wybierz najlepszy zestaw parametrów i po raz kolejny wyrysuj drzewo.

- \*Zadanie 6 (2 pkt)

  - Stwórz klasyfikator oparty o lasy losowe i sprawdź, jak polepszyło to klasyfikację Cukrzycy. Czy dobrane w poprzednim zadaniu parametry sprawdzają się przy lasach losowych?

- \*Zadanie 7 (1 pkt)

  - Znajdź na Kaggle zbiór danych, w których znajdują się zmienne kategoryczne. Czy drzewo decyzyjne w scikit-learn radzi sobie z takimi wartościami? Jeżeli nie, jak można obejść ten problem? Napisz odpowiednią funkcję.

- \*Zadanie 8 (1 pkt)
  - Znajdź na Kaggle lub w scikit-learn zbiór danych do regresji (np. House Prices prediction). Pobierz go i zbuduj drzewo regresyjne.
