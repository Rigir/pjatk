# ![PJATK](../../.assets/banner.png)

Zadanie:
- Napisz program, który będzie wyłapywał 2 największe obiekty wybranego koloru (wybierz kolor, polecam kolory bardzo jaskrawe) i jak zostaną ułożone w jednej linii (z jakimś marginesem) to między nimi zostanie narysowane coś (patrz niżej). Indeksy parzyste - dwie kreski, indeksy nieparzyste - trzy kreski.
 
Zadanie na dodatkowy punkt
- Możesz za to zadanie zdobyć 2 punkty jeśli rozwiniesz je w twórczy sposób, możesz się zapytać czy Twój pomysł się nadaje. Jeśli nie masz pomysłu, to proponuję:
    - Połączenie całych obrysów tak aby przybliżając obiekty do kamery, element łączący się powiększał, a gdy oddalasz, to aby się pomniejszał. Można też zastanowić się nad animacją tego obiektu łączącego.
Można także dodać możliwość wyboru koloru obiektu poprzez pokazanie go do kamery i zaznaczenie jako ROI, a następnie na podstawie jego koloru ustalenie zakresu dla funkcji inRange.