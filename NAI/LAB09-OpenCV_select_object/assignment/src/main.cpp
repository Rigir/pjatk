#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

// g++ `pkg-config --cflags opencv4` cv2.cpp `pkg-config --libs opencv4`

using namespace std;
using namespace cv;

int main(int argc, char **argv)
{
    VideoCapture cap1(0);
    if (!cap1.isOpened())
        return -1;

    std::vector<int> lower = {150, 80, 150};
    std::vector<int> upper = {185, 255, 255};
    namedWindow("Threshold", WINDOW_AUTOSIZE);
    createTrackbar("lh", "Threshold", &lower[0], 255);
    createTrackbar("ls", "Threshold", &lower[1], 255);
    createTrackbar("lv", "Threshold", &lower[2], 255);
    createTrackbar("hh", "Threshold", &upper[0], 255);
    createTrackbar("hs", "Threshold", &upper[1], 255);
    createTrackbar("hv", "Threshold", &upper[2], 255);

    namedWindow("source", WINDOW_AUTOSIZE);
    while (true)
    {
        Mat f1, dst, detected, dilated;
        cap1.read(f1);

        cvtColor(f1, dst, COLOR_BGR2HSV);
        imshow("Color Space Conversions", dst);

        inRange(dst, lower, upper, detected);
        imshow("Threshold", detected);

        auto kernel = getStructuringElement(MORPH_ELLIPSE, Size{5, 5});
        erode(detected, dilated, kernel);
        dilate(dilated, dilated, kernel);
        imshow("Threshold_dilated", dilated);

        vector<vector<Point>> contours;
        vector<Vec4i> hierarchy;
        findContours(dilated, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
        sort(contours.begin(), contours.end(), [](vector<Point> a, vector<Point> b)
             { return contourArea(a) > contourArea(b); });
        if (contours.size() >= 2)
        {
            vector<Point> c1 = contours.at(0);
            Moments m1 = moments(c1);
            Point p1(m1.m10 / m1.m00, m1.m01 / m1.m00);

            vector<Point> c2 = contours.at(1);
            Moments m2 = moments(c2);
            Point p2(m2.m10 / m2.m00, m2.m01 / m2.m00);

            Rect brect1 = boundingRect(Mat(c1).reshape(2));
            rectangle(f1, brect1.tl(), brect1.br(), Scalar(255, 0, 0), 2);

            Rect brect2 = boundingRect(Mat(c2).reshape(2));
            rectangle(f1, brect2.tl(), brect2.br(), Scalar(0, 255, 0), 2);

            if (abs(p2.y - p1.y) < 40)
            {
                Point moveDown(0, 25);
                int line_thickness = (c1.size() + c2.size()) / 50;
                if (line_thickness <= 0)
                    line_thickness = 1;

                line(f1, p1, p2, Scalar(255, 0, 0), line_thickness);
                line(f1, p1 - moveDown, p2 - moveDown, Scalar(0, 255, 0), line_thickness);
            }
        }
        imshow("Source", f1);

        if (waitKey(1) == 27)
            break;
    }
    return 0;
}
